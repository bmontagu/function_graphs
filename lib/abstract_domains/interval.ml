(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Abstract domain of intervals over integers (for testing purposes only).
    Integer over- and under-flows are not supported. *)

type t = IntBar.t * IntBar.t [@@deriving ord, eq]

let hash_fold s (l, u) = IntBar.hash_fold (IntBar.hash_fold s l) u
let bot = (IntBar.PInf, IntBar.MInf)
let top = (IntBar.MInf, IntBar.PInf)
let is_bot = function IntBar.PInf, IntBar.MInf -> true | _ -> false
let is_top = function IntBar.MInf, IntBar.PInf -> true | _ -> false

let make i1 i2 =
  match (i1, i2) with
  | IntBar.PInf, _ | _, IntBar.MInf -> bot
  | Int i1, Int i2 when i2 < i1 -> bot
  | Int _, Int _ | MInf, PInf | MInf, Int _ | Int _, PInf -> (i1, i2)

let singleton i =
  let j = IntBar.Int i in
  (j, j)

let is_singleton = function IntBar.Int i, IntBar.Int j -> i = j | _ -> false

let pp fmt ((u, l) as int) =
  if is_bot int then Format.pp_print_string fmt "⊥"
  else Format.fprintf fmt "[%a,%a]" IntBar.pp u IntBar.pp l

let leq ((l1, u1) as int1) ((l2, u2) as int2) =
  is_bot int1 || ((not @@ is_bot int2) && IntBar.leq l2 l1 && IntBar.leq u1 u2)

let join ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 then int2
  else if is_bot int2 then int1
  else make (IntBar.meet l1 l2) (IntBar.join u1 u2)

let widen ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 then int2
  else if is_bot int2 then int1
  else
    make
      (if IntBar.leq l1 l2 then l1 else IntBar.MInf)
      (if IntBar.leq u2 u1 then u1 else IntBar.PInf)

let meet ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 || is_bot int2 then bot
  else make (IntBar.join l1 l2) (IntBar.meet u1 u2)

let plus ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 || is_bot int2 then bot
  else make (IntBar.plus l1 l2) (IntBar.plus u1 u2)

let minus ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 || is_bot int2 then bot
  else make (IntBar.minus l1 u2) (IntBar.minus u1 l2)

let mult ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 || is_bot int2 then bot
  else
    let l1l2 = IntBar.mult l1 l2
    and l1u2 = IntBar.mult l1 u2
    and u1l2 = IntBar.mult u1 l2
    and u1u2 = IntBar.mult u1 u2 in
    make
      IntBar.(minimum (minimum l1l2 l1u2) (minimum u1l2 u1u2))
      IntBar.(maximum (maximum l1l2 l1u2) (maximum u1l2 u1u2))

let lower_bound = fst
let upper_bound = snd

let le ints1 ints2 =
  if is_bot ints1 || is_bot ints2 then Bools.bot
  else
    Bools.join
      (if IntBar.leq (IntBar.succ @@ upper_bound ints2) (lower_bound ints1) then
         Bools.bot
       else Bools.singleton true)
      (if IntBar.leq (upper_bound ints1) (lower_bound ints2) then Bools.bot
       else Bools.singleton false)

let lt ints1 ints2 =
  if is_bot ints1 || is_bot ints2 then Bools.bot
  else
    Bools.join
      (if IntBar.leq (upper_bound ints2) (lower_bound ints1) then Bools.bot
       else Bools.singleton true)
      (if IntBar.leq (IntBar.succ @@ upper_bound ints1) (lower_bound ints2) then
         Bools.bot
       else Bools.singleton false)

let eq ints1 ints2 =
  if is_bot ints1 || is_bot ints2 then Bools.bot
  else
    Bools.join
      (if
         IntBar.leq (IntBar.succ @@ upper_bound ints1) (lower_bound ints2)
         || IntBar.leq (IntBar.succ @@ upper_bound ints2) (lower_bound ints1)
         (* they are disjoint *)
       then Bools.bot
       else Bools.singleton true)
      (if is_singleton ints1 && is_singleton ints2 && leq ints1 ints2 then
         Bools.bot
       else Bools.singleton false)

module Test = struct
  let print_interval int = pp Format.std_formatter int
  let int13 = make (IntBar.Int 1) (IntBar.Int 3)
  let int05 = make (IntBar.Int 0) (IntBar.Int 5)
  let int47 = make (IntBar.Int 4) (IntBar.Int 7)
  let int89 = make (IntBar.Int 8) (IntBar.Int 9)
  let zero = make (IntBar.Int 0) (IntBar.Int 0)
  let one = make (IntBar.Int 1) (IntBar.Int 1)
  let pos = make (IntBar.Int 1) IntBar.PInf
  let print_bool b = Format.pp_print_bool Format.std_formatter b

  let%expect_test _ =
    print_interval int13;
    [%expect {| [1,3] |}]

  let%expect_test _ =
    print_interval int05;
    [%expect {| [0,5] |}]

  let%expect_test _ =
    print_interval int47;
    [%expect {| [4,7] |}]

  let%expect_test _ =
    print_interval int89;
    [%expect {| [8,9] |}]

  let%expect_test _ =
    print_interval zero;
    [%expect {| [0,0] |}]

  let%expect_test _ =
    print_interval pos;
    [%expect {| [1,+∞] |}]

  let%expect_test _ =
    print_bool (leq int13 int05);
    [%expect {| true |}]

  let%expect_test _ =
    print_bool (leq int13 int47);
    [%expect {| false |}]

  let%expect_test _ =
    print_bool (leq int13 pos);
    [%expect {| true |}]

  let%expect_test _ =
    print_bool (leq int05 pos);
    [%expect {| false |}]

  let%expect_test _ =
    print_bool (leq pos int05);
    [%expect {| false |}]

  let%expect_test _ =
    print_bool (leq pos int13);
    [%expect {| false |}]

  let%expect_test _ =
    print_bool (leq zero pos);
    [%expect {| false |}]

  let%expect_test _ =
    print_bool (leq one pos);
    [%expect {| true |}]

  let%expect_test _ =
    print_interval (join int13 int05);
    [%expect {| [0,5] |}]

  let%expect_test _ =
    print_interval (join int13 int47);
    [%expect {| [1,7] |}]

  let%expect_test _ =
    print_interval (meet int13 int05);
    [%expect {| [1,3] |}]

  let%expect_test _ =
    print_interval (meet int05 int13);
    [%expect {| [1,3] |}]

  let%expect_test _ =
    print_interval (meet int13 int47);
    [%expect {| ⊥ |}]

  let%expect_test _ =
    print_interval (meet int05 int47);
    [%expect {| [4,5] |}]

  let%expect_test _ =
    print_interval (meet int05 pos);
    [%expect {| [1,5] |}]

  let%expect_test _ =
    print_interval (meet zero pos);
    [%expect {| ⊥ |}]
end

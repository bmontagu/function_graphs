(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Abstract domain of intervals over integers (for testing purposes only).
    Integer over- and under-flows are not supported. *)

include Sigs.ABSTRACT_DOMAIN

val compare : t -> t -> int
(** Total order *)

val equal : t -> t -> bool
(** Syntactic equality *)

val hash_fold : Base.Hash.state -> t -> Base.Hash.state
(** Hashing *)

val is_top : t -> bool
(** Tests whether an interval is the largest interval *)

val singleton : int -> t
(** [singleton n] represents the singleton set {m \{n\}} *)

val make : IntBar.t -> IntBar.t -> t
(** [make l u] is the interval {m [l,u]} *)

val is_singleton : t -> bool
(** Tests whether an interval contains exactly one element *)

val plus : t -> t -> t
(** Transfer function for addition *)

val minus : t -> t -> t
(** Transfer function for subtraction *)

val mult : t -> t -> t
(** Transfer function for multiplication *)

val lower_bound : t -> IntBar.t
(** Extracts the lower bound of an interval. Returns {m +\infty} if the interval
    is empty. *)

val upper_bound : t -> IntBar.t
(** Extracts the upper bound of an interval. Returns {m -\infty} if the interval
    is empty. *)

val le : t -> t -> Bools.t
(** Transfer function for the "lesser or equal" arithmetic test *)

val lt : t -> t -> Bools.t
(** Transfer function for the "lesser than" arithmetic test *)

val eq : t -> t -> Bools.t
(** Transfer function for the equality arithmetic test *)

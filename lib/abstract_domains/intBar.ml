(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Integers agumented with -∞ and +∞ (for testing purposes only). Integer over-
    and under-flows are not supported. *)

type t = MInf | PInf | Int of int [@@deriving ord, eq]

let hash_fold s = function
  | MInf -> Base.Hash.fold_int s 0
  | PInf -> Base.Hash.fold_int s 1
  | Int i -> Base.Hash.(fold_int (fold_int s 2) i)

let leq i1 i2 =
  match (i1, i2) with
  | MInf, _ | _, PInf -> true
  | PInf, _ | _, MInf -> false
  | Int i1, Int i2 -> i1 <= i2

let join i1 i2 =
  match (i1, i2) with
  | MInf, i | i, MInf | _, (PInf as i) | (PInf as i), _ -> i
  | Int n1, Int n2 -> if n1 <= n2 then i2 else i1

let meet i1 i2 =
  match (i1, i2) with
  | (MInf as i), _ | _, (MInf as i) | i, PInf | PInf, i -> i
  | Int n1, Int n2 -> if n1 <= n2 then i1 else i2

let pp fmt = function
  | PInf -> Format.pp_print_string fmt "+∞"
  | MInf -> Format.pp_print_string fmt "-∞"
  | Int i -> Format.pp_print_int fmt i

let plus i1 i2 =
  match (i1, i2) with
  | MInf, MInf -> MInf
  | PInf, PInf -> PInf
  | Int n1, Int n2 -> Int (n1 + n2)
  | Int _, PInf | PInf, Int _ -> PInf
  | Int _, MInf | MInf, Int _ -> MInf
  | MInf, PInf | PInf, MInf -> assert false

let neg = function MInf -> PInf | PInf -> MInf | Int n -> Int (-n)
let minus i1 i2 = plus i1 (neg i2)

let mult i1 i2 =
  match (i1, i2) with
  | MInf, MInf | PInf, PInf -> PInf
  | Int n1, Int n2 -> Int (n1 * n2)
  | Int n, PInf | PInf, Int n ->
      if n = 0 then Int 0 else if n > 0 then PInf else MInf
  | Int n, MInf | MInf, Int n ->
      if n = 0 then Int 0 else if n > 0 then MInf else PInf
  | MInf, PInf | PInf, MInf -> MInf

let pred = function (MInf | PInf) as i -> i | Int n -> Int (n - 1)
let succ = function (MInf | PInf) as i -> i | Int n -> Int (n + 1)
let minimum = meet
let maximum = join

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

type t = Bot | Top | B of bool [@@deriving ord, eq]

let pp fmt t =
  let open Format in
  match t with
  | Bot -> pp_print_string fmt "⊥"
  | B b -> fprintf fmt "{%b}" b
  | Top -> fprintf fmt "{%b, %b}" true false

let hash_fold s = function
  | Bot -> Base.Hash.fold_int s 0
  | Top -> Base.Hash.fold_int s 1
  | B b -> Base.Hash.fold_int (Base.Hash.fold_int s 2) (if b then 1 else 0)

let bot = Bot
let is_bot = function Bot -> true | _ -> false
let top = Top
let is_top = function Top -> true | _ -> false
let singleton b = B b

let leq t1 t2 =
  match (t1, t2) with
  | Bot, _ | _, Top -> true
  | _, Bot | Top, _ -> false
  | B b1, B b2 -> Bool.equal b1 b2

let join t1 t2 =
  match (t1, t2) with
  | Bot, t | t, Bot -> t
  | Top, _ | _, Top -> Top
  | (B b1 as t), B b2 -> if Bool.equal b1 b2 then t else Top

let widen = join

let meet t1 t2 =
  match (t1, t2) with
  | Bot, _ | _, Bot -> Bot
  | Top, t | t, Top -> t
  | (B b1 as t), B b2 -> if Bool.equal b1 b2 then t else Bot

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Integers agumented with {m -\infty} and {m +\infty} (for testing purposes
    only). Integer over- and under-flows are not supported. *)

type t =
  | MInf  (** {m -\infty} *)
  | PInf  (** {m +\infty} *)
  | Int of int  (** An integer *)

val compare : t -> t -> int
(** Total order *)

val equal : t -> t -> bool
(** Syntactic equality *)

val hash_fold : Base.Hash.state -> t -> Base.Hash.state
(** Hashing *)

val leq : t -> t -> bool
(** "Lesser or equal" test on integers *)

val join : t -> t -> t
(** Maximum of integers *)

val meet : t -> t -> t
(** Minimum of integers *)

val pp : Format.formatter -> t -> unit
(** Pretty printer *)

val plus : t -> t -> t
(** Addition of integers
    @raise Assert_failure
      if called on [MInf] and [PInf], or on [PInf] and [MInf] *)

val neg : t -> t
(** Computes the opposite of an integer *)

val minus : t -> t -> t
(** Subtraction of integers
    @raise Assert_failure
      if called on [MInf] and [MInf], or on [PInf] and [PInf] *)

val mult : t -> t -> t
(** Multiplication of integers. Always returns [0] if one of the arguments is
    [0]. *)

val pred : t -> t
(** Predecessor of an integer. The predecessors of [PInf] and [MInf] are
    themselves. *)

val succ : t -> t
(** Successor of an integer. The successors of [PInf] and [MInf] are themselves.
*)

val minimum : t -> t -> t
(** Minimum on integers *)

val maximum : t -> t -> t
(** Maximum on integers *)

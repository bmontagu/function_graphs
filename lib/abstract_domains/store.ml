(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Abstract domain for stores *)

module type S = sig
  include Sigs.ABSTRACT_DOMAIN

  type key
  type value

  val empty : t
  val get : t -> key -> value
  val remove : t -> key -> t
  val set : t -> key -> value -> t
  val set_strong : t -> key -> value -> t
end

module Make
    (Key : sig
      type t

      val pp : Format.formatter -> t -> unit
    end)
    (M : sig
      type key = Key.t
      type 'a t

      val empty : 'a t
      val find_opt : key -> 'a t -> 'a option
      val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
      val for_all : (key -> 'a -> bool) -> 'a t -> bool

      val merge :
        (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

      val to_seq : 'a t -> (key * 'a) Seq.t
    end)
    (X : Sigs.ABSTRACT_DOMAIN) : sig
  include S with type key = Key.t and type value = X.t

  val is_top : t -> bool
end = struct
  type key = M.key

  type presence =
    | Always (* the binding must be present in all concrete stores *)
    | Maybe (* the binding might be present in some concrete store *)

  type t =
    | Bot
    | M of (presence * X.t) M.t
      (* The domain of the map is an upper bound of the domains of the
         concrete stores, i.e., is a variable is absent from the map,
         then it cannot be present in any of the concrete stores. If a
         binding is present with the presence flag [Always], then it
         must be present in all the concrete stores. *)
    | Top

  type value = X.t

  let bot = Bot
  let empty = M M.empty
  let top = Top
  let is_bot = function Bot -> true | _ -> false
  let is_top = function Top -> true | _ -> false

  let leq om1 om2 =
    match (om1, om2) with
    | Bot, _ | _, Top -> true
    | _, Bot | Top, _ -> false
    | M m1, M m2 ->
        M.for_all
          (fun x (p1, v1) ->
            match M.find_opt x m2 with
            | None -> false
            | Some (p2, v2) ->
                (match (p1, p2) with
                | _, Maybe | Always, Always -> true
                | _ -> false)
                && X.leq v1 v2)
          m1

  let join_gen join om1 om2 =
    match (om1, om2) with
    | Bot, om | om, Bot -> om
    | Top, _ | _, Top -> Top
    | M m1, M m2 ->
        M
          (M.merge
             (fun _x o1 o2 ->
               match (o1, o2) with
               | None, Some (_p, v) | Some (_p, v), None -> Some (Maybe, v)
               | Some (p1, v1), Some (p2, v2) ->
                   let p12 =
                     match (p1, p2) with
                     | Maybe, _ | _, Maybe -> Maybe
                     | Always, Always -> Always
                   in
                   Some (p12, join v1 v2)
               | None, None -> assert false)
             m1 m2)

  let join = join_gen X.join
  let widen = join_gen X.widen

  exception FoundBot

  let meet om1 om2 =
    match (om1, om2) with
    | Bot, _ | _, Bot -> Bot
    | Top, om | om, Top -> om
    | M m1, M m2 -> (
        try
          M
            (M.merge
               (fun _x ov1 ov2 ->
                 match (ov1, ov2) with
                 | None, None -> assert false
                 | None, _ | _, None -> None
                 | Some (p1, v1), Some (p2, v2) -> (
                     let p12 =
                       match (p1, p2) with
                       | Always, _ | _, Always -> Always
                       | Maybe, Maybe -> Maybe
                     in
                     let v12 = X.meet v1 v2 in
                     match (p12, X.is_bot v12) with
                     | Always, true -> raise FoundBot
                     | Maybe, true -> None
                     | _, false -> Some (p12, v12)))
               m1 m2)
        with FoundBot -> Bot)

  let pp fmt =
    let open Format in
    function
    | Bot -> pp_print_string fmt "⊥"
    | Top -> pp_print_string fmt "⊤"
    | M m ->
        fprintf fmt "@[<v 1>[%a]@]"
          (pp_print_seq
             ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
             (fun fmt (x, (p, v)) ->
               match p with
               | Maybe -> fprintf fmt "@[%a ->@ @[%a@]@]" Key.pp x X.pp v
               | Always -> fprintf fmt "@[%a !->@ @[%a@]@]" Key.pp x X.pp v))
          (M.to_seq m)

  let get s x =
    match s with
    | Bot -> X.bot
    | Top -> X.top
    | M m -> ( match M.find_opt x m with None -> X.bot | Some (_p, v) -> v)

  let remove s x =
    match s with
    | Bot -> Bot
    | Top -> Top
    | M m -> M (M.update x (fun _ -> None) m)

  let set s x v =
    match s with
    | Bot -> Bot
    | Top -> if X.is_bot v then Bot else Top
    | M m ->
        if X.is_bot v then Bot
        else
          M
            (M.update x
               (function
                 | None -> Some (Always, v)
                 | Some (_, v0) -> Some (Always, X.join v0 v))
               m)

  let set_strong s x v =
    match s with
    | Bot -> Bot
    | Top -> if X.is_bot v then Bot else Top
    | M m ->
        if X.is_bot v then Bot else M (M.update x (fun _ -> Some (Always, v)) m)
end

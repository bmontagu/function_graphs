(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Abstract domain for stores *)

(** Signature of stores *)
module type S = sig
  include Sigs.ABSTRACT_DOMAIN

  type key
  type value

  val empty : t
  (** Abstract value that represents the empty store *)

  val get : t -> key -> value
  (** Queries the value of a key in a store *)

  val remove : t -> key -> t
  (** Removes the entry of a key in a store *)

  val set : t -> key -> value -> t
  (** Adds or replaces the entry of a key in a store. If an entry was present
      for that key, the new entry is the join of the former value and the
      provided value. *)

  val set_strong : t -> key -> value -> t
  (** Adds or replaces the entry of a key in a store. If an entry was present
      for that key, the former entry is discarded and the new entry the provided
      value (strong update). *)
end

(** Functor that builds an non-relational abstract domain for stores, given
    implementations for keys, maps of keys, and abstract values that should be
    associated to keys. *)
module Make : functor
  (Key : sig
     type t

     val pp : Format.formatter -> t -> unit
   end)
  (_ : sig
     type key = Key.t
     type 'a t

     val empty : 'a t
     val find_opt : key -> 'a t -> 'a option
     val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
     val for_all : (key -> 'a -> bool) -> 'a t -> bool

     val merge :
       (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

     val to_seq : 'a t -> (key * 'a) Seq.t
   end)
  (X : Sigs.ABSTRACT_DOMAIN)
  -> sig
  include S with type key = Key.t and type value = X.t

  val is_top : t -> bool
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Abstract domain for boolean values, i.e., subsets of
    {m \{\mathrm{true},\mathrm{false}\}} *)

include Sigs.ABSTRACT_DOMAIN

val is_top : t -> bool
(** Tests whether this is the top element *)

val compare : t -> t -> int
(** Total order *)

val equal : t -> t -> bool
(** Syntactic equality *)

val hash_fold : Base.Hash.state -> t -> Base.Hash.state
(** Hashing *)

val singleton : bool -> t
(** [singleton b] represents the singleton set {m \{b\}} *)

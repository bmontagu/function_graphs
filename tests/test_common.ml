(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

module Unit = struct
  type t = unit

  let bot = ()
  let top = ()
  let leq () () = true
  let is_bot () = false
  let join () () = ()
  let widen () () = ()
  let meet () () = ()
  let pp fmt () = Format.pp_print_string fmt "()"
end

module UnitMap = struct
  type key = unit
  type 'a t = 'a option

  let empty = None
  let mem () = function None -> false | Some _ -> true
  let find_opt () o = o
  let singleton () v = Some v
  let update () f o = f o
  let mapi f o = Option.map (f ()) o

  let union f o1 o2 =
    match (o1, o2) with
    | None, o | o, None -> o
    | Some v1, Some v2 -> f () v1 v2

  let iter f o = match o with None -> () | Some v -> f () v
  let fold f o acc = match o with None -> acc | Some v -> f () v acc
  let for_all p = function None -> true | Some v -> p () v

  let pp pp_v fmt = function
    | None -> Format.pp_print_string fmt "⊥"
    | Some v -> pp_v fmt v

  let to_seq = function
    | None -> Seq.empty
    | Some v -> Seq.cons ((), v) Seq.empty
end

module String = struct
  include String

  let pp = Format.pp_print_string
end

module M = Map.Make (String)

module IntervalStore =
  Abstract_domains.Store.Make (String) (M) (Abstract_domains.Interval)

module CFG = struct
  module Node = struct
    type t = int

    let compare = Int.compare
    let equal = Int.equal
    let pp = Format.pp_print_int
  end

  module NodeMap = struct
    include Map.Make (Node)

    let pp pp_v fmt m =
      let open Format in
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             fprintf fmt "@[<v 2>@[%a@] ->@ @[%a@]@]" Node.pp k pp_v v))
        (to_seq m)
  end

  module Reg = struct
    type t = string

    let compare = String.compare
    let equal = String.equal
  end

  module Instr = struct
    type op = Read | Add | Sub | Le | Lt | Eq
    type arg = R of Reg.t | V of int
    type cond = IsZero of Reg.t | IsLt of arg * arg | IsLe of arg * arg
    type t = Nop | Cond of cond | Op of op * Reg.t * arg list
  end

  module Label = struct
    type t = bool option

    let compare o1 o2 =
      match (o1, o2) with
      | None, None -> 0
      | None, Some _ -> -1
      | Some b1, Some b2 -> Bool.compare b1 b2
      | Some _, None -> 1

    let equal o1 o2 = compare o1 o2 = 0

    let pp fmt = function
      | None -> Format.pp_print_string fmt "any"
      | Some b -> Format.pp_print_bool fmt b
  end

  module NodeLabel = struct
    type t = Node.t * Label.t

    let compare (n1, l1) (n2, l2) =
      let i = Node.compare n1 n2 in
      if i <> 0 then i else Label.compare l1 l2

    let equal (n1, l1) (n2, l2) = Node.equal n1 n2 && Label.equal l1 l2
    let pp fmt (n, l) = Format.fprintf fmt "@[%a -[%a]->@]" Node.pp n Label.pp l
  end

  module NodeLabelMap = struct
    include Map.Make (NodeLabel)

    let pp pp_v fmt m =
      let open Format in
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             fprintf fmt "@[<v 2>@[%a@]@ @[%a@]@]" NodeLabel.pp k pp_v v))
        (to_seq m)
  end

  module ValueAnalysis = struct
    type instr = Instr.t
    type label = Label.t
    type value = IntervalStore.t

    open Abstract_domains

    let singleton i = Interval.make (IntBar.Int i) (IntBar.Int i)
    let true_ = singleton 1
    let false_ = singleton 0
    let any_bool = Interval.join true_ false_

    let eval_arg arg s =
      match arg with
      | Instr.R r -> IntervalStore.get s r
      | Instr.V i -> singleton i

    let interval_le v1 v2 =
      if Interval.(is_bot v1 || is_bot v2) then Interval.bot
      else if IntBar.leq (Interval.upper_bound v1) (Interval.lower_bound v2)
      then true_
      else if
        IntBar.leq (Interval.upper_bound v2)
          (IntBar.succ @@ Interval.lower_bound v1)
      then false_
      else any_bool

    let interval_lt v1 v2 =
      if Interval.(is_bot v1 || is_bot v2) then Interval.bot
      else if
        IntBar.leq (Interval.upper_bound v1)
          (IntBar.succ @@ Interval.lower_bound v2)
      then true_
      else if IntBar.leq (Interval.upper_bound v2) (Interval.lower_bound v1)
      then false_
      else any_bool

    let interval_eq v1 v2 =
      if Interval.(is_bot v1 || is_bot v2) then Interval.bot
      else
        let v12 = Interval.meet v1 v2 in
        if Interval.is_bot v12 then false_
        else if Interval.is_singleton v1 && Interval.is_singleton v2 then true_
        else any_bool

    let eval_op op args s =
      match (op, args) with
      | Instr.Read, [ arg ] -> eval_arg arg s
      | Instr.Read, _ -> assert false
      | Instr.Add, [ arg1; arg2 ] ->
          Interval.plus (eval_arg arg1 s) (eval_arg arg2 s)
      | Instr.Add, _ -> assert false
      | Instr.Sub, [ arg1; arg2 ] ->
          Interval.minus (eval_arg arg1 s) (eval_arg arg2 s)
      | Instr.Sub, _ -> assert false
      | Instr.Le, [ arg1; arg2 ] ->
          interval_le (eval_arg arg1 s) (eval_arg arg2 s)
      | Instr.Le, _ -> assert false
      | Instr.Lt, [ arg1; arg2 ] ->
          interval_lt (eval_arg arg1 s) (eval_arg arg2 s)
      | Instr.Lt, _ -> assert false
      | Instr.Eq, [ arg1; arg2 ] ->
          interval_eq (eval_arg arg1 s) (eval_arg arg2 s)
      | Instr.Eq, _ -> assert false

    let set_arg s arg v =
      if Interval.is_bot v then IntervalStore.bot
      else
        match arg with
        | Instr.V _ -> s
        | Instr.R r -> IntervalStore.set_strong s r v

    let eval_cond c label s =
      match c with
      | Instr.IsZero r -> (
          let v_r = IntervalStore.get s r in
          match label with
          | None -> (* we may take both branches *) s
          | Some true ->
              let zero = singleton 0 in
              let v_zero = Interval.meet v_r zero in
              set_arg s (R r) v_zero
          | Some false ->
              let s_neg =
                let neg = Interval.make IntBar.MInf (IntBar.Int (-1)) in
                let v_neg = Interval.meet v_r neg in
                set_arg s (R r) v_neg
              and s_pos =
                let pos = Interval.make (IntBar.Int 1) IntBar.PInf in
                let v_pos = Interval.meet v_r pos in
                set_arg s (R r) v_pos
              in
              IntervalStore.join s_neg s_pos)
      | Instr.IsLe (arg1, arg2) -> (
          let i1 = eval_arg arg1 s and i2 = eval_arg arg2 s in
          if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
          else
            match label with
            | None -> (* we may take both branches *) s
            | Some true ->
                let i1 = Interval.(meet i1 (make IntBar.MInf (upper_bound i2)))
                and i2 =
                  Interval.(meet i2 (make (lower_bound i1) IntBar.PInf))
                in
                IntervalStore.meet s
                  (IntervalStore.meet (set_arg s arg1 i1) (set_arg s arg2 i2))
            | Some false ->
                let i1 =
                  Interval.(
                    meet i1 (make (IntBar.succ @@ lower_bound i2) IntBar.PInf))
                and i2 =
                  Interval.(
                    meet i2 (make IntBar.MInf (IntBar.pred @@ upper_bound i1)))
                in
                IntervalStore.meet s
                  (IntervalStore.meet (set_arg s arg1 i1) (set_arg s arg2 i2)))
      | Instr.IsLt (arg1, arg2) -> (
          let i1 = eval_arg arg1 s and i2 = eval_arg arg2 s in
          if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
          else
            match label with
            | None -> (* we may take both branches *) s
            | Some true ->
                let i1 =
                  Interval.(
                    meet i1 (make IntBar.MInf (IntBar.pred @@ upper_bound i2)))
                and i2 =
                  Interval.(
                    meet i2 (make (IntBar.succ @@ lower_bound i1) IntBar.PInf))
                in
                IntervalStore.meet s
                  (IntervalStore.meet (set_arg s arg1 i1) (set_arg s arg2 i2))
            | Some false ->
                let i1 = Interval.(meet i1 (make (lower_bound i2) IntBar.PInf))
                and i2 =
                  Interval.(meet i2 (make IntBar.MInf (upper_bound i1)))
                in
                IntervalStore.meet s
                  (IntervalStore.meet (set_arg s arg1 i1) (set_arg s arg2 i2)))

    let eval instr label s =
      match instr with
      | Instr.Nop -> s
      | Cond c -> eval_cond c label s
      | Op (op, dst, args) -> IntervalStore.set_strong s dst (eval_op op args s)

    type reg = Reg.t

    let eval_eq r i s =
      let i =
        Interval.meet (IntervalStore.get s r)
          (Interval.make (IntBar.Int i) (IntBar.Int i))
      in
      IntervalStore.set_strong s r i

    let rec fold_int_from_to f l u acc =
      if u < l then acc else fold_int_from_to f (l + 1) u (f l acc)

    let value_node_map_reduce s r l u map reduce start =
      let i = IntervalStore.get s r in
      let i = Interval.(meet i (make (IntBar.Int l) (IntBar.Int u))) in
      if Interval.is_bot i then start
      else
        let l =
          match Interval.lower_bound i with
          | IntBar.Int n -> n
          | _ -> assert false
        and u =
          match Interval.upper_bound i with
          | IntBar.Int n -> n
          | _ -> assert false
        in
        fold_int_from_to (fun n acc -> reduce (map n) acc) l u start
  end
end

module While = struct
  type var = string

  let compare_var = String.compare
  let equal_var = String.equal

  type ('expr, 'bexpr) prog =
    | Assign of var * 'expr
    | Skip
    | Seq of ('expr, 'bexpr) prog * ('expr, 'bexpr) prog
    | If of 'bexpr * ('expr, 'bexpr) prog * ('expr, 'bexpr) prog
    | While of 'bexpr * ('expr, 'bexpr) prog

  let rec compare_prog compare_expr compare_bexpr p1 p2 =
    match (p1, p2) with
    | Assign (x1, e1), Assign (x2, e2) ->
        let n = String.compare x1 x2 in
        if n <> 0 then n else compare_expr e1 e2
    | Assign _, _ -> -1
    | Skip, Assign _ -> 1
    | Skip, Skip -> 0
    | Skip, _ -> -1
    | Seq _, (Assign _ | Skip) -> 1
    | Seq (p1, p1'), Seq (p2, p2') ->
        let n = compare_prog compare_expr compare_bexpr p1 p2 in
        if n <> 0 then n else compare_prog compare_expr compare_bexpr p1' p2'
    | Seq _, _ -> -1
    | If _, (Assign _ | Skip | Seq _) -> 1
    | If (c1, p1, p1'), If (c2, p2, p2') ->
        let n = compare_bexpr c1 c2 in
        if n <> 0 then n
        else
          let n = compare_prog compare_expr compare_bexpr p1 p2 in
          if n <> 0 then n else compare_prog compare_expr compare_bexpr p1' p2'
    | If _, _ -> -1
    | While _, (Assign _ | Skip | Seq _ | If _) -> 1
    | While (c1, p1), While (c2, p2) ->
        let n = compare_bexpr c1 c2 in
        if n <> 0 then n else compare_prog compare_expr compare_bexpr p1 p2

  let rec pp_prog pp_expr pp_bexpr fmt =
    let open Format in
    function
    | Assign (x, e) -> fprintf fmt "@[%s :=@ %a@]" x pp_expr e
    | Skip -> fprintf fmt "skip"
    | Seq (p1, p2) ->
        fprintf fmt "@[<hv>@[%a@];@ @[%a@]@]" (pp_prog pp_expr pp_bexpr) p1
          (pp_prog pp_expr pp_bexpr) p2
    | If (c, p1, p2) ->
        fprintf fmt "@[<hv>if@ @[%a@]@ then@ @[%a@]@ else@ @[%a@]@]" pp_bexpr c
          (pp_prog pp_expr pp_bexpr) p1 (pp_prog pp_expr pp_bexpr) p2
    | While (c, p) ->
        fprintf fmt "@[<hv>while@ @[%a@]@ do@ @[%a@]@ done@]" pp_bexpr c
          (pp_prog pp_expr pp_bexpr) p

  module Expr = struct
    type t =
      | Unknown
      | Const of int
      | Var of var
      | Plus of t * t
      | Minus of t * t

    let rec compare t1 t2 =
      match (t1, t2) with
      | Unknown, Unknown -> 0
      | Unknown, _ -> -1
      | Const _, Unknown -> 1
      | Const n1, Const n2 -> Int.compare n1 n2
      | Const _, _ -> -1
      | Var _, (Unknown | Const _) -> 1
      | Var x1, Var x2 -> String.compare x1 x2
      | Var _, _ -> -1
      | Plus _, (Unknown | Const _ | Var _) -> 1
      | Plus (t1, t1'), Plus (t2, t2') ->
          let n = compare t1 t2 in
          if n <> 0 then n else compare t1' t2'
      | Plus _, _ -> -1
      | Minus _, (Unknown | Const _ | Var _ | Plus _) -> 1
      | Minus (t1, t1'), Minus (t2, t2') ->
          let n = compare t1 t2 in
          if n <> 0 then n else compare t1' t2'

    let rec pp fmt =
      let open Format in
      function
      | Unknown -> pp_print_string fmt "?"
      | Const n -> pp_print_int fmt n
      | Var x -> pp_print_string fmt x
      | Plus (t1, t2) -> fprintf fmt "@[<hv>@[%a@]@ +@ @[%a@]@]" pp t1 pp t2
      | Minus (t1, ((Const _ | Var _) as t2)) ->
          fprintf fmt "@[<hv>@[%a@]@ -@ @[%a@]@]" pp t1 pp t2
      | Minus (t1, t2) -> fprintf fmt "@[<hv>@[%a@]@ -@ (@[%a@])@]" pp t1 pp t2
  end

  module BExpr = struct
    type t =
      | Unknown
      | Bool of bool
      | Le of Expr.t * Expr.t
      | Lt of Expr.t * Expr.t
      | And of t * t
      | Or of t * t

    let rec compare t1 t2 =
      match (t1, t2) with
      | Unknown, Unknown -> 0
      | Unknown, _ -> -1
      | Bool _, Unknown -> 1
      | Bool b1, Bool b2 -> Bool.compare b1 b2
      | Bool _, _ -> -1
      | Le _, (Unknown | Bool _) -> 1
      | Le (t1, t1'), Le (t2, t2') ->
          let n = Expr.compare t1 t2 in
          if n <> 0 then n else Expr.compare t1' t2'
      | Le _, _ -> -1
      | Lt _, (Unknown | Bool _ | Le _) -> 1
      | Lt (t1, t1'), Lt (t2, t2') ->
          let n = Expr.compare t1 t2 in
          if n <> 0 then n else Expr.compare t1' t2'
      | Lt _, _ -> -1
      | And _, (Unknown | Bool _ | Le _ | Lt _) -> 1
      | And (t1, t1'), And (t2, t2') ->
          let n = compare t1 t2 in
          if n <> 0 then n else compare t1' t2'
      | And _, _ -> -1
      | Or _, (Unknown | Bool _ | Le _ | Lt _ | And _) -> 1
      | Or (t1, t1'), Or (t2, t2') ->
          let n = compare t1 t2 in
          if n <> 0 then n else compare t1' t2'

    let rec pp fmt =
      let open Format in
      function
      | Unknown -> pp_print_char fmt '?'
      | Bool b -> pp_print_bool fmt b
      | Le (t1, t2) ->
          fprintf fmt "@[<hv>@[%a@]@ <=@ @[%a@]@]" Expr.pp t1 Expr.pp t2
      | Lt (t1, t2) ->
          fprintf fmt "@[<hv>@[%a@]@ <@ @[%a@]@]" Expr.pp t1 Expr.pp t2
      | And ((Or _ as t1), (Or _ as t2)) ->
          fprintf fmt "@[<hv>(@[%a@])@ &&@ (@[%a@])@]" pp t1 pp t2
      | And ((Or _ as t1), t2) ->
          fprintf fmt "@[<hv>(@[%a@])@ &&@ @[%a@]@]" pp t1 pp t2
      | And (t1, (Or _ as t2)) ->
          fprintf fmt "@[<hv>@[%a@]@ &&@ (@[%a@])@]" pp t1 pp t2
      | And (t1, t2) -> fprintf fmt "@[<hv>@[%a@]@ &&@ @[%a@]@]" pp t1 pp t2
      | Or (t1, t2) -> fprintf fmt "@[<hv>@[%a@]@ ||@ @[%a@]@]" pp t1 pp t2

    let rec negate = function
      | Unknown -> Unknown
      | Bool b -> Bool (not b)
      | Le (t1, t2) -> Lt (t2, t1)
      | Lt (t1, t2) -> Le (t2, t1)
      | And (t1, t2) -> Or (negate t1, negate t2)
      | Or (t1, t2) -> And (negate t1, negate t2)
  end

  module ValueAnalysis = struct
    open Abstract_domains

    let rec eval_expr s = function
      | Expr.Unknown -> Interval.top
      | Expr.Const n ->
          let n = IntBar.Int n in
          Interval.make n n
      | Expr.Var x -> IntervalStore.get s x
      | Expr.Plus (t1, t2) -> Interval.plus (eval_expr s t1) (eval_expr s t2)
      | Expr.Minus (t1, t2) -> Interval.minus (eval_expr s t1) (eval_expr s t2)

    let assign x e s =
      let v = eval_expr s e in
      IntervalStore.set_strong s x v

    let rec bkw_eval_expr t i s =
      if IntervalStore.is_bot s then s
      else
        match t with
        | Expr.Unknown -> if Interval.is_bot i then IntervalStore.bot else s
        | Expr.Const n ->
            if Interval.(is_bot (meet (make (IntBar.Int n) (IntBar.Int n)) i))
            then IntervalStore.bot
            else s
        | Expr.Var x ->
            let ix = IntervalStore.get s x in
            let i = Interval.meet i ix in
            IntervalStore.set_strong s x i
        | Expr.Plus (t1, t2) ->
            IntervalStore.meet
              (bkw_eval_expr t1 Interval.(meet i (minus i (eval_expr s t2))) s)
              (bkw_eval_expr t2 Interval.(meet i (minus i (eval_expr s t1))) s)
        | Expr.Minus (t1, t2) ->
            IntervalStore.meet
              (bkw_eval_expr t1 Interval.(meet i (plus i (eval_expr s t2))) s)
              (bkw_eval_expr t2 Interval.(meet i (minus (eval_expr s t1) i)) s)

    let rec cond_true bexpr s =
      if IntervalStore.is_bot s then s
      else
        match bexpr with
        | BExpr.Unknown -> s
        | BExpr.Bool b -> if b then s else IntervalStore.bot
        | BExpr.Le (t1, t2) ->
            let i1 = eval_expr s t1 and i2 = eval_expr s t2 in
            if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
            else
              let i1 = Interval.(meet i1 (make IntBar.MInf (upper_bound i2)))
              and i2 = Interval.(meet i2 (make (lower_bound i1) IntBar.PInf)) in
              if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
              else
                IntervalStore.meet (bkw_eval_expr t1 i1 s)
                  (bkw_eval_expr t2 i2 s)
        | BExpr.Lt (t1, t2) ->
            let i1 = eval_expr s t1 and i2 = eval_expr s t2 in
            if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
            else
              let i1 =
                Interval.(
                  meet i1 (make IntBar.MInf (IntBar.pred @@ upper_bound i2)))
              and i2 =
                Interval.(
                  meet i2 (make (IntBar.succ @@ lower_bound i1) IntBar.PInf))
              in
              if Interval.is_bot i1 || Interval.is_bot i2 then IntervalStore.bot
              else
                IntervalStore.meet (bkw_eval_expr t1 i1 s)
                  (bkw_eval_expr t2 i2 s)
        | BExpr.And (c1, c2) ->
            IntervalStore.meet (cond_true c1 s) (cond_true c2 s)
        | BExpr.Or (c1, c2) ->
            IntervalStore.join (cond_true c1 s) (cond_true c2 s)

    let cond_false bexpr s = cond_true (BExpr.negate bexpr) s
    let cond bexpr b s = if b then cond_true bexpr s else cond_false bexpr s
  end
end

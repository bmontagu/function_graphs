(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** A simple static analyzer that performs a non-relational analysis for a While
    language, using a non-standard approach. It is defined using a fixpoint
    solver and an abstract domain for function graphs. *)
module While : functor
  (Expr : sig
     type t

     val compare : t -> t -> int
     val pp : Format.formatter -> t -> unit
   end)
  (BExpr : sig
     type t

     val compare : t -> t -> int
     val pp : Format.formatter -> t -> unit
   end)
  (S : sig
     type t

     val join : t -> t -> t
     val pp : Format.formatter -> t -> unit
   end)
  (MFG : sig
     include Function_graphs.Sigs.MFG with type dom = S.t and type codom = S.t

     val iter : (dom -> codom -> unit) -> t -> unit
   end)
  (_ : sig
     val assign : Test_common.While.var -> Expr.t -> S.t -> S.t
     val cond : BExpr.t -> bool -> S.t -> S.t
   end)
  -> sig
  val analyze :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Expr.t, BExpr.t) Test_common.While.prog ->
    S.t ->
    S.t

  val run :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Expr.t, BExpr.t) Test_common.While.prog ->
    S.t ->
    unit
end

module MFG :
  Function_graphs.Sigs.MFG
    with type dom = Test_common.IntervalStore.t
     and type codom = Test_common.IntervalStore.t

module Analyzer : sig
  val analyze :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Test_common.While.Expr.t, Test_common.While.BExpr.t) Test_common.While.prog ->
    Test_common.IntervalStore.t ->
    Test_common.IntervalStore.t

  val run :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Test_common.While.Expr.t, Test_common.While.BExpr.t) Test_common.While.prog ->
    Test_common.IntervalStore.t ->
    unit
end

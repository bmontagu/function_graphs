(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for control-flow graphs using a
    non-standard approach, that performs a non-relational analysis. It is
    implemented using a fixpoint solver based on an abstract domain for function
    graphs. *)

module type CFG = sig
  type t
  type node
  type label
  type instr
  type reg

  val entries : t -> (node * reg list) list
  val exits : t -> (node * reg list) list
  val step : t -> node -> instr * (label * node) list
end

module Make
    (Node : sig
      type t

      val equal : t -> t -> bool
      val pp : Format.formatter -> t -> unit
    end)
    (_ : sig
      type key = Node.t
      type 'a t

      val empty : 'a t
      val singleton : key -> 'a -> 'a t
      val mem : key -> 'a t -> bool
      val find_opt : key -> 'a t -> 'a option
      val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
      val for_all : (key -> 'a -> bool) -> 'a t -> bool
      val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
      val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
      val iter : (key -> 'a -> unit) -> 'a t -> unit

      val pp :
        (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

      val to_seq : 'a t -> (key * 'a) Seq.t
    end)
    (Reg : sig
      type t

      val equal : t -> t -> bool
    end)
    (CFG : CFG with type node = Node.t and type reg = Reg.t)
    (D : sig
      type t
      type key = Reg.t
      type value

      val empty : t
      val get : t -> key -> value
      val set : t -> key -> value -> t
      val bot : t
      val join : t -> t -> t
      val pp : Format.formatter -> t -> unit
    end)
    (_ : sig
      type instr = CFG.instr
      type label = CFG.label
      type value = D.t

      val eval : instr -> label -> value -> value
    end)
    (MFG : sig
      include Function_graphs.Sigs.MFG with type dom = D.t and type codom = D.t

      val iter : (dom -> codom -> unit) -> t -> unit
    end) : sig
  val eval : MFG.strategy array -> CFG.t -> Node.t -> D.t -> D.t

  val run :
    MFG.strategy array ->
    CFG.t ->
    (Node.t * (Reg.t * D.value) list) list ->
    unit
end

module CFG :
  CFG
    with type node = Test_common.CFG.Node.t
     and type label = Test_common.CFG.Label.t
     and type instr = Test_common.CFG.Instr.t
     and type reg = Test_common.CFG.Reg.t

module MFG :
  Function_graphs.Sigs.MFG
    with type dom = Test_common.IntervalStore.t
     and type codom = Test_common.IntervalStore.t

module Analyzer : sig
  val eval :
    MFG.strategy array ->
    CFG.t ->
    Test_common.CFG.Node.t ->
    Test_common.IntervalStore.t ->
    Test_common.IntervalStore.t

  val run :
    MFG.strategy array ->
    CFG.t ->
    (int * (string * Abstract_domains.Interval.t) list) list ->
    unit
end

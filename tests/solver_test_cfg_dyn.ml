(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for control-flow graphs with computed
    jumps using a non-standard approach, that performs a non-relational
    analysis. It is implemented using a fixpoint solver based on an abstract
    domain for function graphs. *)

module type CFG = sig
  type t
  type node
  type label
  type instr
  type reg

  val length : t -> int
  val entries : t -> (node * reg list) list
  val exits : t -> (node * reg list) list
  val step : t -> node -> instr * (label * (node, reg) Either.t) list
end

open Function_graphs

module Make
    (Node : sig
      type t

      val equal : t -> t -> bool
      val pp : Format.formatter -> t -> unit
    end)
    (NodeMap : sig
      type key = Node.t
      type 'a t

      val empty : 'a t
      val singleton : key -> 'a -> 'a t
      val mem : key -> 'a t -> bool
      val find_opt : key -> 'a t -> 'a option
      val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
      val for_all : (key -> 'a -> bool) -> 'a t -> bool
      val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
      val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
      val iter : (key -> 'a -> unit) -> 'a t -> unit

      val pp :
        (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

      val to_seq : 'a t -> (key * 'a) Seq.t
    end)
    (Reg : sig
      type t

      val equal : t -> t -> bool
    end)
    (CFG : CFG with type node = Node.t and type reg = Reg.t)
    (D : sig
      type t
      type key = Reg.t
      type value

      val empty : t
      val get : t -> key -> value
      val set : t -> key -> value -> t
      val bot : t
      val join : t -> t -> t
      val pp : Format.formatter -> t -> unit
    end)
    (A : sig
      type instr = CFG.instr
      type label = CFG.label
      type value = D.t
      type reg = Reg.t

      val eval : instr -> label -> value -> value
      val eval_eq : reg -> Node.t -> value -> value

      val value_node_map_reduce :
        value ->
        reg ->
        int ->
        int ->
        (Node.t -> 'a) ->
        ('a -> 'b -> 'b) ->
        'b ->
        'b
    end)
    (MFG : sig
      include Function_graphs.Sigs.MFG with type dom = D.t and type codom = D.t

      val iter : (dom -> codom -> unit) -> t -> unit
    end) : sig
  val eval : MFG.strategy array -> CFG.t -> Node.t -> D.t -> D.t

  val run :
    MFG.strategy array ->
    CFG.t ->
    (Node.t * (Reg.t * D.value) list) list ->
    unit
end = struct
  let is_exit cfg node =
    List.exists (fun (node', _regs) -> Node.equal node node') (CFG.exits cfg)

  let joins l = List.fold_left D.join D.bot l

  let f_eval cfg eval node input =
    let instr, next = CFG.step cfg node in
    D.join (if is_exit cfg node then input else D.bot)
    @@ joins
         (List.map
            (fun (label, node_or_reg) ->
              let s = A.eval instr label input in
              match node_or_reg with
              | Either.Left node -> eval node s
              | Either.Right reg ->
                  A.value_node_map_reduce s reg 0
                    (CFG.length cfg - 1)
                    (fun node -> eval node (A.eval_eq reg node s))
                    D.join D.bot)
            next)

  module S = Solver.Naive (Node) (NodeMap) (D) (D) (MFG)

  let eval strategy cfg node input =
    fst @@ S.fix_gen ~debug:true strategy (f_eval cfg) node input

  let make_inputs entries start =
    List.map
      (fun (node, regs) ->
        let d =
          List.fold_left
            (fun s (node', args) ->
              if Node.equal node node' then
                List.fold_left
                  (fun s r ->
                    List.fold_left
                      (fun s (r', v) ->
                        if Reg.equal r r' then D.join s (D.set s r v) else s)
                      s args)
                  s regs
              else s)
            D.empty start
        in
        (node, d))
      entries

  let outputs_at_exits0 s exits =
    List.fold_left
      (fun m (node, regs) ->
        let s' =
          List.fold_left (fun s' r -> D.set s' r (D.get s r)) D.empty regs
        in
        NodeMap.update node
          (function None -> Some s' | Some s'' -> Some (D.join s'' s'))
          m)
      NodeMap.empty exits

  let outputs_at_exits outputs exits =
    List.fold_left
      (fun m s ->
        NodeMap.union
          (fun _ s1 s2 -> Some (D.join s1 s2))
          m
          (outputs_at_exits0 s exits))
      NodeMap.empty outputs

  let run strategy cfg start =
    let inputs = make_inputs (CFG.entries cfg) start in
    let outputs =
      List.map (fun (node, input) -> eval strategy cfg node input) inputs
    in
    let res = outputs_at_exits outputs (CFG.exits cfg) in
    let open Format in
    printf "@.Outputs:@.@[<v>%a@]" (NodeMap.pp D.pp) res
end

module CFG = struct
  open Test_common.CFG

  type node = Node.t
  type instr = Instr.t
  type label = bool option
  type reg = Reg.t

  type t = {
    entries : (node * reg list) list;
    exits : (node * reg list) list;
    code : (instr * (label * (Node.t, Reg.t) Either.t) list option) array;
  }

  let length t = Array.length t.code
  let entries t = t.entries
  let exits t = t.exits

  let step t node =
    let code = t.code in
    assert (0 <= node && node < Array.length code);
    let instr, successors = code.(node) in
    let successors =
      match successors with
      | None ->
          if node = Array.length code - 1 then []
          else [ (None, Either.Left (node + 1)) ]
      | Some l -> l
    in
    (instr, successors)
end

module MFG = Simple.Make (Test_common.IntervalStore) (Test_common.IntervalStore)

module Analyzer =
  Make (Test_common.CFG.Node) (Test_common.CFG.NodeMap) (Test_common.CFG.Reg)
    (CFG)
    (Test_common.IntervalStore)
    (Test_common.CFG.ValueAnalysis)
    (MFG)

open Abstract_domains

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (1, [ "x" ]) ];
        code = [| (Op (Read, "x", [ V 0 ]), None); (Nop, None) |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [x !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]

    Number of iterations: 4
    Number of calls: 7
    Number of iterations per point:
    0: 2
    1: 2
    Node numbering:
    0: 0
    1: 1
    Widening points:


    Full graph:
    0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]
    Final MFG:
    [[]
     ====>
     [x !-> [0,0]]]

    Final result:
    [x !-> [0,0]]

    Outputs:
    1 ->
      [x !-> [0,0]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (1, [ "y" ]) ];
        code = [| (Op (Add, "y", [ R "x"; V 1 ]), None); (Nop, None) |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("x", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥]
    New calls:
    1: [x -> [0,2];
        y !-> [1,3]]

    New graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]]

    Iteration #3:
    Initial graph:
    0 ->
      [[x -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]]
    New calls:

    New graph:
    0 ->
      [[x -> [0,2]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]]

    Number of iterations: 4
    Number of calls: 7
    Number of iterations per point:
    0: 2
    1: 2
    Node numbering:
    0: 0
    1: 1
    Widening points:


    Full graph:
    0 ->
      [[x -> [0,2]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 ->
      [[x -> [0,2];
        y !-> [1,3]]
       ====>
       [x -> [0,2];
        y !-> [1,3]]]
    Final MFG:
    [[x -> [0,2]]
     ====>
     [x -> [0,2];
      y !-> [1,3]]]

    Final result:
    [x -> [0,2];
     y !-> [1,3]]

    Outputs:
    1 ->
      [y !-> [1,3]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (4, [ "x" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsZero "b"),
              Some [ (Some true, Left 1); (Some false, Left 2) ] );
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, Left 3) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, Left 3) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]
    New calls:
    1: [b !-> [0,0]]
    2: [b !-> [1,2]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]
    New calls:
    3: [b !-> [0,2];
        x !-> [0,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥]
    New calls:
    4: [b !-> [0,2];
        x !-> [0,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 8
    Number of calls: 33
    Number of iterations per point:
    0: 2
    1: 2
    2: 2
    3: 2
    4: 2
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 4
    Widening points:


    Full graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [[b -> [0,2]]
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    4 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsZero "b"),
              Some [ (Some true, Left 1); (Some false, Left 2) ] );
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, Left 3) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, Left 4) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]
    New calls:
    1: [b !-> [0,0]]
    2: [b !-> [1,2]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]
    New calls:
    3: [b !-> [0,0];
        x !-> [0,0]]
    4: [b !-> [1,2];
        x !-> [1,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥]
    New calls:
    4: [b !-> [0,0];
        x !-> [0,0]]
    5: [b !-> [1,2];
        x !-> [1,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥]
    New calls:
    5: [b !-> [0,2];
        x !-> [0,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #8:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 9
    Number of calls: 45
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    4: 3
    5: 3
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 4
    5: 5
    Widening points:


    Full graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [[b -> [0,2]]
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    5 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsZero "b"),
              Some [ (Some true, Left 1); (Some false, Left 2) ] );
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, Left 4) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, Left 3) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥]
    New calls:
    1: [b !-> [0,0]]
    2: [b !-> [1,2]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥]
    New calls:
    3: [b !-> [1,2];
        x !-> [1,1]]
    4: [b !-> [0,0];
        x !-> [0,0]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    4: [b !-> [1,2];
        x !-> [1,1]]
    5: [b !-> [0,0];
        x !-> [0,0]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,0];
        x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [b !-> [0,2];
        x !-> [0,1]]

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       ⊥];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       ⊥];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       ⊥];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Iteration #8:
    Initial graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 9
    Number of calls: 45
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    4: 3
    5: 3
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 4
    5: 5
    Widening points:


    Full graph:
    0 ->
      [[b -> [0,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    1 ->
      [[b !-> [0,0]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    2 ->
      [[b !-> [1,2]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    3 ->
      [[b !-> [1,2];
        x !-> [1,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 ->
      [[b !-> [0,2];
        x !-> [0,1]]
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [[b -> [0,2]]
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    5 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (3, [ "i" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, Left 2); (Some false, Left 3) ] );
            (* 2 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, Left 1) ]);
            (* 3 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    1: [i !-> [1,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,99]]
    3: [i !-> [100,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞]]];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Number of iterations: 8
    Number of calls: 25
    Number of iterations per point:
    0: 2
    1: 3
    2: 2
    3: 2
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    Widening points:
    1

    Full graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞]]];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    3 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    Final MFG:
    [[]
     ====>
     [i !-> [100,+∞]]]

    Final result:
    [i !-> [100,+∞]]

    Outputs:
    3 ->
      [i !-> [100,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (4, [ "i" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, Left 2); (Some false, Left 4) ] );
            (* 2 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, Left 1) ]);
            (* 3 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, Left 2); (Some false, Left 4) ] );
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    1: [i !-> [1,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,99]]
    4: [i !-> [100,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞]]];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]

    Number of iterations: 8
    Number of calls: 25
    Number of iterations per point:
    0: 2
    1: 3
    2: 2
    4: 2
    Node numbering:
    0: 0
    1: 1
    2: 2
    4: 3
    Widening points:
    1

    Full graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞]]];
    1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    2 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,+∞]]];
    4 ->
      [[i !-> [100,+∞]]
       ====>
       [i !-> [100,+∞]]]
    Final MFG:
    [[]
     ====>
     [i !-> [100,+∞]]]

    Final result:
    [i !-> [100,+∞]]

    Outputs:
    4 ->
      [i !-> [100,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (6, [ "i"; "j" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, Left 2); (Some false, Left 6) ] );
            (* 2 *) (Op (Read, "j", [ V 0 ]), None);
            (* 3 *)
            ( Cond (IsLt (R "j", R "i")),
              Some [ (Some true, Left 4); (Some false, Left 5) ] );
            (* 4 *) (Op (Add, "j", [ R "j"; V 1 ]), Some [ (None, Left 3) ]);
            (* 5 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, Left 1) ]);
            (* 6 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥]
    New calls:
    3: [i !-> [0,0];
        j !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [i !-> [0,0];
        j !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]
    New calls:
    1: [i !-> [1,1];
        j !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,99];
        j -> [0,0]]
    6: [i !-> [100,+∞];
        j -> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       ⊥]
    New calls:
    3: [i !-> [0,+∞];
        j !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #8:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    4: [i !-> [1,+∞];
        j !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #9:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       ⊥];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    3: [i !-> [1,+∞];
        j !-> [1,1]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #10:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,0];
        j !-> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    4: [i !-> [1,+∞];
        j !-> [0,1]]
    5: [i !-> [0,1];
        j !-> [0,1]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,1];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #11:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[i !-> [0,1];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    1: [i !-> [1,2];
        j !-> [0,1]]
    3: [i !-> [1,+∞];
        j !-> [1,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,1];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #12:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,1];
        j !-> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,0]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    2: [i !-> [0,99];
        j -> [0,1]]
    5: [i !-> [0,+∞];
        j !-> [0,+∞]]
    6: [i !-> [100,+∞];
        j -> [0,1]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]

    Iteration #13:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]]
    New calls:
    1: [i !-> [1,+∞];
        j !-> [0,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]]

    Iteration #14:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,1]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]]
    New calls:
    2: [i !-> [0,99];
        j -> [0,+∞]]
    6: [i !-> [100,+∞];
        j -> [0,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]]

    Iteration #15:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]

    Iteration #16:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]

    Iteration #17:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,0]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]

    Iteration #18:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]

    Iteration #19:
    Initial graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,1]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]

    Number of iterations: 20
    Number of calls: 116
    Number of iterations per point:
    0: 4
    1: 6
    2: 6
    3: 7
    4: 5
    5: 6
    6: 6
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 6
    5: 4
    6: 5
    Widening points:
    1,
    3

    Full graph:
    0 ->
      [[]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    1 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    2 ->
      [[i !-> [0,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    3 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    4 ->
      [[i !-> [1,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    5 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]];
    6 ->
      [[i !-> [100,+∞];
        j -> [0,+∞]]
       ====>
       [i !-> [100,+∞];
        j -> [0,+∞]]]
    Final MFG:
    [[]
     ====>
     [i !-> [100,+∞];
      j -> [0,+∞]]]

    Final result:
    [i !-> [100,+∞];
     j -> [0,+∞]]

    Outputs:
    6 ->
      [i !-> [100,+∞];
       j !-> [0,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "i"; "j" ]) ];
        exits = [ (4, [ "i"; "j" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsLt (R "i", V 10)),
              Some [ (Some true, Left 3); (Some false, Left 1) ] );
            (* 1 *) (Op (Add, "i", [ R "i"; V 1 ]), None);
            (* 2 *)
            ( Cond (IsLt (R "j", V 10)),
              Some [ (Some false, Left 4); (Some true, Left 3) ] );
            (* 3 *) (Op (Add, "j", [ R "j"; V 1 ]), Some [ (None, Left 2) ]);
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg
    [
      ( 0,
        [
          ("i", Interval.make (IntBar.Int 0) (IntBar.Int 5));
          ("j", Interval.make (IntBar.Int 0) (IntBar.Int 5));
        ] );
    ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥]
    New calls:
    3: [i !-> [0,5];
        j -> [0,5]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,5]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,5]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,5];
        j !-> [1,6]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,6]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,5]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,6]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,5]]
       ====>
       ⊥]
    New calls:
    3: [i !-> [0,5];
        j !-> [1,6]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,6]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,6]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,6]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,6]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,5];
        j !-> [1,7]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,7]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,6]]
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,7]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,6]]
       ====>
       ⊥]
    New calls:
    3: [i !-> [0,5];
        j !-> [1,7]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,7]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,7]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥]
    New calls:
    2: [i !-> [0,5];
        j !-> [1,+∞]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥]
    New calls:
    4: [i !-> [0,5];
        j !-> [10,+∞]]

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]

    Iteration #9:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       ⊥];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]
    New calls:

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]

    Iteration #10:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]
    New calls:

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]

    Iteration #11:
    Initial graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       ⊥];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]
    New calls:

    New graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]

    Number of iterations: 12
    Number of calls: 38
    Number of iterations per point:
    0: 2
    2: 4
    3: 4
    4: 2
    Node numbering:
    0: 0
    2: 2
    3: 1
    4: 3
    Widening points:
    3

    Full graph:
    0 ->
      [[i -> [0,5];
        j -> [0,5]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 ->
      [[i !-> [0,5];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    3 ->
      [[i !-> [0,5];
        j -> [0,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    4 ->
      [[i !-> [0,5];
        j !-> [10,+∞]]
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]
    Final MFG:
    [[i -> [0,5];
      j -> [0,5]]
     ====>
     [i !-> [0,5];
      j !-> [10,+∞]]]

    Final result:
    [i !-> [0,5];
     j !-> [10,+∞]]

    Outputs:
    4 ->
      [i !-> [0,5];
       j !-> [10,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (7, [ "x"; "y" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "x", [ V 0 ]), None);
            (* 1 *) (Op (Read, "y", [ V 0 ]), None);
            (* 2 *)
            ( Cond (IsLe (R "x", V 99)),
              Some [ (Some true, Left 3); (Some false, Left 7) ] );
            (* 3 *) (Op (Add, "x", [ R "x"; V 1 ]), None);
            (* 4 *)
            ( Cond (IsLe (R "x", V 49)),
              Some [ (Some true, Left 5); (Some false, Left 6) ] );
            (* 5 *) (Op (Add, "y", [ R "y"; V 1 ]), Some [ (None, Left 2) ]);
            (* 6 *) (Op (Sub, "y", [ R "y"; V 1 ]), Some [ (None, Left 2) ]);
            (* 7 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [x !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [0,0];
        y !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    3: [x !-> [0,0];
        y !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    4: [x !-> [1,1];
        y !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [x !-> [1,1];
        y !-> [0,0]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [1,1];
        y !-> [1,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    3: [x !-> [0,1];
        y !-> [0,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    4: [x !-> [1,2];
        y !-> [0,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,1];
        y !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [x !-> [1,2];
        y !-> [0,1]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥]

    Iteration #10:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [1,2];
        y !-> [1,2]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥]

    Iteration #11:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,1];
        y !-> [0,1]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥]
    New calls:
    3: [x !-> [0,99];
        y !-> [0,+∞]]
    7: [x !-> [100,+∞];
        y !-> [0,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥]

    Iteration #12:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥]
    New calls:
    4: [x !-> [1,+∞];
        y !-> [0,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]

    Iteration #13:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,2];
        y !-> [0,1]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]
    New calls:
    5: [x !-> [1,49];
        y !-> [0,+∞]]
    6: [x !-> [50,+∞];
        y !-> [0,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]

    Iteration #14:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]
    New calls:
    2: [x !-> [50,+∞];
        y !-> [-1,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]

    Iteration #15:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]
    New calls:
    3: [x !-> [0,99];
        y !-> [-1,+∞]]
    7: [x !-> [100,+∞];
        y !-> [-1,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]

    Iteration #16:
    Initial graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]]
    New calls:
    4: [x !-> [1,+∞];
        y !-> [-∞,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #17:
    Initial graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [0,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:
    5: [x !-> [1,49];
        y !-> [-∞,+∞]]
    6: [x !-> [50,+∞];
        y !-> [-∞,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #18:
    Initial graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-1,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:
    2: [x !-> [1,+∞];
        y !-> [-∞,+∞]]

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #19:
    Initial graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #20:
    Initial graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [0,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Number of iterations: 21
    Number of calls: 129
    Number of iterations per point:
    0: 3
    1: 3
    2: 6
    3: 5
    4: 6
    5: 5
    6: 4
    7: 4
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 4
    5: 5
    6: 7
    7: 6
    Widening points:
    2

    Full graph:
    0 ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    4 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    5 ->
      [[x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    6 ->
      [[x !-> [50,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    7 ->
      [[x !-> [100,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    Final MFG:
    [[]
     ====>
     [x !-> [100,+∞];
      y !-> [-∞,+∞]]]

    Final result:
    [x !-> [100,+∞];
     y !-> [-∞,+∞]]

    Outputs:
    7 ->
      [x !-> [100,+∞];
       y !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsZero "x"),
              Some [ (Some true, Left 1); (Some false, Left 2) ] );
            (* 1 *) (Nop, Some [ (None, Left 5) ]);
            (* 2 *) (Nop, None);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
            (* XXX 5 is incorrectly marked as a widening point *)
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    1: [x !-> [0,0]]
    2: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    3: [x !-> [-∞,+∞]]
    5: [x !-> [0,0]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    4: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]

    Iteration #4:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]]
    New calls:
    5: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #8:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 9
    Number of calls: 45
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    4: 3
    5: 3
    Node numbering:
    0: 0
    1: 1
    2: 2
    3: 3
    4: 5
    5: 4
    Widening points:
    5

    Full graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsZero "x"),
              Some [ (Some true, Left 3); (Some false, Left 1) ] );
            (* 1 *) (Nop, None);
            (* 2 *) (Nop, Some [ (None, Left 5) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    1: [x !-> [-∞,+∞]]
    3: [x !-> [0,0]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [-∞,+∞]]
    4: [x !-> [0,0]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 8
    Number of calls: 39
    Number of iterations per point:
    0: 2
    1: 2
    2: 2
    3: 2
    4: 2
    5: 2
    Node numbering:
    0: 0
    1: 1
    2: 3
    3: 2
    4: 4
    5: 5
    Widening points:


    Full graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]]
 |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (3, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Nop, Some [ (None, Left 5) ]);
            (* 1 *) (Nop, None);
            (* 2 *) (Nop, Some [ (None, Left 5) ]);
            (* 3 *)
            ( Cond (IsZero "x"),
              Some [ (Some true, Left 0); (Some false, Left 1) ] );
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (3, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    0: [x !-> [0,0]]
    1: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [-∞,+∞]]
    5: [x !-> [0,0]]

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [0,0]]
       ====>
       ⊥]
    New calls:
    5: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]]

    Iteration #4:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [0,0]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 7
    Number of calls: 29
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    5: 3
    Node numbering:
    0: 1
    1: 2
    2: 3
    3: 0
    5: 4
    Widening points:


    Full graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (3, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Nop, None);
            (* 1 *) (Nop, Some [ (None, Left 5) ]);
            (* 2 *) (Nop, Some [ (None, Left 5) ]);
            (* 3 *)
            ( Cond (IsZero "x"),
              Some [ (Some true, Left 0); (Some false, Left 2) ] );
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (3, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    0: [x !-> [0,0]]
    2: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    1: [x !-> [0,0]]
    5: [x !-> [-∞,+∞]]

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #4:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       ⊥];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       ⊥];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 6
    Number of calls: 24
    Number of iterations per point:
    0: 2
    1: 2
    2: 2
    3: 2
    5: 2
    Node numbering:
    0: 1
    1: 3
    2: 2
    3: 0
    5: 4
    Widening points:


    Full graph:
    0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (5, [ "y" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "x", [ V 3 ]), None);
            (* 1 *) (Nop, Some [ (None, Right "x") ]);
            (* 2 *) (Op (Read, "y", [ V 1 ]), Some [ (None, Left 5) ]);
            (* 3 *) (Op (Read, "y", [ V 2 ]), Some [ (None, Left 5) ]);
            (* 4 *) (Op (Read, "y", [ V 3 ]), Some [ (None, Left 5) ]);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (3, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥]
    New calls:
    1: [x !-> [3,3]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥]
    New calls:
    3: [x !-> [3,3]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥]
    New calls:
    5: [x !-> [3,3];
        y !-> [2,2]]

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       ⊥]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[]
       ====>
       ⊥];
    1 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]
    New calls:

    New graph:
    0 ->
      [[]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    1 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]

    Number of iterations: 8
    Number of calls: 26
    Number of iterations per point:
    0: 2
    1: 2
    3: 2
    5: 2
    Node numbering:
    0: 0
    1: 1
    3: 2
    5: 3
    Widening points:


    Full graph:
    0 ->
      [[]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    1 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]];
    5 ->
      [[x !-> [3,3];
        y !-> [2,2]]
       ====>
       [x !-> [3,3];
        y !-> [2,2]]]
    Final MFG:
    [[]
     ====>
     [x !-> [3,3];
      y !-> [2,2]]]

    Final result:
    [x !-> [3,3];
     y !-> [2,2]]

    Outputs:
    5 ->
      [y !-> [2,2]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (6, [ "y" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsLt (R "x", V 6)),
              Some [ (Some true, Left 1); (Some false, Left 6) ] );
            (* 1 *)
            ( Cond (IsLe (V 3, R "x")),
              Some [ (Some true, Left 2); (Some false, Left 6) ] );
            (* 2 *) (Nop, Some [ (None, Right "x") ]);
            (* 3 *) (Op (Read, "y", [ V 1 ]), Some [ (None, Left 6) ]);
            (* 4 *) (Op (Read, "y", [ V 2 ]), Some [ (None, Left 6) ]);
            (* 5 *) (Op (Read, "y", [ V 3 ]), Some [ (None, Left 6) ]);
            (* 6 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    1: [x !-> [-∞,5]]
    6: [x !-> [6,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [6,+∞]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [6,+∞]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [3,5]]
    6: [x !-> [-∞,2]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]]

    Iteration #3:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]]
    New calls:
    3: [x !-> [3,3]]
    4: [x !-> [4,4]]
    5: [x !-> [5,5]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [6,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    4 ->
      [[x !-> [4,4]]
       ====>
       ⊥];
    5 ->
      [[x !-> [5,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #4:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [6,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    4 ->
      [[x !-> [4,4]]
       ====>
       ⊥];
    5 ->
      [[x !-> [5,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:
    6: [x !-> [3,5];
        y !-> [1,3]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]

    Number of iterations: 8
    Number of calls: 43
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    4: 3
    5: 3
    6: 3
    Node numbering:
    0: 0
    1: 1
    2: 3
    3: 4
    4: 5
    5: 6
    6: 2
    Widening points:
    6

    Full graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [1,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [1,3]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞];
      y -> [1,3]]]

    Final result:
    [x !-> [-∞,+∞];
     y -> [1,3]]

    Outputs:
    6 ->
      [y !-> [1,3]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (6, [ "y" ]) ];
        code =
          [|
            (* 0 *)
            ( Cond (IsLt (R "x", V 6)),
              Some [ (Some true, Left 1); (Some false, Left 6) ] );
            (* 1 *)
            ( Cond (IsLe (V 3, R "x")),
              Some [ (Some true, Left 2); (Some false, Left 6) ] );
            (* 2 *) (Nop, Some [ (None, Right "x") ]);
            (* 3 *) (Op (Add, "y", [ R "x"; V 3 ]), Some [ (None, Left 6) ]);
            (* 4 *) (Op (Add, "y", [ R "x"; V 2 ]), Some [ (None, Left 6) ]);
            (* 5 *) (Op (Add, "y", [ R "x"; V 1 ]), Some [ (None, Left 6) ]);
            (* 6 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥]
    New calls:
    1: [x !-> [-∞,5]]
    6: [x !-> [6,+∞]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [6,+∞]]
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [6,+∞]]
       ====>
       ⊥]
    New calls:
    2: [x !-> [3,5]]
    6: [x !-> [-∞,2]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]]

    Iteration #3:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       ⊥];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       ⊥];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]]
    New calls:
    3: [x !-> [3,3]]
    4: [x !-> [4,4]]
    5: [x !-> [5,5]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [6,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    4 ->
      [[x !-> [4,4]]
       ====>
       ⊥];
    5 ->
      [[x !-> [5,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #4:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [6,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [6,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       ⊥];
    4 ->
      [[x !-> [4,4]]
       ====>
       ⊥];
    5 ->
      [[x !-> [5,5]]
       ====>
       ⊥];
    6 ->
      [[x !-> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:
    6: [x !-> [3,5];
        y !-> [6,6]]

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #5:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       ⊥];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]

    Iteration #6:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]

    Iteration #7:
    Initial graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]
    New calls:

    New graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]

    Number of iterations: 8
    Number of calls: 43
    Number of iterations per point:
    0: 3
    1: 3
    2: 3
    3: 3
    4: 3
    5: 3
    6: 3
    Node numbering:
    0: 0
    1: 1
    2: 3
    3: 4
    4: 5
    5: 6
    6: 2
    Widening points:
    6

    Full graph:
    0 ->
      [[x -> [-∞,+∞]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    1 ->
      [[x !-> [-∞,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    2 ->
      [[x !-> [3,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    3 ->
      [[x !-> [3,3]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    4 ->
      [[x !-> [4,4]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    5 ->
      [[x !-> [5,5]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]];
    6 ->
      [[x !-> [-∞,+∞];
        y -> [6,6]]
       ====>
       [x !-> [-∞,+∞];
        y -> [6,6]]]
    Final MFG:
    [[x -> [-∞,+∞]]
     ====>
     [x !-> [-∞,+∞];
      y -> [6,6]]]

    Final result:
    [x !-> [-∞,+∞];
     y -> [6,6]]

    Outputs:
    6 ->
      [y !-> [6,6]] |}]

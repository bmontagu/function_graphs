(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for an untyped lambda-calculus extended
    with recursively defined functions, integers and booleans.

    The analyzer employs an abstract domain introduced in

    Trace-based control-flow analysis

    Montagu, B. & Jensen, T. P.

    Freund, S. N. & Yahav, E. (Eds.)

    PLDI '21: 42nd ACM SIGPLAN International Conference on Programming Language
    Design and Implementation, Virtual Event, Canada, June 20-25, 20211, ACM,
    2021, 482-496

    The analyzer is implemented using a fixpoint solver. *)

module Var : sig
  type t = Test_common.While.var
end

type prim

module Expr : sig
  type t
end

module Lam : sig
  type t = Clos of Var.t * Expr.t | RecClos of Var.t * Var.t * Expr.t
end

module Lambda (Env : sig
  type t
  type value

  val get : t -> Var.t -> value
  val add : t -> Var.t -> value -> t
  val pp : Format.formatter -> t -> unit
end) (V : sig
  type t = Env.value

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val singleton_bool : bool -> t
  val singleton_closure : Env.t -> Var.t -> Expr.t -> t
  val singleton_rec_closure : Var.t -> Env.t -> Var.t -> Expr.t -> t
  val closures_map_join : (Env.t -> Lam.t -> t) -> t -> t
  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include Function_graphs.Sigs.MFG with type dom = Env.t and type codom = V.t

  val iter : (dom -> codom -> unit) -> t -> unit
end) (_ : sig
  val call_prim : prim -> V.t list -> V.t
  val cond : Expr.t -> bool -> Env.t -> Env.t
end) : sig
  val analyze : MFG.strategy array -> Expr.t -> Env.t -> V.t
  val run : MFG.strategy array -> Expr.t -> Env.t -> unit
end

module V : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Env : sig
  type t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module MFG : Function_graphs.Sigs.MFG with type dom = Env.t and type codom = V.t

module Analyzer : sig
  val analyze : MFG.strategy array -> Expr.t -> Env.t -> V.t
  val run : MFG.strategy array -> Expr.t -> Env.t -> unit
end

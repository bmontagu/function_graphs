(** @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2024
*)

(** Abstract Domain of Constants: Given an ordered type of GADT keys over the
    Abstract Domains of constant values, build an Abstract Domain of all
    constants value using a dependent map. *)

module type CONSTANTS = sig
  include Dmap.DORDERED

  (** Generic list of type [_ t] *)
  type tl = [] | ( :: ) : ('a t * 'a) * tl -> tl

  val tops : tl

  val get_module : 'a t -> (module Sigs.LATTICE with type t = 'a)
  (** [get_module k] returns the module corresponding to the abstract domain of
      key [k] *)

  val pp : Format.formatter -> 'a t -> unit
end

module type S = sig
  type 'a key

  include Sigs.LATTICE

  val find_opt : 'a key -> t -> 'a option
  (** [find_opt k c] returns [Some v] if the abstract value of constants [c]
      contains an abstract value [v] corresponding to the constant domain of key
      [k], otherwise returns [None]. *)

  val make : 'a key -> 'a -> t
  (** [make k v] returns an abstract value of constants containing the single
      abstract value [v] corresponding to the contant domain of key [k]. *)

  val only_has_key : 'a key -> t -> bool
  (** [only_has_key k c] returns [true] if the abstract value of constants [c]
      has exactly one element in its domain: the value [k]. Otherwise, returns
      [false]. *)

  val get : 'a key -> t -> 'a * t
  (** [get k c] returns [(v, c')] where [v] is the abstract value corresponding
      to the constant domain of key [k], and [c'] is the remainder abstract
      value (i.e., the value [c] in which [(k,v)] was removed). *)

  val get_val : 'a. 'a key -> ('a -> 'a * 'a) -> t -> 'a * t
  (** [get_val k getter c] returns [(v', c')] where [v'] is the abstract value
      obtained from [getter v], with [v] the abstract value corresponding to the
      constant domain of key [k], and [c'] is the remainder.

      [(v, rc)] = [get k c] [(v', rv)] = [getter v] [c'] = union of [rc] with
      [make k rv] *)
end

module Make (Constants : CONSTANTS) : S with type 'a key = 'a Constants.t

(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type S = sig
  val printf : ('a, Format.formatter, unit) format -> 'a

  val kprintf :
    (Format.formatter -> 'a) -> ('b, Format.formatter, unit, 'a) format4 -> 'b

  val eprintf : ('a, Format.formatter, unit) format -> 'a

  val keprintf :
    (Format.formatter -> 'a) -> ('b, Format.formatter, unit, 'a) format4 -> 'b
end

(** Usual, printf-like logging functions: to [stdout] and [stderr] *)
module Std : S = struct
  let printf = Format.printf
  let kprintf k fmt = Format.kfprintf k Format.std_formatter fmt
  let eprintf = Format.eprintf
  let keprintf k fmt = Format.kfprintf k Format.err_formatter fmt
end

(** Printf-like logging functions: only to [stdout]; [stderr] is ignored *)
module StdNoErr : S = struct
  let printf = Format.printf
  let kprintf k fmt = Format.kfprintf k Format.std_formatter fmt
  let eprintf fmt = Format.ifprintf Format.err_formatter fmt
  let keprintf k fmt = Format.ikfprintf k Format.err_formatter fmt
end

(** Printf-like logging functions: everything is ignored *)
module Silent : S = struct
  let printf fmt = Format.ifprintf Format.std_formatter fmt
  let kprintf k fmt = Format.ikfprintf k Format.std_formatter fmt
  let eprintf fmt = Format.ifprintf Format.err_formatter fmt
  let keprintf k fmt = Format.ikfprintf k Format.err_formatter fmt
end

(** Printf-like logging functions: functor parametrized by std and err
    formatters *)
module Make (F : sig
  val std_formatter : Format.formatter
  val err_formatter : Format.formatter
end) : S = struct
  let printf fmt = Format.fprintf F.std_formatter fmt
  let kprintf k fmt = Format.ikfprintf k F.std_formatter fmt
  let eprintf fmt = Format.fprintf F.err_formatter fmt
  let keprintf k fmt = Format.ikfprintf k F.err_formatter fmt
end

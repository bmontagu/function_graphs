type 'a hash_consed

val node : 'a hash_consed -> 'a
val tag : 'a hash_consed -> int

module Make (H : Hashtbl.HashedType) () : sig
  type t

  val create : int -> t
  val hashcons : t -> H.t -> H.t hash_consed
end

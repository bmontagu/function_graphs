(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** Unification engine, based on a Union-Find data structure. *)

module M = Ptmap

type var = int

type 'a desc = {
  mutable parent : var;
  mutable rank : int;
  mutable term : 'a option (* only representants may have Some _ *);
}

type 'a t = 'a desc M.t ref

let init () : 'a t = ref M.empty

let register x (uf : 'a t) : unit =
  uf :=
    M.update x
      (function
        | None ->
            (* x is the representant of its own class *)
            Some { parent = x; rank = 0; term = None }
        | Some _ -> assert false)
      !uf

let rec find (x : var) (uf : 'a t) : var =
  let dx = M.find x !uf in
  if dx.parent <> x (* x is not the representant of the class *) then (
    let p = find dx.parent uf in
    dx.parent <- p;
    if p <> x then dx.term <- None (* only non-representants may have Some _ *));
  dx.parent

let equivalent x1 x2 (uf : 'a t) : bool = find x1 uf = find x2 uf

let union_distinct x y (uf : 'a t) : unit =
  let x = find x uf and y = find y uf in
  assert (x <> y);
  let dx = M.find x !uf and dy = M.find y !uf in
  if dx.rank < dy.rank then (
    dx.parent <- y;
    dx.term <- None
    (* because x <> y (only non-representants may have Some _) *))
  else (
    dy.parent <- x;
    if dx.rank = dy.rank (* update rank if necessary *) then
      dx.rank <- dx.rank + 1;
    dy.term <- None
    (* because x <> y (only non-representants may have Some _) *))

let union x y uf =
  if x = y then () (* x and y are in the same equivalence class already *)
  else union_distinct x y uf

let get_term x (uf : 'a t) : 'a option = (M.find (find x uf) !uf).term

let set_term x (t : 'a) (uf : 'a t) : unit =
  (M.find (find x uf) !uf).term <- Some t

let rec unify_vars local_solve x1 x2 (uf : 'a t) : unit =
  if equivalent x1 x2 uf then ()
  else
    let ot1 = get_term x1 uf and ot2 = get_term x2 uf in
    union_distinct x1 x2 uf;
    match (ot1, ot2) with
    | None, None -> ()
    | Some t, None | None, Some t ->
        (* setting x2 is equivalent *)
        set_term x1 t uf
    | Some t1, Some t2 ->
        (* using x1 or x2 is equivalent *)
        unify_terms local_solve x1 t1 t2 uf

and unify_terms local_solve x (t1 : 'a) (t2 : 'a) (uf : 'a t) : unit =
  let t12, eqs = local_solve t1 t2 in
  set_term x t12 uf;
  List.iter (fun (x1, x2) -> unify_vars local_solve x1 x2 uf) eqs

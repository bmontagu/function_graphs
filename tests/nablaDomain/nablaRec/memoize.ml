(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2024
*)

type (-'a, +'b) memo_fun = 'a -> 'b

let apply : ('a, 'b) memo_fun -> 'a -> 'b = Fun.id

let memo_int f =
  let m = ref Ptmap.empty in
  fun x ->
    match Ptmap.find x !m with
    | v -> v
    | exception Not_found ->
        let v = f x in
        m := Ptmap.add x v !m;
        v

module type HASHEDTYPE = sig
  type raw_t

  include Hashtbl.HashedType with type t = raw_t Hcons.hash_consed

  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
end

module Make (T : HASHEDTYPE) = struct
  (** Memoization helpers *)

  let memo_int = memo_int

  module HT = Hashtbl.Make (T)

  let memo f =
    let h = HT.create 127 in
    fun x ->
      match HT.find h x with
      | v -> v
      | exception Not_found ->
          let v = f x in
          HT.add h x v;
          v

  let memo_rec func =
    let h = HT.create 127 in
    let rec f x =
      match HT.find h x with
      | v -> v
      | exception Not_found ->
          let v = func f x in
          HT.add h x v;
          v
    in
    f

  module T2 = struct
    type t = T.t * T.t

    let equal (t1, t2) (t3, t4) = T.equal t1 t3 && T.equal t2 t4

    let hash_fold s (t1, t2) =
      Base.Hash.fold_int (Base.Hash.fold_int s (Hcons.tag t2)) (Hcons.tag t1)

    let hash t = Base.Hash.of_fold hash_fold t
  end

  module HT2 = Hashtbl.Make (T2)

  let memo2 f =
    let h = HT2.create 127 in
    fun x1 x2 ->
      let p = (x1, x2) in
      match HT2.find h p with
      | v -> v
      | exception Not_found ->
          let v = f x1 x2 in
          HT2.add h p v;
          v

  let memo2_rec func =
    let h = HT2.create 127 in
    let rec f x1 x2 =
      let p = (x1, x2) in
      match HT2.find h p with
      | v -> v
      | exception Not_found ->
          let v = func f x1 x2 in
          HT2.add h p v;
          v
    in
    f

  module IntT = struct
    type t = int * T.t

    let equal (n1, t1) (n2, t2) = Int.equal n1 n2 && T.equal t1 t2

    let hash_fold s (n, t) =
      Base.Hash.fold_int (Base.Hash.fold_int s (Hcons.tag t)) n

    let hash t = Base.Hash.of_fold hash_fold t
  end

  module HIntT = Hashtbl.Make (IntT)

  let memo_int_t f =
    let h = HIntT.create 127 in
    fun i x ->
      let p = (i, x) in
      match HIntT.find h p with
      | v -> v
      | exception Not_found ->
          let v = f i x in
          HIntT.add h p v;
          v

  module SIntT = Hashset.Make (IntT)

  let memo_rec_int_t_unit func =
    let s = SIntT.create 127 in
    let rec f i x =
      let p = (i, x) in
      if SIntT.mem s p then ()
      else (
        func f i x;
        SIntT.add s p)
    in
    f
end

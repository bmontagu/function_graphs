(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2020-2024
*)

(** Interface of maps that are necessary to implement the NablaRec abstract
    domain *)
module type HashableMap = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val find_opt : key -> 'a t -> 'a option
  val remove : key -> 'a t -> 'a t
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
  val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val bindings : 'a t -> (key * 'a) list

  val hash_fold :
    (Base.Hash.state -> 'a -> Base.Hash.state) ->
    Base.Hash.state ->
    'a t ->
    Base.Hash.state

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

(** Pure implementation of Abstract Values used by the NablaRec analyzer. In
    order to be memoized, recursive function must be defined in an open
    recursive style. Moreover, functions that might call memoized internal
    functions may provide these internal functions as parameters. This module
    does not implement the operations on the abstract domain (inclusion, union,
    intersection, widening, etc.). *)
module Make
    (Constants : AbstractDomains.Constants.S)
    (MFun : HashableMap)
    (MConstruct : HashableMap)
    (MOPP : HashableMap)
    (Env : HashableMap)
    (MField : HashableMap)
    () : sig
  type t = raw_t Hcons.hash_consed
  (** The type definition for abstract values. The encoding follows the
      locally-nameless style of binders. *)

  and raw_t =
    | Abs of {
        height : int;
        constants : Constants.t;
        closures : closures;
        constructs : constructs;
        structures : structures;
      }
    | Mu of int (* height *) * t  (** recursive knot *)
    | Var of int  (** free variable, where the provided integer is a name *)
    | Idx of int
        (** bound variable, where the provided integer is a de Bruijn index *)

  and closures = Clos of int (* height *) * t Env.t MFun.t | ClosTop

  and constructs =
    | Constructs of int (* height *) * t list MConstruct.t
    | ConstructsTop

  and structures =
    | Struct of int (* height *) * t MField.t MOPP.t
      (* the map contains the entries that _must_ be present *)
    | StructTop

  include Memoize.HASHEDTYPE with type raw_t := raw_t and type t := t

  val compare : t -> t -> int
  val build : raw_t -> t
  val height : t -> int
  val clos_is_bot : closures -> bool
  val constructs_is_bot : constructs -> bool
  val struct_is_bot : structures -> bool
  val is_bot : t -> bool
  val is_top : t -> bool
  val is_constant : _ Constants.key -> t -> bool

  val get_constant :
    (t -> t -> t) -> 'a Constants.key -> ('a -> 'a * 'a) -> t -> 'a * t
  (** [get_constant open0 k getter v] where [open0] is a substitution of the
      variable of de Bruijn index 0 (cf. [open_rec] below), returns [(c', v')]
      where [c'] is the abstract value obtained from [getter c], with [c] the
      abstract value corresponding to the constant domain of key [k], and [v']
      is the remainder. *)

  val get_constants : (t -> t -> t) -> 'a Constants.key -> t -> 'a * t
  (** [get_constants open0 k v] where [open0] is a substitution of the variable
      of de Bruijn index 0 (cf. [open_rec] below), returns [(c, v')] where [c]
      is the abstract value corresponding to the constant domain of key [k], and
      [v'] is the remainder. *)

  val clos : t Env.t MFun.t -> closures

  val get_closures :
    (t -> t -> t) -> t -> ((MFun.key * t Env.t) list option * t) option

  val closures_join_map :
    (t -> t -> t) ->
    'a ->
    ('a -> 'a -> 'a) ->
    'a ->
    (MFun.key -> t Env.t -> 'a) ->
    t ->
    'a

  val constructs : t list MConstruct.t -> constructs

  val get_construct :
    (t -> t -> t) -> bool -> MConstruct.key -> t -> (t list option * t) option
  (** [get_construct open0 is_singleton c v] where [open0] is a substitution of
      the variable of de Bruijn index 0 (cf. [open_rec] below), returns
      [Some (tl_opt, v')], when [v] contains a value headed by the constructor
      [c], where [tl_opt] is [None] if [v] contains [ClosTop], else [Some tl]
      with [tl] the abstract values of the respective sub-terms of the values
      headed by [c], and [v'] is the remainder. If [v] contains no value headed
      by [c], returns [None]. *)

  val struct_ : t MField.t MOPP.t -> structures

  val get_structures :
    (t -> t -> t) -> t -> ((MOPP.key * t MField.t) list option * t) option
  (** [get_structures open0 k v] where [open0] is a substitution of the variable
      of de Bruijn index 0 (cf. [open_rec] below), returns [Some (s_opt, v')]
      when [v] contains a structure value, where [s_opt] is [None] if [v]
      contains [StructTop], else [Some sl] with [sl] the list of structure
      bindings, and [v'] the remainder. If [v] contains no structure value,
      returns [None]. *)

  val update_structures :
    (t -> t -> t) -> (t MField.t -> t MField.t option) -> t -> t

  val make :
    constants:Constants.t ->
    closures:closures ->
    constructs:constructs ->
    structures:structures ->
    t

  val clos_bot : closures
  val clos_top : closures
  val constructs_bot : constructs
  val constructs_top : constructs
  val struct_bot : structures
  val struct_top : structures
  val bot : t
  val top : t
  val pp : Format.formatter -> t -> unit

  val is_free_var : int -> t -> bool
  (** [is_free_var x t] tells whether [x] is a free variable in [t] *)

  val check_wf : exn -> exn option -> (int -> t -> unit) -> int -> t -> unit
  (** [check_wf bvar fvar_opt check n t] where [check] is a recursive call to
      [check_wf bvar fvar_opt] checks whether [t] is locally-closed at level [n]
      (i.e. locally-closed for [n = 0]), and raises exception [bvar] otherwise.
      Additionally, it checks whether [t] has no free variable when
      [fvar_opt = Some fvar], and raises exception [fvar] otherwise. *)

  val close : int -> int -> t -> t * bool
  (** [close n x t] turns the variable [x] into a de Bruijn binder in [t]
      considered at depth [n], and returns [true] iff [x] was found. This
      function is only used by the function [mu] defined hereafter. *)

  val mu : bool -> (int -> t -> t * bool) -> int -> t -> t
  (** [mu check close x t] where [close] is a call to the previously defined
      [close 0] function, creates a recursive abstract value, by binding the
      variable [x] in the abstract value [t]. If [n] is not found in [t], then
      [t] is returned. Avoids the creation of non-contractive values. *)

  val open_rec : t -> int -> t -> t
  (** [open_rec t' n t] substitutes the de Bruijn index [n] in [t] with the
      locally closed abstract value [t']. *)

  val free_idx : (t -> Ptset.t) -> t -> Ptset.t
  (** [free_idx fidx t] where [fidx] is a recursive call to [free_idx] returns
      the set of de Bruijn indices that correspond to "free variables" in [t].
  *)
end

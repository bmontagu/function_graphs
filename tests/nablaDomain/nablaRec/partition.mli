(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2024
*)

(** Partition refinement data-structure, and coarsest refinement algorithm. The
    data-structure is an imperative data-structure whose [refine] operation has
    a linear worst-case complexity in the size of the splitting set. Testing
    whether elements belong the same class takes constant time. The computation
    of the coarsest congruence (function [stabilize] follows an algorithm by
    Mycroft, with semi-linear worst-case complexity.

    The standard workflow for this library is the following:

    1. Create a partition with one class and one element in this class using
    [create].

    2. Add new elements to the classes using [add_same_class], or create new
    classes using [add_new_class], until you have gathered all the elements you
    are interested in.

    3. Optionally, from the partition you obtained, that contains all the
    elements of the universe, create the initial partition you are interested
    in, using [refine], that you may call several times.

    4. Get the coarsest congruence that is finer than the initial partition by
    calling [stabilize].

    Bibliographic references:

    J. Hopcroft, “An n log n algorithm for minimizing states in a finite
    automaton,” in Theory of Machines and Computations, Z. Kohavi and A. Paz,
    Eds., Academic Press, 1971, pp. 189–196. doi:
    https://doi.org/10.1016/B978-0-12-417750-5.50022-1.

    D. Gries, “Describing an Algorithm by Hopcroft,” Acta Informatica, vol. 2,
    no. 2, pp. 97–109, 1973, doi: 10.1007/bf00264025.

    M. Habib, C. Paul, and L. Viennoti, “A synthesis on partition refinement: A
    useful routine for strings, graphs, boolean matrices and automata,” in
    Lecture Notes in Computer Science, Springer Berlin Heidelberg, 1998, pp.
    25–38. doi: 10.1007/bfb0028546. *)

(** Elements in a partition *)
module Elt : sig
  type 'a t
  (** ['a t] is the type of elements that contain data of type ['a] *)

  val equal : 'a t -> 'a t -> bool
  (** Equality test on elements: NEVER ever use polymorphic equality! Worst-case
      complexity: O(1). *)

  val equiv : 'a t -> 'a t -> bool
  (** Whether two elements belong to the same equivalence class. Worst-case
      complexity: O(1). *)

  val repr : 'a t -> 'a t
  (** The canonical representant of an element. Worst-case complexity: O(1). *)

  val get : 'a t -> 'a
  (** Accessor for the data within an element. Worst-case complexity: O(1). *)

  val cardinal : 'a t -> int
  (** Cardinal of the equivalence class of an element. Worst-case complexity:
      O(1). *)
end

type 'a t
(** ['a t] is the type of partitions, whose elements contain some data of type
    ['a] *)

val to_list : 'a t -> 'a list list
(** Converts a partition to a list of lists *)

val create : 'a -> 'a t * 'a Elt.t
(** [create v] creates a partition with a single class, that is a singleton.
    Returns this partition, and the element it contains. The element is fresh
    from any other one, and contains [v] as data. *)

val add_same_class : 'a Elt.t -> 'a -> 'a Elt.t
(** [add_same_class elt v] inserts a new element in the same class as the given
    element [elt]. Returns a fresh element that contains [v] as data. *)

val add_new_class : 'a t -> 'a -> 'a Elt.t
(** [add_new_class part v] creates a new class that contains only [v] in the
    partition [part] *)

type 'a iterator = ('a -> unit) -> unit

val iterator : 'a t -> 'a Elt.t iterator iterator
(** Iterator on the elements of a partition, grouped by their classes *)

val refine : 'a t -> 'a Elt.t iterator -> unit
(** [refine p elt_iter], where [p] is a partition and [elt_iter] is an iterator
    of elements that belong the partition [p], refines the partition [p] with
    the set of elements [S] that is denoted by the iterator [elt_iter]. This
    operation replaces every class [C] of that partition with two sets [C ∩ S]
    and [C ∖ S] if both of them are non-empty. Otherwise, the behaviour is
    undefined. Worst-case complexity: O(|S|). *)

val stabilize :
  ('a Elt.t iterator -> 'a Elt.t iterator iterator) -> 'a t -> unit
(** [stabilize pre_images p] refines the partition [p] into the coarsest
    congruence that is finer that [p]. It supposes the existence of a partial
    function [next : lab -> 'a Elt.t -> 'a Elt.t] where [b] is a type of labels,
    that denotes a transition function (think of a finite automaton, for
    example). The argument [pre_images S] must denote the set of pre-images of
    [f], i.e.: [⋃ₗ { { e | f l e ∈ S } }] (from which the empty sets may be
    removed). As a consequence, for every [S' ∈ pre_images S], there exists a
    label [l] such that for every [e' ∈ S'], [f l e' ∈ S].

    The refined partition [p'] that is computed is a congruence, i.e.: for every
    label [l] and elements [e1] and [e2], if [e1] and [e2] belong to some class
    [C] of [p'], then [f l e1] and [f l e2] belong to some class [C'] of [p'].
    Moreover, [p'] must be the coarsest such partition, that is finer than [p],
    i.e., such that every classe of [p'] is a subset of a class of [p]. *)

val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
(** Pretty-printer for partitions *)

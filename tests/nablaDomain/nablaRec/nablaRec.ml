(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2024
*)

(** Abstract domain for closures and values, with recursive knots. *)

(* ensure that we effectively use comparison on integers, and not
   polymorphic comparisons *)
let ( = ) : int -> int -> bool = ( = )
let ( <> ) : int -> int -> bool = ( <> )
let ( <= ) : int -> int -> bool = ( <= )
let ( < ) : int -> int -> bool = ( < )
let max : int -> int -> int = Int.max
let min : int -> int -> int = Int.min
let ( <=> ) = Bool.equal

module MInts = Ptmap
(** Maps indexed by integers *)

module SInts = Ptset
(** Sets of integers *)

(** Pairs of integers *)
module Ints2 : Map.OrderedType with type t = int * int = struct
  type t = int * int [@@deriving ord]
end

module SInts2 = Set.Make (Ints2)
(** Sets of pairs of integers *)

module MInts2 = Map.Make (Ints2)
(** Maps indexed by pairs of integers *)

(** Domain of booleans where the default (bottom) value is [true] *)
module BoolDomainDefaultTrue = struct
  type t = bool

  let bot = true
  let leq b1 b2 = b1 || not b2
  let widen b1 b2 = b1 && b2
end

module type PrettyOrderedType = sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end

module type HashableMap = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val mem : key -> 'a t -> bool
  val remove : key -> 'a t -> 'a t
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
  val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val bindings : 'a t -> (key * 'a) list

  val hash_fold :
    (Base.Hash.state -> 'a -> Base.Hash.state) ->
    Base.Hash.state ->
    'a t ->
    Base.Hash.state

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module MakeHashableMap (X : sig
  include PrettyOrderedType

  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
end) : HashableMap with type key = X.t = struct
  include Map.Make (X)

  let hash_fold f acc m = fold (fun x v acc -> f (X.hash_fold acc x) v) m acc

  let pp pp_val fmt m =
    let open Format in
    fprintf fmt "[@[%a@]]"
      (pp_print_seq
         ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
         (fun fmt (x, v) -> fprintf fmt "@[@[%a@] ->@ @[%a@]@]" X.pp x pp_val v))
      (to_seq m)
end

module Field = struct
  type t = string

  let compare = String.compare
  let pp = Format.pp_print_string
  let hash_fold = Base.Hash.fold_string
end

module MField = MakeHashableMap (Field)

module Unit = struct
  type t = unit

  let compare () () = 0
  let pp _fmt () = ()
  let hash_fold acc () = acc
end

module MUnit = MakeHashableMap (Unit)

module MyInt = struct
  type t = int

  let compare = Int.compare
  let pp = Format.pp_print_int
  let hash_fold = Base.Hash.fold_int
end

module MInt = MakeHashableMap (MyInt)

module MakeRaw
    (Options : sig
      val minimize : bool
      val occurs_threshold : int
      val sim_threshold : int

      val debug_shrinking : bool
      (** Whether to print extra information about the shrinking phase *)

      val check_wf : bool
      (** Whether to perform extra checking for wellformedness *)

      val monotonic_join : bool
    end)
    (Constants : AbstractDomains.Constants.S)
    (Fun : sig
      include PrettyOrderedType

      val is_recursive : t -> bool
    end)
    (MFun : HashableMap with type key = Fun.t)
    (Construct : sig
      include PrettyOrderedType

      val is_singleton : t -> bool
      val is_constant : t -> bool
    end)
    (MConstruct : HashableMap with type key = Construct.t)
    (OPP : Map.OrderedType)
    (MOPP : HashableMap with type key = OPP.t)
    (Var : Map.OrderedType)
    (Env : HashableMap with type key = Var.t)
    (Field : PrettyOrderedType)
    (MField : HashableMap with type key = Field.t)
    () =
struct
  module T =
    NablaValue.Make (Constants) (MFun) (MConstruct) (MOPP) (Env) (MField) ()

  module M = Memoize.Make (T)
  (** Memoization helpers *)

  include T

  (** Transforms an integer into a lowercase letter, possibly with an integer
      suffix *)
  let int_to_char i = (Char.chr (Char.code 'a' + (i mod 26)), i / 26)

  let pp_int_var prefix fmt x =
    let c, n = int_to_char x in
    if n = 0 then Format.fprintf fmt "%s%c" prefix c
    else Format.fprintf fmt "%s%c%i" prefix c n

  (** [check_wf ~closed msg n t] checks whether [t] is locally-closed at level
      [n] (i.e. locally-closed for [n = 0]). Additionally, it checks wheter [t]
      has no free variable when [closed = true]. *)
  let check_wf =
    if Options.check_wf then (
      let exception FVAR in
      let exception BVAR in
      let check_closed = M.memo_rec_int_t_unit (T.check_wf BVAR (Some FVAR))
      and check_not_closed = M.memo_rec_int_t_unit (T.check_wf BVAR None) in
      fun ~closed msg n t ->
        try
          if closed then Memoize.apply check_closed n t
          else Memoize.apply check_not_closed n t
        with
        | FVAR ->
            Format.eprintf
              "ERROR in NablaRec.%s: ill-formed abstract value (free variable \
               found)@.@[%a@]@."
              msg pp t;
            assert false
        | BVAR ->
            Format.eprintf
              "ERROR in NablaRec.%s: ill-formed abstract value (bound variable \
               found)@.@[%a@]@."
              msg pp t;
            assert false)
    else fun ~closed:_ _msg _n _t -> ()

  (** [close x t] closes the variables [x] in the locally-closed term [t], i.e.
      turns the variable [x] into the 0 de Bruijn index. *)
  let close =
    Memoize.apply @@ M.memo_int_t
    @@ fun x t ->
    check_wf ~closed:false "close (arg)" 0 t;
    let res = T.close 0 x t in
    check_wf ~closed:false "close (res)" 1 (fst res);
    if Options.check_wf && is_free_var x (fst res) then
      Format.eprintf
        "ERROR in NablaRec.close (res): variable %a should not be free \
         in@.@[%a@]@."
        (pp_int_var "_") x pp t;
    res

  (** [mu x t] creates a recursive abstract value, by binding the variable [x]
      in the abstract value [t]. If [n] is not found in [t], then [t] is
      returned. Avoids the creation of non-contractive values. *)
  let mu = Memoize.apply @@ M.memo_int_t (T.mu Options.check_wf close)

  (** [open0 t' t] substitutes the de Bruijn index [0] in [t] with the locally
      closed abstract value [t']. *)
  let open0 =
    Memoize.apply @@ M.memo2
    @@ fun t' t ->
    check_wf ~closed:false "open0 (arg1)" 0 t';
    check_wf ~closed:false "open0 (arg2)" 1 t;
    let res = T.open_rec t' 0 t in
    check_wf ~closed:false "open0 (res)" 0 res;
    res

  (** [open_var x t] opens a value, by assigning the variable [x] to the 0 de
      Bruijn index of [t]. *)
  let open_var x t = open0 (build @@ Var x) t

  (** [free_idx t] returns the set of de Bruijn indices that correspond to "free
      variables" in [t] *)
  let free_idx = Memoize.apply @@ M.memo_rec T.free_idx

  (** [is_closed t] tells whether [t] has all its free variables bound, i.e., it
      has no free de Bruijn indices *)
  let is_closed t = SInts.is_empty @@ free_idx t

  let singleton_constant constants : t =
    make ~constants ~closures:clos_bot ~constructs:constructs_bot
      ~structures:struct_bot

  let get_constant k getter t = T.get_constant open0 k getter t
  let get_constants k t = T.get_constants open0 k t

  let singleton_closure code env : t =
    if Env.exists (fun _ v -> is_bot v) env then bot
    else
      make ~constants:Constants.bot
        ~closures:(clos (MFun.singleton code env))
        ~constructs:constructs_bot ~structures:struct_bot

  let get_closures = T.get_closures open0

  let closures_join_map bot join fclos_top fclos =
    T.closures_join_map open0 bot join fclos_top fclos

  let singleton_construct c l : t =
    if List.exists is_bot l then bot
    else
      make ~constants:Constants.bot ~closures:clos_bot
        ~constructs:(constructs @@ MConstruct.singleton c l)
        ~structures:struct_bot

  let get_construct c = T.get_construct open0 (Construct.is_singleton c) c

  let singleton_structure opp env : t =
    if MField.exists (fun _ v -> is_bot v) env then bot
    else
      make ~constants:Constants.bot ~closures:clos_bot
        ~constructs:constructs_bot
        ~structures:(struct_ (MOPP.singleton opp env))

  let get_structures = T.get_structures open0
  let update_structures = T.update_structures open0

  module AtomicPath = struct
    type t =
      | InClos of Fun.t * Var.t
          (** Path within a closure: which function, and which variable in the
              environment *)
      | InConstruct of Construct.t * int
          (** Path within a construct: the last interger is position in the
              tuple *)
      | InStruct of OPP.t * Field.t
          (** Path within a structure: which position in the structure *)
    [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | InClos (f, _) -> fprintf fmt "[%a]" Fun.pp f
      | InConstruct (c, j) -> fprintf fmt ".%a@@%i" Construct.pp c j
      | InStruct (_, f) -> fprintf fmt ".(%a)" Field.pp f
  end

  module Unif = struct
    type var = int

    type node = {
      constants : Constants.t;
      closures : node_closures;
      constructs : node_constructs;
      structures : node_structures;
    }

    and node_closures = ClosTop | Clos of var Env.t MFun.t
    and node_constructs = ConstructsTop | Constructs of var list MConstruct.t
    and node_structures = StructTop | Struct of var MField.t MOPP.t

    type graph = node Uf.t

    let clos_is_empty = function ClosTop -> false | Clos m -> MFun.is_empty m

    let constructs_is_empty = function
      | ConstructsTop -> false
      | Constructs m -> MConstruct.is_empty m

    let struct_is_empty = function
      | StructTop -> false
      | Struct m -> MOPP.is_empty m

    let node_is_empty ~weak { constants; closures; constructs; structures } =
      (weak || Constants.is_bot constants)
      && clos_is_empty closures
      && constructs_is_empty constructs
      && struct_is_empty structures

    let is_empty ~weak graph n =
      node_is_empty ~weak (Option.get @@ Uf.get_term n graph)

    (* a node that represents the empty set *)
    let bottom_node =
      {
        constants = Constants.bot;
        closures = Clos MFun.empty;
        constructs = Constructs MConstruct.empty;
        structures = Struct MOPP.empty;
      }

    let clos_leq_head clos1 clos2 =
      match (clos1, clos2) with
      | _, ClosTop -> true
      | ClosTop, _ -> false
      | Clos m1, Clos m2 -> MFun.for_all (fun c _ -> MFun.mem c m2) m1

    let constructs_leq_head constructs1 constructs2 =
      match (constructs1, constructs2) with
      | _, ConstructsTop -> true
      | ConstructsTop, _ -> false
      | Constructs m1, Constructs m2 ->
          MConstruct.for_all (fun c _vs1 -> MConstruct.mem c m2) m1

    let struct_leq_head struct1 struct2 =
      match (struct1, struct2) with
      | _, StructTop -> true
      | StructTop, _ -> false
      | Struct m1, Struct m2 -> MOPP.for_all (fun c _ -> MOPP.mem c m2) m1

    let node_leq_head ~weak
        {
          constants = constants1;
          closures = clos1;
          constructs = constructs1;
          structures = struct1;
        }
        {
          constants = constants2;
          closures = clos2;
          constructs = constructs2;
          structures = struct2;
        } =
      (weak || Constants.leq constants1 constants2)
      && clos_leq_head clos1 clos2
      && constructs_leq_head constructs1 constructs2
      && struct_leq_head struct1 struct2

    let leq_head ~weak graph t1 t2 =
      let node1 = Option.get @@ Uf.get_term t1 graph
      and node2 = Option.get @@ Uf.get_term t2 graph in
      node_leq_head ~weak node1 node2

    (** iterator on the intersection of two maps *)
    let env_iter2 f m1 m2 =
      Env.iter
        (fun x v1 ->
          match Env.find_opt x m2 with None -> () | Some v2 -> f x v1 v2)
        m1

    (** iterator on the intersection of two maps *)
    let mfield_iter2 f m1 m2 =
      MField.iter
        (fun x v1 ->
          match MField.find_opt x m2 with None -> () | Some v2 -> f x v1 v2)
        m1

    let join_vars ~widen (graph : graph) x1 x2 =
      let constants_join = if widen then Constants.widen else Constants.join
      and clos_join c1 c2 =
        match (c1, c2) with
        | (ClosTop as c), _ | _, (ClosTop as c) ->
            (* Format.eprintf "UNIFY TOP@."; *)
            (c, [])
        | Clos m1, Clos m2 ->
            let eqs = ref [] in
            let m12 =
              MFun.union
                (fun _f env1 env2 ->
                  (* gather the equalities between variables *)
                  env_iter2
                    (fun _x var1 var2 ->
                      if var1 <> var2 then eqs := (var1, var2) :: !eqs)
                    env1 env2;
                  Some env1)
                m1 m2
            in
            (Clos m12, !eqs)
      and constructs_join t1 t2 =
        match (t1, t2) with
        | (ConstructsTop as t), _ | _, (ConstructsTop as t) ->
            (* Format.eprintf "UNIFY TOP@."; *)
            (t, [])
        | Constructs m1, Constructs m2 ->
            let eqs = ref [] in
            let m12 =
              MConstruct.union
                (fun _i l1 l2 ->
                  (* gather the equalities between variables *)
                  List.iter2
                    (fun var1 var2 ->
                      if var1 <> var2 then eqs := (var1, var2) :: !eqs)
                    l1 l2;
                  Some l1)
                m1 m2
            in
            (Constructs m12, !eqs)
      and struct_join c1 c2 =
        match (c1, c2) with
        | (StructTop as c), _ | _, (StructTop as c) ->
            (* Format.eprintf "UNIFY TOP@."; *)
            (c, [])
        | Struct m1, Struct m2 ->
            let eqs = ref [] in
            let m12 =
              MOPP.union
                (fun _f env1 env2 ->
                  (* gather the equalities between variables *)
                  (* Recall that the entries are the fields that
                     _must_ be present. Therefore, we take the
                     intersection of the keys *)
                  let env = ref MField.empty in
                  mfield_iter2
                    (fun x var1 var2 ->
                      if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                      env := MField.add x var1 !env)
                    env1 env2;
                  Some !env)
                m1 m2
            in
            (Struct m12, !eqs)
      in
      let local_solve t1 t2 =
        match (t1, t2) with
        | ( {
              constants = constants1;
              closures = closures1;
              constructs = constructs1;
              structures = structures1;
            },
            {
              constants = constants2;
              closures = closures2;
              constructs = constructs2;
              structures = structures2;
            } ) ->
            let closures12, eqs_clos = clos_join closures1 closures2 in
            let constructs12, eqs_constructs =
              constructs_join constructs1 constructs2
            in
            let structures12, eqs_struct =
              struct_join structures1 structures2
            in
            let res =
              {
                constants = constants_join constants1 constants2;
                closures = closures12;
                constructs = constructs12;
                structures = structures12;
              }
            and eqs = eqs_struct @ eqs_constructs @ eqs_clos in
            (res, eqs)
      in
      Uf.unify_vars local_solve x1 x2 graph

    (** [union_vars graph x1 x2] adds to [graph] new nodes that represent the
        unions of the abstract values pointed by [x1] and [x2]. It works in a
        way that is similar to building the product of the automata starting at
        [x1] and [x2], but keeping more edges. *)
    let union_vars ~widen r (graph : graph) x1 x2 =
      let constants_join = if widen then Constants.widen else Constants.join in
      let get_var =
        let cache = ref MInts2.empty in
        fun x1 x2 ->
          let p = (x1, x2) in
          try MInts2.find p !cache
          with Not_found ->
            let n = !r in
            incr r;
            cache := MInts2.add p n !cache;
            Uf.register n graph;
            n
      in
      let clos_union c1 c2 =
        match (c1, c2) with
        | (ClosTop as c), _ | _, (ClosTop as c) -> (c, [])
        | Clos m1, Clos m2 ->
            let eqs = ref [] in
            let m12 =
              MFun.union
                (fun _f env1 env2 ->
                  (* gather the equalities between variables *)
                  let env12 =
                    Env.union
                      (fun _x var1 var2 ->
                        if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                        Some (get_var var1 var2))
                      env1 env2
                  in
                  Some env12)
                m1 m2
            in
            (Clos m12, !eqs)
      and constructs_union t1 t2 =
        match (t1, t2) with
        | (ConstructsTop as t), _ | _, (ConstructsTop as t) -> (t, [])
        | Constructs m1, Constructs m2 ->
            let eqs = ref [] in
            let m12 =
              MConstruct.union
                (fun _c l1 l2 ->
                  (* gather the equalities between variables *)
                  let l12 =
                    List.map2
                      (fun var1 var2 ->
                        if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                        get_var var1 var2)
                      l1 l2
                  in
                  Some l12)
                m1 m2
            in
            (Constructs m12, !eqs)
      and struct_union c1 c2 =
        match (c1, c2) with
        | (StructTop as c), _ | _, (StructTop as c) -> (c, [])
        | Struct m1, Struct m2 ->
            let eqs = ref [] in
            let m12 =
              MOPP.union
                (fun _f env1 env2 ->
                  (* gather the equalities between variables *)
                  let env12 =
                    (* Recall XXX that the entries are the fields that
                       _must_ be present. Therefore, we take the
                       union of the keys *)
                    MField.merge
                      (fun _x ovar1 ovar2 ->
                        match (ovar1, ovar2) with
                        | None, _ | _, None -> None
                        | Some var1, Some var2 ->
                            if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                            Some (get_var var1 var2))
                      env1 env2
                  in
                  Some env12)
                m1 m2
            in
            (Struct m12, !eqs)
      in
      let local_solve x t1 t2 =
        match (t1, t2) with
        | ( {
              constants = constants1;
              closures = closures1;
              constructs = constructs1;
              structures = structures1;
            },
            {
              constants = constants2;
              closures = closures2;
              constructs = constructs2;
              structures = structures2;
            } ) ->
            let closures12, clos_eqs = clos_union closures1 closures2 in
            let constructs12, constructs_eqs =
              constructs_union constructs1 constructs2
            in
            let structures12, struct_eqs =
              struct_union structures1 structures2
            in
            let res =
              {
                constants = constants_join constants1 constants2;
                closures = closures12;
                constructs = constructs12;
                structures = structures12;
              }
            and eqs = struct_eqs @ constructs_eqs @ clos_eqs in
            Uf.set_term x res graph;
            (res, eqs)
      in
      let started = ref SInts.empty in
      let rec process todo =
        match todo with
        | [] -> ()
        | (x1, x2) :: todo -> (
            let x12 = get_var x1 x2 in
            match Uf.get_term x12 graph with
            | Some _ ->
                (* already handled *)
                process todo
            | None ->
                (* not handled yet *)
                let v1 = Option.get @@ Uf.get_term x1 graph
                and v2 = Option.get @@ Uf.get_term x2 graph in
                let res, more_todo = local_solve x12 v1 v2 in
                Uf.set_term x12 res graph;
                let todo =
                  if SInts.mem x12 !started then todo
                  else (
                    started := SInts.add x12 !started;
                    more_todo @ todo)
                in
                process todo)
      in
      process [ (x1, x2) ];
      get_var x1 x2

    (** [meet_vars graph x1 x2] adds to [graph] new nodes that represent the
        intersections of the abstract values pointed by [x1] and [x2]. It works
        by building the product of the automata starting at [x1] and [x2]. *)
    let meet_vars r (graph : graph) x1 x2 =
      let get_var =
        let cache = ref MInts2.empty in
        fun x1 x2 ->
          let p = (x1, x2) in
          try MInts2.find p !cache
          with Not_found ->
            let n = !r in
            incr r;
            cache := MInts2.add p n !cache;
            Uf.register n graph;
            n
      in
      let clos_meet c1 c2 =
        match (c1, c2) with
        | ClosTop, c | c, ClosTop -> (c, [])
        | Clos m1, Clos m2 ->
            let eqs = ref [] in
            let m12 =
              MFun.merge
                (fun _f oenv1 oenv2 ->
                  match (oenv1, oenv2) with
                  | Some env1, Some env2 ->
                      (* gather the equalities between variables *)
                      let env12 =
                        Env.union
                          (fun _x var1 var2 ->
                            if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                            Some (get_var var1 var2))
                          env1 env2
                      in
                      Some env12
                  | _, (None as none) | (None as none), _ -> none)
                m1 m2
            in
            (Clos m12, !eqs)
      and constructs_meet t1 t2 =
        match (t1, t2) with
        | ConstructsTop, t | t, ConstructsTop -> (t, [])
        | Constructs m1, Constructs m2 ->
            let eqs = ref [] in
            let m12 =
              MConstruct.merge
                (fun _c ol1 ol2 ->
                  match (ol1, ol2) with
                  | Some l1, Some l2 ->
                      (* gather the equalities between variables *)
                      let l12 =
                        List.map2
                          (fun var1 var2 ->
                            if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                            get_var var1 var2)
                          l1 l2
                      in
                      Some l12
                  | _, (None as none) | (None as none), _ -> none)
                m1 m2
            in
            (Constructs m12, !eqs)
      and struct_meet c1 c2 =
        match (c1, c2) with
        | StructTop, c | c, StructTop -> (c, [])
        | Struct m1, Struct m2 ->
            let eqs = ref [] in
            let m12 =
              MOPP.merge
                (fun _f oenv1 oenv2 ->
                  match (oenv1, oenv2) with
                  | None, _ | _, None -> None
                  | Some env1, Some env2 ->
                      (* gather the equalities between variables *)
                      let env12 =
                        (* Recall that the entries are the fields that
                           _must_ be present. Therefore, we take the
                           union of the keys *)
                        MField.union
                          (fun _x var1 var2 ->
                            if var1 <> var2 then eqs := (var1, var2) :: !eqs;
                            Some (get_var var1 var2))
                          env1 env2
                      in
                      Some env12)
                m1 m2
            in
            (Struct m12, !eqs)
      in
      let local_solve x t1 t2 =
        match (t1, t2) with
        | ( {
              constants = constants1;
              closures = closures1;
              constructs = constructs1;
              structures = structures1;
            },
            {
              constants = constants2;
              closures = closures2;
              constructs = constructs2;
              structures = structures2;
            } ) ->
            let closures12, clos_eqs = clos_meet closures1 closures2 in
            let constructs12, constructs_eqs =
              constructs_meet constructs1 constructs2
            in
            let structures12, struct_eqs =
              struct_meet structures1 structures2
            in
            let res =
              {
                constants = Constants.meet constants1 constants2;
                closures = closures12;
                constructs = constructs12;
                structures = structures12;
              }
            and eqs = struct_eqs @ constructs_eqs @ clos_eqs in
            Uf.set_term x res graph;
            (res, eqs)
      in
      let started = ref SInts.empty in
      let rec process todo =
        match todo with
        | [] -> ()
        | (x1, x2) :: todo -> (
            let x12 = get_var x1 x2 in
            match Uf.get_term x12 graph with
            | Some _ ->
                (* already handled *)
                process todo
            | None ->
                (* not handled yet *)
                let v1 = Option.get @@ Uf.get_term x1 graph
                and v2 = Option.get @@ Uf.get_term x2 graph in
                let res, more_todo = local_solve x12 v1 v2 in
                Uf.set_term x12 res graph;
                let todo =
                  if SInts.mem x12 !started then todo
                  else (
                    started := SInts.add x12 !started;
                    more_todo @ todo)
                in
                process todo)
      in
      process [ (x1, x2) ];
      get_var x1 x2

    module MemoFix =
      Memo.Fix.Priority.Make (Logs.Silent) (Int) (MInts) (BoolDomainDefaultTrue)

    (** [is_empty_raw_with_hyp hyp graph t] tests whether the node [t] in the
        graph [graph] is semantically empty, assuming the nodes in the list
        [hyp] are already semantically empty *)
    let is_empty_raw_with_hyp hyp graph t =
      let is_hyp = List.mem t hyp in
      let is_empty =
        MemoFix.solve ~debug:false ~transient:false @@ fun is_empty t ->
        let clos_is_empty = function
          | ClosTop -> false
          | Clos m ->
              MFun.for_all
                (fun f env ->
                  (* fixpoints through recursive functions are non-empty *)
                  (not @@ Fun.is_recursive f)
                  && Env.exists (fun _ v -> is_empty v) env)
                m
        and constructs_is_empty = function
          | ConstructsTop -> false
          | Constructs m ->
              MConstruct.for_all (fun _ l1 -> List.exists is_empty l1) m
        and struct_is_empty = function
          | StructTop -> false
          | Struct m ->
              MOPP.for_all
                (fun _opp env -> MField.exists (fun _ v -> is_empty v) env)
                m
        in
        is_hyp
        ||
        match Uf.get_term t graph with
        | Some { constants; closures; constructs; structures } ->
            Constants.is_bot constants && clos_is_empty closures
            && constructs_is_empty constructs
            && struct_is_empty structures
        | None -> assert false
      in
      fst @@ is_empty t

    (** [is_empty_raw graph t] tests whether the node [t] is semantically empty
        in the graph [graph] *)
    let is_empty_raw graph t = is_empty_raw_with_hyp [] graph t

    let encode ~share n (graph : graph) t =
      let cache = M.HT.create 127 in
      let fresh_var () =
        let i = !n in
        incr n;
        i
      in
      let rec encode env t : int =
        if
          share && is_closed t
          (* we share the same nodes for identical closed sub-terms *)
        then (
          match M.HT.find_opt cache t with
          | Some i -> i
          | None ->
              let var = fresh_var () in
              Uf.register var graph;
              let i = encode_ env var t in
              M.HT.add cache t i;
              i)
        else
          let var = fresh_var () in
          Uf.register var graph;
          encode_ env var t
      (* [encode_ env dst t] encodes [t] as a graph with destination
         note [dst], and environment on bound variables [env] *)
      and encode_ env dst (t : t) : int =
        match Hcons.node t with
        | Idx n -> Ralist.nth env n
        | Var _ -> assert false
        | Mu (_, t) -> encode_ (Ralist.cons dst env) dst t
        | Abs { height = _; constants; closures; constructs; structures } ->
            let t' : node =
              {
                constants;
                closures = clos_encode env closures;
                constructs = constructs_encode env constructs;
                structures = struct_encode env structures;
              }
            in
            Uf.set_term dst t' graph;
            dst
      and clos_encode env : closures -> node_closures = function
        | ClosTop -> ClosTop
        | Clos (_, m) ->
            Clos (MFun.map (fun e -> Env.map (fun t -> encode env t) e) m)
      and constructs_encode env : constructs -> node_constructs = function
        | ConstructsTop -> ConstructsTop
        | Constructs (_, m) ->
            Constructs (MConstruct.map (List.map (encode env)) m)
      and struct_encode env : structures -> node_structures = function
        | StructTop -> StructTop
        | Struct (_, m) -> Struct (MOPP.map (MField.map (encode env)) m)
      in
      encode Ralist.empty t

    let decode (graph : graph) i =
      let cache = ref MInts.empty in
      let loops = ref SInts.empty in
      let rec decode i : t =
        (* IMPORTANT: always work with representants! *)
        let i = Uf.find i graph in
        match MInts.find_opt i !cache with
        | None -> (
            (* not explored yet *)
            let old_cache = !cache in
            cache := MInts.add i None !cache;
            match Uf.get_term i graph with
            | None -> assert false (* we forgot to set a term in some node *)
            | Some { constants; closures; constructs; structures } ->
                let closures = clos_decode closures in
                let constructs = constructs_decode constructs in
                let structures = struct_decode structures in
                let is_recursive = SInts.mem i !loops in
                let res =
                  if
                    Constants.is_bot constants && clos_is_bot closures
                    && constructs_is_bot constructs
                    && struct_is_bot structures
                  then bot
                  else make ~constants ~closures ~constructs ~structures
                in
                let res =
                  if is_recursive then
                    if is_empty_raw graph i then bot else mu i res
                  else res
                in
                (* do not forget to discard the new changes in the cache
                   if [i] is a recursive variable, otherwise [i] could
                   escape its scope *)
                cache :=
                  MInts.add i (Some res)
                    (if is_recursive then old_cache else !cache);
                res)
        | Some (Some t) ->
            (* we have detected sharing *)
            t
        | Some None ->
            (* we have detected a cycle *)
            loops := SInts.add i !loops;
            (match Uf.get_term i graph with
            | Some _ -> ()
            | None -> assert false);
            build @@ Var i
      and clos_decode : node_closures -> closures = function
        | ClosTop -> ClosTop
        | Clos m -> clos @@ MFun.map (Env.map decode) m
      and constructs_decode : node_constructs -> constructs = function
        | ConstructsTop -> ConstructsTop
        | Constructs m -> constructs @@ MConstruct.map (List.map decode) m
      and struct_decode : node_structures -> structures = function
        | StructTop -> StructTop
        | Struct m -> struct_ @@ MOPP.map (MField.map decode) m
      in
      decode i

    module Minimize = struct
      module AtomicPathMap = Map.Make (AtomicPath)

      module ShallowNode : Map.OrderedType with type t = node = struct
        type t = node

        let compare_shallow_closures clos1 clos2 =
          match (clos1, clos2) with
          | ClosTop, ClosTop -> 0
          | Clos _, ClosTop -> 1
          | ClosTop, Clos _ -> -1
          | Clos m1, Clos m2 ->
              let env_is_empty _ env = Env.is_empty env in
              let m1 = MFun.filter env_is_empty m1
              and m2 = MFun.filter env_is_empty m2 in
              MFun.compare (fun _ _ -> 0) m1 m2

        let compare_shallow_constructs constructs1 constructs2 =
          match (constructs1, constructs2) with
          | ConstructsTop, ConstructsTop -> 0
          | Constructs _, ConstructsTop -> 1
          | ConstructsTop, Constructs _ -> -1
          | Constructs m1, Constructs m2 ->
              let m1 = MConstruct.filter (fun c _ -> Construct.is_constant c) m1
              and m2 =
                MConstruct.filter (fun c _ -> Construct.is_constant c) m2
              in
              MConstruct.compare (fun _ _ -> 0) m1 m2

        let compare_shallow_structures struct1 struct2 =
          match (struct1, struct2) with
          | StructTop, StructTop -> 0
          | Struct _, StructTop -> 1
          | StructTop, Struct _ -> -1
          | Struct m1, Struct m2 ->
              let env_is_empty _ env = MField.is_empty env in
              let m1 = MOPP.filter env_is_empty m1
              and m2 = MOPP.filter env_is_empty m2 in
              MOPP.compare (fun _ _ -> 0) m1 m2

        let compare
            {
              constants = constants1;
              closures = clos1;
              constructs = constructs1;
              structures = struct1;
            }
            {
              constants = constants2;
              closures = clos2;
              constructs = constructs2;
              structures = struct2;
            } =
          let i =
            if
              Constants.leq constants1 constants2
              && Constants.leq constants2 constants1
            then 0
            else Constants.compare constants1 constants2
          in
          if i <> 0 then i
          else
            let c = compare_shallow_closures clos1 clos2 in
            if c <> 0 then c
            else
              let c = compare_shallow_constructs constructs1 constructs2 in
              if c <> 0 then c else compare_shallow_structures struct1 struct2
      end

      module ShallowNodeMap = Map.Make (ShallowNode)

      type resolver = int Partition.Elt.t MInts.t
      type pre_image_map = SInts.t AtomicPathMap.t MInts.t

      (** Converts a set of integers into an iterator of elements for the
          [Partition] module *)
      let iter_class (resolver : resolver) (class_ : SInts.t) :
          int Partition.Elt.t Partition.iterator =
       fun next -> SInts.iter (fun i -> next (MInts.find i resolver)) class_

      (** Splits a partition along a split set *)
      let split_class (resolver : resolver) (s : SInts.t)
          (part : int Partition.t) : unit =
        Partition.refine part (iter_class resolver s)

      (** Computes three items:

          - a resolver, i.e., a map from node IDs to their corresponding element
            for the [Partition] module

          - a map of pre-images, that for every node ID [i] and atomic path
            [path], produces the set of node IDs [j] such that [j] goes to [i]
            with the atomic path [path]

          - a partition of the node IDs of the graph, grouped into their initial
            classes, i.e. equivalence wrt [ShallowNode.compare], that tells
            whether nodes accept the same constants. *)
      let preprocess_info graph root :
          resolver * pre_image_map * int Partition.t =
        let seen : resolver ref = ref MInts.empty
        and smp : SInts.t ShallowNodeMap.t ref = ref ShallowNodeMap.empty
        (* shallow node map, that represents the initial classes *)
        and rev_deps : pre_image_map ref = ref MInts.empty in
        let add_smp node i =
          smp :=
            ShallowNodeMap.update node
              (function
                | None -> Some (SInts.singleton i)
                | Some s -> Some (SInts.add i s))
              !smp
        and add_rev_deps parent atomic_path child =
          let child = Uf.find child graph in
          rev_deps :=
            MInts.update child
              (function
                | None ->
                    Some
                      (AtomicPathMap.singleton atomic_path
                         (SInts.singleton parent))
                | Some parents ->
                    Some
                      (AtomicPathMap.update atomic_path
                         (function
                           | None -> Some (SInts.singleton parent)
                           | Some parents -> Some (SInts.add parent parents))
                         parents))
              !rev_deps
        in
        (* register the bottom node with index -1
           /!\ this relies on the invariant that -1 has not been
           registered yet! *)
        Uf.register (-1) graph;
        Uf.set_term (-1) bottom_node graph;
        add_smp bottom_node (-1);
        let partition, bot_elt = Partition.create (-1) in
        seen := MInts.add (-1) bot_elt !seen;
        let rec browse i =
          let i = Uf.find i graph in
          if MInts.mem i !seen then ()
          else
            let i_elt = Partition.add_same_class bot_elt i in
            seen := MInts.add i i_elt !seen;
            match Uf.get_term i graph with
            | None -> assert false
            | Some ({ constants = _; closures; constructs; structures } as node)
              ->
                add_smp node i;
                clos_browse i closures;
                constructs_browse i constructs;
                struct_browse i structures
        and clos_browse parent = function
          | ClosTop -> ()
          | Clos m ->
              MFun.iter
                (fun f env ->
                  Env.iter
                    (fun x i ->
                      add_rev_deps parent (AtomicPath.InClos (f, x)) i;
                      browse i)
                    env)
                m
        and constructs_browse parent = function
          | ConstructsTop -> ()
          | Constructs m ->
              MConstruct.iter
                (fun n is ->
                  List.iteri
                    (fun j i ->
                      add_rev_deps parent (AtomicPath.InConstruct (n, j)) i;
                      browse i)
                    is)
                m
        and struct_browse parent = function
          | StructTop -> ()
          | Struct m ->
              MOPP.iter
                (fun opp env ->
                  MField.iter
                    (fun x i ->
                      add_rev_deps parent (AtomicPath.InStruct (opp, x)) i;
                      browse i)
                    env)
                m
        in
        browse root;
        let seen = !seen and rev_deps = !rev_deps in
        (* Now that we have computes the partition that contains all
           the nodes (grouped into a single class), split this class
           into smaller classes, according to the acceptance of
           constants *)
        ShallowNodeMap.iter (fun _ s -> split_class seen s partition) !smp;
        (seen, rev_deps, partition)

      let debug_minimize = false

      (** Computes the iterator of the pre-images of a set of elements, given as
          an iterator *)
      let get_pre_images (resolver : resolver) (pre_image_map : pre_image_map)
          (elts_iter : int Partition.Elt.t Partition.iterator) :
          int Partition.Elt.t Partition.iterator Partition.iterator =
        let r = ref AtomicPathMap.empty in
        elts_iter (fun elt ->
            let i = Partition.Elt.get elt in
            match MInts.find_opt i pre_image_map with
            | None -> ()
            | Some m ->
                r :=
                  AtomicPathMap.union
                    (fun _ s1 s2 -> Some (SInts.union s1 s2))
                    m !r);
        let m = !r in
        fun f -> AtomicPathMap.iter (fun _path s -> f (iter_class resolver s)) m

      (** Computes a function that computes the pre-images, and an initial
          partition, where elements are grouped into classes according to
          [ShallowNode.compare], that tells whether nodes accept the same
          constants. *)
      let init graph root =
        let resolver, rev_deps, partition = preprocess_info graph root in
        let pre_images = get_pre_images resolver rev_deps in
        (pre_images, partition)

      (** Canonizes a graph, given a partition, by unioning the variables of the
          same class *)
      let canonize_graph graph partition =
        Partition.iterator partition (fun class_iter ->
            class_iter (fun elt ->
                let elt_i = Partition.Elt.get elt
                and elt_canonized = Partition.Elt.(get @@ repr elt) in
                Uf.union elt_i elt_canonized graph))

      (** Minimizes a graph, by computing the coarsest congruence that
          distinguishes constants, and canonizing this graph according to this
          congruence *)
      let minimize (graph : graph) root =
        let pre_images, partition = init graph root in
        if debug_minimize then (
          Format.eprintf "MINIMIZE START@.";
          Format.eprintf "Initial classes:@.@[%a@]@."
            (Partition.pp Format.pp_print_int)
            partition);
        Partition.stabilize pre_images partition;
        if debug_minimize then (
          Format.eprintf "Final classes:@.@[%a@]@."
            (Partition.pp Format.pp_print_int)
            partition;
          Format.eprintf "MINIMIZE END@.@.");
        canonize_graph graph partition
    end

    type path = AtomicPath.t list
    (** Paths are lists of atomic paths: the first element in that list is the
        atomic paths to follow *)

    let pp fmt = function
      | [] -> Format.pp_print_string fmt "ε"
      | path ->
          Format.pp_print_list
            ~pp_sep:(fun _fmt () -> ())
            AtomicPath.pp fmt path

    (** [find_at_path g p x] finds in the graph [g] the node that is reached
        from [x] by following the path [p] *)
    let rec find_at_path (graph : graph) (path : path) x =
      match path with
      | [] ->
          (* Format.eprintf "Found: @[%a@]@." pp t; *)
          x
      | l :: ls -> (
          match Uf.get_term x graph with
          | None -> assert false
          | Some { constants = _; closures; constructs; structures } -> (
              match l with
              | InClos (f, x) -> clos_find_at_path graph f x ls closures
              | InConstruct (w, p) ->
                  constructs_find_at_path graph w p ls constructs
              | InStruct (f, x) -> struct_find_at_path graph f x ls structures))

    and clos_find_at_path cache f x ls = function
      | ClosTop -> raise Not_found
      | Clos m -> m |> MFun.find f |> Env.find x |> find_at_path cache ls

    and constructs_find_at_path cache w i ls = function
      | ConstructsTop -> raise Not_found
      | Constructs m ->
          m |> MConstruct.find w
          |> (fun l -> List.nth l i)
          |> find_at_path cache ls

    and struct_find_at_path cache f x ls = function
      | StructTop -> raise Not_found
      | Struct m -> m |> MOPP.find f |> MField.find x |> find_at_path cache ls

    let find_at_path cache path t = find_at_path cache (List.rev path) t
  end

  let with_encoding ~share f t =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t = Unif.encode ~share n graph t in
    f graph enc_t;
    Unif.decode graph enc_t

  let with_encoding2 ~share f t1 t2 =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode ~share n graph t1 in
    let enc_t2 = Unif.encode ~share n graph t2 in
    f graph enc_t1 enc_t2;
    Unif.decode graph enc_t1

  let with_encoding3 ~share f t1 t2 =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode ~share n graph t1 in
    let enc_t2 = Unif.encode ~share n graph t2 in
    let enc_t12 = f n graph enc_t1 enc_t2 in
    Unif.decode graph enc_t12

  module MemoFix2 =
    Memo.Fix.Priority.Make (Logs.Silent) (Ints2) (MInts2)
      (BoolDomainDefaultTrue)

  (* [leq_gen_raw ~weak graph t1 t2] is an inclusion test between two
     abstract values [t1] and [t2] that are encoded in a same graph
     [graph]. If [weak = true], then the integer parts of [t1] and
     [t2] are ignored. *)
  let leq_gen_raw ~weak graph =
    let is_empty_raw = Memoize.apply @@ M.memo_int @@ Unif.is_empty_raw graph in
    let leq =
      MemoFix2.solve ~debug:false ~transient:false @@ fun leq (t1, t2) ->
      let leq t1 t2 = t1 = t2 || leq (t1, t2) in
      let clos_leq (clos1 : Unif.node_closures) (clos2 : Unif.node_closures) =
        match (clos1, clos2) with
        | _, ClosTop -> true
        | ClosTop, Clos _ -> false
        | Clos m1, Clos m2 ->
            MFun.for_all
              (fun t env1 ->
                match MFun.find_opt t m2 with
                | None ->
                    (not weak)
                    && MFun.for_all
                         (fun f env ->
                           (* fixpoints through recursive functions are non-empty *)
                           (not @@ Fun.is_recursive f)
                           && Env.exists (fun _ v -> is_empty_raw v) env)
                         m1
                | Some env2 -> Env.equal leq env1 env2)
              m1
      and constructs_leq (t1 : Unif.node_constructs) (t2 : Unif.node_constructs)
          =
        match (t1, t2) with
        | _, ConstructsTop -> true
        | ConstructsTop, Constructs _ -> false
        | Constructs m1, Constructs m2 ->
            MConstruct.for_all
              (fun c l1 ->
                match MConstruct.find_opt c m2 with
                | None -> (not weak) && List.exists is_empty_raw l1
                | Some l2 ->
                    ListExtra.same_length l1 l2 && List.for_all2 leq l1 l2)
              m1
      and struct_leq (struct1 : Unif.node_structures)
          (struct2 : Unif.node_structures) =
        match (struct1, struct2) with
        | _, StructTop -> true
        | StructTop, Struct _ -> false
        | Struct m1, Struct m2 ->
            MOPP.for_all
              (fun opp env1 ->
                match MOPP.find_opt opp m2 with
                | None ->
                    (not weak)
                    && MOPP.for_all
                         (fun _opp env ->
                           MField.exists (fun _ v -> is_empty_raw v) env)
                         m1
                | Some env2 ->
                    MField.for_all
                      (fun f v2 ->
                        match MField.find_opt f env1 with
                        | None -> false
                        | Some v1 -> leq v1 v2)
                      env2)
              m1
      in
      match (Uf.get_term t1 graph, Uf.get_term t2 graph) with
      | ( Some
            {
              constants = constants1;
              closures = clos1;
              constructs = constructs1;
              structures = struct1;
            },
          Some
            {
              constants = constants2;
              closures = clos2;
              constructs = constructs2;
              structures = struct2;
            } ) ->
          (if weak then
             Constants.is_bot constants1 || (not @@ Constants.is_bot constants2)
           else Constants.leq constants1 constants2)
          && clos_leq clos1 clos2
          && constructs_leq constructs1 constructs2
          && struct_leq struct1 struct2
      | None, _ | _, None -> assert false
    in
    fun t1 t2 -> fst @@ leq (t1, t2)

  (* [leq_gen ~weak t1 t2] is an inclusion test between [t1] and [t2].
     If [weak = true], then the integer parts of [t1] and [t2] are
     ignored. *)
  let leq_gen ~weak t1 t2 =
    let graph : Unif.node Uf.t = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode ~share:true n graph t1 in
    let enc_t2 = Unif.encode ~share:true n graph t2 in
    leq_gen_raw ~weak graph enc_t1 enc_t2

  let leq =
    let leq = Memoize.apply @@ M.memo2 @@ leq_gen ~weak:false in
    fun t1 t2 -> t1 == t2 || leq t1 t2

  let leq_weak =
    let leq = Memoize.apply @@ M.memo2 @@ leq_gen ~weak:true in
    fun t1 t2 -> t1 == t2 || leq t1 t2

  let leq_gen ~weak t1 t2 = if weak then leq_weak t1 t2 else leq t1 t2

  let minimize =
    if Options.minimize then (
      Memoize.apply @@ M.memo
      @@ fun t ->
      let t' = with_encoding ~share:true Unif.Minimize.minimize t in
      if Options.check_wf && (not @@ leq t' t) then
        Format.eprintf
          "WARNING: minimization creates a precision loss:@.Before \
           minimization:@.@[%a@]@.After minimization:@.@[%a@]@."
          pp t pp t';
      t')
    else Stdlib.Fun.id

  let[@inline] join_gen ~monotonic ~widen t1 t2 =
    if t1 == t2 || is_bot t2 then t1
    else if is_bot t1 then t2
    else if monotonic then
      with_encoding3 ~share:true (Unif.union_vars ~widen) (minimize t1)
        (minimize t2)
      |> minimize
    else with_encoding2 ~share:false (Unif.join_vars ~widen) t1 t2 |> minimize

  let join =
    let join =
      Memoize.apply @@ M.memo2
      @@ join_gen ~monotonic:Options.monotonic_join ~widen:false
    in
    fun t1 t2 -> if t1 == t2 then t1 else join t1 t2

  let meet =
    let meet =
      Memoize.apply @@ M.memo2 @@ with_encoding3 ~share:true Unif.meet_vars
    in
    fun t1 t2 -> if t1 == t2 then t1 else meet t1 t2

  let may_recursively_contain_closures =
    Memoize.apply @@ M.memo_rec
    @@ fun may_recursively_contain_closures t ->
    match Hcons.node t with
    | Abs { height = _; constants = _; closures; constructs; structures } -> (
        (not @@ clos_is_bot closures)
        || (match constructs with
           | ConstructsTop -> true
           | Constructs (_, m) ->
               MConstruct.exists
                 (fun _c ts -> List.exists may_recursively_contain_closures ts)
                 m)
        ||
        match structures with
        | StructTop -> true
        | Struct (_, m) ->
            MOPP.exists
              (fun _opp ts ->
                MField.exists
                  (fun _f t -> may_recursively_contain_closures t)
                  ts)
              m)
    | Mu (_h, t0) ->
        (* we do not perform a substitution here for performance reasons *)
        may_recursively_contain_closures t0
    | Idx _ -> false
    | Var _ ->
        (* this case should never happen *)
        assert false

  type (-'a, +'b) memo_fun = ('a, 'b) Memoize.memo_fun

  let apply_memo = Memoize.apply

  let collect_in_constants ~of_constants ~bot ~top ~join =
    M.memo_rec @@ fun recursively_contained_pointers t ->
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } -> (
        let acc = of_constants constants in
        let acc =
          match closures with
          | ClosTop -> top
          | Clos (_, m) ->
              MFun.fold
                (fun _c ts acc ->
                  Env.fold
                    (fun _x t acc ->
                      join acc @@ recursively_contained_pointers t)
                    ts acc)
                m acc
        in
        let acc =
          match constructs with
          | ConstructsTop -> top
          | Constructs (_, m) ->
              MConstruct.fold
                (fun _c ts acc ->
                  List.fold_left
                    (fun acc t -> join acc @@ recursively_contained_pointers t)
                    acc ts)
                m acc
        in
        match structures with
        | StructTop -> top
        | Struct (_, m) ->
            MOPP.fold
              (fun _opp ts acc ->
                MField.fold
                  (fun _f t acc -> join acc @@ recursively_contained_pointers t)
                  ts acc)
              m acc)
    | Mu (_h, t0) ->
        (* we do not perform a substitution here for performance reasons *)
        recursively_contained_pointers t0
    | Idx _ | Var _ -> bot

  (** [truncate h t] truncates [t] at height [h] by replacing sub-terms that are
      too high with [top] *)
  let truncate n h t =
    let fresh_var () =
      let var = !n in
      incr n;
      var
    in
    let rec truncate h t =
      if height t <= h then t
      else
        match Hcons.node t with
        | Idx _ -> assert false (* this case should never happen *)
        | Var _ -> t
        | Mu (_, t) ->
            let var = fresh_var () in
            mu var @@ truncate h (open_var var t)
        | Abs { height = _; constants; closures; constructs; structures } ->
            make ~constants ~closures:(clos_truncate h closures)
              ~constructs:(constructs_truncate h constructs)
              ~structures:(struct_truncate h structures)
    and clos_truncate h c =
      match c with
      | ClosTop -> clos_top
      | Clos (h', m) ->
          if h' <= h then c
          else if h <= 0 then
            if clos_is_bot c then c
            else (* Format.eprintf "TRUNCATE ClosTop@."; *)
              clos_top
          else
            let h = h - 1 in
            clos (MFun.map (Env.map (truncate h)) m)
    and constructs_truncate h t =
      match t with
      | ConstructsTop -> constructs_top
      | Constructs (h', m) ->
          if h' <= h then t
          else if h <= 0 then
            if constructs_is_bot t then t
            else (* Format.eprintf "TRUNCATE ConstructsTop@."; *)
              constructs_top
          else
            let h = h - 1 in
            constructs (MConstruct.map (List.map (truncate h)) m)
    and struct_truncate h str =
      match str with
      | StructTop -> struct_top
      | Struct (h', m) ->
          if h' <= h then str
          else if h <= 0 then
            if struct_is_bot str then str
            else (* Format.eprintf "TRUNCATE StructTop@."; *)
              struct_top
          else
            let h = h - 1 in
            struct_ (MOPP.map (MField.map (truncate h)) m)
    in
    truncate h t

  module PMap = Map.Make (AtomicPath)
  (** Maps indexed by atomic paths. They are used to compute how many times an
      atomic path occurs in an abstract value. *)

  module Path = struct
    (** [count_occurs_in_paths t] produces a map whose keys are atomic paths,
        that associates to each atomic path how many times it occurs in [t]. The
        output map only contains strictly positive integers. *)
    let count_occurs_in_paths =
      let join_maps m1 m2 =
        PMap.union
          (fun _ (n1, h1) (n2, h2) -> Some (max n1 n2, max h1 h2))
          m1 m2
      and count_path p h m =
        PMap.update p
          (function
            | None -> Some (1, h) | Some (n, h') -> Some (n + 1, max h' h))
          m
      in
      let rec count h t =
        match Hcons.node t with
        | Var _ | Idx _ -> PMap.empty
        | Mu (_, t) -> count h t
        | Abs { height = _; constants = _; closures; constructs; structures } ->
            join_maps (clos_count h closures)
            @@ join_maps
                 (constructs_count h constructs)
                 (struct_count h structures)
      and clos_count h = function
        | ClosTop -> PMap.empty
        | Clos (_, m) ->
            MFun.fold
              (fun f env acc ->
                Env.fold
                  (fun x t acc ->
                    count (h + 1) t
                    |> count_path (AtomicPath.InClos (f, x)) h
                    |> join_maps acc)
                  env acc)
              m PMap.empty
      and constructs_count h = function
        | ConstructsTop -> PMap.empty
        | Constructs (_, m) ->
            MConstruct.fold
              (fun w l acc ->
                ListExtra.fold_lefti
                  (fun acc i t ->
                    count (h + 1) t
                    |> count_path (AtomicPath.InConstruct (w, i)) h
                    |> join_maps acc)
                  acc l)
              m PMap.empty
      and struct_count h = function
        | StructTop -> PMap.empty
        | Struct (_, m) ->
            MOPP.fold
              (fun opp env acc ->
                MField.fold
                  (fun x t acc ->
                    count (h + 1) t
                    |> count_path (AtomicPath.InStruct (opp, x)) h
                    |> join_maps acc)
                  env acc)
              m PMap.empty
      in
      count 0

    (** [max_occurs t] counts the maximum number of occurrences of any atomic
        path in [t] *)
    let max_occurs = Memoize.apply @@ M.memo count_occurs_in_paths
  end

  let clash_positions graph v1 v2 =
    let cache = ref SInts2.empty in
    let depth = ref MInts.empty in
    let record_depth t n =
      depth :=
        MInts.update t
          (function None -> Some n | Some n' -> Some (min n n'))
          !depth
    and get_depth t = MInts.find t !depth in
    let add_clash ctxt t1 t2 acc =
      (ctxt, get_depth t1, get_depth t2, t2) :: acc
    in
    let rec browse n1 n2 ctxt t1 t2 acc =
      if SInts2.mem (t1, t2) !cache then acc
      else (
        cache := SInts2.add (t1, t2) !cache;
        record_depth t1 n1;
        record_depth t2 n2;
        match (Uf.get_term t1 graph, Uf.get_term t2 graph) with
        | ( Some
              {
                Unif.constants = _;
                closures = clos1;
                constructs = constructs1;
                structures = struct1;
              },
            Some
              ({
                 constants = _;
                 closures = clos2;
                 constructs = constructs2;
                 structures = struct2;
               } as node2) ) ->
            let n1 = get_depth t1 and n2 = get_depth t2 in
            if n1 <> n2 then
              (* depths are not synchronized anymore: we should stop *)
              if n1 < n2 && (not @@ Unif.node_is_empty ~weak:true node2) then
                (* we have found a clash *)
                add_clash ctxt t1 t2 acc
              else acc
            else
              acc
              |> browse_clos n1 n2 ctxt t1 clos1 t2 clos2
              |> browse_constructs n1 n2 ctxt t1 constructs1 t2 constructs2
              |> browse_struct n1 n2 ctxt t1 struct1 t2 struct2
        | None, _ | _, None -> assert false)
    and browse_clos n1 n2 ctxt t1 clos1 t2 clos2 acc =
      match (clos1, clos2) with
      | _, ClosTop | ClosTop, Clos _ -> acc
      | Clos m1, Clos m2 ->
          let n1' = n1 + 1 and n2' = n2 + 1 in
          MFun.fold
            (fun t env2 acc ->
              match MFun.find_opt t m1 with
              | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
              | Some env1 ->
                  Env.fold
                    (fun x t2' acc ->
                      let t1' = Env.find x env1 in
                      browse n1' n2'
                        ((AtomicPath.InClos (t, x), n2, t2) :: ctxt)
                        t1' t2' acc)
                    env2 acc)
            m2 acc
    and browse_constructs n1 n2 ctxt t1 constructs1 t2 constructs2 acc =
      let n1' = n1 + 1 and n2' = n2 + 1 in
      match (constructs1, constructs2) with
      | _, ConstructsTop | ConstructsTop, Constructs _ -> acc
      | Constructs m1, Constructs m2 ->
          MConstruct.fold
            (fun i l2 acc ->
              match MConstruct.find_opt i m1 with
              | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
              | Some l1 ->
                  let acc =
                    if
                      n1 = n2
                      && not
                           (List.exists (Unif.is_empty ~weak:true graph) l1
                           <=> List.exists (Unif.is_empty ~weak:true graph) l2)
                    then add_clash ctxt t1 t2 acc
                    else acc
                  in
                  ListExtra.fold_lefti2
                    (fun acc j t1' t2' ->
                      browse n1' n2'
                        ((AtomicPath.InConstruct (i, j), n2, t2) :: ctxt)
                        t1' t2' acc)
                    acc l1 l2)
            m2 acc
    and browse_struct n1 n2 ctxt t1 struct1 t2 struct2 acc =
      match (struct1, struct2) with
      | _, StructTop | StructTop, Struct _ -> acc
      | Struct m1, Struct m2 ->
          let n1' = n1 + 1 and n2' = n2 + 1 in
          MOPP.fold
            (fun opp env2 acc ->
              match MOPP.find_opt opp m1 with
              | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
              | Some env1 ->
                  MField.fold
                    (fun x t2' acc ->
                      let t1' = MField.find x env1 in
                      browse n1' n2'
                        ((AtomicPath.InStruct (opp, x), n2, t2) :: ctxt)
                        t1' t2' acc)
                    env2 acc)
            m2 acc
    in
    browse 0 0 [] v1 v2 []

  let path_of_ctxt ctxt = List.map (fun (p, _, _) -> p) ctxt

  let rec refold0 graph ctxt orig_depth pos_depth pos (success, failures) =
    match ctxt with
    | [] -> (success, failures)
    | (_atomic_path, ancestor_depth, ancestor) :: ctxt ->
        if orig_depth >= ancestor_depth then
          if
            leq_gen_raw ~weak:true graph pos ancestor
            &&
            (* prevent folding nodes that could create a
                  semantically empty recursive node *)
            (not @@ Unif.is_empty_raw_with_hyp [ pos ] graph ancestor)
          then (
            if Options.debug_shrinking then
              Format.eprintf
                "  %a (orig_depth=%i, ancestor_depth=%i, pos_depth=%i)@."
                Unif.pp
                (List.rev (path_of_ctxt ctxt))
                orig_depth ancestor_depth pos_depth;
            Unif.join_vars ~widen:false graph ancestor pos;
            (* XXX: should we use widening here? *)
            (true, []))
          else
            let failures =
              if
                orig_depth < pos_depth
                || Unif.leq_head ~weak:true graph pos ancestor
                   &&
                   (* prevent folding nodes that could create a
                         semantically empty recursive node *)
                   (not @@ Unif.is_empty_raw_with_hyp [ pos ] graph ancestor)
              then (
                let path = (* atomic_path :: *) path_of_ctxt ctxt in
                if Options.debug_shrinking then
                  Format.eprintf "  possible replacement at %a@." Unif.pp
                    (List.rev path);
                path :: failures)
              else failures
            in
            refold0 graph ctxt orig_depth pos_depth pos (success, failures)
        else refold0 graph ctxt orig_depth pos_depth pos (success, failures)

  let refold graph ctxt orig_depth depth pos =
    if Options.debug_shrinking then
      Format.eprintf "Folds at position %a:@." Unif.pp
        (List.rev (path_of_ctxt ctxt));
    refold0 graph ctxt orig_depth depth pos (false, [])

  let perform_refolds t1 t2 =
    let graph = Uf.init () and n = ref 0 in
    let t1_enc = Unif.encode ~share:false n graph t1 in
    let t2_enc = Unif.encode ~share:false n graph t2 in
    let positions = clash_positions graph t1_enc t2_enc in
    let positions =
      List.sort
        (fun (_ctxt1, orig_depth1, depth1, _node1)
             (_ctxt2, orig_depth2, depth2, _node2) ->
          let n = Int.compare depth1 depth2 in
          if n <> 0 then n else Int.compare orig_depth1 orig_depth2)
        positions
    in
    let success, progressed, failures =
      (* XXX: use failures to try some more refolds when [success=false]! *)
      List.fold_left
        (fun (success_acc, progress_acc, failures_acc)
             (ctxt, depth_orig, depth, pos) ->
          let success, failures = refold graph ctxt depth_orig depth pos in
          if success then (success_acc, true, failures_acc)
          else
            let pos_path = path_of_ctxt ctxt in
            let failures =
              List.map (fun ancestor_path -> (pos_path, ancestor_path)) failures
            in
            (false, progress_acc, failures @ failures_acc))
        (true, false, []) positions
    in
    let t2 = Unif.decode graph t2_enc in
    (success, progressed, failures, t2)

  let occurs_leq m (m1 : (int * int) PMap.t) (m2 : (int * int) PMap.t) =
    PMap.for_all
      (fun p (n1, _h1) ->
        match PMap.find_opt p m2 with
        | None -> n1 <= m
        | Some (n2, _h2) -> n1 <= max m n2)
      m1

  let rec shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t =
    if max_iter < 0 then (
      Format.eprintf
        "INFO: NablaRec.shrink: maximum number of iterations was reached@.";
      truncate (ref 0) max_height t)
    else
      (* Format.eprintf "INFO: NablaRec.shrink: START perform_refolds@."; *)
      let success, progressed, failures, t = perform_refolds t_orig t in
      (* Format.eprintf "INFO: NablaRec.shrink: END perform_refolds@."; *)
      check_wf ~closed:true "shrink loop" 0 t;
      if success then t
      else if progressed then (
        if Options.debug_shrinking then
          Format.eprintf
            "INFO: NablaRec.shrink: unsuccessful but progress made@.";
        let max_iter = max_iter - 1 in
        shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t)
      else if ListExtra.is_empty failures then t
      else (
        if Options.debug_shrinking then
          Format.eprintf "INFO: NablaRec.shrink: handling failures@.";
        let t =
          with_encoding ~share:false
            (fun graph enc_t ->
              List.iter
                (fun (from, to_) ->
                  (* [from] is deeper in the tree than [to_], thus
                     [to_] should serve as the base for widening *)
                  try
                    let to_node = Unif.find_at_path graph to_ enc_t
                    and from_node = Unif.find_at_path graph from enc_t in
                    Unif.join_vars
                      ~widen:false (* XXX: should we force widening here? *)
                      graph to_node from_node
                  with Not_found ->
                    (* unification has created some ClosTop *)
                    ())
                failures)
            t
        in
        check_wf ~closed:true "shrink loop" 0 t;
        shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t)

  let widen =
    Memoize.apply @@ M.memo2
    @@ fun t1 t2 ->
    if t1 == t2 then t1
    else
      let t12 =
        join_gen ~monotonic:false ~widen:true (minimize t1) (minimize t2)
      in
      if Options.check_wf && (not @@ leq t1 t12) then
        Format.eprintf
          "ERROR: NablaRec.widen: widening not expansive \
           (LHS)@.@[%a@]@.@.@[%a@]@.@."
          pp t1 pp t12;
      if Options.check_wf && (not @@ leq t2 t12) then
        Format.eprintf "ERROR: NablaRec.widen: widening not expansive (RHS)@.";
      if height t12 <= height t1 then
        (* Format.eprintf *)
        (* "@[WIDEN h = %i@ @[t1 =@ @[%a@]@]@ @[t2 =@ @[%a@]@]@.result \ *)
           (*    =@.@[%a@]@.@.@]" *)
        (*   h1 *)
        (*   pp *)
        (*   t1 *)
        (*   pp *)
        (*   t2 *)
        (*   pp *)
        (*   t12; *)
        t12
      else
        let occurs1 = Path.max_occurs t1 and occurs12 = Path.max_occurs t12 in
        let occurs_threshold = Options.occurs_threshold in
        if occurs_leq occurs_threshold occurs12 occurs1 then
          (* Format.eprintf *)
          (* "@[WIDEN h = %i@ @[t1 =@ @[%a@]@]@ @[t2 =@ @[%a@]@]@.result \ *)
             (*    =@.@[%a@]@.@.@]" *)
          (*   h1 *)
          (*   pp *)
          (*   t1 *)
          (*   pp *)
          (*   t2 *)
          (*   pp *)
          (*   t12; *)
          t12
        else
          let h1 = height t1 in
          if Options.debug_shrinking then
            Format.eprintf
              "@[SHRINK h = %i@ @[orig =@ @[%a@]@]@ @[t =@ @[%a@]@]@.@]" h1 pp
              t1 pp t12;
          let res =
            shrink
              ~max_iter:(3 * (h1 + 1))
              ~occurs:occurs1 ~occurs_threshold ~max_height:h1 t1 t12
          in
          let res = minimize res in
          if Options.debug_shrinking then
            Format.eprintf "SHRINK result =@.@[%a@]@.@." pp res;
          if Options.check_wf && (not @@ leq t12 res) then
            Format.eprintf
              "ERROR: NablaRec.widen: shrinking not expansive@.before \
               =@.@[%a@]@.after =@.@[%a@]@."
              pp t12 pp res;
          res

  (* Two abstract values are similar if their heights differ by at
     least 2, or if they have comparable shapes *)
  let sim t1 t2 =
    let n1 = height t1 and n2 = height t2 in
    abs (n1 - n2) >= Options.sim_threshold || leq_gen ~weak:true t1 t2
end

module Make
    (Options : sig
      val minimize : bool
      val occurs_threshold : int
      val sim_threshold : int

      val debug_shrinking : bool
      (** Whether to print extra information about the shrinking phase *)

      val check_wf : bool
      (** Whether to perform extra checking for wellformedness *)

      val monotonic_join : bool
    end)
    (Constants : AbstractDomains.Constants.S)
    (Fun : sig
      include PrettyOrderedType

      val is_recursive : t -> bool
    end)
    (MFun : HashableMap with type key = Fun.t)
    (Construct : sig
      include PrettyOrderedType

      val is_singleton : t -> bool
      val is_constant : t -> bool
    end)
    (MConstruct : HashableMap with type key = Construct.t)
    (OPP : Map.OrderedType)
    (MOPP : HashableMap with type key = OPP.t)
    (Var : Map.OrderedType)
    (Env : HashableMap with type key = Var.t)
    (Field : PrettyOrderedType)
    (MField : HashableMap with type key = Field.t)
    () : sig
  include AbstractDomains.Sigs.LATTICE

  val sim : t -> t -> bool
  val singleton_constant : Constants.t -> t
  val is_constant : 'a Constants.key -> t -> bool
  val get_constant : 'a Constants.key -> ('a -> 'a * 'a) -> t -> 'a * t
  val get_constants : 'a Constants.key -> t -> 'a * t
  val singleton_construct : Construct.t -> t list -> t
  val get_construct : Construct.t -> t -> (t list option * t) option
  val singleton_closure : Fun.t -> t Env.t -> t
  val get_closures : t -> ((Fun.t * t Env.t) list option * t) option

  val closures_join_map :
    'a -> ('a -> 'a -> 'a) -> 'a -> (Fun.t -> t Env.t -> 'a) -> t -> 'a

  val may_recursively_contain_closures : t -> bool

  type (-'a, +'b) memo_fun

  val apply_memo : ('a, 'b) memo_fun -> 'a -> 'b

  val collect_in_constants :
    of_constants:(Constants.t -> 'a) ->
    bot:'a ->
    top:'a ->
    join:('a -> 'a -> 'a) ->
    (t, 'a) memo_fun

  val singleton_structure : OPP.t -> t MField.t -> t
  val get_structures : t -> ((OPP.t * t MField.t) list option * t) option
  val update_structures : (t MField.t -> t MField.t option) -> t -> t
end =
  MakeRaw (Options) (Constants) (Fun) (MFun) (Construct) (MConstruct) (OPP)
    (MOPP)
    (Var)
    (Env)
    (Field)
    (MField)
    ()

(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2024
*)

(** Partition refinement data-structure, and coarsest refinement algorithm. The
    data-structure is an imperative data-structure whose [refine] operation has
    a linear worst-case complexity in the size of the splitting set. Testing
    whether elements belong the same class takes constant time. The computation
    of the coarsest congruence (function [stabilize] follows an algorithm by
    Mycroft, with semi-linear worst-case complexity.

    Bibliographic references:

    J. Hopcroft, “An n log n algorithm for minimizing states in a finite
    automaton,” in Theory of Machines and Computations, Z. Kohavi and A. Paz,
    Eds., Academic Press, 1971, pp. 189–196. doi:
    https://doi.org/10.1016/B978-0-12-417750-5.50022-1.

    D. Gries, “Describing an Algorithm by Hopcroft,” Acta Informatica, vol. 2,
    no. 2, pp. 97–109, 1973, doi: 10.1007/bf00264025.

    M. Habib, C. Paul, and L. Viennoti, “A synthesis on partition refinement: A
    useful routine for strings, graphs, boolean matrices and automata,” in
    Lecture Notes in Computer Science, Springer Berlin Heidelberg, 1998, pp.
    25–38. doi: 10.1007/bfb0028546. *)

(* enable this flag for debugging only *)
let do_check = false

(** Doubly-linked, circular, non-empty, mutable lists. *)
module DLList : sig
  type ('a, 'c) t = {
    data : 'a;  (** Data recorded in a cell *)
    mutable class_ : 'c;  (** Class a cell belongs to *)
    mutable seen : bool;
        (** Tells whether this element has been already processed (used by
            [refine] to avoid processing an element more than once) *)
    mutable prev : ('a, 'c) t;
        (** The previous cell in the doubly-linked list *)
    mutable next : ('a, 'c) t;  (** The next cell in the doubly-linked list *)
  }

  val create : 'a -> 'c -> ('a, 'c) t
  val insert_single_right : ('a, 'c) t -> ('a, 'c) t -> unit
  val swap : ('a, 'c) t -> ('a, 'c) t -> unit

  val iter_right_from_to :
    (('a, 'c) t -> unit) -> ('a, 'c) t -> ('a, 'c) t -> unit

  val mem_from_to : ('a, 'c) t -> ('a, 'c) t -> ('a, 'c) t -> bool
  val length_from_to : ('a, 'c) t -> ('a, 'c) t -> int
  val to_list_from_to : ('a, 'c) t -> ('a, 'c) t -> 'a list
  val is_circular : ('a, 'c) t -> bool
end = struct
  type ('a, 'c) t = {
    data : 'a;
    mutable class_ : 'c;
    mutable seen : bool;
    mutable prev : ('a, 'c) t;
    mutable next : ('a, 'c) t;
  }

  let has_no_prev cell = cell.prev == cell
  let has_no_next cell = cell.next == cell
  let is_single cell = has_no_prev cell && has_no_next cell

  let create v c =
    let rec cell =
      { data = v; class_ = c; seen = false; prev = cell; next = cell }
    in
    if do_check then assert (is_single cell);
    cell

  let link cell1 cell2 =
    if do_check then assert (cell1 != cell2);
    cell1.next <- cell2;
    cell2.prev <- cell1

  let insert_single_right dst cell =
    if do_check then (
      assert (has_no_prev cell);
      assert (has_no_next cell));
    link cell dst.next;
    link dst cell

  (** swaps two cells, assuming they are distinct *)
  let swap_non_alias cell1 cell2 =
    if do_check then assert (cell1 != cell2);
    let prev1 = cell1.prev
    and next1 = cell1.next
    and prev2 = cell2.prev
    and next2 = cell2.next in
    if next1 == cell2 then
      if next2 == cell1 then
        (* the list has only 2 elements and is circular: there is
           nothing to do *)
        ()
      else (
        link cell1 next2;
        link cell2 cell1;
        link prev1 cell2)
    else if prev1 == cell2 then (
      link prev2 cell1;
      link cell1 cell2;
      link cell2 next1)
    else (
      link prev2 cell1;
      link cell1 next2;
      link cell2 next1;
      link prev1 cell2)

  (** swaps two cells *)
  let swap cell1 cell2 =
    let old_cell1_prev = cell1.prev
    and old_cell1_next = cell1.next
    and old_cell2_prev = cell2.prev
    and old_cell2_next = cell2.next in
    let old_adjacent12 = old_cell1_next == cell2
    and old_adjacent21 = old_cell2_next == cell1 in
    if cell1 != cell2 then (
      swap_non_alias cell1 cell2;
      if do_check then (
        let adjacent12 = cell1.next == cell2
        and adjacent21 = cell2.next == cell1 in
        assert (cell1 != cell2);
        if old_adjacent12 then (
          assert adjacent21;
          if old_adjacent21 then (
            assert (old_cell1_prev == cell1.prev);
            assert (old_cell2_next == cell2.next))
          else (
            assert (old_cell1_prev == cell2.prev);
            assert (old_cell2_next == cell1.next)))
        else if old_adjacent21 then (
          assert adjacent12;
          assert (old_cell1_next == cell2.next);
          assert (old_cell2_prev == cell1.prev))
        else (
          assert (old_cell1_prev == cell2.prev);
          assert (old_cell1_next == cell2.next);
          assert (old_cell2_prev == cell1.prev);
          assert (old_cell2_next == cell1.next))))

  let rec iter_right_from_to f from to_ =
    if from == to_ then f from
    else (
      f from;
      iter_right_from_to f from.next to_)

  let rec to_list_from_to_aux first last acc =
    let acc = last.data :: acc in
    if first == last then acc else to_list_from_to_aux first last.prev acc

  let to_list_from_to first last = to_list_from_to_aux first last []

  let rec mem_from_to from to_ elt =
    if from == to_ then elt == from
    else elt == from || mem_from_to from.next to_ elt

  let rec length_from_to_aux from to_ acc =
    if from == to_ then acc else length_from_to_aux from.next to_ (acc + 1)

  let length_from_to from to_ = length_from_to_aux from to_ 1

  let is_circular =
    let rec loop start seen cur =
      start == cur
      || (not @@ List.exists (( == ) cur) seen)
         && loop start (cur :: seen) cur.next
    in
    fun l -> loop l [] l.next
end

(** A block, i.e., a contiguous subset of a doubly-linked list. Blocks denote
    equivalence classes of elements. *)
module Block = struct
  type 'a t = {
    mutable first : ('a, 'a t) DLList.t;
        (** First element of the block (included) *)
    mutable last : ('a, 'a t) DLList.t;
        (** Last element of the block (included) *)
    mutable len : int;  (** Number of elements in a block *)
    mutable split_start : ('a, 'a t) DLList.t;
        (** Indicates where a split must be performed. [split_start] must be
            between [first] and [last]. Split must produce two blocks: the first
            one starts at [first] and ends just before [split_start] (and thus,
            can be empty); the second one starts at [split_start] and ends at
            [last]. If [split_start = first] then no split must be performed. *)
    mutable split_len : int;
        (** Tells how many elements are on the LHS of the split. *)
    mutable next_split : 'a t;
        (** Pointer to the next block where a split has been recorded. This is a
            singly-linked list of blocks. *)
    next_block : 'a t;
        (** Pointer to the next block, or pointer to self if this is the last
            block (à la sentinel) *)
    mutable in_work_list : bool;
        (** Tells whether this block belongs to the work list for [stabilize],
            i.e. the list of blocks for which splitting might have to be
            considered. *)
    mutable work_list_next : 'a t;
        (** If [in_work_list = true], then this field points to the next block
            in the work list (or to this block if we are at the end of the list,
            à la sentinel). If [in_work_list = false], then this field points to
            this block. *)
  }

  let is_singleton (b : 'a t) : bool = b.len = 1

  (** Swaps two cells of a block *)
  let swap (b : 'a t) (cell1 : ('a, 'c) DLList.t) (cell2 : ('a, 'c) DLList.t) =
    if cell1 != cell2 then (
      let first = b.first and last = b.last in
      if first == cell1 then b.first <- cell2
      else if first == cell2 then b.first <- cell1;
      if last == cell2 then b.last <- cell1
      else if last == cell1 then b.last <- cell2;
      DLList.swap cell1 cell2)

  (** Record that a split must be performed in the block [b] for the element
      [cell], where [start_of_split_list] is the current head of the list of
      splits. *)
  let record_split (b : 'a t) (cell : ('a, 'c) DLList.t)
      (start_of_split_list : 'a t option) =
    if is_singleton b || cell.seen then start_of_split_list
    else (
      cell.seen <- true;
      let cur_split_start = b.split_start in
      let never_split = cur_split_start == b.first in
      if cur_split_start == b.last then (
        (* we have no slot left: the split will be "complete" for this
           class, meaning that it is useless *)
        b.split_start <- b.first;
        b.split_len <- 0;
        start_of_split_list)
      else (
        if cur_split_start != cell then swap b cur_split_start cell;
        b.split_start <- cell.next;
        b.split_len <- b.split_len + 1;
        if never_split then (
          (* this is a new class to split *)
          (match start_of_split_list with
          | None -> ()
          | Some list_head -> b.next_split <- list_head);
          Some b)
        else
          (* this class has already been marked for split *)
          start_of_split_list))

  let to_list l = DLList.to_list_from_to l.first l.last

  (** Iterator for the elements of a block *)
  let iter f l = DLList.iter_right_from_to f l.first l.last

  let rec mem_split_list_aux b start =
    b == start
    || (start.next_split != start && mem_split_list_aux b start.next_split)

  (** Tells whether a block belongs to the split list whose head is [ostart] *)
  let mem_split_list (b : 'a t) (ostart : 'a t option) =
    match ostart with None -> false | Some start -> mem_split_list_aux b start

  let rec for_all (p : 'a t -> bool) (start : 'a t) =
    p start && (start.next_block == start || for_all p start.next_block)

  let rec iter_blocks (p : 'a t -> unit) (start : 'a t) =
    p start;
    if start.next_block != start then iter_blocks p start.next_block

  let rec to_list_blocks (start : 'a t) =
    start
    ::
    (if start.next_block == start then [] else to_list_blocks start.next_block)

  let rec iter_split_list_loop f (class_ : 'a t) =
    f class_;
    if class_.next_split != class_ then iter_split_list_loop f class_.next_split

  let iter_split_list f (ostart : 'a t option) =
    match ostart with None -> () | Some start -> iter_split_list_loop f start
end

type 'a t = {
  mutable blocks_head : 'a Block.t;
  mutable work_list_head : 'a Block.t option;
}

(** An element is a list of descriptors *)
module Elt = struct
  type 'a t = ('a, 'a Block.t) DLList.t

  (** Equality test on elements: NEVER ever use polymorphic equality! *)
  let equal : 'a t -> 'a t -> bool = ( == )

  (** Whether two elements belong to the same equivalence class *)
  let equiv (type a) (elt1 : a t) (elt2 : a t) : bool =
    elt1.class_ == elt2.class_

  (** The canonical representant of an element *)
  let repr (type a) (elt : a t) : a t = elt.class_.first

  (** Accessor for the data within an element *)
  let get (type a) (elt : a t) : a = elt.data

  (** Cardinal of the equivalence class of an element *)
  let cardinal (type a) (elt : a t) : int = elt.class_.len
end

(** Invariants of the data-structure *)
module Check : sig
  val check : loose:bool -> 'a t -> unit
  val check_split_list : 'a Block.t option -> 'a Block.t -> unit
end = struct
  (** Checks that a descriptor points to the right class *)
  let check_elt_class (class_ : 'a Block.t) (elt : 'a Elt.t) =
    assert (elt.class_ == class_)

  (** Checks that the [split_len] field is the set of elements in the list that
      starts with [first] and ends strictly before [split_start]. *)
  let check_split_len (class_ : 'a Block.t) =
    let class_ = class_ in
    let n = DLList.length_from_to class_.first class_.split_start in
    assert (class_.split_len = n - 1);
    assert (class_.split_len < class_.len)

  (** Checks that the [split_start] is well posititioned:
      - if [loose = true], then [split_start] must be in the list between
        [first] (included) and [last] (included)
      - if [loose = false], then [split_start] must be equal to [first] *)
  let check_split_start ~loose (class_ : 'a Block.t) =
    let class_ = class_ in
    if loose then
      assert (DLList.mem_from_to class_.first class_.last class_.split_start)
    else assert (class_.split_start == class_.first)

  (** Checks that when [loose = false], [next_split] points to its own class
      (mening that there is no next element in the split list). *)
  let check_next_split_class ~loose (class_ : 'a Block.t) =
    if not loose then assert (class_.next_split == class_)

  (** Checks that the length of a class is the number of elements in the list
      from [first] (included) to [last] (included). *)
  let check_class_len (class_ : 'a Block.t) =
    let class_ = class_ in
    let n = DLList.length_from_to class_.first class_.last in
    assert (class_.len = n)

  (** Checks that all the descriptors of a block point to this block *)
  let check_class_ptr (p : 'a Block.t) = Block.iter (check_elt_class p) p

  let check_seen_info ~loose (p : 'a Block.t) =
    if not loose then Block.iter (fun cell -> assert (not cell.seen)) p

  (** Well-formedness of the current class *)
  let check_class ~loose (class_ : 'a Block.t) =
    check_class_ptr class_;
    check_class_len class_;
    check_split_start ~loose class_;
    check_next_split_class ~loose class_;
    check_split_len class_;
    check_seen_info ~loose class_

  (** Checks whether a blocl [b] is reachable from the work list whose head is
      [hd]. *)
  let rec block_is_in_work_list (b : 'a Block.t) (hd : 'a Block.t) =
    b == hd
    || (hd.work_list_next != hd && block_is_in_work_list b hd.work_list_next)

  (** Checks that in the work list that starts with [cur], all the blocks have
      their [in_work_list] field set to [true]. *)
  let rec check_work_list_from (cur : 'a Block.t) : unit =
    assert cur.Block.in_work_list;
    if cur.Block.work_list_next != cur then
      check_work_list_from cur.work_list_next

  (** Checks that:

      - if the work list is empty, then every block has its [in_work_list] field
        set to [false], and its [work_list_next] field set to itself

      - if the work list is not empty, then every block in the list has its
        [in_work_list] field set to [true]

      - if the work list is not empty, then every block whose [in_work_list]
        field is set to [true] belongs to the work list. *)
  let check_work_list (p : 'a t) : unit =
    match p.work_list_head with
    | None ->
        assert (
          Block.for_all
            (fun block ->
              (not @@ block.Block.in_work_list)
              && block.Block.work_list_next == block)
            p.blocks_head)
    | Some hd ->
        check_work_list_from hd;
        assert (
          Block.for_all
            (fun block ->
              (not @@ block.Block.in_work_list)
              || block_is_in_work_list block hd)
            p.blocks_head)

  (** Well-formedness of all the classes of a partition *)
  let check ~loose (type a) (p : a t) : unit =
    assert (DLList.is_circular p.blocks_head.first);
    Block.iter_blocks (fun cur -> check_class ~loose cur) p.blocks_head;
    check_work_list p

  (** Disable checking when [do_check = false] *)
  let check = if do_check then check else fun ~loose:_ _ -> ()

  let check_split_list (ostart : 'a Block.t option) =
    Block.iter_split_list
      (fun block ->
        if block.first == block.split_start then
          Block.iter (fun elt -> assert elt.seen) block
        else (
          DLList.iter_right_from_to
            (fun elt -> assert elt.seen)
            block.first block.split_start.prev;
          DLList.iter_right_from_to
            (fun elt -> assert (not elt.seen))
            block.split_start block.last))
      ostart

  let check_split_list = if do_check then check_split_list else fun _ -> ()

  (** Checks that a class that is not in the split list that is reachable from
      [ostart] satisfly the strict property for [split_start]. *)
  let check_split_list_complement_class (ostart : 'a Block.t option)
      (class_ : 'a Block.t) =
    if not @@ Block.mem_split_list class_ ostart then (
      check_split_start ~loose:false class_;
      check_seen_info ~loose:false class_;
      assert (class_.next_split == class_))

  (** Checks that every class that is not in the split list that is reachable
      from [ostart] satisfly the strict property for [split_start]. *)
  let check_split_list_complement (ostart : 'a Block.t option) (p : 'a Block.t)
      : unit =
    Block.iter_blocks
      (fun cur -> check_split_list_complement_class ostart cur)
      p

  (** Disable checking when [do_check = false] *)
  let check_split_list =
    if do_check then (fun start p ->
      check_split_list_complement start p;
      check_split_list start)
    else fun _ _ -> ()
end

(** Converts a list of blocks to a list of lists *)
let blocks_to_list (p : 'a Block.t) : 'a list list =
  p |> Block.to_list_blocks |> List.map Block.to_list

(** Converts a partition to a list of lists *)
let to_list (p : 'a t) : 'a list list = blocks_to_list p.blocks_head

(** [create v] creates a partition with a single class, that is a singleton.
    Returns this partition, and the element it contains. The element is fresh
    from any other one, and contains [v] as data. *)
let create v =
  let rec elt =
    { DLList.data = v; class_ = block; seen = false; prev = elt; next = elt }
  and block =
    {
      Block.first = elt;
      last = elt;
      len = 1;
      split_start = elt;
      split_len = 0;
      next_split = block;
      next_block = block;
      in_work_list = true;
      work_list_next =
        block (* pointer to self: there is no next element in the work list *);
    }
  in
  let part = { blocks_head = block; work_list_head = Some block } in
  Check.check ~loose:false part;
  (part, elt)

(** [add_same_class elt v] inserts a new element in the same class as the given
    element [elt]. Returns a fresh element that contains [v] as data. *)
let add_same_class (type a) (elt : a Elt.t) (v : a) =
  let class_ = elt.class_ in
  let cell = DLList.create v class_ in
  DLList.insert_single_right class_.last cell;
  class_.last <- cell;
  class_.len <- class_.len + 1;
  cell

(** [add_new_class part v] creates a new class that contains only [v] in the
    partition [part] *)
let add_new_class (type a) (part : a t) (v : a) : a Elt.t =
  let rec elt =
    { DLList.data = v; class_ = block; seen = false; prev = elt; next = elt }
  and block =
    {
      Block.first = elt;
      last = elt;
      len = 1;
      split_start = elt;
      split_len = 0;
      next_split = block;
      next_block = part.blocks_head;
      in_work_list = true;
      work_list_next =
        (match part.work_list_head with None -> block | Some wl -> wl);
    }
  in
  part.blocks_head <- block;
  part.work_list_head <- Some block;
  Check.check ~loose:false part;
  elt

(** Splits the class of [elt] into two classes of the elements that are
    _strictly_ before [elt] and the rest. Thus, the second part always contains
    [elt]. If the first part is empty, then no split happens at all. It also
    updates the work list: if the class that is splitted was already in the work
    list, then the two new classes are put in this list. If the class that is
    splitted was not in the work list, then the smaller of the two new classes
    is put in the work list. *)
let split_at (elt : 'a Elt.t) (part : 'a t) : unit =
  let elt_class = elt.class_ in
  let elt_class_first = elt_class.first in
  let elt_class_len = elt_class.len in
  if elt == elt_class_first then
    (* we have no split to do, but we this means that all the elements
       of this class have been seen: we must clean the [seen] fields
       of elements *)
    Block.iter (fun cell -> cell.seen <- false) elt_class
  else
    (* the LHS is not empty: we have something to split *)
    let old_prev = elt.prev in
    elt_class.first <- elt;
    elt_class.split_start <- elt;
    let elt_class_split_len = elt_class.split_len in
    elt_class.split_len <- 0;
    elt_class.len <- elt_class_len - elt_class_split_len;
    let rec class_descr =
      {
        Block.first = elt_class_first;
        last = old_prev;
        len = elt_class_split_len;
        split_start = elt_class_first;
        split_len = 0;
        next_split = class_descr;
        next_block = part.blocks_head;
        in_work_list = false;
        work_list_next =
          class_descr
          (* we will (possibly) insert this block in the work list
             later in this function *);
      }
    in
    part.blocks_head <- class_descr;
    let () =
      if elt_class.in_work_list then (
        (* This block was in the work list: we must keep the two new
           blocks in the work list *)
        match part.work_list_head with
        | None ->
            (* there was no element in the work list: this is
               impossible because the current block was in it! *)
            assert false
        | Some hd ->
            class_descr.in_work_list <- true;
            class_descr.work_list_next <- hd;
            part.work_list_head <- Some class_descr)
      else
        (* This block was not in the work list: we must insert the
           smaller of the two new blocks in the work list *)
        let selected_class =
          if elt_class.len <= class_descr.len then elt.class_ else class_descr
        in
        selected_class.in_work_list <- true;
        (match part.work_list_head with
        | None -> ()
        | Some hd -> selected_class.work_list_next <- hd);
        part.work_list_head <- Some selected_class
    in
    DLList.iter_right_from_to
      (fun e ->
        e.class_ <- class_descr;
        e.seen <- false)
      elt_class_first old_prev

(** Splits the class provided as argument on its [split_start] field, and cleans
    the entries of this class that are related to splitting. *)
let split_class (class_ : 'a Block.t) (part : 'a t) : unit =
  split_at class_.split_start part;
  class_.split_start <- class_.first;
  class_.next_split <- class_

(** Split all the classes starting from the class provided as argument. *)
let rec split_classes_loop (class_ : 'a Block.t) (part : 'a t) : unit =
  let next = class_.next_split in
  split_class class_ part;
  if next != class_ then split_classes_loop next part

(** Split all the classes starting from the class provided as argument, if any.
*)
let split_classes (start : 'a Block.t option) (part : 'a t) : unit =
  match start with None -> () | Some class_ -> split_classes_loop class_ part

(** Record in the data-structure that a split on [elt] must be performed, but do
    not perform the split. *)
let record_split (type a) (elt : a Elt.t) split_list =
  Block.record_split elt.class_ elt split_list

type 'a iterator = ('a -> unit) -> unit

(** [refine p elt_iter], where [p] is a partition and [elt_iter] is an iterator
    of elements that belong the partition [p], refines the partition [p] with
    the set of elements [S] that is denoted by the iterator [elt_iter]. This
    operation replaces every class [C] of that partition with two sets [C ∩ S]
    and [C ∖ S] if both of them are non-empty. Worst-case complexity: O(|S|). *)
let refine (part : 'a t) (iterate : 'a Elt.t iterator) : unit =
  let open Check in
  let split_list = ref None in
  let is_first = ref true in
  iterate (fun elt ->
      check ~loose:(not !is_first) part;
      check_split_list !split_list elt.class_;
      if !is_first then is_first := false;
      split_list := record_split elt !split_list;
      check ~loose:true part;
      check_split_list !split_list elt.class_);
  split_classes !split_list part;
  match !split_list with None -> () | Some _ -> check ~loose:false part

module Iterator = struct
  let of_class (c : 'a Block.t) : 'a Elt.t iterator =
    let first = c.first in
    let last = c.last in
    let rec iter f cur =
      let next = cur.DLList.next in
      f cur;
      if cur != last then iter f next
    in
    fun f -> iter f first

  (** Iterator on the elements of a partition, grouped by their classes *)
  let of_partition (p : 'a t) : 'a Elt.t iterator iterator =
    let first = p.blocks_head in
    let iter f class_ =
      Block.iter_blocks (fun class_ -> f (of_class class_)) class_
    in
    fun f -> iter f first
end

(** Iterator on the elements of a partition, grouped by their classes *)
let iterator = Iterator.of_partition

exception Empty
(** Exception that is raised by [pop_work_list] *)

(** Picks a block in the work list and removes it from the work list. If the
    list was empty, the exception [Empty] is raised. *)
let pop_work_list (part : 'a t) : 'a Elt.t iterator =
  Check.check ~loose:false part;
  match part.work_list_head with
  | None -> raise Empty
  | Some hd ->
      part.work_list_head <-
        (if hd.work_list_next == hd then None else Some hd.work_list_next);
      hd.in_work_list <- false;
      hd.work_list_next <- hd;
      Check.check ~loose:false part;
      Iterator.of_class hd

(** Stabilization loop: as long as there is a block in the work list, compute
    its pre-images, and refine the partition with the pre-images (which may
    replace some block with new blocks (of strictly smaller sizes) or remove
    some other blocks). *)
let rec stabilize_loop
    (get_pre_images : 'a Elt.t iterator -> 'a Elt.t iterator iterator)
    (part : 'a t) : unit =
  match pop_work_list part with
  | exception Empty -> ()
  | class_iter ->
      Check.check ~loose:false part;
      let split_sets_iter = get_pre_images class_iter in
      split_sets_iter (refine part);
      Check.check ~loose:false part;
      stabilize_loop get_pre_images part

(** [stabilize pre_image p] refines the partition [p] into the coarsest
    congruence that is finer that [p]. It supposes the existence of a partial
    function [next : lab -> 'a Elt.t -> 'a Elt.t] where [b] is a type of labels,
    that denotes a transition function (think of a finite automaton, for
    example). The argument [pre_image S] must denote the set of pre-images of
    [f], i.e.: [⋃ₗ { { e | f l e ∈ S } }] (from which the empty sets may be
    removed). As a consequence, for every [S' ∈ pre_image S], there exists a
    label [l] such that for every [e' ∈ S'], [f l e' ∈ S].

    The refined partition [p'] that is computed is a congruence, i.e.: for every
    label [l] and elements [e1] and [e2], if [e1] and [e2] belong to some class
    [C] of [p'], then [f l e1] and [f l e2] belong to some class [C'] of [p'].
    Moreover, [p'] must be the coarsest such partition, that is finer than [p],
    i.e., such that every classe of [p'] is a subset of a class of [p]. *)
let stabilize (get_pre_images : 'a Elt.t iterator -> 'a Elt.t iterator iterator)
    (part : 'a t) : unit =
  Check.check ~loose:false part;
  stabilize_loop get_pre_images part;
  Check.check ~loose:false part

(** Pretty printer *)
let pp (pp_elt : Format.formatter -> 'a -> unit) fmt (part : 'a t) =
  let open Format in
  fprintf fmt "[@[@ ";
  let first_class = ref true in
  Iterator.of_partition part (fun class_iter ->
      if !first_class then first_class := false else pp_print_space fmt ();
      fprintf fmt "[@[@ ";
      let first_elt = ref true in
      class_iter (fun elt ->
          if !first_elt then first_elt := false else pp_print_space fmt ();
          fprintf fmt "@[%a@]" pp_elt (Elt.get elt));
      fprintf fmt "@ @]]");
  fprintf fmt "@ @]]"

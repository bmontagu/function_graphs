(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2020-2024
*)

module type HashableMap = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val find_opt : key -> 'a t -> 'a option
  val remove : key -> 'a t -> 'a t
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
  val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val bindings : 'a t -> (key * 'a) list

  val hash_fold :
    (Base.Hash.state -> 'a -> Base.Hash.state) ->
    Base.Hash.state ->
    'a t ->
    Base.Hash.state

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module Make
    (Constants : AbstractDomains.Constants.S)
    (MFun : HashableMap)
    (MConstruct : HashableMap)
    (MOPP : HashableMap)
    (Env : HashableMap)
    (MField : HashableMap)
    () =
struct
  type t = raw_t Hcons.hash_consed
  (** The type definition for abstract values. The encoding follows the
      locally-nameless style of binders. *)

  and raw_t =
    | Abs of {
        height : int;
        constants : Constants.t;
        closures : closures;
        constructs : constructs;
        structures : structures;
      }
    | Mu of int (* height *) * t  (** recursive knot *)
    | Var of int  (** free variable, where the provided integer is a name *)
    | Idx of int
        (** bound variable, where the provided integer is a de Bruijn index *)

  and closures = Clos of int (* height *) * t Env.t MFun.t | ClosTop

  and constructs =
    | Constructs of int (* height *) * t list MConstruct.t
    | ConstructsTop

  and structures =
    | Struct of int (* height *) * t MField.t MOPP.t
      (* the map contains the entries that _must_ be present *)
    | StructTop

  let compare t1 t2 = Int.compare (Hcons.tag t1) (Hcons.tag t2)
  let equal = ( == )
  let hash_fold s t = Base.Hash.fold_int s (Hcons.tag t)
  let hash t = Base.Hash.of_fold hash_fold t

  module T = struct
    type t = raw_t

    let closures_equal c1 c2 =
      c1 == c2
      ||
      match (c1, c2) with
      | ClosTop, ClosTop -> true
      | Clos (_, m1), Clos (_, m2) ->
          m1 == m2
          || MFun.equal (fun e1 e2 -> e1 == e2 || Env.equal ( == ) e1 e2) m1 m2
      | _, _ -> false

    let constructs_equal t1 t2 =
      t1 == t2
      ||
      match (t1, t2) with
      | ConstructsTop, ConstructsTop -> true
      | Constructs (_, m1), Constructs (_, m2) ->
          m1 == m2
          || MConstruct.equal
               (fun l1 l2 -> l1 == l2 || List.equal ( == ) l1 l2)
               m1 m2
      | _, _ -> false

    let structures_equal str1 str2 =
      str1 == str2
      ||
      match (str1, str2) with
      | StructTop, StructTop -> true
      | Struct (_, m1), Struct (_, m2) ->
          m1 == m2
          || MOPP.equal
               (fun e1 e2 -> e1 == e2 || MField.equal ( == ) e1 e2)
               m1 m2
      | _, _ -> false

    let equal t1 t2 =
      match (t1, t2) with
      | ( Abs
            {
              height = _;
              constants = v1;
              closures = c1;
              constructs = t1;
              structures = str1;
            },
          Abs
            {
              height = _;
              constants = v2;
              closures = c2;
              constructs = t2;
              structures = str2;
            } ) ->
          Constants.equal v1 v2 && closures_equal c1 c2
          && constructs_equal t1 t2 && structures_equal str1 str2
      | Mu (_, t1), Mu (_, t2) -> t1 == t2
      | Var x1, Var x2 -> x1 = x2
      | Idx i1, Idx i2 -> i1 = i2
      | _, _ -> false

    let closures_hash_fold acc c =
      match c with
      | ClosTop -> Base.Hash.fold_int acc 0
      | Clos (_, m) ->
          Base.Hash.(
            fold_int
              (MFun.hash_fold
                 (fun acc env ->
                   Env.hash_fold
                     (fun acc t -> fold_int acc (Hcons.tag t))
                     acc env)
                 acc m)
              1)

    let constructs_hash_fold acc t =
      match t with
      | ConstructsTop -> Base.Hash.fold_int acc 0
      | Constructs (_, m) ->
          Base.Hash.(
            fold_int
              (MConstruct.hash_fold
                 (List.fold_left (fun acc t -> fold_int acc (Hcons.tag t)))
                 acc m)
              1)

    let structures_hash_fold acc str =
      match str with
      | StructTop -> Base.Hash.fold_int acc 0
      | Struct (_, mopp) ->
          Base.Hash.(
            fold_int
              (MOPP.hash_fold
                 (fun acc env ->
                   MField.hash_fold
                     (fun acc t -> fold_int acc (Hcons.tag t))
                     acc env)
                 acc mopp)
              1)

    let hash_fold s t =
      match t with
      | Abs { height = _; constants; closures; constructs; structures } ->
          Base.Hash.fold_int
            (Constants.hash_fold
               (closures_hash_fold
                  (constructs_hash_fold
                     (structures_hash_fold s structures)
                     constructs)
                  closures)
               constants)
            0
      | Mu (_, t) -> Base.Hash.(fold_int (fold_int s (Hcons.tag t)) 1)
      | Var x -> Base.Hash.(fold_int (fold_int s x) 2)
      | Idx i -> Base.Hash.(fold_int (fold_int s i) 3)

    let hash t = Base.Hash.of_fold hash_fold t
  end

  module HT = Hcons.Make (T) ()

  let hcons_tbl = HT.create 127
  let build v = HT.hashcons hcons_tbl v
  let height_clos = function Clos (h, _) -> h | ClosTop -> 0
  let height_constructs = function Constructs (h, _) -> h | ConstructsTop -> 0
  let height_structs = function Struct (h, _) -> h | StructTop -> 0

  let height t =
    match Hcons.node t with
    | Abs { height; _ } | Mu (height, _) -> height
    | Var _ | Idx _ -> 0

  let make ~constants ~closures ~constructs ~structures =
    build
    @@ Abs
         {
           height =
             max (if Constants.is_bot constants then 0 else 1)
             @@ max (height_clos closures)
             @@ max (height_constructs constructs)
             @@ height_structs structures;
           constants;
           closures;
           constructs;
           structures;
         }

  let clos_bot = Clos (0, MFun.empty)
  let clos_top = ClosTop
  let constructs_bot = Constructs (0, MConstruct.empty)
  let constructs_top = ConstructsTop
  let struct_bot = Struct (0, MOPP.empty)
  let struct_top = StructTop

  let bot : t =
    make ~constants:Constants.bot ~closures:clos_bot ~constructs:constructs_bot
      ~structures:struct_bot

  let top : t =
    make ~constants:Constants.top ~closures:clos_top ~constructs:constructs_top
      ~structures:struct_top

  let clos_is_bot = function Clos (_, m) -> MFun.is_empty m | ClosTop -> false

  let constructs_is_bot = function
    | Constructs (_h, m) -> MConstruct.is_empty m
    | ConstructsTop -> false

  let struct_is_bot = function
    | Struct (_h, mopp) -> MOPP.is_empty mopp
    | StructTop -> false

  let is_bot t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } ->
        Constants.is_bot constants && clos_is_bot closures
        && constructs_is_bot constructs
        && struct_is_bot structures
    | Mu _ | Var _ | Idx _ -> false

  let clos_is_top = function ClosTop -> true | Clos _ -> false

  let constructs_is_top = function
    | Constructs _ -> false
    | ConstructsTop -> true

  let struct_is_top = function Struct _ -> false | StructTop -> true

  let is_top t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } ->
        Constants.is_top constants && clos_is_top closures
        && constructs_is_top constructs
        && struct_is_top structures
    | Mu _ | Var _ | Idx _ -> false

  let rec is_constant k t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } ->
        Constants.only_has_key k constants
        && clos_is_bot closures
        && constructs_is_bot constructs
        && struct_is_bot structures
    | Mu (_, t) -> is_constant k t
    | Var _ | Idx _ -> assert false

  let rec get_constant open0 k getter t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } ->
        let ints, constants = Constants.get_val k getter constants in
        (ints, make ~constants ~closures ~constructs ~structures)
    | Mu (_h, t') -> get_constant open0 k getter (open0 t t')
    | Var _ | Idx _ -> assert false

  let rec get_constants open0 k t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } ->
        let ints, constants = Constants.get k constants in
        (ints, make ~constants ~closures ~constructs ~structures)
    | Mu (_h, t') -> get_constants open0 k (open0 t t')
    | Var _ | Idx _ -> assert false

  let normalize_clos_map m =
    MFun.filter (fun _f env -> Env.for_all (fun _x v -> not @@ is_bot v) env) m

  let compute_clos_height m =
    if MFun.is_empty m then 0
    else
      succ
      @@ MFun.fold
           (fun _ env acc ->
             Env.fold (fun _x tx acc -> max (height tx) acc) env acc)
           m 0

  let clos m =
    let m = normalize_clos_map m in
    let h = compute_clos_height m in
    Clos (h, m)

  let rec get_closures open0 t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures } -> (
        let rem = make ~constants ~closures:clos_bot ~constructs ~structures in
        match closures with
        | ClosTop -> Some (None, rem)
        | Clos (_h, m) ->
            let l = MFun.fold (fun f env acc -> (f, env) :: acc) m [] in
            Some (Some l, rem))
    | Mu (_h, t') -> get_closures open0 (open0 t t')
    | Var _ | Idx _ -> assert false

  let rec closures_join_map open0 bot join fclos_top fclos t =
    match Hcons.node t with
    | Abs { closures; _ } -> (
        match closures with
        | ClosTop -> fclos_top
        | Clos (_h, m) -> MFun.fold (fun t c acc -> join (fclos t c) acc) m bot)
    | Mu (_h, t0) ->
        closures_join_map open0 bot join fclos_top fclos (open0 t t0)
    | Idx _ | Var _ ->
        (* this case should never happen *)
        assert false

  let normalize_constructs_map m =
    MConstruct.filter_map
      (fun _i l ->
        if List.for_all (fun v -> not @@ is_bot v) l then Some l else None)
      m

  let compute_constructs_height m =
    if MConstruct.is_empty m then 0
    else
      succ
      @@ MConstruct.fold
           (fun _c l acc ->
             max acc (List.fold_left (fun h v -> max h (height v)) 0 l))
           m 0

  let constructs m =
    let m = normalize_constructs_map m in
    let h = compute_constructs_height m in
    Constructs (h, m)

  let rec get_construct open0 is_singleton c t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs = t'; structures } -> (
        match t' with
        | ConstructsTop -> Some (None, t)
        | Constructs (_h, m) -> (
            match MConstruct.find_opt c m with
            | None -> None
            | Some vs ->
                let rem =
                  let constructs =
                    if is_singleton then constructs @@ MConstruct.remove c m
                    else t'
                  in
                  make ~constants ~closures ~constructs ~structures
                in
                Some (Some vs, rem)))
    | Mu (_h, t') -> get_construct open0 is_singleton c (open0 t t')
    | Var _ | Idx _ -> assert false

  let normalize_struct_map m =
    MOPP.filter
      (fun _opp env -> MField.for_all (fun _x v -> not @@ is_bot v) env)
      m

  let compute_struct_height m =
    if MOPP.is_empty m then 0
    else
      succ
      @@ MOPP.fold
           (fun _ env acc ->
             MField.fold (fun _x tx acc -> max (height tx) acc) env acc)
           m 0

  let struct_ m =
    let m = normalize_struct_map m in
    let h = compute_struct_height m in
    Struct (h, m)

  let rec get_structures open0 t =
    match Hcons.node t with
    | Abs { height = _; constants; closures; constructs; structures = t' } -> (
        match t' with
        | StructTop ->
            let rem = t in
            Some (None, rem)
        | Struct (_h, m) ->
            let vs = MOPP.bindings m in
            let rem =
              make ~constants ~closures ~constructs ~structures:struct_bot
            in
            Some (Some vs, rem))
    | Mu (_h, t') -> get_structures open0 (open0 t t')
    | Var _ | Idx _ -> assert false

  let rec update_structures open0 update t =
    match Hcons.node t with
    | Abs { structures = StructTop; _ } -> t
    | Abs { constants; closures; constructs; structures = Struct (_h, m); _ } ->
        let m' = MOPP.filter_map (fun _ -> update) m in
        if m == m' then t
        else make ~constants ~closures ~constructs ~structures:(struct_ m')
    | Mu (_h, t0) -> update_structures open0 update (open0 t t0)
    | Idx _ | Var _ ->
        (* this case should never happen *)
        assert false

  (** Transforms an integer into a lowercase letter, possibly with an integer
      suffix *)
  let int_to_char i = (Char.chr (Char.code 'a' + (i mod 26)), i / 26)

  let pp_int_var prefix fmt x =
    let c, n = int_to_char x in
    if n = 0 then Format.fprintf fmt "%s%c" prefix c
    else Format.fprintf fmt "%s%c%i" prefix c n

  let pp fmt v =
    let n = ref 0 in
    let rec pp env fmt v =
      if is_bot v then Format.fprintf fmt "⊥"
      else if is_top v then Format.fprintf fmt "⊤"
      else
        match Hcons.node v with
        | Abs { height = _; constants; closures; constructs; structures } ->
            let open Format in
            fprintf fmt "@[<hv 0>{@[<hv 1>";
            if not @@ Constants.is_bot constants then
              fprintf fmt "@ @[%a@]" Constants.pp constants;
            if not @@ clos_is_bot closures then
              fprintf fmt "@ @[closures =@ %a@]" (pp_closures env) closures;
            if not @@ constructs_is_bot constructs then
              fprintf fmt "@ @[constructs =@ %a@]" (pp_constructs env)
                constructs;
            if not @@ struct_is_bot structures then
              fprintf fmt "@ @[structures =@ %a@]" (pp_structures env)
                structures;
            fprintf fmt "@]@ }@]"
        | Var x -> pp_int_var "_" fmt x
        | Idx n -> (
            try Format.pp_print_string fmt (Ralist.nth env n)
            with Not_found ->
              (* this should never happen *)
              Format.fprintf fmt "#UNBOUND_VARIABLE_%i#" n)
        | Mu (_h, t) ->
            let var = Format.asprintf "%a" (pp_int_var "'") !n in
            incr n;
            Format.fprintf fmt "@[%a@ as %s@]" (pp (Ralist.cons var env)) t var;
            decr n
    and pp_closures env fmt = function
      | ClosTop -> Format.pp_print_string fmt "⊤"
      | Clos (_h, m) -> MFun.pp (Env.pp (pp env)) fmt m
    and pp_constructs env fmt = function
      | ConstructsTop -> Format.pp_print_string fmt "⊤"
      | Constructs (_h, m) ->
          MConstruct.pp
            (fun fmt lc ->
              if ListExtra.is_empty lc then Format.pp_print_string fmt " ."
              else
                Format.fprintf fmt "@ %a"
                  (Format.pp_print_list
                     ~pp_sep:(fun fmt () -> Format.fprintf fmt "@ × ")
                     (pp env))
                  lc)
            fmt m
    and pp_structures env fmt = function
      | StructTop -> Format.pp_print_string fmt "⊤"
      | Struct (_h, m) ->
          if MOPP.is_empty m then Format.pp_print_string fmt "∅"
          else
            Format.fprintf fmt "@[<hv 0>{@[<hv 1> %a@]@ }@]"
              (Format.pp_print_list
                 ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
                 (fun fmt (_mopp, mx) ->
                   Format.fprintf fmt "@[%a@]" (MField.pp (pp env)) mx))
              (MOPP.bindings m)
    in
    pp Ralist.nil fmt v

  (** [is_free_var x t] tells whether [x] is a free variable in [t] *)
  let rec is_free_var x t =
    match Hcons.node t with
    | Idx _ -> false
    | Var y -> x = y
    | Mu (_, t) -> is_free_var x t
    | Abs { height = _; constants = _; closures; constructs; structures } ->
        clos_is_free_var x closures
        || constructs_is_free_var x constructs
        || struct_is_free_var x structures

  and clos_is_free_var x = function
    | ClosTop -> false
    | Clos (_, m) ->
        MFun.exists (fun _ env -> Env.exists (fun _ t -> is_free_var x t) env) m

  and constructs_is_free_var x = function
    | ConstructsTop -> false
    | Constructs (_, m) ->
        MConstruct.exists
          (fun _ l -> List.exists (fun t -> is_free_var x t) l)
          m

  and struct_is_free_var x = function
    | StructTop -> false
    | Struct (_, m) ->
        MOPP.exists
          (fun _ env -> MField.exists (fun _ t -> is_free_var x t) env)
          m

  (** [check_wf bvar fvar_opt check n t] checks whether [t] is locally-closed at
      level [n] (i.e. locally-closed for [n = 0]), and raises exception [bvar]
      otherwise. Additionally, it checks whether [t] has no free variable when
      [fvar_opt = Some fvar], and raises exception [fvar] otherwise. *)
  let check_wf bvar fvar_opt check =
    let clos_check lvl = function
      | ClosTop -> ()
      | Clos (_, m) -> MFun.iter (fun _ -> Env.iter (fun _ -> check lvl)) m
    and constructs_check lvl = function
      | ConstructsTop -> ()
      | Constructs (_, m) -> MConstruct.iter (fun _ -> List.iter (check lvl)) m
    and struct_check lvl = function
      | StructTop -> ()
      | Struct (_, m) -> MOPP.iter (fun _ -> MField.iter (fun _ -> check lvl)) m
    in
    fun lvl t ->
      match Hcons.node t with
      | Var _ -> Option.iter raise fvar_opt
      | Idx n -> if not (0 <= n && n < lvl) then raise bvar
      | Mu (_, t) -> check (succ lvl) t
      | Abs { height = _; constants = _; closures; constructs; structures } ->
          clos_check lvl closures;
          constructs_check lvl constructs;
          struct_check lvl structures

  (** [close n x t] turns the variable [x] into a de Bruijn binder in [t]
      considered at depth [n], and returns [true] iff [x] was found. This
      function is only used by the function [mu] defined hereafter. *)
  let close n x t =
    let found = ref false in
    let rec close_aux n x t =
      match Hcons.node t with
      | Var y ->
          if x = y then (
            found := true;
            build @@ Idx n)
          else t
      | Idx m -> if m < n then t else build @@ Idx (succ m)
      | Mu (h, t) -> build @@ Mu (h, close_aux (succ n) x t)
      | Abs { height; constants; closures; constructs; structures } ->
          build
          @@ Abs
               {
                 height;
                 constants;
                 closures = close_closures_aux n x closures;
                 constructs = close_constructs_aux n x constructs;
                 structures = close_structures_aux n x structures;
               }
    and close_closures_aux n x = function
      | ClosTop as c -> c
      | Clos (h, m) -> Clos (h, MFun.map (Env.map (close_aux n x)) m)
    and close_constructs_aux n x = function
      | ConstructsTop as t -> t
      | Constructs (h, m) ->
          Constructs (h, MConstruct.map (List.map (close_aux n x)) m)
    and close_structures_aux n x = function
      | StructTop as c -> c
      | Struct (h, m) -> Struct (h, MOPP.map (MField.map (close_aux n x)) m)
    in
    let t' = close_aux n x t in
    (t', !found)

  (** [mu check close x t] creates a recursive abstract value, by binding the
      variable [x] in the abstract value [t]. If [n] is not found in [t], then
      [t] is returned. Avoids the creation of non-contractive values. *)
  let mu check close x t =
    let res =
      match Hcons.node t with
      | Var y -> if x = y then bot else t
      | Idx _ -> assert false
      | _ ->
          let t', found = close x t in
          if found then build @@ Mu (height t', t') else t
    in
    if check && is_free_var x res then
      Format.eprintf
        "ERROR in NablaRec.mu (res): variable %a should not be free \
         in@.@[%a@]@."
        (pp_int_var "_") x pp t;
    res

  (** [open_rec t' n t] substitutes the de Bruijn index [n] in [t] with the
      locally closed abstract value [t']. *)
  let rec open_rec t' n t =
    match Hcons.node t with
    | Var _ -> t
    | Idx m -> if m < n then t else if m = n then t' else build @@ Idx (pred m)
    | Mu (_h, t) ->
        let t1 = open_rec t' (succ n) t in
        build @@ Mu (height t1, t1)
    | Abs { height = _; constants; closures; constructs; structures } ->
        make ~constants
          ~closures:(open_rec_closures t' n closures)
          ~constructs:(open_rec_constructs t' n constructs)
          ~structures:(open_rec_structures t' n structures)

  and open_rec_closures t' n c =
    match c with
    | ClosTop as c -> c
    | Clos (_h, m) -> clos @@ MFun.map (Env.map (open_rec t' n)) m

  and open_rec_constructs t' n c =
    match c with
    | ConstructsTop as t -> t
    | Constructs (_h, m) ->
        constructs @@ MConstruct.map (List.map (open_rec t' n)) m

  and open_rec_structures t' n c =
    match c with
    | StructTop as c -> c
    | Struct (_h, m) -> struct_ @@ MOPP.map (MField.map (open_rec t' n)) m

  (** [free_idx fidx t] returns the set of de Bruijn indices that correspond to
      "free variables" in [t] *)
  let free_idx fidx =
    let free_idx_closures c =
      match c with
      | ClosTop -> Ptset.empty
      | Clos (_h, m) ->
          MFun.fold
            (fun _f env acc ->
              Env.fold (fun _x t' acc -> Ptset.union acc @@ fidx t') env acc)
            m Ptset.empty
    and free_idx_constructs c =
      match c with
      | ConstructsTop -> Ptset.empty
      | Constructs (_h, m) ->
          MConstruct.fold
            (fun _c ts acc ->
              List.fold_left (fun acc t' -> Ptset.union acc @@ fidx t') acc ts)
            m Ptset.empty
    and free_idx_structures c =
      match c with
      | StructTop -> Ptset.empty
      | Struct (_h, m) ->
          MOPP.fold
            (fun _opp m acc ->
              MField.fold (fun _ t' acc -> Ptset.union acc @@ fidx t') m acc)
            m Ptset.empty
    in
    fun t ->
      match Hcons.node t with
      | Var _ -> Ptset.empty
      | Idx m -> Ptset.singleton m
      | Mu (_h, t) -> fidx t |> Ptset.remove 0 |> Ptset.map pred
      | Abs { height = _; constants = _; closures; constructs; structures } ->
          Ptset.union (free_idx_closures closures)
          @@ Ptset.union (free_idx_constructs constructs)
          @@ free_idx_structures structures
end

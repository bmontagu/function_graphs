(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2024
*)

(** Interface of maps that are expected to implement the NablaRec abstract
    domain *)
module type HashableMap = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val mem : key -> 'a t -> bool
  val remove : key -> 'a t -> 'a t
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
  val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val bindings : 'a t -> (key * 'a) list

  val hash_fold :
    (Base.Hash.state -> 'a -> Base.Hash.state) ->
    Base.Hash.state ->
    'a t ->
    Base.Hash.state

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

(** Interface of types that have a total order and a pretty printer *)
module type PrettyOrderedType = sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end

(** Helper functor that creates a hashable map *)
module MakeHashableMap (X : sig
  include PrettyOrderedType

  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
end) : HashableMap with type key = X.t

(** Generic version of the NablaRec abstract domain *)
module Make
    (_ : sig
      val minimize : bool
      val occurs_threshold : int
      val sim_threshold : int

      val debug_shrinking : bool
      (** Whether to print extra information about the shrinking phase *)

      val check_wf : bool
      (** Whether to perform extra checking for wellformedness *)

      val monotonic_join : bool
    end)
    (Constants : AbstractDomains.Constants.S)
    (Fun : sig
      include PrettyOrderedType

      val is_recursive : t -> bool
    end)
    (_ : HashableMap with type key = Fun.t)
    (Construct : sig
      include PrettyOrderedType

      val is_singleton : t -> bool
      val is_constant : t -> bool
    end)
    (_ : HashableMap with type key = Construct.t)
    (OPP : Map.OrderedType)
    (_ : HashableMap with type key = OPP.t)
    (Var : Map.OrderedType)
    (Env : HashableMap with type key = Var.t)
    (Field : PrettyOrderedType)
    (MField : HashableMap with type key = Field.t)
    () : sig
  include AbstractDomains.Sigs.LATTICE

  val sim : t -> t -> bool
  val singleton_constant : Constants.t -> t
  val is_constant : 'a Constants.key -> t -> bool
  val get_constant : 'a Constants.key -> ('a -> 'a * 'a) -> t -> 'a * t
  val get_constants : 'a Constants.key -> t -> 'a * t
  val singleton_construct : Construct.t -> t list -> t
  val get_construct : Construct.t -> t -> (t list option * t) option
  val singleton_closure : Fun.t -> t Env.t -> t
  val get_closures : t -> ((Fun.t * t Env.t) list option * t) option

  val closures_join_map :
    'a -> ('a -> 'a -> 'a) -> 'a -> (Fun.t -> t Env.t -> 'a) -> t -> 'a

  val may_recursively_contain_closures : t -> bool

  type (-'a, +'b) memo_fun

  val apply_memo : ('a, 'b) memo_fun -> 'a -> 'b

  val collect_in_constants :
    of_constants:(Constants.t -> 'a) ->
    bot:'a ->
    top:'a ->
    join:('a -> 'a -> 'a) ->
    (t, 'a) memo_fun

  val singleton_structure : OPP.t -> t MField.t -> t
  val get_structures : t -> ((OPP.t * t MField.t) list option * t) option
  val update_structures : (t MField.t -> t MField.t option) -> t -> t
end

(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2024
*)

type (-'a, +'b) memo_fun

val apply : ('a, 'b) memo_fun -> 'a -> 'b

module type HASHEDTYPE = sig
  type raw_t

  include Hashtbl.HashedType with type t = raw_t Hcons.hash_consed

  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
end

module Make (T : HASHEDTYPE) : sig
  (** Memoization helpers *)

  val memo_int : (int -> 'a) -> (int, 'a) memo_fun
  (** [memo_int f] returns a memoized function [g] such that [g n] is [f n], for
      all [int] [n]. *)

  module HT : Hashtbl.S with type key = T.t

  val memo : (T.t -> 'a) -> (T.t, 'a) memo_fun
  (** [memo f] returns a memoized function [g] such that [g x] is [f x], for all
      [x] of type [T.t]. *)

  val memo_rec : ((T.t -> 'a) -> T.t -> 'a) -> (T.t, 'a) memo_fun
  (** [memo_rec] takes an openly recursively defined function [f] and returns a
      closed memoized function [g] such that [g x] is [f g x], for all [x] of
      type [T.t]. *)

  val memo2 : (T.t -> T.t -> 'a) -> (T.t, T.t -> 'a) memo_fun
  (** [memo2 f] returns a memoized function [g] such that [g x y] is [f x y],
      for all [x] and [y] of type [T.t]. *)

  val memo2_rec :
    ((T.t -> T.t -> 'a) -> T.t -> T.t -> 'a) -> (T.t, T.t -> 'a) memo_fun
  (** [memo2_rec f] takes an openly recursively defined function [f] and returns
      a closed memoized function [g] such that [g x1 x2] computes [f g x1 x2]
      only once, for all [x1] and [x2] of type [T.t]. *)

  val memo_int_t : (int -> T.t -> 'a) -> (int, T.t -> 'a) memo_fun
  (** [memo_int_t f] returns a memoized function [g] such that [g n x] is
      [f n x], for all [int] [n] and all [x] of type [T.t]. *)

  val memo_rec_int_t_unit :
    ((int -> T.t -> unit) -> int -> T.t -> unit) -> (int, T.t -> unit) memo_fun
  (** [memo_rec_int_t_unit] takes an openly recursively defined function [f] and
      returns a closed memoized function [g] such that [g n x] computes
      [f g n x] only once, for all [int] [n] and all [x] of type [T.t]. *)
end

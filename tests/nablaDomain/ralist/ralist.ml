(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** Random Access Lists, inspired by Chris Okasaki's "Purely Functional Data
    Structures"
    https://www.cambridge.org/fr/academic/subjects/computer-science/programming-languages-and-applied-logic/purely-functional-data-structures?format=PB&isbn=9780521663502

    [cons], [uncons], [nth] and [nth_opt] have a logarithmic time complexity. *)

(** Type of random access lists. The following invariant is maintained: there is
    never [Cons0 Nil] in well formed lists. *)
type 'a t =
  | Nil  (** Empty list *)
  | Cons0 of ('a * 'a) t
      (** Shift, that corresponds to "fun x -> x * 2" in integers *)
  | Cons1 of 'a * ('a * 'a) t
      (** Shift, that corresponds to "fun x -> x * 2 + 1" in integers *)

let nil = Nil
let empty = nil
let is_empty = function Nil -> true | _ -> false

let rec cons : 'a. 'a -> 'a t -> 'a t =
 fun x l ->
  match l with
  | Nil -> Cons1 (x, nil)
  | Cons0 l0 -> Cons1 (x, l0)
  | Cons1 (y, l1) -> Cons0 (cons (x, y) l1)

let rec uncons : 'a. 'a t -> ('a * 'a t) option = function
  | Nil -> None
  | Cons0 l0 -> (
      match uncons l0 with
      | None as n -> n
      | Some ((x1, x2), l0') -> Some (x1, Cons1 (x2, l0')))
  | Cons1 (x, Nil) -> Some (x, nil)
  | Cons1 (x, l1) -> Some (x, Cons0 l1)

let lift_fcons f (x, y) l = f x (Cons1 (y, l))

let rec match_ : 'a 'b. 'a t -> (unit -> 'b) -> ('a -> 'a t -> 'b) -> 'b =
 fun l fnil fcons ->
  match l with
  | Nil -> fnil ()
  | Cons0 l0 -> match_ l0 fnil (lift_fcons fcons)
  | Cons1 (x, Nil) -> fcons x nil
  | Cons1 (x, l1) -> fcons x (Cons0 l1)

let rec hd : 'a. 'a t -> 'a = function
  | Nil -> raise (Failure "Ralist.hd")
  | Cons0 l -> fst @@ hd l
  | Cons1 (x, _) -> x

let rec tl : 'a. 'a t -> 'a t = function
  | Nil -> raise (Failure "Ralist.tl")
  | Cons0 l -> ( match tl l with Nil -> nil | l' -> Cons0 l')
  | Cons1 (_, Nil) -> nil
  | Cons1 (_, l) -> Cons0 l

let rec nth : 'a. 'a t -> int -> 'a =
 fun l i ->
  match l with
  | Nil -> raise (Failure "Ralist.nth")
  | Cons1 (x, l1) ->
      if i = 0 then x
      else
        let i = i - 1 in
        let x1, x2 = nth l1 (i / 2) in
        if i mod 2 = 0 then x1 else x2
  | Cons0 l0 ->
      let x1, x2 = nth l0 (i / 2) in
      if i mod 2 = 0 then x1 else x2

let nth l n = if n < 0 then raise (Invalid_argument "Ralist.nth") else nth l n

let rec nth_opt : 'a. 'a t -> int -> 'a option =
 fun l i ->
  match l with
  | Nil -> None
  | Cons1 (x, l1) -> (
      if i = 0 then Some x
      else
        match nth_opt l1 (i - 1) with Some (x, _) -> Some x | None as n -> n)
  | Cons0 l0 -> (
      match nth_opt l0 (i / 2) with
      | Some (x1, x2) -> Some (if i mod 2 = 0 then x1 else x2)
      | None as n -> n)

let nth_opt l n =
  if n < 0 then raise (Invalid_argument "Ralist.nth_opt") else nth_opt l n

let rec init_aux i n f =
  if i = n then nil else cons (f i) (init_aux (i + 1) n f)

let init n f =
  if n < 0 then raise (Invalid_argument "Ralist.init") else init_aux 0 n f

let lift_map f (x, y) = (f x, f y)

let rec map : 'a 'b. ('a -> 'b) -> 'a t -> 'b t =
 fun f l ->
  match l with
  | Nil -> nil
  | Cons0 l0 -> Cons0 (map (lift_map f) l0)
  | Cons1 (x, l1) -> Cons1 (f x, map (lift_map f) l1)

let lift_mapi f i (x1, x2) =
  let j, v1 = f i x1 in
  let k, v2 = f j x2 in
  (k, (v1, v2))

let rec mapi_aux : 'a 'b. int -> (int -> 'a -> int * 'b) -> 'a t -> 'b t =
 fun i f l ->
  match l with
  | Nil -> nil
  | Cons0 l -> Cons0 (mapi_aux i (lift_mapi f) l)
  | Cons1 (x, l) ->
      let j, v = f i x in
      Cons1 (v, mapi_aux j (lift_mapi f) l)

let mapi f l =
  let g i x = (i + 1, f i x) in
  mapi_aux 0 g l

let lift_fold_right f (x, y) acc = f x (f y acc)

let rec fold_right : 'a 'b. ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b =
 fun f l acc ->
  match l with
  | Nil -> acc
  | Cons0 l0 -> fold_right (lift_fold_right f) l0 acc
  | Cons1 (x, l1) -> f x (fold_right (lift_fold_right f) l1 acc)

let lift_fold_left f acc (x, y) = f (f acc x) y

let rec fold_left : 'a 'b. ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b =
 fun f acc l ->
  match l with
  | Nil -> acc
  | Cons0 l0 -> fold_left (lift_fold_left f) acc l0
  | Cons1 (x, l1) -> fold_left (lift_fold_left f) (f acc x) l1

let rec length : 'a. 'a t -> int = function
  | Nil -> 0
  | Cons0 l -> 2 * length l
  | Cons1 (_, l) -> 1 + (2 * length l)

let to_list l = fold_right List.cons l []
let of_list l = List.fold_right cons l nil
let append l1 l2 = fold_right cons l1 l2
let rev_append l1 l2 = fold_left (fun l x -> cons x l) l2 l1
let rev l = rev_append l nil
let flatten l = fold_right append l nil
let concat = flatten

let lift_iter f (x1, x2) =
  f x1;
  f x2

let rec iter : 'a. ('a -> unit) -> 'a t -> unit =
 fun f l ->
  match l with
  | Nil -> ()
  | Cons0 l0 -> iter (lift_iter f) l0
  | Cons1 (x, l1) ->
      f x;
      iter (lift_iter f) l1

let lift_iteri f i (x1, x2) =
  let j = f i x1 in
  f j x2

let rec iteri_aux : 'a. int -> (int -> 'a -> int) -> 'a t -> unit =
 fun i f l ->
  match l with
  | Nil -> ()
  | Cons0 l0 -> iteri_aux i (lift_iteri f) l0
  | Cons1 (x, l1) ->
      let j = f i x in
      iteri_aux j (lift_iteri f) l1

let iteri f l =
  let g i x =
    f i x;
    i + 1
  in
  iteri_aux 0 g l

let lift_split f (x1, x2) = (f x1, f x2)

let rec split :
    'a 'b 'c 'd.
    ('a * 'b -> 'c) -> ('a * 'b -> 'd) -> ('a * 'b) t -> 'c t * 'd t =
 fun fl fr l ->
  match l with
  | Nil -> (nil, nil)
  | Cons0 l -> (
      match split (lift_split fl) (lift_split fr) l with
      | Nil, Nil -> (nil, nil)
      | Nil, r -> (nil, Cons0 r)
      | l, Nil -> (Cons0 l, nil)
      | l, r -> (Cons0 l, Cons0 r))
  | Cons1 (x, l) ->
      let l, r = split (lift_split fl) (lift_split fr) l in
      (Cons1 (fl x, l), Cons1 (fr x, r))

let split l = split fst snd l
let lift_combine f (a1, a2) (b1, b2) = (f a1 b1, f a2 b2)

let rec combine_aux : 'a 'b 'c. ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t =
 fun f l r ->
  match (l, r) with
  | Nil, Nil -> nil
  | Nil, _ | _, Nil -> raise (Invalid_argument "Ralist.combine")
  | Cons0 l, Cons0 r -> Cons0 (combine_aux (lift_combine f) l r)
  | Cons1 (xl, l), Cons1 (xr, r) ->
      Cons1 (f xl xr, combine_aux (lift_combine f) l r)
  | Cons0 _, Cons1 (_, Nil) | Cons1 (_, Nil), Cons0 _ ->
      raise (Invalid_argument "Ralist.combine")
  | Cons0 l, Cons1 (xr, r) -> (
      match uncons l with
      | None -> assert false
      | Some ((xl1, xl2), l) ->
          cons (f xl1 xr) (combine_aux f (Cons1 (xl2, l)) (Cons0 r)))
  | Cons1 (xl, l), Cons0 r -> (
      match uncons r with
      | None -> assert false
      | Some ((xr1, xr2), r) ->
          cons (f xl xr1) (combine_aux f (Cons0 l) (Cons1 (xr2, r))))

let combine l1 l2 = combine_aux (fun x1 x2 -> (x1, x2)) l1 l2

let rec seq0 : ('a * 'a) Seq.t -> 'a Seq.t =
 fun s () ->
  match s () with
  | Seq.Nil -> Seq.Nil
  | Cons ((x1, x2), xs) -> Seq.cons x1 (Seq.cons x2 (seq0 xs)) ()

let rec to_seq : 'a. 'a t -> 'a Seq.t =
 fun l () ->
  match l with
  | Nil -> Seq.empty ()
  | Cons0 l -> seq0 (to_seq l) ()
  | Cons1 (x, l) -> Seq.cons x (seq0 (to_seq l)) ()

let rec of_seq s =
  match s () with Seq.Nil -> nil | Seq.Cons (x, xs) -> cons x (of_seq xs)

let lift_pp pp ~pp_sep fmt (x1, x2) =
  pp fmt x1;
  pp_sep fmt ();
  pp fmt x2

let pp ?(pp_sep : Format.formatter -> unit -> unit = Format.pp_print_cut) =
  let rec pp :
      'a. (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit =
   fun ppx fmt l ->
    match l with
    | Nil -> ()
    | Cons0 l -> pp (lift_pp ~pp_sep ppx) fmt l
    | Cons1 (x, l) ->
        ppx fmt x;
        pp_sep fmt ();
        pp (lift_pp ~pp_sep ppx) fmt l
  in
  pp

let lift_exists p (x1, x2) = p x1 || p x2

let rec exists : 'a. ('a -> bool) -> 'a t -> bool =
 fun p l ->
  match l with
  | Nil -> false
  | Cons0 l -> exists (lift_exists p) l
  | Cons1 (x, l) -> p x || exists (lift_exists p) l

let lift_for_all p (x1, x2) = p x1 && p x2

let rec for_all : 'a. ('a -> bool) -> 'a t -> bool =
 fun p l ->
  match l with
  | Nil -> true
  | Cons0 l -> for_all (lift_for_all p) l
  | Cons1 (x, l) -> p x && for_all (lift_for_all p) l

(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2020-2023
*)

module type LATTICE = sig
  type t

  val compare : t -> t -> int
  val equal : t -> t -> bool
  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
  val bot : t
  val is_bot : t -> bool
  val top : t
  val is_top : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module type ABS_DOMAIN = sig
  type concrete

  include LATTICE

  val singleton : concrete -> t
  val mem : concrete -> t -> bool
  val get_val : concrete -> t -> t * t
end

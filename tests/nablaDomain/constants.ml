(** @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2024
*)

module type CONSTANTS = sig
  include Dmap.DORDERED

  type tl = [] | ( :: ) : ('a t * 'a) * tl -> tl

  val tops : tl
  val get_module : 'a t -> (module Sigs.LATTICE with type t = 'a)
  val pp : Format.formatter -> _ t -> unit
end

module type S = sig
  type 'a key

  include Sigs.LATTICE

  val find_opt : 'a. 'a key -> t -> 'a option
  val make : 'a. 'a key -> 'a -> t
  val only_has_key : 'a. 'a key -> t -> bool
  val get : 'a. 'a key -> t -> 'a * t
  val get_val : 'a. 'a key -> ('a -> 'a * 'a) -> t -> 'a * t
end

module Make (Constants : CONSTANTS) = struct
  (* In this abstract domain, we enforce the invariant that no entry
     is mapped to bottom. *)

  include Dmap.Make (Constants)

  let compare =
    let fun2 (type a) (k : a key) : a -> a -> int =
      let module M = (val Constants.get_module k) in
      M.compare
    in
    compare { fun2 }

  let equal =
    let fun2 (type a) (k : a key) : a -> a -> bool =
      let module M = (val Constants.get_module k) in
      M.equal
    in
    equal { fun2 }

  let leq d1 d2 =
    let fun1 (type a) (k : a key) (v1 : a) : bool =
      let module M = (val Constants.get_module k) in
      M.is_bot v1
      || match find_opt k d2 with Some v2 -> M.leq v1 v2 | None -> false
    in
    for_all { fun1 } d1

  let hash_fold d s =
    let fun1 (type a) (k : a key) : a -> _ =
      let module M = (val Constants.get_module k) in
      Fun.flip M.hash_fold
    in
    fold { fun1 } s d

  let meet =
    let merge_fun (type a) (k : a key) (ox1 : a option) (ox2 : a option) :
        a option =
      match (ox1, ox2) with
      | None, _ | _, None -> None
      | Some x1, Some x2 ->
          let module M = (val Constants.get_module k) in
          let x12 = M.meet x1 x2 in
          if M.is_bot x12 then None else Some x12
    in
    merge { merge_fun }

  let join =
    let union_fun (type a) (k : a key) (v1 : a) (v2 : a) : a option =
      let module M = (val Constants.get_module k) in
      Some (M.join v1 v2)
    in
    union { union_fun }

  let widen =
    let union_fun (type a) (k : a key) (v1 : a) (v2 : a) : a option =
      let module M = (val Constants.get_module k) in
      Some (M.widen v1 v2)
    in
    union { union_fun }

  let bot = empty

  let top =
    let open Constants in
    let rec make acc = function
      | [] -> acc
      | (k, t) :: l -> make (add k t acc) l
    in
    make empty tops

  let make (type a) (k : a key) (v : a) : t =
    let module M = (val Constants.get_module k) in
    if M.is_bot v then bot else singleton k v

  let is_bot = is_empty
  let is_top m = leq top m
  let only_has_key k d = mem k d && is_empty (remove k d)

  let get (type a) (k : a key) d =
    match find_opt k d with
    | Some v -> (v, remove k d)
    | None ->
        let module M = (val Constants.get_module k) in
        (M.bot, d)

  let get_val (type a) (k : a key) (get : a -> a * a) d =
    let module M = (val Constants.get_module k) in
    match find_opt k d with
    | Some v ->
        let v, rem = get v in
        if M.is_bot rem then (v, remove k d) else (v, add k rem d)
    | None -> (M.bot, d)

  let pp fmt d =
    let is_first = ref true in
    let fun1 (type a) (k : a key) (v : a) : unit =
      let module M = (val Constants.get_module k) in
      if not @@ M.is_bot v then
        if !is_first then (
          is_first := false;
          Format.fprintf fmt "%a =@ %a" Constants.pp k M.pp v)
        else Format.fprintf fmt ",@ %a =@ %a" Constants.pp k M.pp v
    in
    iter { fun1 } d
end

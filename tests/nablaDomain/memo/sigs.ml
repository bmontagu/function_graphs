(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

(** Module signature that contains a simple logging function *)
module type LOG = sig
  val eprintf : ('a, Format.formatter, unit) format -> 'a
end

(** Module signature for types equipped with a total order *)
module type WITH_COMPARE = sig
  type t

  val compare : t -> t -> int
end

(** Module signature for types equipped with a pretty-printer *)
module type WITH_PP = sig
  type t

  val pp : Format.formatter -> t -> unit
end

(** Module signature for parameterized types equipped with a pretty-printer *)
module type WITH_PP1 = sig
  type 'a t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

(** A minimalistic module signature for finite maps *)
module type MAP = sig
  type 'a t
  type key

  val empty : 'a t
  val find_opt : key -> 'a t -> 'a option
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val cardinal : 'a t -> int
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val to_seq : 'a t -> (key * 'a) Seq.t
end

(** A larger module signature for finite maps *)
module type MAP2 = sig
  include MAP

  val fold_similar : (key -> 'a -> 'b -> 'b) -> key -> 'a t -> 'b -> 'b
  (** [fold_similar f k m acc] folds the function [f] over the elements that are
      "similar" to the key [k] in the map [m], starting with accumulator [acc]
  *)
end

(** Module type of dependent maps, i.e. where the type of data may depend on the
    values of the keys *)
module type DMAP = sig
  type t
  type 'a key

  val empty : t
  val find_opt : 'a key -> t -> 'a option
  val update : 'a key -> ('a option -> 'a option) -> t -> t
  val cardinal : t -> int

  include WITH_PP with type t := t
end

(** Abstract domain *)
module type DOMAIN = sig
  type t

  val bot : t
  (** Bottom element *)

  val leq : t -> t -> bool
  (** Pre-order *)

  val widen : t -> t -> t
  (** Over-approximation of the upper-bound, that ensures convergence: for any
      sequence [(v_n)_n], the sequence [(u_n)_n], defined as follows, must be
      stationary: * [u_0] is arbitrary * [u_(n+1) = widen u_n v_n] *)
end

module type NAIVE_SOLVER = functor
  (_ : LOG)
  (X : WITH_COMPARE)
  (_ : sig
     include MAP with type key = X.t

     val merge :
       (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
   end)
  (D : DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end

module type SOLVER = functor
  (_ : LOG)
  (X : WITH_COMPARE)
  (_ : MAP with type key = X.t)
  (D : DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end

module type NAIVE_SOLVER_WITH_INPUT_WIDENING = functor
  (_ : LOG)
  (X : sig
     include WITH_COMPARE

     val widen : t -> t -> t
   end)
  (_ : sig
     include MAP2 with type key = X.t

     val merge :
       (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
   end)
  (D : DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((bool -> X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end

module type SOLVER_WITH_INPUT_WIDENING = functor
  (_ : LOG)
  (X : sig
     include WITH_COMPARE

     val widen : t -> t -> t
   end)
  (_ : MAP2 with type key = X.t)
  (D : DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((bool -> X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
  (** [solve ~debug ~transient g] computes a function [f] such that
      [g f b x <= f b x] for every [b] and [x]. The boolean argument indicates
      whether the fixpoint solver should use widening on the inputs in [X] for a
      particular call. *)
end

module type MFG_DOMAIN = sig
  type t
  type dom
  type codom

  val bot : t
  val leq : t -> t -> bool
  val singleton : dom -> t
  val apply : t -> dom -> codom
  val update : (dom -> codom) -> t -> t
  val widen : int -> t -> t -> t
end

module type NAIVE_SOLVER_MFG = functor
  (_ : LOG)
  (X : WITH_COMPARE)
  (_ : sig
     include MAP with type key = X.t

     val merge :
       (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
   end)
  (MFG : MFG_DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) ->
    X.t ->
    MFG.dom ->
    MFG.codom * (X.t * MFG.t) Statistics.t
end

module type SOLVER_MFG = functor
  (_ : LOG)
  (X : WITH_COMPARE)
  (_ : MAP with type key = X.t)
  (MFG : MFG_DOMAIN)
  -> sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) ->
    X.t ->
    MFG.dom ->
    MFG.codom * (X.t * MFG.t) Statistics.t
end

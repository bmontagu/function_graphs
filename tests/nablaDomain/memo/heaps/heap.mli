open Common

(** Functor that improves the time complexity of [find_min] so that is has
    constant worst-case time.

    [is_empty h] runs in [O(1)] worst-case time.

    [insert] has the same time complexity as [H.insert].

    [find_min] runs in [O(1)] worst-case time.

    [delete_min] has the same time complexity as [H.pop_min].

    [pop_min] has the same time complexity as [H.pop_min].

    [merge] has the same time complexity as [H.merge]. *)
module ExplicitMin (H : HEAP) : HEAP with type Elt.t = H.Elt.t

(** Leftist Heaps

    [is_empty h] runs in [O(1)] worst-case time.

    [insert x h] runs in [O(log |h|)] worst-case time.

    [find_min h] runs in [O(1)] worst-case time.

    [delete_min h] runs in [O(log |h|)] worst-case time.

    [pop_min h] runs in [O(log |h|)] worst-case time.

    [merge h1 h2] runs in [O(log (|h_1| + |h_2|))] worst-case time. *)
module Leftist (X : ORDERED) : HEAP with type Elt.t = X.t

(** Binomial Heaps

    [is_empty h] runs in [O(1)] worst-case time.

    [insert x h] runs in [O(log |h|)] worst-case time and [O(1)] amortized time.

    [find_min h] runs in [O(log |h|)] worst-case time.

    [delete_min h] runs in [O(log |h|)] worst-case time.

    [pop_min h] runs in [O(log |h|)] worst-case time.

    [merge h1 h2] runs in [O(log (|h_1| + |h_2|))] worst-case time. *)
module Binomial (X : ORDERED) : HEAP with type Elt.t = X.t

(** Splay Heaps

    [is_empty h] runs in [O(1)] worst-case time.

    [insert x h] runs in [O(log |h|)] amortized time.

    [find_min h] runs in [O(log |h|)] amortized time.

    [delete_min h] runs in [O(log |h|)] amortized time.

    [pop_min h] runs in [O(log |h|)] amortized time.

    [merge h1 h2] runs in [O(|h_1| + |h_2|)] worst-case time. *)
module Splay (X : ORDERED) : HEAP with type Elt.t = X.t

(** Pairing Heaps

    [is_empty h] runs in [O(1)] worst-case time.

    [insert x h] runs in [O(1)] worst-case time.

    [find_min h] runs in [O(1)] worst-case time.

    [delete_min h] runs in [O(|h|)] worst-case time and [O(log |h|)] amortized
    time.

    [pop_min h] runs in [O(|h|)] worst-case time and in [O(log |h|)] amortized
    time.

    [merge h1 h2] runs in [O(1)] worst-case time. *)
module Pairing (X : ORDERED) : HEAP with type Elt.t = X.t

(** Skew Heaps

    [is_empty h] runs in [O(1)] worst-case time.

    [insert x h] runs in [O(1)] worst-case time.

    [find_min h] runs in [O(log |h|)] worst-case time.

    [delete_min h] runs in [O(log |h|)] worst-case time and [O(log |h|)]
    amortized time.

    [pop_min h] runs in [O(log |h|)] worst-case time and in [O(log |h|)]
    amortized time.

    [merge h1 h2] runs in [O(log (|h1| + |h2|))] worst-case time. *)
module Skew (X : ORDERED) : HEAP with type Elt.t = X.t

(** Bootstrap functor

    [is_empty h] runs in [O(1)] worst-case time.

    [insert] has the same time complexity as [Make(X).insert].

    [find_min] runs in [O(1)] worst-case time.

    [delete_min] has the same time complexity as the sum of [Make(X).pop_min]
    and [Make(X).merge].

    [pop_min] has the same time complexity as the sum of [Make(X).pop_min] and
    [Make(X).merge].

    [merge] has the same time complexity as [Make(X).insert]. *)
module Bootstrap
    (_ : functor (X : ORDERED) -> HEAP with type Elt.t = X.t)
    (X : ORDERED) : HEAP with type Elt.t = X.t

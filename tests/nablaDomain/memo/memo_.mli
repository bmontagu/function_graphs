(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

open Sigs

(** Functor to create memoizing functions *)
module Make
    (_ : LOG)
    (X : WITH_PP)
    (_ : sig
      include MAP with type key = X.t
      include WITH_PP1 with type 'a t := 'a t
    end)
    (V : WITH_PP) : sig
  val memo : (X.t -> V.t) -> X.t -> V.t
  (** [memo f] memoizes the [(v, f v)] pairs in a table, so that subsequent
      calls to the memoized [f] are faster *)

  val fix :
    ?transient:bool -> ?debug:bool -> ((X.t -> V.t) -> X.t -> V.t) -> X.t -> V.t
  (** [fix ~transient ~debug f] memoizes the functional [f] so that intermediate
      results in the computation of the fixpoint of [f] are recorded in a table
      for faster computation. This function is meant to exploit memoization when
      defining a recursive function. If [transient = true], then the table
      persists, and is also used to memoize subsequent calls to the fixpoint.
      This means that if [transient = false], then the cache is not shared
      between different calls to the fixpoint (the cache is emptied at the end
      of each call). If [debug = true], then the contents of the cache is
      printed at the end of the fixpoint computation. *)
end

(** Functor to create memoizing functions for dependently-typed functions *)
module MakeDep
    (_ : LOG)
    (X : sig
      type 'a t
    end)
    (_ : DMAP with type 'a key = 'a X.t) : sig
  val memo : ('a X.t -> 'a) -> 'a X.t -> 'a

  type poly = { fun_ : 'a. 'a X.t -> 'a } [@@unboxed]

  val fix : ?transient:bool -> ?debug:bool -> (poly -> poly) -> poly
end

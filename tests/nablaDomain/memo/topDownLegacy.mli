(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

module Make : SOLVER
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type [(X.t -> D.t) -> (X.t -> D.t)]. [D] must be equipped with a
    widening. It is necessary that the functional is called on a finite number
    of values of [X], so that the fixpoint solver terminates.

    This implementation is inspired by the Top-Down fixpoint algorithm, and
    avoids unnecessary recomputations by maintaining a dependency graph.

    Charlier, B. L. & Van Hentenryck, P. A Universal Top-Down Fixpoint Algorithm
    Brown University, 1992

    Fecht, C. & Seidl, H. A Faster Solver for General Systems of Equations Sci.
    Comput. Program., 1999, 35, 137-161 *)

module MakeWithInputWidening : SOLVER_WITH_INPUT_WIDENING
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type

    [(bool -> X.t -> D.t) -> (bool -> X.t -> D.t)].

    Both [X] and [D] must be equipped with a widening. The boolean argument
    indicates whether the fixpoint solver should use widening on [X].

    This implementation is inspired by the Top-Down fixpoint algorithm, and
    avoids unnecessary recomputations by maintaining a dependency graph.

    Charlier, B. L. & Van Hentenryck, P. A Universal Top-Down Fixpoint Algorithm
    Brown University, 1992

    Fecht, C. & Seidl, H. A Faster Solver for General Systems of Equations Sci.
    Comput. Program., 1999, 35, 137-161 *)

module MakeMFG : SOLVER_MFG
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice for function graphs equipped with widening. The
    goal is to compute a function [f] of type

    [X.t -> MFG.dom -> MFG.codom],

    that is a post-fixpoint of a functional of type

    [(X.t -> MFG.dom -> MFG.codom) -> (X.t -> MFG.dom -> MFG.codom)].

    [MFG] must be equipped with a widening. The functional has to be called on a
    finite number of elements of type [X.t].

    This implementation is inspired by the Top-Down fixpoint algorithm, and
    avoids unnecessary recomputations by maintaining a dependency graph.

    Charlier, B. L. & Van Hentenryck, P. A Universal Top-Down Fixpoint Algorithm
    Brown University, 1992

    Fecht, C. & Seidl, H. A Faster Solver for General Systems of Equations Sci.
    Comput. Program., 1999, 35, 137-161 *)

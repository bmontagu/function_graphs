(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

module Make2
    (Log : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP) : sig
  type 'a fun_ = (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> 'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t)
end = struct
  module X = struct
    type _ t = X1 : XIn1.t -> XOut1.t t | X2 : XIn2.t -> XOut2.t t

    let compare (type a b) (x1 : a t) (x2 : b t) : (a, b) Dmap.cmp =
      match (x1, x2) with
      | X1 v, X1 w ->
          let n = XIn1.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X2 v, X2 w ->
          let n = XIn2.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X1 _, X2 _ -> Lt
      | X2 _, X1 _ -> Gt
  end

  module M = struct
    include Dmap.Make (X)

    let pp fmt m =
      Format.pp_print_seq
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
        (fun fmt (Binding (x, v)) ->
          match x with
          | X1 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn1.pp x XOut1.pp v
          | X2 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn2.pp x XOut2.pp v)
        fmt (to_seq m)
  end

  module DMemo = Memo_.MakeDep (Log) (X) (M)

  type 'a fun_ = (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> 'a

  let combine (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) : DMemo.poly -> DMemo.poly =
   fun f ->
    let f1 x = f.fun_ (X.X1 x) and f2 x = f.fun_ (X.X2 x) in
    let f12 : type a. a X.t -> a = function
      | X.X1 v1 -> func1 f1 f2 v1
      | X.X2 v2 -> func2 f1 f2 v2
    in
    { fun_ = f12 }

  let split (g : DMemo.poly) : (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t) =
    let f1 x = g.fun_ (X1 x) and f2 x = g.fun_ (X2 x) in
    (f1, f2)

  let fix ?transient ?debug (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) =
    split @@ DMemo.fix ?transient ?debug @@ combine func1 func2
end

module Make3
    (Log : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> (XIn3.t -> XOut3.t) -> 'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t) * (XIn3.t -> XOut3.t)
end = struct
  module X = struct
    type _ t =
      | X1 : XIn1.t -> XOut1.t t
      | X2 : XIn2.t -> XOut2.t t
      | X3 : XIn3.t -> XOut3.t t

    let compare (type a b) (x1 : a t) (x2 : b t) : (a, b) Dmap.cmp =
      match (x1, x2) with
      | X1 v, X1 w ->
          let n = XIn1.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X2 v, X2 w ->
          let n = XIn2.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X3 v, X3 w ->
          let n = XIn3.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X1 _, (X2 _ | X3 _) | X2 _, X3 _ -> Lt
      | (X2 _ | X3 _), X1 _ | X3 _, X2 _ -> Gt
  end

  module M = struct
    include Dmap.Make (X)

    let pp fmt m =
      Format.pp_print_seq
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
        (fun fmt (Binding (x, v)) ->
          match x with
          | X1 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn1.pp x XOut1.pp v
          | X2 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn2.pp x XOut2.pp v
          | X3 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn3.pp x XOut3.pp v)
        fmt (to_seq m)
  end

  module DMemo = Memo_.MakeDep (Log) (X) (M)

  type 'a fun_ =
    (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> (XIn3.t -> XOut3.t) -> 'a

  let combine (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_) :
      DMemo.poly -> DMemo.poly =
   fun f ->
    let f1 x = f.fun_ (X.X1 x)
    and f2 x = f.fun_ (X.X2 x)
    and f3 x = f.fun_ (X.X3 x) in
    let f123 : type a. a X.t -> a = function
      | X.X1 v1 -> func1 f1 f2 f3 v1
      | X.X2 v2 -> func2 f1 f2 f3 v2
      | X.X3 v3 -> func3 f1 f2 f3 v3
    in
    { fun_ = f123 }

  let split (g : DMemo.poly) :
      (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t) * (XIn3.t -> XOut3.t) =
    let f1 x = g.fun_ (X1 x)
    and f2 x = g.fun_ (X2 x)
    and f3 x = g.fun_ (X3 x) in
    (f1, f2, f3)

  let fix ?transient ?debug (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_) =
    split @@ DMemo.fix ?transient ?debug @@ combine func1 func2 func3
end

module Make4
    (Log : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP)
    (XIn4 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut4 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn4.t -> XOut4.t) fun_ ->
    (XIn1.t -> XOut1.t)
    * (XIn2.t -> XOut2.t)
    * (XIn3.t -> XOut3.t)
    * (XIn4.t -> XOut4.t)
end = struct
  module X = struct
    type _ t =
      | X1 : XIn1.t -> XOut1.t t
      | X2 : XIn2.t -> XOut2.t t
      | X3 : XIn3.t -> XOut3.t t
      | X4 : XIn4.t -> XOut4.t t

    let compare (type a b) (x1 : a t) (x2 : b t) : (a, b) Dmap.cmp =
      match (x1, x2) with
      | X1 v, X1 w ->
          let n = XIn1.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X2 v, X2 w ->
          let n = XIn2.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X3 v, X3 w ->
          let n = XIn3.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X4 v, X4 w ->
          let n = XIn4.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X1 _, (X2 _ | X3 _ | X4 _) | X2 _, (X3 _ | X4 _) | X3 _, X4 _ -> Lt
      | (X2 _ | X3 _ | X4 _), X1 _ | (X3 _ | X4 _), X2 _ | X4 _, X3 _ -> Gt
  end

  module M = struct
    include Dmap.Make (X)

    let pp fmt m =
      Format.pp_print_seq
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
        (fun fmt (Binding (x, v)) ->
          match x with
          | X1 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn1.pp x XOut1.pp v
          | X2 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn2.pp x XOut2.pp v
          | X3 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn3.pp x XOut3.pp v
          | X4 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn4.pp x XOut4.pp v)
        fmt (to_seq m)
  end

  module DMemo = Memo_.MakeDep (Log) (X) (M)

  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    'a

  let combine (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_)
      (func4 : (XIn4.t -> XOut4.t) fun_) : DMemo.poly -> DMemo.poly =
   fun f ->
    let f1 x = f.fun_ (X.X1 x)
    and f2 x = f.fun_ (X.X2 x)
    and f3 x = f.fun_ (X.X3 x)
    and f4 x = f.fun_ (X.X4 x) in
    let f1234 : type a. a X.t -> a = function
      | X.X1 v1 -> func1 f1 f2 f3 f4 v1
      | X.X2 v2 -> func2 f1 f2 f3 f4 v2
      | X.X3 v3 -> func3 f1 f2 f3 f4 v3
      | X.X4 v4 -> func4 f1 f2 f3 f4 v4
    in
    { fun_ = f1234 }

  let split (g : DMemo.poly) :
      (XIn1.t -> XOut1.t)
      * (XIn2.t -> XOut2.t)
      * (XIn3.t -> XOut3.t)
      * (XIn4.t -> XOut4.t) =
    let f1 x = g.fun_ (X1 x)
    and f2 x = g.fun_ (X2 x)
    and f3 x = g.fun_ (X3 x)
    and f4 x = g.fun_ (X4 x) in
    (f1, f2, f3, f4)

  let fix ?transient ?debug (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_)
      (func4 : (XIn4.t -> XOut4.t) fun_) =
    split @@ DMemo.fix ?transient ?debug @@ combine func1 func2 func3 func4
end

module Make5
    (Log : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP)
    (XIn4 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut4 : WITH_PP)
    (XIn5 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut5 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    (XIn5.t -> XOut5.t) ->
    'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn4.t -> XOut4.t) fun_ ->
    (XIn5.t -> XOut5.t) fun_ ->
    (XIn1.t -> XOut1.t)
    * (XIn2.t -> XOut2.t)
    * (XIn3.t -> XOut3.t)
    * (XIn4.t -> XOut4.t)
    * (XIn5.t -> XOut5.t)
end = struct
  module X = struct
    type _ t =
      | X1 : XIn1.t -> XOut1.t t
      | X2 : XIn2.t -> XOut2.t t
      | X3 : XIn3.t -> XOut3.t t
      | X4 : XIn4.t -> XOut4.t t
      | X5 : XIn5.t -> XOut5.t t

    let compare (type a b) (x1 : a t) (x2 : b t) : (a, b) Dmap.cmp =
      match (x1, x2) with
      | X1 v, X1 w ->
          let n = XIn1.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X2 v, X2 w ->
          let n = XIn2.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X3 v, X3 w ->
          let n = XIn3.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X4 v, X4 w ->
          let n = XIn4.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X5 v, X5 w ->
          let n = XIn5.compare v w in
          if n = 0 then Eq else if n < 0 then Lt else Gt
      | X1 _, (X2 _ | X3 _ | X4 _ | X5 _)
      | X2 _, (X3 _ | X4 _ | X5 _)
      | X3 _, (X4 _ | X5 _)
      | X4 _, X5 _ ->
          Lt
      | (X2 _ | X3 _ | X4 _ | X5 _), X1 _
      | (X3 _ | X4 _ | X5 _), X2 _
      | (X4 _ | X5 _), X3 _
      | X5 _, X4 _ ->
          Gt
  end

  module M = struct
    include Dmap.Make (X)

    let pp fmt m =
      Format.pp_print_seq
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
        (fun fmt (Binding (x, v)) ->
          match x with
          | X1 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn1.pp x XOut1.pp v
          | X2 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn2.pp x XOut2.pp v
          | X3 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn3.pp x XOut3.pp v
          | X4 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn4.pp x XOut4.pp v
          | X5 x -> Format.fprintf fmt "@[%a:@ %a@]" XIn5.pp x XOut5.pp v)
        fmt (to_seq m)
  end

  module DMemo = Memo_.MakeDep (Log) (X) (M)

  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    (XIn5.t -> XOut5.t) ->
    'a

  let combine (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_)
      (func4 : (XIn4.t -> XOut4.t) fun_) (func5 : (XIn5.t -> XOut5.t) fun_) :
      DMemo.poly -> DMemo.poly =
   fun f ->
    let f1 x = f.fun_ (X.X1 x)
    and f2 x = f.fun_ (X.X2 x)
    and f3 x = f.fun_ (X.X3 x)
    and f4 x = f.fun_ (X.X4 x)
    and f5 x = f.fun_ (X.X5 x) in
    let f12345 : type a. a X.t -> a = function
      | X.X1 v1 -> func1 f1 f2 f3 f4 f5 v1
      | X.X2 v2 -> func2 f1 f2 f3 f4 f5 v2
      | X.X3 v3 -> func3 f1 f2 f3 f4 f5 v3
      | X.X4 v4 -> func4 f1 f2 f3 f4 f5 v4
      | X.X5 v5 -> func5 f1 f2 f3 f4 f5 v5
    in
    { fun_ = f12345 }

  let split (g : DMemo.poly) :
      (XIn1.t -> XOut1.t)
      * (XIn2.t -> XOut2.t)
      * (XIn3.t -> XOut3.t)
      * (XIn4.t -> XOut4.t)
      * (XIn5.t -> XOut5.t) =
    let f1 x = g.fun_ (X1 x)
    and f2 x = g.fun_ (X2 x)
    and f3 x = g.fun_ (X3 x)
    and f4 x = g.fun_ (X4 x)
    and f5 x = g.fun_ (X5 x) in
    (f1, f2, f3, f4, f5)

  let fix ?transient ?debug (func1 : (XIn1.t -> XOut1.t) fun_)
      (func2 : (XIn2.t -> XOut2.t) fun_) (func3 : (XIn3.t -> XOut3.t) fun_)
      (func4 : (XIn4.t -> XOut4.t) fun_) (func5 : (XIn5.t -> XOut5.t) fun_) =
    split
    @@ DMemo.fix ?transient ?debug
    @@ combine func1 func2 func3 func4 func5
end

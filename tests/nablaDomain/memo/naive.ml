(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

module LogSilent : LOG = struct
  let eprintf fmt = Format.ifprintf Format.err_formatter fmt
end

(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type [(X.t -> D.t) -> (X.t -> D.t)]. [D] must be equipped with a
    widening. It is necessary that the functional is called on a finite number
    of values of [X], so that the fixpoint solver terminates.

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)
module Make
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : sig
      include MAP with type key = X.t

      val merge :
        (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    end)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** type used to indicate whether we might have detected a cycle *)
  type cycle_status = NoCycle | MayCycle

  let join_status s1 s2 =
    match (s1, s2) with
    | NoCycle, s | s, NoCycle -> s
    | MayCycle, MayCycle -> MayCycle

  type metadata = { visited : bool; status : cycle_status }
  (** Whether a node has been visited already, and whether it may belong to a
      cycle *)

  type cache = (metadata * D.t) M.t

  (** Initializes the cache *)
  let init () = ref (M.empty : cache)

  (** Empties the cache *)
  let reset r = r := (M.empty : cache)

  (** Marks a node as not visited in the local cache: it must not be present
      already in the local cache. The local cache is updated so that the node is
      marked as not visited, and uses the already cached value from the global
      cache if there is one already. *)
  let mark_not_visited local global node =
    local :=
      M.update node
        (function
          | Some _ -> assert false
          | None -> (
              match M.find_opt node !global with
              | Some ({ visited = _; status }, value) ->
                  Some ({ visited = false; status }, value)
              | None -> Some ({ visited = false; status = NoCycle }, D.bot)))
        !local

  (** Gets an entry on the cache *)
  let get cache node = M.find_opt node !cache

  (** Updates the value associated to a node with a new value, and marks the
      node as visited. There must be an entry for that node in the cache
      already. *)
  let update_and_mark_visited cache node new_value =
    cache :=
      M.update node
        (function
          | Some ({ visited = _; status }, _value) ->
              Some ({ visited = true; status }, new_value)
          | None -> assert false)
        !cache

  (* let pp_cache fmt (m:t) =
   *   M.pp (fun fmt (_meta, v) -> D.pp fmt v) fmt m *)

  (** Edges are only recorded for statistics. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges
  end

  (** Computes a post-fixpoint of some higher-order functional. *)
  let solve ?(debug = false) ?(transient = true) f =
    let global : cache ref =
      (* the global cache, that persists across iterations *)
      init ()
    and local : cache ref =
      (* the local cache, that is used inside a single iteration only *)
      init ()
    and edges =
      (* the edges that we have so far recorded *)
      Edges.init ()
    and time =
      (* how many times the functional has been called *)
      ref 0
    in
    (* one exploration of the graph *)
    let rec explore_from x =
      (* explores the graph from the node [x] *)
      match get local x with
      | Some ({ visited; status = _ }, y) ->
          if not visited then
            (* we query for a node, while its visit is not finished
               yet: we have a cycle *)
            local :=
              M.update x
                (function _ -> Some ({ visited; status = MayCycle }, y))
                !local;
          y
      | None ->
          let () = mark_not_visited local global x in
          let explore_from_and_record_calls z =
            (* record the call edge *)
            Edges.record edges x z;
            explore_from z
          in
          incr time;
          let y = f explore_from_and_record_calls x in
          update_and_mark_visited local x y;
          y
    in
    (* iterate the graph explorations until we get a post-fixpoint *)
    let nb_iterations = ref 0 in
    let rec stabilize_from x =
      incr nb_iterations;
      reset local;
      let y = explore_from x (* start exploring *)
      and changed = ref false (* tells whether the graph was modified *)
      and nb_states = ref 0
      and nb_new_states = ref 0
      and nb_explored_states = ref 0
      and nb_unstable_states = ref 0
      and nb_cycles = ref 0 in
      global :=
        M.merge
          (fun _x' oglobal olocal ->
            match (oglobal, olocal) with
            | None, Some _ ->
                changed := true;
                incr nb_new_states;
                incr nb_explored_states;
                olocal
            | Some _, None ->
                incr nb_states;
                oglobal
            | ( Some ({ visited = _; status = status_g }, g),
                Some ({ visited = _; status = status_l }, l) ) ->
                incr nb_states;
                incr nb_explored_states;
                let status = join_status status_g status_l in
                (match status with NoCycle -> () | MayCycle -> incr nb_cycles);
                if D.leq l g then
                  (* local value is smaller: we have a stable point *)
                  Some ({ visited = false; status }, g)
                else (
                  (* the point is not stable *)
                  (match status with
                  | NoCycle -> ()
                  (* only consider changes of cyclic nodes *)
                  | MayCycle -> changed := true);
                  incr nb_unstable_states;
                  let join =
                    match status with
                    | NoCycle -> fun _g l -> l
                    | MayCycle -> D.widen
                  in
                  Some ({ visited = false; status }, join g l))
            | None, None -> assert false)
          !global !local;
      if debug then
        Log.eprintf
          "Iteration %i: states=%i explored=%i unstable=%i new=%i cycles=%i@."
          !nb_iterations !nb_states !nb_explored_states !nb_unstable_states
          !nb_new_states !nb_cycles;
      if not !changed then
        (* the graph has not changed: we have a post-fixpoint *)
        y
      else (* we have not converged yet: iterate more *)
        stabilize_from x
    in
    fun x ->
      let res = stabilize_from x in
      let nb_nodes = M.cardinal !global
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !global !edges in
      if debug then
        Log.eprintf
          (* "Elements in cache:@.@[%a@]@.\
           * " *)
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@. Total \
           number of calls: %i@."
          (* pp_cache !global *)
          nb_nodes nb_edges max_fanout med_fanout !nb_iterations !time;
      let stats =
        {
          Statistics.entries =
            !global |> M.to_seq |> Seq.map (fun (x, (_metadata, v)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = !nb_iterations;
          calls = !time;
        }
      in
      if not transient then reset global;
      nb_iterations := 0;
      (res, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type [(bool -> X.t -> D.t) -> (bool -> X.t -> D.t)]. Both [X] and [D]
    must be equipped with a widening. The boolean argument indicates whether the
    fixpoint solver should use widening on [X].

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)
module MakeWithInputWidening
    (Log : LOG)
    (X : sig
      include WITH_COMPARE

      val widen : t -> t -> t
    end)
    (M : sig
      include MAP2 with type key = X.t

      val merge :
        (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    end)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((bool -> X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** type used to indicate whether we might have detected a cycle *)
  type cycle_status = NoCycle | MayCycle

  let join_status s1 s2 =
    match (s1, s2) with
    | NoCycle, s | s, NoCycle -> s
    | MayCycle, MayCycle -> MayCycle

  type metadata = { visited : bool; status : cycle_status }
  (** Whether a node has been visited already, and whether it may belong to a
      cycle *)

  type cache = (metadata * D.t) M.t

  (** Initializes the cache *)
  let init () = ref (M.empty : cache)

  (** Empties the cache *)
  let reset r = r := (M.empty : cache)

  (** Marks a node as not visited in the local cache: it must not be present
      already in the local cache. The local cache is updated so that the node is
      marked as not visited, and uses the already cached value from the global
      cache if there is one already. *)
  let mark_not_visited local global node =
    local :=
      M.update node
        (function
          | Some _ -> assert false
          | None -> (
              match M.find_opt node !global with
              | Some ({ visited = _; status }, value) ->
                  Some ({ visited = false; status }, value)
              | None -> Some ({ visited = false; status = NoCycle }, D.bot)))
        !local

  (** Gets an entry on the cache *)
  let get cache node = M.find_opt node !cache

  (** Updates the value associated to a node with a new value, and marks the
      node as visited. There must be an entry for that node in the cache
      already. *)
  let update_and_mark_visited cache node new_value =
    cache :=
      M.update node
        (function
          | Some ({ visited = _; status }, _value) ->
              Some ({ visited = true; status }, new_value)
          | None -> assert false)
        !cache

  (* let pp_cache fmt (m:t) =
   *   M.pp (fun fmt (_meta, v) -> D.pp fmt v) fmt m *)

  (** Edges are only recorded for statistics. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges
  end

  (** Computes a post-fixpoint of some higher-order functional. *)
  let solve ?(debug = false) ?(transient = true) f =
    let global : cache ref =
      (* the global cache, that persists across iterations *)
      init ()
    and local : cache ref =
      (* the local cache, that is used inside a single iteration only *)
      init ()
    and edges =
      (* the edges that we have so far recorded *)
      Edges.init ()
    and time =
      (* how many times the functional has been called *)
      ref 0
    in
    let widen_input x =
      (* function that maps some input to a widened version of that input *)
      match
        M.fold_similar
          (fun z _ acc ->
            match acc with
            | None -> Some z
            | Some z_acc -> Some (X.widen z_acc z))
          x !local None
      with
      | Some x' -> X.widen x' x
      | None -> x
    in
    (* one exploration of the graph *)
    let rec explore_from widen x =
      (* explores the graph from the node [x], using [widen] as a
         hint on whether to perform input widening *)
      match get local x with
      | Some ({ visited; status = _ }, y) ->
          if not visited then
            (* we query for a node, while its visit is not finished
               yet: we have a cycle *)
            local :=
              M.update x
                (function _ -> Some ({ visited; status = MayCycle }, y))
                !local;
          y
      | None ->
          let x' =
            (* the new input we should consider *)
            if widen then (
              let x' = widen_input x in
              (* record the widening edge *)
              if X.compare x x' <> 0 then Edges.record edges x x';
              x')
            else x
          in
          let () = mark_not_visited local global x in
          let explore_from_and_record_calls widen z =
            (* record the call edge *)
            Edges.record edges x z;
            explore_from widen z
          in
          incr time;
          let y = f explore_from_and_record_calls x' in
          update_and_mark_visited local x y;
          y
    in
    (* iterate the graph explorations until we get a post-fixpoint *)
    let nb_iterations = ref 0 in
    let rec stabilize_from x =
      incr nb_iterations;
      reset local;
      let y = explore_from false x (* start exploring with no widening *)
      and changed = ref false (* tells whether the graph was modified *)
      and nb_states = ref 0
      and nb_new_states = ref 0
      and nb_explored_states = ref 0
      and nb_unstable_states = ref 0
      and nb_cycles = ref 0 in
      global :=
        M.merge
          (fun _x' oglobal olocal ->
            match (oglobal, olocal) with
            | None, Some _ ->
                changed := true;
                incr nb_new_states;
                incr nb_explored_states;
                olocal
            | Some _, None ->
                incr nb_states;
                oglobal
            | ( Some ({ visited = _; status = status_g }, g),
                Some ({ visited = _; status = status_l }, l) ) ->
                incr nb_states;
                incr nb_explored_states;
                let status = join_status status_g status_l in
                (match status with NoCycle -> () | MayCycle -> incr nb_cycles);
                if D.leq l g then
                  (* local value is smaller: we have a stable point *)
                  Some ({ visited = false; status }, g)
                else (
                  (* the point is not stable *)
                  (match status with
                  | NoCycle -> ()
                  (* only consider changes of cyclic nodes *)
                  | MayCycle -> changed := true);
                  incr nb_unstable_states;
                  let join =
                    match status with
                    | NoCycle -> fun _g l -> l
                    | MayCycle -> D.widen
                  in
                  Some ({ visited = false; status }, join g l))
            | None, None -> assert false)
          !global !local;
      if debug then
        Log.eprintf
          "Iteration %i: states=%i explored=%i unstable=%i new=%i cycles=%i@."
          !nb_iterations !nb_states !nb_explored_states !nb_unstable_states
          !nb_new_states !nb_cycles;
      if not !changed then
        (* the graph has not changed: we have a post-fixpoint *)
        y
      else (* we have not converged yet: iterate more *)
        stabilize_from x
    in
    fun x ->
      let res = stabilize_from x in
      let nb_nodes = M.cardinal !global
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !global !edges in
      if debug then
        Log.eprintf
          (* "Elements in cache:@.@[%a@]@.\
           * " *)
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@. Total \
           number of calls: %i@."
          (* pp_cache !global *)
          nb_nodes nb_edges max_fanout med_fanout !nb_iterations !time;
      let stats =
        {
          Statistics.entries =
            !global |> M.to_seq |> Seq.map (fun (x, (_metadata, v)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = !nb_iterations;
          calls = !time;
        }
      in
      if not transient then reset global;
      nb_iterations := 0;
      (res, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice for function graphs equipped with widening. The
    goal is to compute a function [f] of type

    [X.t -> MFG.dom -> MFG.codom],

    that is a post-fixpoint of a functional of type

    [(X.t -> MFG.dom -> MFG.codom) -> (X.t -> MFG.dom -> MFG.codom)].

    [MFG] must be equipped with a widening. The functional has to be called on a
    finite number of elements of type [X.t].

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)
module MakeMFG
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : sig
      include MAP with type key = X.t

      val merge :
        (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    end)
    (MFG : sig
      type t
      type dom
      type codom

      val bot : t
      val leq : t -> t -> bool
      val singleton : dom -> t
      val apply : t -> dom -> codom
      val update : (dom -> codom) -> t -> t
      val widen : int -> t -> t -> t
    end) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) ->
    X.t ->
    MFG.dom ->
    MFG.codom * (X.t * MFG.t) Statistics.t
end = struct
  (** type used to indicate whether we might have detected a cycle *)
  type cycle_status = NoCycle | MayCycle

  let join_status s1 s2 =
    match (s1, s2) with
    | NoCycle, s | s, NoCycle -> s
    | MayCycle, MayCycle -> MayCycle

  type metadata = { visited : bool; status : cycle_status; iterations : int }
  (** Whether a node has been visited already, whether it may belong to a cycle,
      and how many times a data was modified *)

  type cache = (metadata * MFG.t) M.t

  (** Initializes the cache *)
  let init () = ref (M.empty : cache)

  (** Empties the cache *)
  let reset r = r := (M.empty : cache)

  (** Marks a node as not visited in the local cache: it must not be present
      already in the local cache. The local cache is updated so that the node is
      marked as not visited, and uses the already cached value from the global
      cache if there is one already. *)
  let mark_not_visited local global node =
    local :=
      M.update node
        (function
          | Some _ -> assert false
          | None -> (
              match M.find_opt node !global with
              | Some ({ visited = _; status; iterations }, value) ->
                  Some ({ visited = false; status; iterations }, value)
              | None ->
                  Some
                    ( { visited = false; status = NoCycle; iterations = 1 },
                      MFG.bot )))
        !local

  (** Gets an entry on the cache *)
  let get cache node = M.find_opt node !cache

  (** Updates the value associated to a node with a new value, and marks the
      node as visited. There must be an entry for that node in the cache
      already. *)
  let update_and_mark_visited cache node new_value =
    cache :=
      M.update node
        (function
          | Some ({ visited = _; status; iterations }, value) ->
              Some
                ( { visited = true; status; iterations = iterations + 1 },
                  MFG.widen iterations value new_value )
          | None -> assert false)
        !cache

  (* let pp_cache fmt (m:t) =
   *   M.pp (fun fmt (_meta, v) -> D.pp fmt v) fmt m *)

  (** Edges are only recorded for statistics. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges
  end

  type continue = Continue of MFG.t | Done of MFG.codom

  (** Computes a post-fixpoint of some higher-order functional. *)
  let solve ?(debug = false) ?(transient = true) f =
    let global : cache ref =
      (* the global cache, that persists across iterations *)
      init ()
    and local : cache ref =
      (* the local cache, that is used inside a single iteration only *)
      init ()
    and edges =
      (* the edges that we have so far recorded *)
      Edges.init ()
    and time =
      (* how many times the functional has been called *)
      ref 0
    in
    (* one exploration of the graph *)
    let rec explore_from x z =
      (* explores the graph from the node [x] *)
      let continue =
        match get local x with
        | Some ({ visited; status = _; iterations }, y) ->
            if not visited then
              (* we query for a node, while its visit is not finished
                 yet: we have a cycle *)
              local :=
                M.update x
                  (function
                    | _ -> Some ({ visited; status = MayCycle; iterations }, y))
                  !local;
            let y' = MFG.widen iterations y (MFG.singleton z) in
            if MFG.leq y' y then Done (MFG.apply y z) else Continue y'
        | None -> (
            let () = mark_not_visited local global x in
            match get local x with
            | None -> assert false
            | Some ({ visited = _; status = _; iterations }, y) ->
                let d =
                  let y' = MFG.widen iterations y (MFG.singleton z) in
                  if MFG.leq y' y then y else y'
                in
                Continue d)
      in
      match continue with
      | Done v -> v
      | Continue d ->
          local :=
            M.update x
              (function
                | None -> assert false
                | Some ({ visited; status; iterations }, _) ->
                    Some ({ visited; status; iterations = iterations + 1 }, d))
              !local;
          let y = MFG.update (call_f_from x) d in
          update_and_mark_visited local x y;
          MFG.apply y z
    and explore_from_and_record_calls x z =
      (* record the call edge *)
      Edges.record edges x z;
      explore_from z
    and call_f_from x z =
      incr time;
      f (explore_from_and_record_calls x) x z
    in
    (* iterate the graph explorations until we get a post-fixpoint *)
    let nb_iterations = ref 0 in
    let rec stabilize_from x z =
      incr nb_iterations;
      reset local;
      let y = explore_from x z (* start exploring *)
      and changed = ref false (* tells whether the graph was modified *)
      and nb_states = ref 0
      and nb_new_states = ref 0
      and nb_explored_states = ref 0
      and nb_unstable_states = ref 0
      and nb_cycles = ref 0 in
      global :=
        M.merge
          (fun _x' oglobal olocal ->
            match (oglobal, olocal) with
            | None, Some _ ->
                changed := true;
                incr nb_new_states;
                incr nb_explored_states;
                olocal
            | Some _, None ->
                incr nb_states;
                oglobal
            | ( Some ({ visited = _; status = status_g; iterations = n_g }, g),
                Some ({ visited = _; status = status_l; iterations = n_l }, l) )
              ->
                incr nb_states;
                incr nb_explored_states;
                let status = join_status status_g status_l in
                (match status with NoCycle -> () | MayCycle -> incr nb_cycles);
                let iterations = n_g + n_l in
                if MFG.leq l g then
                  (* local value is smaller: we have a stable point *)
                  Some ({ visited = false; status; iterations }, g)
                else (
                  (* the point is not stable *)
                  (match status with
                  | NoCycle -> ()
                  (* only consider changes of cyclic nodes *)
                  | MayCycle -> changed := true);
                  incr nb_unstable_states;
                  let join =
                    match status with
                    | NoCycle -> fun _g l -> l
                    | MayCycle -> MFG.widen iterations
                  in
                  Some ({ visited = false; status; iterations }, join g l))
            | None, None -> assert false)
          !global !local;
      if debug then
        Log.eprintf
          "Iteration %i: states=%i explored=%i unstable=%i new=%i cycles=%i@."
          !nb_iterations !nb_states !nb_explored_states !nb_unstable_states
          !nb_new_states !nb_cycles;
      if not !changed then
        (* the graph has not changed: we have a post-fixpoint *)
        y
      else (* we have not converged yet: iterate more *)
        stabilize_from x z
    in
    fun x y ->
      let res = stabilize_from x y in
      let nb_nodes = M.cardinal !global
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !global !edges in
      if debug then
        Log.eprintf
          (* "Elements in cache:@.@[%a@]@.\
           * " *)
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          (* pp_cache !global *)
          nb_nodes nb_edges max_fanout med_fanout !nb_iterations !time;
      let stats =
        {
          Statistics.entries =
            !global |> M.to_seq |> Seq.map (fun (x, (_metadata, v)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = !nb_iterations;
          calls = !time;
        }
      in
      if not transient then reset global;
      nb_iterations := 0;
      (res, stats)
end

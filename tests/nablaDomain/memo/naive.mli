(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

module Make : NAIVE_SOLVER
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type [(X.t -> D.t) -> (X.t -> D.t)]. [D] must be equipped with a
    widening. It is necessary that the functional is called on a finite number
    of values of [X], so that the fixpoint solver terminates.

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)

module MakeWithInputWidening : NAIVE_SOLVER_WITH_INPUT_WIDENING
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice equipped with widening. The goal is to compute a
    function [f] of type [X.t -> D.t], that is a post-fixpoint of a functional
    of type

    [(bool -> X.t -> D.t) -> (X.t -> D.t)].

    Both [X] and [D] must be equipped with a widening. The boolean argument
    indicates whether the fixpoint solver should use widening on [X].

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)

module MakeMFG : NAIVE_SOLVER_MFG
(** Functor to compute post-fixpoints of recursively defined functions with
    values in a semi-lattice for function graphs equipped with widening. The
    goal is to compute a function [f] of type

    [X.t -> MFG.dom -> MFG.codom],

    that is a post-fixpoint of a functional of type

    [(X.t -> MFG.dom -> MFG.codom) -> (X.t -> MFG.dom -> MFG.codom)].

    [MFG] must be equipped with a widening. The functional has to be called on a
    finite number of elements of type [X.t].

    This implementation is naive, as it does not try to be clever about avoiding
    unnecessary computations. *)

(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

val is_empty : 'a list -> bool
val init : (int -> 'a) -> int -> 'a list
val fold_lefti : ('a -> int -> 'b -> 'a) -> 'a -> 'b list -> 'a

val fold_lefti2 :
  ('a -> int -> 'b -> 'c -> 'a) -> 'a -> 'b list -> 'c list -> 'a

val fold_righti : (int -> 'a -> 'b -> 'b) -> 'a list -> 'b -> 'b

val fold_righti2 :
  (int -> 'a -> 'b -> 'c -> 'c) -> 'a list -> 'b list -> 'c -> 'c

val compare : ('a -> 'b -> int) -> 'a list -> 'b list -> int
val same_length : 'a list -> 'b list -> bool

val split_at : int -> 'a list -> 'a list * 'a list
(** [split_at n l] returns two lists [l1] and [l2] such that [l = l1 @ l2] and
    [List.length l1 = min n (List.length l)]
    @raise Invalid_argument when [n < 0] *)

module type MONAD = sig
  type 'a m

  val return : 'a -> 'a m
  val ( let* ) : 'a m -> ('a -> 'b m) -> 'b m
  val ( let+ ) : 'a m -> ('a -> 'b) -> 'b m
end

module Monadic (M : MONAD) : sig
  val fold_leftm : ('a -> 'b -> 'a M.m) -> 'a -> 'b list -> 'a M.m
  (** Monadic fold_left on lists *)

  val fold_leftim : ('a -> int -> 'b -> 'a M.m) -> 'a -> 'b list -> 'a M.m
  (** Monadic fold_lefti on lists *)

  val fold_leftm2 :
    ('a -> 'b -> 'c -> 'a M.m) -> 'a -> 'b list -> 'c list -> 'a M.m
  (** Monadic fold_left2 on lists *)

  val fold_leftim2 :
    ('a -> int -> 'b -> 'c -> 'a M.m) -> 'a -> 'b list -> 'c list -> 'a M.m
  (** Monadic fold_lefti2 on lists *)

  val fold_leftm_option :
    ('a -> 'b -> 'a option M.m) -> 'a -> 'b list -> 'a option M.m
  (** Monadic fold_left on lists with early stop when [None] is produced *)

  val fold_rightm : ('a -> 'b -> 'b M.m) -> 'a list -> 'b -> 'b M.m
  (** Monadic fold_right on lists *)

  val fold_rightm2 :
    ('a -> 'b -> 'c -> 'c M.m) -> 'a list -> 'b list -> 'c -> 'c M.m
  (** Monadic fold_right2 on lists *)

  val fold_rightm_option :
    ('a -> 'b -> 'b option M.m) -> 'a list -> 'b -> 'b option M.m
  (** Monadic fold_right on lists with early stop when [None] is produced *)
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Function_graphs

(** A simple static analyzer that performs a non-relational analysis for a While
    language, using a non-standard approach. It is defined using a fixpoint
    solver and an abstract domain for function graphs. *)
module While (Expr : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (BExpr : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (S : sig
  type t

  val join : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include Function_graphs.Sigs.MFG with type dom = S.t and type codom = S.t

  val iter : (dom -> codom -> unit) -> t -> unit
end) (A : sig
  val assign : Test_common.While.var -> Expr.t -> S.t -> S.t
  val cond : BExpr.t -> bool -> S.t -> S.t
end) : sig
  val analyze :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Expr.t, BExpr.t) Test_common.While.prog ->
    S.t ->
    S.t

  val run :
    sparse:bool ->
    loop_unfold:int ->
    MFG.strategy array ->
    (Expr.t, BExpr.t) Test_common.While.prog ->
    S.t ->
    unit
end = struct
  let rec analyze ~sparse ~loop_unfold request p s =
    let open Test_common.While in
    let analyze p s =
      if sparse then analyze ~sparse ~loop_unfold request p s else request p s
    in
    match p with
    | Assign (x, e) -> A.assign x e s
    | Skip -> s
    | Seq (p1, p2) -> s |> analyze p1 |> analyze p2
    | If (c, p1, p2) ->
        S.join
          (s |> A.cond c true |> analyze p1)
          (s |> A.cond c false |> analyze p2)
    | While (c, body) as while_ ->
        let rec loop n s =
          if n <= 0 then
            S.join
              (s |> A.cond c false)
              (s |> A.cond c true |> analyze body |> request while_)
          else
            S.join
              (s |> A.cond c false)
              (s |> A.cond c true |> analyze body |> loop (n - 1))
        in
        loop loop_unfold s

  module P = struct
    type t = (Expr.t, BExpr.t) Test_common.While.prog

    let compare = Test_common.While.compare_prog Expr.compare BExpr.compare
    let pp fmt p = Test_common.While.pp_prog Expr.pp BExpr.pp fmt p
  end

  module PMap = struct
    include Map.Make (P)

    let pp pp_v fmt m =
      let open Format in
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             fprintf fmt "@[<v 2>@[%a@] ->@ @[%a@]@]" P.pp k pp_v v))
        (to_seq m)
  end

  module M = Solver.Naive (P) (PMap) (S) (S) (MFG)

  let analyze ~sparse ~loop_unfold strategy prog input =
    fst
    @@ M.fix_gen ~debug:true strategy (analyze ~sparse ~loop_unfold) prog input

  let run ~sparse ~loop_unfold strategy p s =
    ignore @@ analyze ~sparse ~loop_unfold strategy p s
end

module MFG =
  Functional.Make (Test_common.IntervalStore) (Test_common.IntervalStore)

module Analyzer =
  While (Test_common.While.Expr) (Test_common.While.BExpr)
    (Test_common.IntervalStore)
    (MFG)
    (Test_common.While.ValueAnalysis)

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Assign ("i", Const 0))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      i := 0 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      i := 0 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      i := 0 ->
        [[] ====> [i !-> [0,0]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      i := 0: 2
      Node numbering:
      i := 0: 0
      Widening points:


      Full graph:
      i := 0 ->
        [[] ====> [i !-> [0,0]]]
      Final MFG:
      [[] ====> [i !-> [0,0]]]

      Final result:
      [i !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq (Assign ("i", Const 0), Assign ("j", Const 1)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      i := 0; j := 1 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      i := 0; j := 1 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      i := 0; j := 1 ->
        [[] ====> [i !-> [0,0];
                   j !-> [1,1]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      i := 0; j := 1: 2
      Node numbering:
      i := 0; j := 1: 0
      Widening points:


      Full graph:
      i := 0; j := 1 ->
        [[] ====> [i !-> [0,0];
                   j !-> [1,1]]]
      Final MFG:
      [[] ====> [i !-> [0,0];
                 j !-> [1,1]]]

      Final result:
      [i !-> [0,0];
       j !-> [1,1]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq (Assign ("i", Const 0), Assign ("i", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      i := 0; i := 2 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      i := 0; i := 2 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      i := 0; i := 2 ->
        [[] ====> [i !-> [2,2]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      i := 0; i := 2: 2
      Node numbering:
      i := 0; i := 2: 0
      Widening points:


      Full graph:
      i := 0; i := 2 ->
        [[] ====> [i !-> [2,2]]]
      Final MFG:
      [[] ====> [i !-> [2,2]]]

      Final result:
      [i !-> [2,2]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (If (Unknown, Assign ("i", Const 0), Assign ("i", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      if ? then i := 0 else i := 2 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      if ? then i := 0 else i := 2 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      if ? then i := 0 else i := 2 ->
        [[] ====> [i !-> [0,2]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      if ? then i := 0 else i := 2: 2
      Node numbering:
      if ? then i := 0 else i := 2: 0
      Widening points:


      Full graph:
      if ? then i := 0 else i := 2 ->
        [[] ====> [i !-> [0,2]]]
      Final MFG:
      [[] ====> [i !-> [0,2]]]

      Final result:
      [i !-> [0,2]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (If (Unknown, Assign ("i", Const 0), Assign ("j", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      if ? then i := 0 else j := 2 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      if ? then i := 0 else j := 2 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      if ? then i := 0 else j := 2 ->
        [[] ====> [i -> [0,0];
                   j -> [2,2]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      if ? then i := 0 else j := 2: 2
      Node numbering:
      if ? then i := 0 else j := 2: 0
      Widening points:


      Full graph:
      if ? then i := 0 else j := 2 ->
        [[] ====> [i -> [0,0];
                   j -> [2,2]]]
      Final MFG:
      [[] ====> [i -> [0,0];
                 j -> [2,2]]]

      Final result:
      [i -> [0,0];
       j -> [2,2]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C; A; C |]
    (Seq
       ( Assign ("i", Const 0),
         While (Lt (Var "i", Const 100), Assign ("i", Plus (Var "i", Const 1)))
       ))
    Test_common.IntervalStore.empty;
  [%expect {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [1,1]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [2,2]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [1,+∞]] ====> ⊥]

    Iteration #3:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [1,+∞]] ====> ⊥]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [2,2]]
    while i < 100 do i := i + 1 done: [i !-> [2,100]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [2,2]] ====> ⊥;
       [i !-> [2,100]] ====> ⊥;
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #4:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [2,2]] ====> ⊥;
       [i !-> [2,100]] ====> ⊥;
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [3,3]]
    while i < 100 do i := i + 1 done: [i !-> [3,100]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [2,2]] ====> ⊥;
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> ⊥;
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #5:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [2,2]] ====> ⊥;
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> ⊥;
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [3,3]]
    while i < 100 do i := i + 1 done: [i !-> [3,100]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #6:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [3,3]]
    while i < 100 do i := i + 1 done: [i !-> [3,100]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #7:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [3,3]]
    while i < 100 do i := i + 1 done: [i !-> [3,100]]

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> [i !-> [100,100]]];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]

    Number of iterations: 8
    Number of calls: 31
    Number of iterations per point:
    i := 0; while i < 100 do i := i + 1 done: 2
    while i < 100 do i := i + 1 done: 6
    Node numbering:
    i := 0; while i < 100 do i := i + 1 done: 0
    while i < 100 do i := i + 1 done: 1
    Widening points:
    while i < 100 do i := i + 1 done

    Full graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> [i !-> [100,100]]];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [2,2]] ====> [i !-> [100,100]];
       [i !-> [2,100]] ====> [i !-> [100,100]];
       [i !-> [1,+∞]] ====> [i !-> [100,+∞]]]
    Final MFG:
    [[] ====> [i !-> [100,100]]]

    Final result:
    [i !-> [100,100]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:false ~loop_unfold:0 [| C; A; A; C |]
    (Seq
       ( Assign ("i", Const 0),
         While (Lt (Var "i", Const 100), Assign ("i", Plus (Var "i", Const 1)))
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]
    New calls:
    i := 0: []

    New graph:
    i := 0 ->
      [[] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0 ->
      [[] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]

    Iteration #3:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #4:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    i := i + 1: [i !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #5:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #6:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> ⊥]

    Iteration #7:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> ⊥]
    New calls:
    i := i + 1: [i !-> [0,99]]
    while i < 100 do i := i + 1 done: [i !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #8:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> ⊥];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    i := i + 1: [i !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #9:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    while i < 100 do i := i + 1 done: [i !-> [1,100]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> ⊥;
       [i !-> [1,100]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #10:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> ⊥;
       [i !-> [1,100]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    i := i + 1: [i !-> [1,99]]
    while i < 100 do i := i + 1 done: [i !-> [2,2]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #11:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> ⊥;
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    i := i + 1: [i !-> [1,99]]
    while i < 100 do i := i + 1 done: [i !-> [2,2]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #12:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    i := i + 1: [i !-> [1,99]]
    while i < 100 do i := i + 1 done: [i !-> [2,2]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,0]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Iteration #13:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do i := i + 1 done ->
      [[i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,0]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    New calls:
    i := i + 1: [i !-> [1,99]]
    while i < 100 do i := i + 1 done: [i !-> [2,2]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> [i !-> [100,100]]];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]

    Number of iterations: 14
    Number of calls: 81
    Number of iterations per point:
    i := 0: 2
    i := i + 1: 5
    i := 0; while i < 100 do i := i + 1 done: 2
    while i < 100 do i := i + 1 done: 7
    Node numbering:
    i := 0: 1
    i := i + 1: 3
    i := 0; while i < 100 do i := i + 1 done: 0
    while i < 100 do i := i + 1 done: 2
    Widening points:
    while i < 100 do i := i + 1 done

    Full graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [1,1]] ====> [i !-> [2,2]];
       [i !-> [0,0]] ====> [i !-> [1,1]];
       [i !-> [0,99]] ====> [i !-> [1,100]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[] ====> [i !-> [100,100]]];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,0]] ====> [i !-> [100,100]];
       [i !-> [1,1]] ====> [i !-> [100,100]];
       [i !-> [1,100]] ====> [i !-> [100,100]];
       [i !-> [0,+∞]] ====> [i !-> [100,+∞]]]
    Final MFG:
    [[] ====> [i !-> [100,100]]]

    Final result:
    [i !-> [100,100]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C; A; C |]
    (Seq
       ( Assign ("i", Const 0),
         Seq
           ( Assign ("j", Const 0),
             While
               ( Lt (Var "i", Const 100),
                 Seq
                   ( Assign ("j", Const 1),
                     Seq
                       ( While
                           ( Lt (Var "j", Const 100),
                             Assign ("j", Plus (Var "j", Const 1)) ),
                         Assign ("i", Plus (Var "i", Const 1)) ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [2,2]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [3,3]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> ⊥]

    Iteration #3:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> ⊥]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [3,3]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,3]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,100]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]

    Iteration #4:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,3]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,100]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]

    Iteration #5:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]

    Iteration #6:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [2,2]] ====> ⊥;
       [i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]

    Iteration #7:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]
    New calls:
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done:
    [i !-> [1,1];
     j !-> [100,100]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]

    Iteration #8:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> ⊥]

    Iteration #9:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> ⊥]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,+∞];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]

    Iteration #10:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]
    New calls:
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done:
    [i !-> [1,+∞];
     j !-> [100,+∞]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,+∞];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥;
       [i !-> [1,+∞];
        j !-> [100,+∞]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]

    Iteration #11:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥;
       [i !-> [1,+∞];
        j !-> [100,+∞]] ====> ⊥];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [1,99];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,+∞];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥;
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]

    Iteration #12:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> ⊥;
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [1,99];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,+∞];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> [i !-> [100,+∞];
                                j !-> [100,+∞]];
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]

    Iteration #13:
    Initial graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> ⊥];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> [i !-> [100,+∞];
                                j !-> [100,+∞]];
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]
    New calls:
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,4]]
    while j < 100 do j := j + 1 done: [i !-> [0,0];
                                       j !-> [4,100]]
    while j < 100 do j := j + 1 done: [i !-> [1,1];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [1,99];
                                       j !-> [2,2]]
    while j < 100 do j := j + 1 done: [i !-> [0,+∞];
                                       j !-> [3,100]]

    New graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> [i !-> [100,+∞];
                 j !-> [100,+∞]]];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> [i !-> [100,+∞];
                                j !-> [100,+∞]];
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]

    Number of iterations: 14
    Number of calls: 78
    Number of iterations per point:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done: 2
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done: 4
    while j < 100 do j := j + 1 done: 8
    Node numbering:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done: 0
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done: 2
    while j < 100 do j := j + 1 done: 1
    Widening points:
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done,
    while j < 100 do j := j + 1 done

    Full graph:
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[] ====> [i !-> [100,+∞];
                 j !-> [100,+∞]]];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [1,1];
        j !-> [100,100]] ====> [i !-> [100,+∞];
                                j !-> [100,+∞]];
       [i !-> [1,+∞];
        j !-> [100,+∞]]
       ====> [i !-> [100,+∞];
              j !-> [100,+∞]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [3,3]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,2]] ====> [i !-> [0,0];
                            j !-> [100,100]];
       [i !-> [0,0];
        j !-> [3,100]] ====> [i !-> [0,0];
                              j !-> [100,100]];
       [i !-> [0,0];
        j !-> [2,+∞]] ====> [i !-> [0,0];
                               j !-> [100,+∞]];
       [i !-> [0,+∞];
        j !-> [2,+∞]] ====> [i !-> [0,+∞];
                               j !-> [100,+∞]]]
    Final MFG:
    [[] ====> [i !-> [100,+∞];
               j !-> [100,+∞]]]

    Final result:
    [i !-> [100,+∞];
     j !-> [100,+∞]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq
       ( Assign ("x", Const 0),
         If
           ( Or (Lt (Var "x", Const 0), Lt (Const 0, Var "x")),
             Assign ("y", Const 1),
             Assign ("y", Const 2) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[] ====> ⊥]
      New calls:

      New graph:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[] ====> [x !-> [0,0];
                   y !-> [2,2]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2: 2
      Node numbering:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2: 0
      Widening points:


      Full graph:
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[] ====> [x !-> [0,0];
                   y !-> [2,2]]]
      Final MFG:
      [[] ====> [x !-> [0,0];
                 y !-> [2,2]]]

      Final result:
      [x !-> [0,0];
       y !-> [2,2]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq
       ( Assign ("x", Unknown),
         If
           ( Or (Lt (Var "x", Const 0), Lt (Const 0, Var "x")),
             Assign ("x", Const 0),
             Skip ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Iteration #0:
      Initial graph:

      New calls:

      New graph:
      x := ?; if x < 0 || 0 < x then x := 0 else skip ->
        [[] ====> ⊥]

      Iteration #1:
      Initial graph:
      x := ?; if x < 0 || 0 < x then x := 0 else skip ->
        [[] ====> ⊥]
      New calls:

      New graph:
      x := ?; if x < 0 || 0 < x then x := 0 else skip ->
        [[] ====> [x !-> [0,0]]]

      Number of iterations: 2
      Number of calls: 2
      Number of iterations per point:
      x := ?; if x < 0 || 0 < x then x := 0 else skip: 2
      Node numbering:
      x := ?; if x < 0 || 0 < x then x := 0 else skip: 0
      Widening points:


      Full graph:
      x := ?; if x < 0 || 0 < x then x := 0 else skip ->
        [[] ====> [x !-> [0,0]]]
      Final MFG:
      [[] ====> [x !-> [0,0]]]

      Final result:
      [x !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:1 [| C; A; C |]
    (Seq
       ( Assign ("n", Const 50),
         Seq
           ( Assign ("i", Const 0),
             While
               ( Lt (Var "i", Var "n"),
                 Seq
                   ( Assign ("i", Plus (Var "i", Const 1)),
                     Assign ("n", Minus (Var "n", Const 1)) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [2,2];
                                                 n !-> [48,48]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥]

    Iteration #2:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [4,4];
                                                 n !-> [46,46]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> ⊥]

    Iteration #3:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> ⊥]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [4,4];
                                                 n !-> [46,46]]
    while i < n do i := i + 1; n := n - 1 done: [i !-> [4,47];
                                                 n !-> [3,46]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [4,4];
        n !-> [46,46]] ====> ⊥;
       [i !-> [4,47];
        n !-> [3,46]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]

    Iteration #4:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [4,4];
        n !-> [46,46]] ====> ⊥;
       [i !-> [4,47];
        n !-> [3,46]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,6];
                                                 n !-> [44,44]]
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,45];
                                                 n !-> [5,44]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [4,4];
        n !-> [46,46]] ====> ⊥;
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]

    Iteration #5:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [4,4];
        n !-> [46,46]] ====> ⊥;
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,6];
                                                 n !-> [44,44]]
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,45];
                                                 n !-> [5,44]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]

    Iteration #6:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> ⊥;
       [i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,6];
                                                 n !-> [44,44]]
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,45];
                                                 n !-> [5,44]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,2];
        n !-> [48,48]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]

    Iteration #7:
    Initial graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> ⊥];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,2];
        n !-> [48,48]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]
    New calls:
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,6];
                                                 n !-> [44,44]]
    while i < n do i := i + 1; n := n - 1 done: [i !-> [6,45];
                                                 n !-> [5,44]]

    New graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> [i !-> [4,47];
                 n !-> [3,46]]];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]

    Number of iterations: 8
    Number of calls: 31
    Number of iterations per point:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done: 2
    while i < n do i := i + 1; n := n - 1 done: 6
    Node numbering:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done: 0
    while i < n do i := i + 1; n := n - 1 done: 1
    Widening points:
    while i < n do i := i + 1; n := n - 1 done

    Full graph:
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[] ====> [i !-> [4,47];
                 n !-> [3,46]]];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [2,2];
        n !-> [48,48]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,4];
        n !-> [46,46]] ====> [i !-> [4,47];
                              n !-> [3,46]];
       [i !-> [4,47];
        n !-> [3,46]] ====> [i !-> [4,47];
                             n !-> [3,46]];
       [i !-> [2,+∞];
        n !-> [-∞,48]] ====> [i !-> [2,+∞];
                                n !-> [-∞,48]]]
    Final MFG:
    [[] ====> [i !-> [4,47];
               n !-> [3,46]]]

    Final result:
    [i !-> [4,47];
     n !-> [3,46]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq
       ( Assign ("i", Const 0),
         While (Le (Var "i", Const 100), Assign ("i", Minus (Var "i", Const 1)))
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥]
    New calls:
    while i <= 100 do i := i - 1 done: [i !-> [-1,-1]]

    New graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥];
    while i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥];
    while i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥]
    New calls:
    while i <= 100 do i := i - 1 done: [i !-> [-2,-2]]

    New graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥];
    while i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥;
       [i !-> [-∞,-1]] ====> ⊥]

    Number of iterations: 3
    Number of calls: 6
    Number of iterations per point:
    i := 0; while i <= 100 do i := i - 1 done: 1
    while i <= 100 do i := i - 1 done: 2
    Node numbering:
    i := 0; while i <= 100 do i := i - 1 done: 0
    while i <= 100 do i := i - 1 done: 1
    Widening points:
    while i <= 100 do i := i - 1 done

    Full graph:
    i := 0; while i <= 100 do i := i - 1 done ->
      [[] ====> ⊥];
    while i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥;
       [i !-> [-∞,-1]] ====> ⊥]
    Final MFG:
    [[] ====> ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq
       ( Assign ("i", Const 0),
         While
           ( And (Unknown, Le (Var "i", Const 100)),
             Assign ("i", Minus (Var "i", Const 1)) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> ⊥]
    New calls:
    while ? && i <= 100 do i := i - 1 done: [i !-> [-1,-1]]

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [0,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [0,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> ⊥]
    New calls:
    while ? && i <= 100 do i := i - 1 done: [i !-> [-2,-2]]

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [0,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-1,-1]];
       [i !-> [-∞,-1]] ====> ⊥]

    Iteration #3:
    Initial graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [0,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-1,-1]];
       [i !-> [-∞,-1]] ====> ⊥]
    New calls:
    while ? && i <= 100 do i := i - 1 done: [i !-> [-2,-2]]
    while ? && i <= 100 do i := i - 1 done: [i !-> [-∞,-2]]

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-1,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-1,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]

    Iteration #4:
    Initial graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-1,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-1,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]
    New calls:
    while ? && i <= 100 do i := i - 1 done: [i !-> [-2,-2]]
    while ? && i <= 100 do i := i - 1 done: [i !-> [-∞,-2]]

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-1,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-∞,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]

    Iteration #5:
    Initial graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-1,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-∞,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]
    New calls:
    while ? && i <= 100 do i := i - 1 done: [i !-> [-2,-2]]
    while ? && i <= 100 do i := i - 1 done: [i !-> [-∞,-2]]

    New graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-∞,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-∞,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]

    Number of iterations: 6
    Number of calls: 15
    Number of iterations per point:
    i := 0; while ? && i <= 100 do i := i - 1 done: 3
    while ? && i <= 100 do i := i - 1 done: 4
    Node numbering:
    i := 0; while ? && i <= 100 do i := i - 1 done: 0
    while ? && i <= 100 do i := i - 1 done: 1
    Widening points:
    while ? && i <= 100 do i := i - 1 done

    Full graph:
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[] ====> [i !-> [-∞,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-1,-1]] ====> [i !-> [-∞,-1]];
       [i !-> [-∞,-1]] ====> [i !-> [-∞,-1]]]
    Final MFG:
    [[] ====> [i !-> [-∞,0]]]

    Final result:
    [i !-> [-∞,0]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C |]
    (Seq
       ( Assign ("x", Const 0),
         While
           ( Lt (Var "x", Const 10),
             Seq
               ( Assign ("y", Const 0),
                 While
                   (Lt (Var "y", Var "x"), Assign ("y", Plus (Var "y", Const 1)))
               ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[] ====> ⊥]
    New calls:
    while x < 10 do y := 0; while y < x do y := y + 1 done done:
    [x !-> [0,0];
     y !-> [0,0]]

    New graph:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[] ====> ⊥];
    while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[x !-> [0,0];
        y !-> [0,0]] ====> ⊥]

    Number of iterations: 2
    Number of calls: 3
    Number of iterations per point:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done: 1
    while x < 10 do y := 0; while y < x do y := y + 1 done done: 1
    Node numbering:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done: 0
    while x < 10 do y := 0; while y < x do y := y + 1 done done: 1
    Widening points:
    while x < 10 do y := 0; while y < x do y := y + 1 done done

    Full graph:
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[] ====> ⊥];
    while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[x !-> [0,0];
        y !-> [0,0]] ====> ⊥]
    Final MFG:
    [[] ====> ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( While
                   (Le (Var "y", Var "x"), Assign ("y", Plus (Var "y", Const 1))),
                 Assign ("x", Var "y") ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥]
    New calls:
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [1,1];
     y !-> [1,1]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [1,1];
                                      y !-> [2,2]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]

    Iteration #3:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]
    New calls:

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]

    Iteration #4:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]
    New calls:
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,2];
     y !-> [2,2]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> ⊥]

    Iteration #5:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> ⊥]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [1,999];
                                      y !-> [2,1000]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,2];
     y !-> [2,2]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,+∞];
     y !-> [2,+∞]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]

    Iteration #6:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> ⊥];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [2,2];
                                      y !-> [3,3]]
    while y <= x do y := y + 1 done: [x !-> [2,999];
                                      y !-> [3,1000]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [3,+∞];
     y !-> [3,+∞]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]

    Iteration #7:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [2,2];
                                      y !-> [3,3]]
    while y <= x do y := y + 1 done: [x !-> [2,999];
                                      y !-> [3,1000]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,1000];
     y !-> [2,1000]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]

    Iteration #8:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [2,2];
                                      y !-> [3,3]]
    while y <= x do y := y + 1 done: [x !-> [2,999];
                                      y !-> [3,1000]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,1000];
     y !-> [2,1000]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]

    Iteration #9:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]
    New calls:
    while y <= x do y := y + 1 done: [x !-> [2,2];
                                      y !-> [3,3]]
    while y <= x do y := y + 1 done: [x !-> [2,999];
                                      y !-> [3,1000]]
    while x < 1000 do while y <= x do y := y + 1 done; x := y done:
    [x !-> [2,1000];
     y !-> [2,1000]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> [x !-> [1000,+∞];
                 y !-> [2,+∞]]];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]

    Number of iterations: 10
    Number of calls: 48
    Number of iterations per point:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done: 2
    while y <= x do y := y + 1 done: 4
    while x < 1000 do while y <= x do y := y + 1 done; x := y done: 6
    Node numbering:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done: 0
    while y <= x do y := y + 1 done: 2
    while x < 1000 do while y <= x do y := y + 1 done; x := y done: 1
    Widening points:
    while y <= x do y := y + 1 done,
    while x < 1000 do while y <= x do y := y + 1 done; x := y done

    Full graph:
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[] ====> [x !-> [1000,+∞];
                 y !-> [2,+∞]]];
    while y <= x do y := y + 1 done ->
      [[x !-> [1,1];
        y !-> [2,2]] ====> [x !-> [1,1];
                            y !-> [2,2]];
       [x !-> [1,999];
        y !-> [2,1000]] ====> [x !-> [1,999];
                               y !-> [2,1000]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [1000,+∞];
                            y !-> [2,+∞]];
       [x !-> [2,+∞];
        y !-> [2,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [2,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [1000,+∞];
                               y !-> [1,+∞]]]
    Final MFG:
    [[] ====> [x !-> [1000,+∞];
               y !-> [2,+∞]]]

    Final result:
    [x !-> [1000,+∞];
     y !-> [2,+∞]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( Seq
                   ( Assign ("y", Const 0),
                     While
                       ( Le (Var "y", Const 1000),
                         Assign ("y", Plus (Var "y", Const 1)) ) ),
                 Assign ("x", Var "y") ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [1,1]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [2,2]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥]

    Iteration #3:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> ⊥]

    Iteration #4:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [2,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [3,3]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #5:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [3,3]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #6:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #7:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #8:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #9:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done:
    [x !-> [1001,1001];
     y !-> [1001,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]] ====> ⊥]

    Iteration #10:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001]]]

    Iteration #11:
    Initial graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [4,4]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> [x !-> [1001,1001];
                 y !-> [1001,1001]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001]]]

    Number of iterations: 12
    Number of calls: 61
    Number of iterations per point:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done: 2
    while y <= 1000 do y := y + 1 done: 8
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done: 2
    Node numbering:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done: 0
    while y <= 1000 do y := y + 1 done: 1
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done: 2
    Widening points:
    while y <= 1000 do y := y + 1 done,
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done

    Full graph:
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[] ====> [x !-> [1001,1001];
                 y !-> [1001,1001]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [3,3]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001]]]
    Final MFG:
    [[] ====> [x !-> [1001,1001];
               y !-> [1001,1001]]]

    Final result:
    [x !-> [1001,1001];
     y !-> [1001,1001]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( Seq
                   ( Assign ("y", Const 0),
                     While
                       ( Le (Var "y", Const 1000),
                         Assign ("y", Plus (Var "y", Const 1)) ) ),
                 Seq
                   ( Seq
                       ( Assign ("z", Const 0),
                         While
                           ( Le (Var "z", Var "y"),
                             Assign ("z", Plus (Var "z", Const 1)) ) ),
                     Assign ("x", Var "y") ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [1,1]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [2,2]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> ⊥]

    Iteration #3:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [2,2]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [2,1001]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #4:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #5:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #6:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]

    Iteration #7:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [1,1]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥]

    Iteration #8:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [2,2]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]] ====> ⊥]

    Iteration #9:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [2,2]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [2,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]

    Iteration #10:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]

    Iteration #11:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]

    Iteration #12:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]] ====> ⊥;
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]

    Iteration #13:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done: [x !-> [1001,1001];
           y !-> [1001,1001];
           z !-> [1002,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]] ====> ⊥]

    Iteration #14:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]] ====> ⊥]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001];
              z !-> [1002,1002]]]

    Iteration #15:
    Initial graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> ⊥];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001];
              z !-> [1002,1002]]]
    New calls:
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,3]]
    while y <= 1000 do y := y + 1 done: [x !-> [0,0];
                                         y !-> [3,1001]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,3]]
    while z <= y do z := z + 1 done:
    [x !-> [0,0];
     y !-> [1001,1001];
     z !-> [3,1002]]

    New graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> [x !-> [1001,1001];
                 y !-> [1001,1001];
                 z !-> [1002,1002]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001];
              z !-> [1002,1002]]]

    Number of iterations: 16
    Number of calls: 105
    Number of iterations per point:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done: 2
    while y <= 1000 do y := y + 1 done: 6
    while z <= y do z := z + 1 done: 6
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done: 2
    Node numbering:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done: 0
    while y <= 1000 do y := y + 1 done: 1
    while z <= y do z := z + 1 done: 2
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done: 3
    Widening points:
    while y <= 1000 do y := y + 1 done,
    while z <= y do z := z + 1 done,
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done

    Full graph:
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[] ====> [x !-> [1001,1001];
                 y !-> [1001,1001];
                 z !-> [1002,1002]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [2,2]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,1]] ====> [x !-> [0,0];
                            y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [2,1001]] ====> [x !-> [0,0];
                               y !-> [1001,1001]];
       [x !-> [0,0];
        y !-> [1,+∞]] ====> [x !-> [0,0];
                               y !-> [1001,+∞]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,2]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,1]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [2,1002]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,1002]];
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]
       ====> [x !-> [0,0];
              y !-> [1001,1001];
              z !-> [1002,+∞]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]
       ====> [x !-> [1001,1001];
              y !-> [1001,1001];
              z !-> [1002,1002]]]
    Final MFG:
    [[] ====> [x !-> [1001,1001];
               y !-> [1001,1001];
               z !-> [1002,1002]]]

    Final result:
    [x !-> [1001,1001];
     y !-> [1001,1001];
     z !-> [1002,1002]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| A; C; A; C |]
    (Seq
       ( Seq
           ( Assign ("a1", Const 0),
             Seq
               ( Assign ("a2", Const 0),
                 Seq
                   ( Assign ("a3", Const 0),
                     Seq
                       ( Assign ("a4", Const 0),
                         Seq (Assign ("a5", Const 0), Assign ("r", Const 0)) )
                   ) ) ),
         Seq
           ( While
               (Lt (Var "a1", Const 20), Assign ("a1", Plus (Var "a1", Const 1))),
             Seq
               ( While
                   ( Lt (Var "a2", Const 20),
                     Assign ("a2", Plus (Var "a2", Const 1)) ),
                 Seq
                   ( While
                       ( Lt (Var "a3", Const 20),
                         Assign ("a3", Plus (Var "a3", Const 1)) ),
                     Seq
                       ( While
                           ( Lt (Var "a4", Const 20),
                             Assign ("a4", Plus (Var "a4", Const 1)) ),
                         While
                           ( Lt (Var "a5", Const 20),
                             Assign ("a5", Plus (Var "a5", Const 1)) ) ) ) ) )
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [1,1];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #2:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [2,2];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #3:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,3];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #4:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,3];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [2,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #5:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #6:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #7:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #8:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #9:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [1,1];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #10:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [2,2];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #11:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,3];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #12:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,3];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [2,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #13:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #14:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #15:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #16:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #17:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [1,1];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #18:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [2,2];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #19:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,3];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #20:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,3];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [2,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #21:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #22:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #23:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #24:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #25:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [1,1];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #26:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [2,2];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #27:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,3];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #28:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,3];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [2,20];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #29:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #30:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #31:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #32:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]

    Iteration #33:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [1,1];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #34:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [2,2];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #35:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,3];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====> ⊥]

    Iteration #36:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====> ⊥]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,3];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [2,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Iteration #37:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [4,4];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Iteration #38:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [4,4];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Iteration #39:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [4,4];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Iteration #40:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====> ⊥;
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [4,4];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Iteration #41:
    Initial graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====> ⊥];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    New calls:
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [4,4];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a1 < 20 do a1 := a1 + 1 done:
    [a1 !-> [3,20];
     a2 !-> [0,0];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [4,4];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a2 < 20 do a2 := a2 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [3,20];
     a3 !-> [0,0];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [4,4];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a3 < 20 do a3 := a3 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [3,20];
     a4 !-> [0,0];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [4,4];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a4 < 20 do a4 := a4 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [3,20];
     a5 !-> [0,0];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [4,4];
     r !-> [0,0]]
    while a5 < 20 do a5 := a5 + 1 done:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [3,20];
     r !-> [0,0]]

    New graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]

    Number of iterations: 42
    Number of calls: 622
    Number of iterations per point:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done: 2
    while a1 < 20 do a1 := a1 + 1 done: 8
    while a2 < 20 do a2 := a2 + 1 done: 8
    while a3 < 20 do a3 := a3 + 1 done: 8
    while a4 < 20 do a4 := a4 + 1 done: 8
    while a5 < 20 do a5 := a5 + 1 done: 8
    Node numbering:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done: 0
    while a1 < 20 do a1 := a1 + 1 done: 1
    while a2 < 20 do a2 := a2 + 1 done: 2
    while a3 < 20 do a3 := a3 + 1 done: 3
    while a4 < 20 do a4 := a4 + 1 done: 4
    while a5 < 20 do a5 := a5 + 1 done: 5
    Widening points:
    while a1 < 20 do a1 := a1 + 1 done,
    while a2 < 20 do a2 := a2 + 1 done,
    while a3 < 20 do a3 := a3 + 1 done,
    while a4 < 20 do a4 := a4 + 1 done,
    while a5 < 20 do a5 := a5 + 1 done

    Full graph:
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[] ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [3,3];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,2];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [2,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,1];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [1,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [3,3];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,2];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [2,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,1];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [1,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [3,3];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,2];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [2,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,1];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [3,3];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,2];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [2,20];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,1];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [3,3];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,2];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [2,20];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,1];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]];
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,+∞];
        r !-> [0,0]]]
    Final MFG:
    [[] ====>
     [a1 !-> [20,20];
      a2 !-> [20,20];
      a3 !-> [20,20];
      a4 !-> [20,20];
      a5 !-> [20,20];
      r !-> [0,0]]]

    Final result:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [20,20];
     r !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("FALSE", Const 0), Assign ("TRUE", Const 1)),
         Seq
           ( Seq (Assign ("x", Const 100), Assign ("b", Var "TRUE")),
             While
               ( And (Le (Var "b", Var "TRUE"), Le (Var "TRUE", Var "b")),
                 Seq
                   ( Assign ("x", Minus (Var "x", Const 1)),
                     If
                       ( Lt (Var "x", Const 0),
                         Assign ("b", Var "TRUE"),
                         Assign ("b", Var "FALSE") ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,0];
           x !-> [99,99]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]] ====> ⊥]

    Iteration #2:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]] ====> ⊥]
    New calls:

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [99,99]]]

    Iteration #3:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [99,99]]]
    New calls:

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> [FALSE !-> [0,0];
                 TRUE !-> [1,1];
                 b !-> [0,0];
                 x !-> [99,99]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [99,99]]]

    Number of iterations: 4
    Number of calls: 7
    Number of iterations per point:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done: 2
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done: 2
    Node numbering:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done: 0
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done: 1
    Widening points:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done

    Full graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[] ====> [FALSE !-> [0,0];
                 TRUE !-> [1,1];
                 b !-> [0,0];
                 x !-> [99,99]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [99,99]]]
    Final MFG:
    [[] ====> [FALSE !-> [0,0];
               TRUE !-> [1,1];
               b !-> [0,0];
               x !-> [99,99]]]

    Final result:
    [FALSE !-> [0,0];
     TRUE !-> [1,1];
     b !-> [0,0];
     x !-> [99,99]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("FALSE", Const 0), Assign ("TRUE", Const 1)),
         Seq
           ( Seq (Assign ("x", Const 100), Assign ("b", Var "TRUE")),
             While
               ( And (Le (Var "b", Var "TRUE"), Le (Var "TRUE", Var "b")),
                 Seq
                   ( Assign ("x", Minus (Var "x", Const 1)),
                     If
                       ( Le (Const 0, Var "x"),
                         Assign ("b", Var "TRUE"),
                         Assign ("b", Var "FALSE") ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [99,99]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥]

    Iteration #2:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [98,98]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥]

    Iteration #3:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [97,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]

    Iteration #4:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [97,97]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,98]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]

    Iteration #5:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [96,96]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]

    Iteration #6:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]] ====> ⊥]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [96,96]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]

    Iteration #7:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [96,96]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]

    Iteration #8:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]] ====> ⊥;
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [96,96]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]

    Iteration #9:
    Initial graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> ⊥];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]
    New calls:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [1,1];
           x !-> [96,96]]
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: [FALSE !-> [0,0];
           TRUE !-> [1,1];
           b !-> [0,1];
           x !-> [-∞,97]]

    New graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> [FALSE !-> [0,0];
                 TRUE !-> [1,1];
                 b !-> [0,0];
                 x !-> [-∞,98]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]

    Number of iterations: 10
    Number of calls: 46
    Number of iterations per point:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: 2
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: 8
    Node numbering:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: 0
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done: 1
    Widening points:
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done

    Full graph:
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[] ====> [FALSE !-> [0,0];
                 TRUE !-> [1,1];
                 b !-> [0,0];
                 x !-> [-∞,98]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [97,97]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [98,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,98]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]];
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====> [FALSE !-> [0,0];
              TRUE !-> [1,1];
              b !-> [0,0];
              x !-> [-∞,98]]]
    Final MFG:
    [[] ====> [FALSE !-> [0,0];
               TRUE !-> [1,1];
               b !-> [0,0];
               x !-> [-∞,98]]]

    Final result:
    [FALSE !-> [0,0];
     TRUE !-> [1,1];
     b !-> [0,0];
     x !-> [-∞,98]] |}]

let%expect_test _ =
  Analyzer.run ~sparse:true ~loop_unfold:0 [| A; C; A; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 0)),
         While
           ( Le (Var "x", Const 99),
             Seq
               ( Assign ("x", Plus (Var "x", Const 1)),
                 If
                   ( Le (Var "x", Const 49),
                     Assign ("y", Plus (Var "y", Const 1)),
                     Assign ("y", Minus (Var "y", Const 1)) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [1,1];
     y !-> [1,1]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]

    Iteration #2:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,2];
     y !-> [2,2]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥]

    Iteration #3:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [3,3];
     y !-> [3,3]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> ⊥]

    Iteration #4:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> ⊥]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [3,3];
     y !-> [3,3]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,100];
     y !-> [0,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [3,3];
        y !-> [3,3]] ====> ⊥;
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]]]

    Iteration #5:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [3,3];
        y !-> [3,3]] ====> ⊥;
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]]]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,4];
     y !-> [4,4]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [3,100];
     y !-> [-1,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [4,4];
        y !-> [4,4]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [0,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]]]

    Iteration #6:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> ⊥;
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [4,4];
        y !-> [4,4]] ====> ⊥;
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [1,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [0,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]]]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [5,5];
     y !-> [5,5]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,100];
     y !-> [-2,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [0,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [-1,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]] ====> ⊥]

    Iteration #7:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [0,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> ⊥;
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]] ====> [x !-> [100,+∞];
                               y !-> [0,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [-1,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]] ====> ⊥]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [5,5];
     y !-> [5,5]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,100];
     y !-> [-2,+∞]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,100];
     y !-> [-∞,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [-1,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]

    Iteration #8:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> ⊥];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]] ====> [x !-> [100,+∞];
                                y !-> [-1,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [5,5];
     y !-> [5,5]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,100];
     y !-> [-2,+∞]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,100];
     y !-> [-∞,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [1,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]

    Iteration #9:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [1,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [1,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [5,5];
     y !-> [5,5]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,100];
     y !-> [-2,+∞]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,100];
     y !-> [-∞,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [1,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]

    Iteration #10:
    Initial graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [1,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]
    New calls:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [5,5];
     y !-> [5,5]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [4,100];
     y !-> [-2,+∞]]
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done:
    [x !-> [2,100];
     y !-> [-∞,+∞]]

    New graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [-∞,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]

    Number of iterations: 11
    Number of calls: 69
    Number of iterations per point:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done: 3
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done: 9
    Node numbering:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done: 0
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done: 1
    Widening points:
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done

    Full graph:
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[] ====> [x !-> [100,+∞];
                 y !-> [-∞,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [4,4];
        y !-> [4,4]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [3,3];
        y !-> [3,3]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,1];
        y !-> [1,1]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [2,2];
        y !-> [2,2]] ====> [x !-> [100,+∞];
                            y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [2,100];
        y !-> [0,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [3,100];
        y !-> [-1,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]];
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]
       ====> [x !-> [100,+∞];
              y !-> [-∞,+∞]]]
    Final MFG:
    [[] ====> [x !-> [100,+∞];
               y !-> [-∞,+∞]]]

    Final result:
    [x !-> [100,+∞];
     y !-> [-∞,+∞]] |}]

let%expect_test _ =
  Analyzer.run ~loop_unfold:0 ~sparse:true [| A; C |]
    (Seq
       ( Seq
           ( Seq
               ( Seq
                   ( Seq (Assign ("m", Const 0), Assign ("M", Const 512)),
                     Assign ("x", Unknown) ),
                 Seq (Assign ("min", Var "m"), Assign ("max", Var "M")) ),
             If
               ( Le (Var "min", Var "max"),
                 If (Le (Var "x", Var "max"), Assign ("max", Var "x"), Skip),
                 If (Lt (Var "x", Var "min"), Assign ("max", Var "min"), Skip)
               ) ),
         Seq
           ( Assign ("y", Var "max"),
             If
               ( And (Le (Var "m", Var "y"), Le (Var "y", Var "M")),
                 Assign ("result", Const 1),
                 Assign ("result", Const 0) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[] ====> ⊥]
    New calls:

    New graph:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[] ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,1];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]]

    Number of iterations: 2
    Number of calls: 2
    Number of iterations per point:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0: 2
    Node numbering:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0: 0
    Widening points:


    Full graph:
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[] ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,1];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]]
    Final MFG:
    [[] ====>
     [M !-> [512,512];
      m !-> [0,0];
      max !-> [-∞,512];
      min !-> [0,0];
      result !-> [0,1];
      x !-> [-∞,+∞];
      y !-> [-∞,512]]]

    Final result:
    [M !-> [512,512];
     m !-> [0,0];
     max !-> [-∞,512];
     min !-> [0,0];
     result !-> [0,1];
     x !-> [-∞,+∞];
     y !-> [-∞,512]] |}]

let%expect_test _ =
  Analyzer.run ~loop_unfold:0 ~sparse:false [| A; C |]
    (Seq
       ( Assign ("n", Const 0),
         While
           ( Bool true,
             If
               ( Lt (Var "n", Const 60),
                 Assign ("n", Plus (Var "n", Const 1)),
                 Assign ("n", Const 0) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]
    New calls:
    n := 0: []

    New graph:
    n := 0 ->
      [[] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]

    Iteration #2:
    Initial graph:
    n := 0 ->
      [[] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]
    New calls:

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]

    Iteration #3:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥]
    New calls:
    while true do if n < 60 then n := n + 1 else n := 0 done: [n !-> [0,0]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]

    Iteration #4:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]
    New calls:
    if n < 60 then n := n + 1 else n := 0: [n !-> [0,0]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]

    Iteration #5:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]
    New calls:
    n := n + 1: [n !-> [0,0]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]

    Iteration #6:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]

    Iteration #7:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]

    Iteration #8:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥]
    New calls:
    while true do if n < 60 then n := n + 1 else n := 0 done: [n !-> [1,1]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥]

    Iteration #9:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥]
    New calls:
    if n < 60 then n := n + 1 else n := 0: [n !-> [1,1]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥]

    Iteration #10:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> ⊥];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥]
    New calls:
    n := 0: [n !-> [60,+∞]]
    n := n + 1: [n !-> [0,59]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [1,1]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> ⊥];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥]

    Iteration #11:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> ⊥];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> ⊥];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥]
    New calls:
    n := 0: [n !-> [60,+∞]]
    n := n + 1: [n !-> [0,59]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [1,1]]
    while true do if n < 60 then n := n + 1 else n := 0 done: [n !-> [0,1]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥]

    Iteration #12:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,1]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥]
    New calls:
    n := 0: [n !-> [60,+∞]]
    n := n + 1: [n !-> [0,59]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [1,1]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [0,1]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,+∞]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥]

    Iteration #13:
    Initial graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,+∞]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,0]] ====> ⊥;
       [n !-> [1,1]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥]
    New calls:
    n := 0: [n !-> [60,+∞]]
    n := n + 1: [n !-> [0,59]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [1,1]]
    if n < 60 then n := n + 1 else n := 0: [n !-> [0,1]]
    while true do if n < 60 then n := n + 1 else n := 0 done: [n !-> [0,+∞]]

    New graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,+∞]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥;
       [n !-> [0,+∞]] ====> ⊥]

    Number of iterations: 14
    Number of calls: 80
    Number of iterations per point:
    n := 0: 4
    n := n + 1: 4
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done: 1
    if n < 60 then n := n + 1 else n := 0: 5
    while true do if n < 60 then n := n + 1 else n := 0 done: 4
    Node numbering:
    n := 0: 1
    n := n + 1: 4
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done: 0
    if n < 60 then n := n + 1 else n := 0: 3
    while true do if n < 60 then n := n + 1 else n := 0 done: 2
    Widening points:
    n := 0,
    while true do if n < 60 then n := n + 1 else n := 0 done

    Full graph:
    n := 0 ->
      [[] ====> [n !-> [0,0]];
       [n -> [60,+∞]] ====> [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [1,+∞]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[] ====> ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,0]] ====> [n !-> [1,1]];
       [n !-> [0,+∞]] ====> [n !-> [0,+∞]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [1,1]] ====> ⊥;
       [n !-> [0,0]] ====> ⊥;
       [n !-> [0,1]] ====> ⊥;
       [n !-> [0,+∞]] ====> ⊥]
    Final MFG:
    [[] ====> ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run ~loop_unfold:0 ~sparse:false [| A; C |]
    (Seq
       ( Assign ("i", Const 0),
         While
           ( Lt (Var "i", Const 4),
             Seq
               ( Assign ("j", Const 0),
                 Seq
                   ( While
                       ( Lt (Var "j", Const 4),
                         Seq
                           ( Assign ("i", Plus (Var "i", Const 1)),
                             Assign ("j", Plus (Var "j", Const 1)) ) ),
                     Assign ("i", Plus (Minus (Var "i", Var "j"), Const 1)) ) )
           ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]
    New calls:
    i := 0: []

    New graph:
    i := 0 ->
      [[] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]

    Iteration #2:
    Initial graph:
    i := 0 ->
      [[] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]

    Iteration #3:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥]
    New calls:
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: [i !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #4:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #5:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    j := 0: [i !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #6:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #7:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [0,0];
     j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]

    Iteration #8:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥]
    New calls:
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [0,0];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #9:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [0,0];
                             j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #10:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    i := i + 1: [i !-> [0,0];
                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #11:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #12:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    j := j + 1: [i !-> [1,1];
                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #13:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #14:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #15:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,1];
                                                 j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]

    Iteration #16:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #17:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    i := i + 1: [i !-> [0,+∞];
                 j !-> [0,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]

    Iteration #18:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #19:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    j := j + 1: [i !-> [1,+∞];
                 j !-> [0,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]

    Iteration #20:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]

    Iteration #21:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,1];
                               j !-> [1,1]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]

    Iteration #22:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥]

    Iteration #23:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #24:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [1,1];
        j !-> [1,1]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #25:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> ⊥;
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #26:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i - j + 1: [i !-> [0,+∞];
                     j !-> [4,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #27:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #28:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #29:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #30:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: [i !-> [-∞,+∞];
           j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #31:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #32:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> ⊥];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> ⊥;
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    j := 0: [i !-> [-∞,+∞];
             j -> [4,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> ⊥];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #33:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> ⊥];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> ⊥];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #34:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,+∞];
     j !-> [0,0]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]

    Iteration #35:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> ⊥];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥]

    Iteration #36:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Iteration #37:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    New calls:
    i := i + 1: [i !-> [-∞,+∞];
                 j !-> [0,+∞]]
    i := i - j + 1: [i !-> [-∞,+∞];
                     j !-> [4,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Iteration #38:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> ⊥];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Iteration #39:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    New calls:
    j := j + 1: [i !-> [-∞,+∞];
                 j !-> [0,+∞]]
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Iteration #40:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> ⊥];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Iteration #41:
    Initial graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    New calls:
    i := i + 1; j := j + 1: [i !-> [1,1];
                             j !-> [1,1]]
    i := i + 1; j := j + 1: [i !-> [0,+∞];
                             j !-> [0,3]]
    i := i + 1; j := j + 1: [i !-> [-∞,+∞];
                             j !-> [0,3]]
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1:
    [i !-> [-∞,3];
     j !-> [4,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [1,+∞];
                                                 j !-> [1,+∞]]
    while j < 4 do i := i + 1; j := j + 1 done: [i !-> [-∞,+∞];
                                                 j !-> [0,0]]

    New graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]

    Number of iterations: 42
    Number of calls: 565
    Number of iterations per point:
    i := 0: 2
    i := i + 1: 6
    i := i - j + 1: 4
    j := 0: 4
    j := j + 1: 6
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: 2
    i := i + 1; j := j + 1: 8
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1: 4
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1: 4
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: 4
    while j < 4 do i := i + 1; j := j + 1 done: 8
    Node numbering:
    i := 0: 1
    i := i + 1: 8
    i := i - j + 1: 10
    j := 0: 4
    j := j + 1: 9
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: 0
    i := i + 1; j := j + 1: 7
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1: 3
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1: 5
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done: 2
    while j < 4 do i := i + 1; j := j + 1 done: 6
    Widening points:
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done,
    while j < 4 do i := i + 1; j := j + 1 done

    Full graph:
    i := 0 ->
      [[] ====> [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [0,0]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [0,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [0,+∞]]];
    i := i - j + 1 ->
      [[i !-> [0,+∞];
        j !-> [4,+∞]] ====> [i !-> [-∞,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [0,0]] ====> [i !-> [0,0];
                            j !-> [0,0]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]] ====> [i !-> [-∞,+∞];
                              j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [1,1];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [1,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[] ====> [i !-> [4,+∞];
                 j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [1,1];
                            j !-> [1,1]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [1,+∞];
                               j !-> [1,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [1,+∞]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,0]] ====> [i !-> [-∞,+∞];
                            j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [0,0]] ====> [i !-> [4,+∞];
                            j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [4,+∞]] ====> [i !-> [4,+∞];
                               j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [0,0];
        j !-> [0,0]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [1,1];
        j !-> [1,1]] ====> [i !-> [0,+∞];
                            j !-> [4,+∞]];
       [i !-> [0,+∞];
        j !-> [0,+∞]] ====> [i !-> [0,+∞];
                               j !-> [4,+∞]];
       [i !-> [-∞,+∞];
        j !-> [0,+∞]]
       ====> [i !-> [-∞,+∞];
              j !-> [4,+∞]]]
    Final MFG:
    [[] ====> [i !-> [4,+∞];
               j !-> [4,+∞]]]

    Final result:
    [i !-> [4,+∞];
     j !-> [4,+∞]] |}]

let%expect_test _ =
  Analyzer.run ~loop_unfold:1 ~sparse:true [| A; C |]
    (While (Bool true, Assign ("x", Const 0)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    while true do x := 0 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    while true do x := 0 done ->
      [[] ====> ⊥]
    New calls:
    while true do x := 0 done: [x !-> [0,0]]

    New graph:
    while true do x := 0 done ->
      [[] ====> ⊥;
       [x !-> [0,0]] ====> ⊥]

    Number of iterations: 2
    Number of calls: 3
    Number of iterations per point:
    while true do x := 0 done: 2
    Node numbering:
    while true do x := 0 done: 0
    Widening points:
    while true do x := 0 done

    Full graph:
    while true do x := 0 done ->
      [[] ====> ⊥;
       [x !-> [0,0]] ====> ⊥]
    Final MFG:
    [[] ====> ⊥;
     [x !-> [0,0]] ====> ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run ~loop_unfold:0 ~sparse:false [| A; C |]
    (Seq
       ( Assign ("x", Const 2),
         While
           ( Lt (Const 0, Var "x"),
             If
               ( Le (Var "x", Const 1),
                 Skip,
                 Assign ("x", Minus (Var "x", Const 1)) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]

    Iteration #1:
    Initial graph:
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]
    New calls:
    x := 2: []

    New graph:
    x := 2 ->
      [[] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]

    Iteration #2:
    Initial graph:
    x := 2 ->
      [[] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]
    New calls:

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]

    Iteration #3:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥]
    New calls:
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [2,2]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]

    Iteration #4:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [2,2]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]

    Iteration #5:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]
    New calls:
    x := x - 1: [x !-> [2,2]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]

    Iteration #6:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]
    New calls:

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]

    Iteration #7:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]
    New calls:

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]

    Iteration #8:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥]
    New calls:
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [1,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥]

    Iteration #9:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]

    Iteration #10:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> ⊥];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]
    New calls:
    skip: [x !-> [-∞,1]]
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]

    Iteration #11:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> ⊥];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥]

    Iteration #12:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]

    Iteration #13:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [-∞,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥;
       [x !-> [-∞,2]] ====> ⊥]

    Iteration #14:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> ⊥;
       [x !-> [-∞,2]] ====> ⊥]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]
    if x <= 1 then skip else x := x - 1: [x !-> [1,2]]
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [-∞,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥;
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]

    Iteration #15:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> ⊥;
       [x !-> [2,2]] ====> ⊥;
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]
    if x <= 1 then skip else x := x - 1: [x !-> [1,2]]
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [-∞,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]

    Iteration #16:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> ⊥;
       [x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]
    if x <= 1 then skip else x := x - 1: [x !-> [1,2]]
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [-∞,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [2,2]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]

    Iteration #17:
    Initial graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [2,2]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]
    New calls:
    if x <= 1 then skip else x := x - 1: [x !-> [1,1]]
    if x <= 1 then skip else x := x - 1: [x !-> [1,2]]
    while 0 < x do if x <= 1 then skip else x := x - 1 done: [x !-> [-∞,1]]

    New graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> [x !-> [-∞,0]]];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> [x !-> [-∞,0]];
       [x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]

    Number of iterations: 18
    Number of calls: 109
    Number of iterations per point:
    x := 2: 2
    x := x - 1: 2
    skip: 2
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done: 2
    if x <= 1 then skip else x := x - 1: 4
    while 0 < x do if x <= 1 then skip else x := x - 1 done: 6
    Node numbering:
    x := 2: 1
    x := x - 1: 4
    skip: 5
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done: 0
    if x <= 1 then skip else x := x - 1: 3
    while 0 < x do if x <= 1 then skip else x := x - 1 done: 2
    Widening points:
    while 0 < x do if x <= 1 then skip else x := x - 1 done

    Full graph:
    x := 2 ->
      [[] ====> [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]]];
    skip ->
      [[x !-> [-∞,1]] ====> [x !-> [-∞,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[] ====> [x !-> [-∞,0]]];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [2,2]] ====> [x !-> [1,1]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [2,2]] ====> [x !-> [-∞,0]];
       [x !-> [1,1]] ====> [x !-> [-∞,0]];
       [x !-> [-∞,2]] ====> [x !-> [-∞,0]]]
    Final MFG:
    [[] ====> [x !-> [-∞,0]]]

    Final result:
    [x !-> [-∞,0]] |}]

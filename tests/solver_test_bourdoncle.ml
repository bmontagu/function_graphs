(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Test module for examples found in F. Bourdoncle's Ph.D. thesis on functional
    partitioning and minimal function graphs. *)

open Function_graphs
open Abstract_domains
module MFG = Functional.Make (Interval) (Interval)
module S = Solver.Simple (MFG)

let run strategy f x =
  let res, _ = S.fix_gen ~debug:true strategy f x in
  Format.printf "@.Final result:@.@[%a@]@." Interval.pp res

let interval i j = Interval.make (IntBar.Int i) (IntBar.Int j)

(* let rec mc91 n =
     if n > 100
     then n - 10
     else mc91 (mc91 (n + 11))
*)
let f_mc91 mc91 n =
  let above_101 = Interval.(meet n (make (IntBar.Int 101) IntBar.PInf))
  and below_100 = Interval.(meet n (make IntBar.MInf (IntBar.Int 100))) in
  let result_above_101 =
    if Interval.is_bot above_101 then Interval.bot
    else Interval.minus above_101 (interval 10 10)
  and result_below_100 =
    if Interval.is_bot below_100 then Interval.bot
    else mc91 (mc91 @@ Interval.plus below_100 (interval 11 11))
  in
  Interval.join result_above_101 result_below_100

let%expect_test _ =
  run [| C; A; A; A; C |] f_mc91 (interval 0 50);
  [%expect
    {|
      Iteration #0:
      Initial graph:
      ⊥
      New graph:
      [[0,50] ====> ⊥]

      Iteration #1:
      Initial graph:
      [[0,50] ====> ⊥]
      New graph:
      [[0,50] ====> ⊥;
       [0,+∞] ====> ⊥]

      Iteration #2:
      Initial graph:
      [[0,50] ====> ⊥;
       [0,+∞] ====> ⊥]
      New graph:
      [[0,50] ====> ⊥;
       [11,61] ====> ⊥;
       [11,111] ====> ⊥;
       [0,+∞] ====> [91,+∞]]

      Iteration #3:
      Initial graph:
      [[0,50] ====> ⊥;
       [11,61] ====> ⊥;
       [11,111] ====> ⊥;
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,50] ====> ⊥;
       [22,72] ====> ⊥;
       [22,111] ====> ⊥;
       [11,61] ====> ⊥;
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Iteration #4:
      Initial graph:
      [[11,61] ====> ⊥;
       [22,72] ====> ⊥;
       [0,50] ====> ⊥;
       [22,111] ====> ⊥;
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,50] ====> ⊥;
       [33,83] ====> ⊥;
       [91,101] ====> ⊥;
       [33,111] ====> ⊥;
       [11,61] ====> ⊥;
       [22,72] ====> ⊥;
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Iteration #5:
      Initial graph:
      [[22,72] ====> ⊥;
       [91,101] ====> ⊥;
       [0,50] ====> ⊥;
       [11,61] ====> ⊥;
       [33,83] ====> ⊥;
       [33,111] ====> ⊥;
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,50] ====> ⊥;
       [22,72] ====> ⊥;
       [91,101] ====> [91,91];
       [11,61] ====> ⊥;
       [33,83] ====> ⊥;
       [33,111] ====> [91,101];
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Iteration #6:
      Initial graph:
      [[33,83] ====> ⊥;
       [91,101] ====> [91,91];
       [33,111] ====> [91,101];
       [11,61] ====> ⊥;
       [22,72] ====> ⊥;
       [0,50] ====> ⊥;
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,50] ====> ⊥;
       [33,83] ====> [91,91];
       [91,101] ====> [91,91];
       [33,111] ====> [91,101];
       [11,61] ====> ⊥;
       [22,72] ====> ⊥;
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Iteration #7:
      Initial graph:
      [[22,72] ====> ⊥;
       [91,101] ====> [91,91];
       [0,50] ====> ⊥;
       [11,61] ====> ⊥;
       [33,83] ====> [91,91];
       [33,111] ====> [91,101];
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,50] ====> [91,91];
       [22,72] ====> [91,91];
       [91,101] ====> [91,91];
       [11,61] ====> [91,91];
       [33,83] ====> [91,91];
       [33,111] ====> [91,101];
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Number of iterations: 8

      Final MFG:
      [[33,83] ====> [91,91];
       [91,101] ====> [91,91];
       [33,111] ====> [91,101];
       [11,61] ====> [91,91];
       [22,72] ====> [91,91];
       [0,50] ====> [91,91];
       [22,111] ====> [91,101];
       [11,111] ====> [91,101];
       [0,+∞] ====> [91,+∞]]

      Final result:
      [91,91] |}]

(* let rec mc91 n =
     if n > 100
     then n - 10
     else mc91 (mc91 (mc91 (mc91 (mc91 (mc91 (mc91 (mc91 (mc91 (mc91 (n + 91))))))))))
*)
let f_mc10 mc n =
  let above_101 = Interval.(meet n (make (IntBar.Int 101) IntBar.PInf))
  and below_100 = Interval.(meet n (make IntBar.MInf (IntBar.Int 100))) in
  let result_above_101 =
    if Interval.is_bot above_101 then Interval.bot
    else Interval.minus above_101 (interval 10 10)
  and result_below_100 =
    if Interval.is_bot below_100 then Interval.bot
    else
      mc
        (mc
           (mc
              (mc
                 (mc
                    (mc
                       (mc
                          (mc
                             (mc
                                (mc @@ Interval.plus below_100 (interval 91 91))))))))))
  in
  Interval.join result_above_101 result_below_100

let%expect_test _ =
  run [| C; A; A; A; C |] f_mc10 (interval 0 0);
  [%expect
    {|
      Iteration #0:
      Initial graph:
      ⊥
      New graph:
      [[0,0] ====> ⊥]

      Iteration #1:
      Initial graph:
      [[0,0] ====> ⊥]
      New graph:
      [[0,0] ====> ⊥;
       [0,+∞] ====> ⊥]

      Iteration #2:
      Initial graph:
      [[0,0] ====> ⊥;
       [0,+∞] ====> ⊥]
      New graph:
      [[0,0] ====> ⊥;
       [91,91] ====> ⊥;
       [91,191] ====> ⊥;
       [0,+∞] ====> [91,+∞]]

      Iteration #3:
      Initial graph:
      [[0,0] ====> ⊥;
       [91,91] ====> ⊥;
       [91,191] ====> ⊥;
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [182,182] ====> ⊥;
       [182,191] ====> ⊥;
       [91,91] ====> ⊥;
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Iteration #4:
      Initial graph:
      [[91,91] ====> ⊥;
       [182,182] ====> ⊥;
       [0,0] ====> ⊥;
       [182,191] ====> ⊥;
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [91,91] ====> ⊥;
       [91,181] ====> ⊥;
       [182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Iteration #5:
      Initial graph:
      [[182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [0,0] ====> ⊥;
       [91,91] ====> ⊥;
       [91,181] ====> ⊥;
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [91,91] ====> ⊥;
       [91,181] ====> [91,171];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Iteration #6:
      Initial graph:
      [[91,91] ====> ⊥;
       [91,181] ====> [91,171];
       [182,182] ====> [172,172];
       [0,0] ====> ⊥;
       [182,191] ====> [172,181];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [91,91] ====> [91,171];
       [91,181] ====> [91,171];
       [182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Iteration #7:
      Initial graph:
      [[182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [0,0] ====> ⊥;
       [91,91] ====> [91,171];
       [91,181] ====> [91,171];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]
      New graph:
      [[0,0] ====> [91,171];
       [182,182] ====> [172,172];
       [182,191] ====> [172,181];
       [91,91] ====> [91,171];
       [91,181] ====> [91,171];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Number of iterations: 8

      Final MFG:
      [[91,91] ====> [91,171];
       [91,181] ====> [91,171];
       [182,182] ====> [172,172];
       [0,0] ====> [91,171];
       [182,191] ====> [172,181];
       [91,191] ====> [91,181];
       [0,+∞] ====> [91,+∞]]

      Final result:
      [91,171] |}]

(* let rec loop n =
     if n < 100
     then loop (n-1)
     else n
*)
let f_loop_bourdoncle loop n =
  let below_99 = Interval.(meet n (make IntBar.MInf (IntBar.Int 99)))
  and above_100 = Interval.(meet n (make (IntBar.Int 100) IntBar.PInf)) in
  let result_below_99 =
    if Interval.is_bot below_99 then Interval.bot
    else loop (Interval.plus below_99 (interval 1 1))
  and result_above_100 =
    if Interval.is_bot above_100 then Interval.bot else above_100
  in
  Interval.join result_below_99 result_above_100

let%expect_test _ =
  run [| C; A; C |] f_loop_bourdoncle (interval 0 0);
  [%expect
    {|
      Iteration #0:
      Initial graph:
      ⊥
      New graph:
      [[0,0] ====> ⊥]

      Iteration #1:
      Initial graph:
      [[0,0] ====> ⊥]
      New graph:
      [[0,0] ====> ⊥;
       [0,+∞] ====> ⊥]

      Iteration #2:
      Initial graph:
      [[0,0] ====> ⊥;
       [0,+∞] ====> ⊥]
      New graph:
      [[0,0] ====> ⊥;
       [1,1] ====> ⊥;
       [1,100] ====> ⊥;
       [0,+∞] ====> [100,+∞]]

      Iteration #3:
      Initial graph:
      [[0,0] ====> ⊥;
       [1,1] ====> ⊥;
       [1,100] ====> ⊥;
       [0,+∞] ====> [100,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [1,1] ====> ⊥;
       [1,100] ====> [100,100];
       [0,+∞] ====> [100,+∞]]

      Iteration #4:
      Initial graph:
      [[1,1] ====> ⊥;
       [1,100] ====> [100,100];
       [0,0] ====> ⊥;
       [0,+∞] ====> [100,+∞]]
      New graph:
      [[0,0] ====> ⊥;
       [1,1] ====> [100,100];
       [1,100] ====> [100,100];
       [0,+∞] ====> [100,+∞]]

      Iteration #5:
      Initial graph:
      [[0,0] ====> ⊥;
       [1,1] ====> [100,100];
       [1,100] ====> [100,100];
       [0,+∞] ====> [100,+∞]]
      New graph:
      [[0,0] ====> [100,100];
       [1,1] ====> [100,100];
       [1,100] ====> [100,100];
       [0,+∞] ====> [100,+∞]]

      Number of iterations: 6

      Final MFG:
      [[1,1] ====> [100,100];
       [1,100] ====> [100,100];
       [0,0] ====> [100,100];
       [0,+∞] ====> [100,+∞]]

      Final result:
      [100,100] |}]

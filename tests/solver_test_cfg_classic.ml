(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for control-flow graphs using a standard
    approach, that performs a non-relational analysis. It is implemented using a
    fixpoint solver. *)

module type CFG = sig
  type t
  type node
  type label
  type instr
  type reg

  val entries : t -> (node * reg list) list
  val exits : t -> (node * reg list) list
  val outgoing_labels : t -> node -> label list
  val backstep : t -> node -> (node * label) list * instr
end

open Function_graphs

module Make
    (Node : sig
      type t

      val equal : t -> t -> bool
    end)
    (Label : sig
      type t
    end)
    (NodeMap : sig
      type key = Node.t
      type 'a t

      val empty : 'a t
      val find_opt : key -> 'a t -> 'a option
      val update : key -> ('a option -> 'a option) -> 'a t -> 'a t

      val pp :
        (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
    end)
    (NodeLabel : sig
      type t = Node.t * Label.t

      val pp : Format.formatter -> t -> unit
    end)
    (NodeLabelMap : sig
      type key = Node.t * Label.t
      type 'a t

      val empty : 'a t
      val singleton : key -> 'a -> 'a t
      val mem : key -> 'a t -> bool
      val find_opt : key -> 'a t -> 'a option
      val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
      val for_all : (key -> 'a -> bool) -> 'a t -> bool
      val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
      val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
      val iter : (key -> 'a -> unit) -> 'a t -> unit

      val pp :
        (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

      val to_seq : 'a t -> (key * 'a) Seq.t
    end)
    (Reg : sig
      type t

      val equal : t -> t -> bool
    end)
    (CFG :
      CFG with type node = Node.t and type reg = Reg.t and type label = Label.t)
    (D : sig
      type t
      type key = Reg.t
      type value

      val empty : t
      val get : t -> key -> value
      val set : t -> key -> value -> t
      val bot : t
      val join : t -> t -> t
      val pp : Format.formatter -> t -> unit
    end)
    (A : sig
      type instr = CFG.instr
      type label = CFG.label
      type value = D.t

      val eval : instr -> label -> value -> value
    end)
    (MFG : sig
      include Function_graphs.Sigs.MFG with type dom = unit and type codom = D.t

      val iter : (dom -> codom -> unit) -> t -> unit
    end) : sig
  val eval :
    MFG.strategy array ->
    CFG.t ->
    (Node.t -> D.t) ->
    Node.t * Label.t ->
    unit ->
    D.t

  val run :
    MFG.strategy array ->
    CFG.t ->
    (Node.t * (Reg.t * D.value) list) list ->
    unit
end = struct
  let joins l = List.fold_left D.join D.bot l

  let f_eval cfg inputs =
    let backstep = CFG.backstep cfg in
    fun eval (node, lab) () ->
      let preds, instr = backstep node in
      D.join
        (A.eval instr lab (inputs node))
        (joins
           (List.map (fun parent -> A.eval instr lab (eval parent ())) preds))

  module S =
    Solver.Naive (NodeLabel) (NodeLabelMap) (Test_common.Unit) (D) (MFG)

  let eval strategy cfg inputs node () =
    fst @@ S.fix_gen ~debug:true strategy (f_eval cfg inputs) node ()

  let make_inputs entries start =
    let l =
      List.map
        (fun (node, regs) ->
          let d =
            List.fold_left
              (fun s (node', args) ->
                if Node.equal node node' then
                  List.fold_left
                    (fun s r ->
                      List.fold_left
                        (fun s (r', v) ->
                          if Reg.equal r r' then D.join s (D.set s r v) else s)
                        s args)
                    s regs
                else s)
              D.empty start
          in
          (node, d))
        entries
    in
    let m =
      List.fold_left
        (fun m (n, d) ->
          NodeMap.update n
            (function None -> Some d | Some d' -> Some (D.join d' d))
            m)
        NodeMap.empty l
    in
    fun node -> match NodeMap.find_opt node m with Some d -> d | None -> D.bot

  let outputs_at_exits exits =
    List.fold_left
      (fun m (node, regs, s) ->
        let s' =
          List.fold_left (fun s' r -> D.set s' r (D.get s r)) D.empty regs
        in
        NodeMap.update node
          (function None -> Some s' | Some s'' -> Some (D.join s'' s'))
          m)
      NodeMap.empty exits

  let run strategy cfg start =
    let inputs = make_inputs (CFG.entries cfg) start in
    let outputs =
      List.map
        (fun (node, regs) ->
          ( node,
            regs,
            joins
            @@ List.map
                 (fun lab -> eval strategy cfg inputs (node, lab) ())
                 (CFG.outgoing_labels cfg node) ))
        (CFG.exits cfg)
    in
    let res = outputs_at_exits outputs in
    let open Format in
    printf "@.Outputs:@.@[<v>%a@]" (NodeMap.pp D.pp) res
end

module CFG = struct
  open Test_common.CFG

  type node = Node.t
  type instr = Instr.t
  type label = bool option
  type reg = Reg.t

  type t = {
    entries : (node * reg list) list;
    exits : (node * reg list) list;
    code : (instr * (label * Node.t) list option) array;
  }

  let preds code =
    let len = Array.length code in
    let preds = Array.make len [] in
    for i = 0 to len - 1 do
      match code.(i) with
      | _instr, None ->
          if i <> len - 1 then preds.(i + 1) <- (None, i) :: preds.(i + 1)
      | _instr, Some l ->
          List.iter (fun (lab, j) -> preds.(j) <- (lab, i) :: preds.(j)) l
    done;
    fun n -> preds.(n)

  let reverse t =
    let preds = preds t.code in
    {
      entries = t.exits;
      exits = t.entries;
      code = Array.mapi (fun n (instr, _next) -> (instr, Some (preds n))) t.code;
    }

  let entries t = t.entries
  let exits t = t.exits

  let outgoing_labels t node =
    let code = t.code in
    assert (0 <= node && node < Array.length code);
    let _instr, successors = code.(node) in
    match successors with None -> [ None ] | Some l -> List.map fst l

  let step t node =
    let code = t.code in
    assert (0 <= node && node < Array.length code);
    let instr, successors = code.(node) in
    let successors =
      match successors with
      | None ->
          if node = Array.length code - 1 then [] else [ (None, node + 1) ]
      | Some l -> l
    in
    (instr, successors)

  let backstep t =
    let t = reverse t in
    fun node ->
      let instr, preds = step t node in
      let preds = List.map (fun (label, node) -> (node, label)) preds in
      (preds, instr)
end

module MFG = Simple.Make (Test_common.Unit) (Test_common.IntervalStore)

module Analyzer =
  Make (Test_common.CFG.Node) (Test_common.CFG.Label) (Test_common.CFG.NodeMap)
    (Test_common.CFG.NodeLabel)
    (Test_common.CFG.NodeLabelMap)
    (Test_common.CFG.Reg)
    (CFG)
    (Test_common.IntervalStore)
    (Test_common.CFG.ValueAnalysis)
    (MFG)

open Abstract_domains

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (1, [ "x" ]) ];
        code = [| (Op (Read, "x", [ V 0 ]), None); (Nop, None) |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Number of iterations: 4
    Number of calls: 7
    Number of iterations per point:
    0 -[any]->: 2
    1 -[any]->: 2
    Node numbering:
    0 -[any]->: 1
    1 -[any]->: 0
    Widening points:


    Full graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    Final MFG:
    [()
     ====>
     [x !-> [0,0]]]

    Final result:
    [x !-> [0,0]]

    Outputs:
    1 ->
      [x !-> [0,0]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (1, [ "y" ]) ];
        code = [| (Op (Add, "y", [ R "x"; V 1 ]), None); (Nop, None) |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("x", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]]

    Number of iterations: 4
    Number of calls: 7
    Number of iterations per point:
    0 -[any]->: 2
    1 -[any]->: 2
    Node numbering:
    0 -[any]->: 1
    1 -[any]->: 0
    Widening points:


    Full graph:
    0 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]];
    1 -[any]->
      [()
       ====>
       [x -> [0,2];
        y !-> [1,3]]]
    Final MFG:
    [()
     ====>
     [x -> [0,2];
      y !-> [1,3]]]

    Final result:
    [x -> [0,2];
     y !-> [1,3]]

    Outputs:
    1 ->
      [y !-> [1,3]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (4, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Cond (IsZero "b"), Some [ (Some true, 1); (Some false, 2) ]);
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, 3) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, 3) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    3 -[any]->: ()

    New graph:
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    2 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()
    0 -[true]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 8
    Number of calls: 37
    Number of iterations per point:
    0 -[false]->: 2
    0 -[true]->: 2
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[any]->: 2
    4 -[any]->: 2
    Node numbering:
    0 -[false]->: 4
    0 -[true]->: 5
    1 -[any]->: 2
    2 -[any]->: 3
    3 -[any]->: 1
    4 -[any]->: 0
    Widening points:


    Full graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [()
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    4 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Cond (IsZero "b"), Some [ (Some true, 1); (Some false, 2) ]);
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, 3) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, 4) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    4 -[any]->: ()

    New graph:
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[any]->: ()
    3 -[any]->: ()

    New graph:
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()
    1 -[any]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[true]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]

    Iteration #8:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]

    Iteration #9:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 10
    Number of calls: 55
    Number of iterations per point:
    0 -[false]->: 1
    0 -[true]->: 2
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[any]->: 2
    4 -[any]->: 3
    5 -[any]->: 3
    Node numbering:
    0 -[false]->: 4
    0 -[true]->: 6
    1 -[any]->: 5
    2 -[any]->: 2
    3 -[any]->: 3
    4 -[any]->: 1
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [()
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    5 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "b" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Cond (IsZero "b"), Some [ (Some true, 1); (Some false, 2) ]);
            (* 1 *) (Op (Read, "x", [ V 0 ]), Some [ (None, 4) ]);
            (* 2 *) (Op (Read, "x", [ V 1 ]), Some [ (None, 3) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; C |] cfg
    [ (0, [ ("b", Interval.make (IntBar.Int 0) (IntBar.Int 2)) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    4 -[any]->: ()

    New graph:
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    3 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[true]->: ()
    2 -[any]->: ()

    New graph:
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]

    Iteration #8:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]

    Iteration #9:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]

    Number of iterations: 10
    Number of calls: 55
    Number of iterations per point:
    0 -[false]->: 2
    0 -[true]->: 1
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[any]->: 2
    4 -[any]->: 3
    5 -[any]->: 3
    Node numbering:
    0 -[false]->: 6
    0 -[true]->: 4
    1 -[any]->: 2
    2 -[any]->: 5
    3 -[any]->: 3
    4 -[any]->: 1
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[false]->
      [()
       ====>
       [b !-> [1,2]]];
    0 -[true]->
      [()
       ====>
       [b !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [b !-> [0,0];
        x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       [b !-> [1,2];
        x !-> [1,1]]];
    4 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [b !-> [0,2];
        x !-> [0,1]]]
    Final MFG:
    [()
     ====>
     [b !-> [0,2];
      x !-> [0,1]]]

    Final result:
    [b !-> [0,2];
     x !-> [0,1]]

    Outputs:
    5 ->
      [x !-> [0,1]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (3, [ "i" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, 2); (Some false, 3) ] );
            (* 2 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, 1) ]);
            (* 3 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[false]->: ()

    New graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    2 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #10:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #11:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #12:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥]

    Iteration #13:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       [i !-> [100,+∞]]]

    Number of iterations: 14
    Number of calls: 62
    Number of iterations per point:
    0 -[any]->: 1
    1 -[false]->: 2
    1 -[true]->: 6
    2 -[any]->: 5
    3 -[any]->: 2
    Node numbering:
    0 -[any]->: 2
    1 -[false]->: 1
    1 -[true]->: 4
    2 -[any]->: 3
    3 -[any]->: 0
    Widening points:
    0 -[any]->,
    2 -[any]->

    Full graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[any]->
      [()
       ====>
       [i !-> [100,+∞]]]
    Final MFG:
    [()
     ====>
     [i !-> [100,+∞]]]

    Final result:
    [i !-> [100,+∞]]

    Outputs:
    3 ->
      [i !-> [100,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (4, [ "i" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, 2); (Some false, 4) ] );
            (* 2 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, 1) ]);
            (* 3 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, 2); (Some false, 4) ] );
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[false]->: ()
    3 -[false]->: ()

    New graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    2 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[true]->: ()
    3 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,1]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,2]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #10:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #11:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,3]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #12:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #13:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [i !-> [100,+∞]]]

    Number of iterations: 14
    Number of calls: 86
    Number of iterations per point:
    0 -[any]->: 1
    1 -[false]->: 2
    1 -[true]->: 6
    2 -[any]->: 5
    3 -[false]->: 1
    3 -[true]->: 1
    4 -[any]->: 2
    Node numbering:
    0 -[any]->: 3
    1 -[false]->: 1
    1 -[true]->: 5
    2 -[any]->: 4
    3 -[false]->: 2
    3 -[true]->: 6
    4 -[any]->: 0
    Widening points:
    0 -[any]->,
    2 -[any]->

    Full graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99]]];
    2 -[any]->
      [()
       ====>
       [i !-> [1,+∞]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       [i !-> [100,+∞]]]
    Final MFG:
    [()
     ====>
     [i !-> [100,+∞]]]

    Final result:
    [i !-> [100,+∞]]

    Outputs:
    4 ->
      [i !-> [100,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (6, [ "i"; "j" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "i", [ V 0 ]), None);
            (* 1 *)
            ( Cond (IsLt (R "i", V 100)),
              Some [ (Some true, 2); (Some false, 6) ] );
            (* 2 *) (Op (Read, "j", [ V 0 ]), None);
            (* 3 *)
            ( Cond (IsLt (R "j", R "i")),
              Some [ (Some true, 4); (Some false, 5) ] );
            (* 4 *) (Op (Add, "j", [ R "j"; V 1 ]), Some [ (None, 3) ]);
            (* 5 *) (Op (Add, "i", [ R "i"; V 1 ]), Some [ (None, 1) ]);
            (* 6 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| C; A; A; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[false]->: ()

    New graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    1 -[false]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    5 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    3 -[false]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[any]->: ()
    4 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[true]->: ()
    3 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #10:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #11:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #12:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #13:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #14:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #15:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #16:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,1];
        j -> [0,0]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #17:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #18:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #19:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,1];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #20:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #21:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,1]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #22:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,1];
        j !-> [0,1]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #23:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #24:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,2];
        j -> [0,1]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #25:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #26:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #27:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,2];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #28:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,1]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #29:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,2]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #30:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,2];
        j !-> [0,2]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #31:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #32:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,3];
        j -> [0,2]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #33:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #34:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #35:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,3];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #36:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,2]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,3]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #37:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,3]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,3]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,3]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #38:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,3];
        j !-> [0,3]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,3]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #39:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,4];
        j !-> [0,3]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #40:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       ⊥];
    1 -[true]->
      [()
       ====>
       [i !-> [0,4];
        j -> [0,3]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99];
        j -> [0,+∞]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥]

    Iteration #41:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99];
        j -> [0,+∞]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,4];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99];
        j -> [0,+∞]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]]

    Number of iterations: 42
    Number of calls: 352
    Number of iterations per point:
    0 -[any]->: 1
    1 -[false]->: 2
    1 -[true]->: 7
    2 -[any]->: 7
    3 -[false]->: 6
    3 -[true]->: 9
    4 -[any]->: 8
    5 -[any]->: 6
    6 -[any]->: 2
    Node numbering:
    0 -[any]->: 2
    1 -[false]->: 1
    1 -[true]->: 7
    2 -[any]->: 5
    3 -[false]->: 4
    3 -[true]->: 8
    4 -[any]->: 6
    5 -[any]->: 3
    6 -[any]->: 0
    Widening points:
    0 -[any]->,
    2 -[any]->,
    4 -[any]->,
    5 -[any]->

    Full graph:
    0 -[any]->
      [()
       ====>
       [i !-> [0,0]]];
    1 -[false]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]];
    1 -[true]->
      [()
       ====>
       [i !-> [0,99];
        j -> [0,+∞]]];
    2 -[any]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,0]]];
    3 -[false]->
      [()
       ====>
       [i !-> [0,+∞];
        j !-> [0,+∞]]];
    3 -[true]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [1,+∞]]];
    5 -[any]->
      [()
       ====>
       [i !-> [1,+∞];
        j !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [i !-> [100,+∞];
        j !-> [0,+∞]]]
    Final MFG:
    [()
     ====>
     [i !-> [100,+∞];
      j !-> [0,+∞]]]

    Final result:
    [i !-> [100,+∞];
     j !-> [0,+∞]]

    Outputs:
    6 ->
      [i !-> [100,+∞];
       j !-> [0,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "i"; "j" ]) ];
        exits = [ (4, [ "i"; "j" ]) ];
        code =
          [|
            (* 0 *)
            (Cond (IsLt (R "i", V 10)), Some [ (Some true, 3); (Some false, 1) ]);
            (* 1 *) (Op (Add, "i", [ R "i"; V 1 ]), None);
            (* 2 *)
            (Cond (IsLt (R "j", V 10)), Some [ (Some false, 4); (Some true, 3) ]);
            (* 3 *) (Op (Add, "j", [ R "j"; V 1 ]), Some [ (None, 2) ]);
            (* 4 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg
    [
      ( 0,
        [
          ("i", Interval.make (IntBar.Int 0) (IntBar.Int 5));
          ("j", Interval.make (IntBar.Int 0) (IntBar.Int 5));
        ] );
    ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[false]->: ()

    New graph:
    2 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    2 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    3 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()
    0 -[true]->: ()
    2 -[true]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,6]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,9]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,9]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,9]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]

    Number of iterations: 10
    Number of calls: 56
    Number of iterations per point:
    0 -[false]->: 1
    0 -[true]->: 2
    1 -[any]->: 1
    2 -[false]->: 2
    2 -[true]->: 3
    3 -[any]->: 3
    4 -[any]->: 2
    Node numbering:
    0 -[false]->: 4
    0 -[true]->: 5
    1 -[any]->: 2
    2 -[false]->: 1
    2 -[true]->: 6
    3 -[any]->: 3
    4 -[any]->: 0
    Widening points:
    1 -[any]->,
    3 -[any]->

    Full graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j -> [0,5]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]];
    2 -[true]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,9]]];
    3 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [1,+∞]]];
    4 -[any]->
      [()
       ====>
       [i !-> [0,5];
        j !-> [10,+∞]]]
    Final MFG:
    [()
     ====>
     [i !-> [0,5];
      j !-> [10,+∞]]]

    Final result:
    [i !-> [0,5];
     j !-> [10,+∞]]

    Outputs:
    4 ->
      [i !-> [0,5];
       j !-> [10,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, []) ];
        exits = [ (7, [ "x"; "y" ]) ];
        code =
          [|
            (* 0 *) (Op (Read, "x", [ V 0 ]), None);
            (* 1 *) (Op (Read, "y", [ V 0 ]), None);
            (* 2 *)
            (Cond (IsLe (R "x", V 99)), Some [ (Some true, 3); (Some false, 7) ]);
            (* 3 *) (Op (Add, "x", [ R "x"; V 1 ]), None);
            (* 4 *)
            (Cond (IsLe (R "x", V 49)), Some [ (Some true, 5); (Some false, 6) ]);
            (* 5 *) (Op (Add, "y", [ R "y"; V 1 ]), Some [ (None, 2) ]);
            (* 6 *) (Op (Sub, "y", [ R "y"; V 1 ]), Some [ (None, 2) ]);
            (* 7 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, []) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[false]->: ()

    New graph:
    2 -[false]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    2 -[false]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    5 -[any]->: ()
    6 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    4 -[false]->: ()
    4 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    3 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[false]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #8:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #9:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #10:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #11:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #12:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #13:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,1];
        y !-> [1,1]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #14:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       ⊥];
    2 -[true]->
      [()
       ====>
       [x !-> [0,1];
        y !-> [0,1]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]

    Iteration #15:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]

    Iteration #16:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       ⊥];
    4 -[true]->
      [()
       ====>
       [x !-> [1,2];
        y !-> [0,1]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]

    Iteration #17:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       ⊥];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]

    Iteration #18:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [0,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]

    Iteration #19:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [0,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]

    Iteration #20:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [0,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [0,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]

    Iteration #21:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]

    Iteration #22:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-1,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]

    Iteration #23:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-1,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-1,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #24:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-1,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-1,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Iteration #25:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [0,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Number of iterations: 26
    Number of calls: 235
    Number of iterations per point:
    0 -[any]->: 1
    1 -[any]->: 1
    2 -[false]->: 4
    2 -[true]->: 6
    3 -[any]->: 6
    4 -[false]->: 4
    4 -[true]->: 6
    5 -[any]->: 5
    6 -[any]->: 3
    7 -[any]->: 4
    Node numbering:
    0 -[any]->: 5
    1 -[any]->: 2
    2 -[false]->: 1
    2 -[true]->: 9
    3 -[any]->: 8
    4 -[false]->: 6
    4 -[true]->: 7
    5 -[any]->: 3
    6 -[any]->: 4
    7 -[any]->: 0
    Widening points:
    1 -[any]->,
    5 -[any]->,
    6 -[any]->

    Full graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    2 -[false]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    2 -[true]->
      [()
       ====>
       [x !-> [0,99];
        y !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    4 -[false]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    4 -[true]->
      [()
       ====>
       [x !-> [1,49];
        y !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [1,+∞];
        y !-> [-∞,+∞]]];
    6 -[any]->
      [()
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    7 -[any]->
      [()
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]
    Final MFG:
    [()
     ====>
     [x !-> [100,+∞];
      y !-> [-∞,+∞]]]

    Final result:
    [x !-> [100,+∞];
     y !-> [-∞,+∞]]

    Outputs:
    7 ->
      [x !-> [100,+∞];
       y !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Cond (IsZero "x"), Some [ (Some true, 1); (Some false, 2) ]);
            (* 1 *) (Nop, Some [ (None, 5) ]);
            (* 2 *) (Nop, None);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
            (* XXX 5 is incorrectly marked as a widening point *)
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    4 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[true]->: ()
    3 -[any]->: ()

    New graph:
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[any]->: ()

    New graph:
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #8:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #9:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 10
    Number of calls: 57
    Number of iterations per point:
    0 -[false]->: 2
    0 -[true]->: 1
    1 -[any]->: 1
    2 -[any]->: 2
    3 -[any]->: 2
    4 -[any]->: 2
    5 -[any]->: 3
    Node numbering:
    0 -[false]->: 6
    0 -[true]->: 3
    1 -[any]->: 1
    2 -[any]->: 5
    3 -[any]->: 4
    4 -[any]->: 2
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    4 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [()
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (0, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Cond (IsZero "x"), Some [ (Some true, 3); (Some false, 1) ]);
            (* 1 *) (Nop, None);
            (* 2 *) (Nop, Some [ (None, 5) ]);
            (* 3 *) (Nop, None);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (0, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    2 -[any]->: ()
    4 -[any]->: ()

    New graph:
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    3 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[false]->: ()
    0 -[true]->: ()

    New graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[false]->
      [()
       ====>
       ⊥];
    0 -[true]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #6:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #7:
    Initial graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 8
    Number of calls: 44
    Number of iterations per point:
    0 -[false]->: 2
    0 -[true]->: 2
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[any]->: 2
    4 -[any]->: 2
    5 -[any]->: 2
    Node numbering:
    0 -[false]->: 5
    0 -[true]->: 6
    1 -[any]->: 3
    2 -[any]->: 1
    3 -[any]->: 4
    4 -[any]->: 2
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    0 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [()
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (3, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Nop, Some [ (None, 5) ]);
            (* 1 *) (Nop, None);
            (* 2 *) (Nop, Some [ (None, 5) ]);
            (* 3 *) (Cond (IsZero "x"), Some [ (Some true, 0); (Some false, 1) ]);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (3, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    2 -[any]->: ()
    4 -[any]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    3 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    3 -[false]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]

    Iteration #7:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [0,0]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 8
    Number of calls: 46
    Number of iterations per point:
    0 -[any]->: 2
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[false]->: 2
    3 -[true]->: 1
    4 -[any]->: 1
    5 -[any]->: 3
    Node numbering:
    0 -[any]->: 1
    1 -[any]->: 4
    2 -[any]->: 2
    3 -[false]->: 6
    3 -[true]->: 5
    4 -[any]->: 3
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [()
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

let%expect_test _ =
  let cfg =
    CFG.
      {
        entries = [ (3, [ "x" ]) ];
        exits = [ (5, [ "x" ]) ];
        code =
          [|
            (* 0 *) (Nop, None);
            (* 1 *) (Nop, Some [ (None, 5) ]);
            (* 2 *) (Nop, Some [ (None, 5) ]);
            (* 3 *) (Cond (IsZero "x"), Some [ (Some true, 0); (Some false, 2) ]);
            (* 4 *) (Nop, None);
            (* 5 *) (Nop, None);
          |];
      }
  in
  Analyzer.run [| A; C; A; A; C |] cfg [ (3, [ ("x", Interval.top) ]) ];
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    1 -[any]->: ()
    2 -[any]->: ()
    4 -[any]->: ()

    New graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    0 -[any]->: ()
    3 -[false]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:
    3 -[true]->: ()

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       ⊥];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       ⊥];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]

    Iteration #5:
    Initial graph:
    0 -[any]->
      [()
       ====>
       ⊥];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       ⊥]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]

    Iteration #6:
    Initial graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       ⊥];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]
    New calls:

    New graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]

    Number of iterations: 7
    Number of calls: 39
    Number of iterations per point:
    0 -[any]->: 2
    1 -[any]->: 2
    2 -[any]->: 2
    3 -[false]->: 1
    3 -[true]->: 2
    4 -[any]->: 1
    5 -[any]->: 2
    Node numbering:
    0 -[any]->: 4
    1 -[any]->: 1
    2 -[any]->: 2
    3 -[false]->: 5
    3 -[true]->: 6
    4 -[any]->: 3
    5 -[any]->: 0
    Widening points:


    Full graph:
    0 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    1 -[any]->
      [()
       ====>
       [x !-> [0,0]]];
    2 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[false]->
      [()
       ====>
       [x !-> [-∞,+∞]]];
    3 -[true]->
      [()
       ====>
       [x !-> [0,0]]];
    4 -[any]->
      [()
       ====>
       ⊥];
    5 -[any]->
      [()
       ====>
       [x !-> [-∞,+∞]]]
    Final MFG:
    [()
     ====>
     [x !-> [-∞,+∞]]]

    Final result:
    [x !-> [-∞,+∞]]

    Outputs:
    5 ->
      [x !-> [-∞,+∞]] |}]

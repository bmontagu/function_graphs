(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for a first-order purely functional
    language as described in

    Data Flow Analysis of Applicative Programs Using Minimal Function Graphs

    Neil D. Jones and Alan Mycroft

    Proceedings of the 13th ACM SIGACT-SIGPLAN symposium on Principles of
    programming languages - POPL '86

    ACM Press, 1986

    The analyzer is implemented using a fixpoint solver. *)

type var = Test_common.While.var
type fun_id = var

type prim =
  | Bool of bool (* arity 0 *)
  | Int of int (* arity 0 *)
  | Plus (* arity 2 *)
  | Minus (* arity 2 *)
  | Mult (* arity 2 *)
  | Le (* arity 2 *)
  | Lt (* arity 2 *)
  | Eq (* arity 2 *)

type expr =
  | Var of var
  | Prim of prim * expr list
  | Call of fun_id * expr list
  | IfThenElse of expr * expr * expr

type decl = fun_id * var list * expr
type program = decl list

let pp_args pp fmt l =
  let open Format in
  fprintf fmt "@[<v 1>(%a)@]"
    (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp)
    l

let rec pp_expr fmt =
  let open Format in
  function
  | Var x -> pp_print_string fmt x
  | Prim (prim, es) -> pp_prim fmt prim es
  | Call (f, es) -> fprintf fmt "@[<v 2>%s@ %a@]" f (pp_args pp_expr) es
  | IfThenElse (e, e1, e2) ->
      fprintf fmt "@[<v>if @[%a@]@ then @[%a@]@ else @[%a@]@]" pp_expr e pp_expr
        e1 pp_expr e2

and pp_prim fmt prim es =
  let open Format in
  match (prim, es) with
  | Bool b, [] -> pp_print_bool fmt b
  | Bool _, _ -> assert false
  | Int n, [] -> pp_print_int fmt n
  | Int _, _ -> assert false
  | ( Plus,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1);
        (Prim ((Le | Lt | Eq), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ + @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Plus, [ ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1); e2 ] ->
      fprintf fmt "@[@[(%a)@]@ + @[%a@]@]" pp_expr e1 pp_expr e2
  | Plus, [ e1; (Prim ((Le | Lt | Eq), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ + @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Plus, [ e1; e2 ] -> fprintf fmt "@[@[%a@]@ + @[%a@]@]" pp_expr e1 pp_expr e2
  | Plus, _ -> assert false
  | ( Minus,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1);
        (Prim ((Plus | Minus | Le | Lt | Eq), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ - @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Minus, [ ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1); e2 ] ->
      fprintf fmt "@[@[(%a)@]@ - @[%a@]@]" pp_expr e1 pp_expr e2
  | Minus, [ e1; (Prim ((Plus | Minus | Le | Lt | Eq), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ - @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Minus, [ e1; e2 ] ->
      fprintf fmt "@[@[%a@]@ - @[%a@]@]" pp_expr e1 pp_expr e2
  | Minus, _ -> assert false
  | ( Mult,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq | Plus | Minus), _)) as e1);
        (Prim ((Le | Lt | Eq | Plus | Minus), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ * @[(%a)@]@]" pp_expr e1 pp_expr e2
  | ( Mult,
      [ ((IfThenElse _ | Prim ((Le | Lt | Eq | Plus | Minus), _)) as e1); e2 ] )
    ->
      fprintf fmt "@[@[(%a)@]@ * @[%a@]@]" pp_expr e1 pp_expr e2
  | Mult, [ e1; (Prim ((Le | Lt | Eq | Plus | Minus), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ + @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Mult, [ e1; e2 ] -> fprintf fmt "@[@[%a@]@ * @[%a@]@]" pp_expr e1 pp_expr e2
  | Mult, _ -> assert false
  | ( Le,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1);
        (Prim ((Le | Lt | Eq), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ <= @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Le, [ e1; (Prim ((Le | Lt | Eq), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ <= @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Le, [ ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1); e2 ] ->
      fprintf fmt "@[@[(%a)@]@ <= @[%a@]@]" pp_expr e1 pp_expr e2
  | Le, [ e1; e2 ] -> fprintf fmt "@[@[%a@]@ <= @[%a@]@]" pp_expr e1 pp_expr e2
  | Le, _ -> assert false
  | ( Lt,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1);
        (Prim ((Le | Lt | Eq), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ < @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Lt, [ e1; (Prim ((Le | Lt | Eq), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ < @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Lt, [ ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1); e2 ] ->
      fprintf fmt "@[@[(%a)@]@ < @[%a@]@]" pp_expr e1 pp_expr e2
  | Lt, [ e1; e2 ] -> fprintf fmt "@[@[%a@]@ < @[%a@]@]" pp_expr e1 pp_expr e2
  | Lt, _ -> assert false
  | ( Eq,
      [
        ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1);
        (Prim ((Le | Lt | Eq), _) as e2);
      ] ) ->
      fprintf fmt "@[@[(%a)@]@ = @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Eq, [ e1; (Prim ((Le | Lt | Eq), _) as e2) ] ->
      fprintf fmt "@[@[%a@]@ = @[(%a)@]@]" pp_expr e1 pp_expr e2
  | Eq, [ ((IfThenElse _ | Prim ((Le | Lt | Eq), _)) as e1); e2 ] ->
      fprintf fmt "@[@[(%a)@]@ = @[%a@]@]" pp_expr e1 pp_expr e2
  | Eq, [ e1; e2 ] -> fprintf fmt "@[@[%a@]@ = @[%a@]@]" pp_expr e1 pp_expr e2
  | Eq, _ -> assert false

let pp_decl fmt (f, args, e) =
  let open Format in
  fprintf fmt "@[<v 2>fun %s@ %a =@ %a@]" f (pp_args pp_print_string) args
    pp_expr e

let pp_program fmt decls =
  let open Format in
  fprintf fmt "@[<v>%a@]"
    (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@.") pp_decl)
    decls

open Function_graphs

module FO_MFG (Env : sig
  type t
  type value

  val bot : t
  val is_bot : t -> bool
  val pp : Format.formatter -> t -> unit
  val get : t -> var -> value
  val make : (var * value) list -> t
end) (V : sig
  type t = Env.value

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val singleton_bool : bool -> t
  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include Function_graphs.Sigs.MFG with type dom = Env.t and type codom = V.t

  val iter : (dom -> codom -> unit) -> t -> unit
end) (A : sig
  val call_prim : prim -> V.t list -> V.t
  val cond : expr -> bool -> Env.t -> Env.t
end) : sig
  val analyze : MFG.strategy array -> program -> fun_id -> Env.t -> V.t
  val run : MFG.strategy array -> program -> fun_id -> Env.t -> unit
end = struct
  let analyze_args f es =
    Option.map List.rev
    @@ List.fold_left
         (fun ovs e ->
           match ovs with
           | None -> None
           | Some vs ->
               let v = f e in
               if V.is_bot v then None else Some (v :: vs))
         (Some []) es

  let strictly f x oy = match oy with None -> V.bot | Some y -> f x y

  let analyze_expr make_env call env expr =
    let rec analyze_expr env expr =
      match expr with
      | Var x -> Env.get env x
      | Prim (prim, es) ->
          strictly A.call_prim prim @@ analyze_args (analyze_expr env) es
      | Call (f, es) ->
          call f @@ make_env f @@ analyze_args (analyze_expr env) es
      | IfThenElse (e_cond, e_true, e_false) ->
          let v_cond = analyze_expr env e_cond in
          let v_true =
            if V.leq (V.singleton_bool true) v_cond then
              analyze_expr (A.cond e_cond true env) e_true
            else V.bot
          and v_false =
            if V.leq (V.singleton_bool false) v_cond then
              analyze_expr (A.cond e_cond false env) e_false
            else V.bot
          in
          V.join v_true v_false
    in
    analyze_expr env expr

  let call (prog : program) =
    let make_env f ovs =
      match ovs with
      | None -> Env.bot
      | Some vs -> (
          match List.find_opt (fun (g, _, _) -> String.equal f g) prog with
          | None -> Env.bot
          | Some (_f, xs, _e) ->
              if List.length xs = List.length vs then
                Env.make @@ List.map2 (fun x v -> (x, v)) xs vs
              else Env.bot)
    in
    fun call f env ->
      if Env.is_bot env then V.bot
      else
        match List.find_opt (fun (g, _, _) -> String.equal f g) prog with
        | None -> V.bot
        | Some (_f, _xs, e) -> analyze_expr make_env call env e

  module FunId = struct
    type t = fun_id

    let compare = Test_common.While.compare_var
    let pp = Format.pp_print_string
  end

  module FunIdMap = struct
    include Map.Make (FunId)

    let pp pp_v fmt m =
      let open Format in
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             fprintf fmt "@[<v 2>@[%a@] ->@ @[%a@]@]" FunId.pp k pp_v v))
        (to_seq m)
  end

  module M = Solver.Naive (FunId) (FunIdMap) (Env) (V) (MFG)

  let analyze strategy prog fun_id input =
    fst @@ M.fix_gen ~debug:true strategy (call prog) fun_id input

  let run strategy prog f env = ignore @@ analyze strategy prog f env
end

module V : sig
  open Abstract_domains

  type t

  val bot : t
  val is_bot : t -> bool
  val top : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val get_ints : t -> Interval.t
  val singleton_bool : bool -> t
  val make_ints : Interval.t -> t
  val singleton_int : int -> t
  val minus : t -> t -> t
  val plus : t -> t -> t
  val mult : t -> t -> t
  val le : t -> t -> t
  val lt : t -> t -> t
  val eq : t -> t -> t
end = struct
  open Abstract_domains

  type t = { ints : Interval.t; bools : Bools.t }

  let bot = { ints = Interval.bot; bools = Bools.bot }
  let is_bot { ints; bools } = Interval.is_bot ints && Bools.is_bot bools
  let top = { ints = Interval.top; bools = Bools.top }
  let get_ints v = v.ints

  let pp fmt ({ bools; ints } as t) =
    let open Format in
    if is_bot t then pp_print_string fmt "⊥"
    else (
      fprintf fmt "@[<v 2>{";
      if not @@ Bools.is_bot bools then
        fprintf fmt "@[bools = @[%a@]@]" Bools.pp bools;
      if not @@ Interval.is_bot ints then
        fprintf fmt "@[ints = @[%a@]@]" Interval.pp ints;
      fprintf fmt "}@]")

  let leq { ints = ints1; bools = bools1 } { ints = ints2; bools = bools2 } =
    Interval.leq ints1 ints2 && Bools.leq bools1 bools2

  let join { ints = ints1; bools = bools1 } { ints = ints2; bools = bools2 } =
    { ints = Interval.join ints1 ints2; bools = Bools.join bools1 bools2 }

  let widen { ints = ints1; bools = bools1 } { ints = ints2; bools = bools2 } =
    { ints = Interval.widen ints1 ints2; bools = Bools.join bools1 bools2 }

  let meet { ints = ints1; bools = bools1 } { ints = ints2; bools = bools2 } =
    let t =
      { ints = Interval.meet ints1 ints2; bools = Bools.meet bools1 bools2 }
    in
    if is_bot t then bot else t

  let singleton_bool b = { bot with bools = Bools.singleton b }
  let make_ints i = { bot with ints = i }
  let singleton_int n = make_ints (Interval.singleton n)

  let plus { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with ints = Interval.plus ints1 ints2 }

  let minus { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with ints = Interval.minus ints1 ints2 }

  let mult { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with ints = Interval.mult ints1 ints2 }

  let le { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with bools = Interval.le ints1 ints2 }

  let lt { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with bools = Interval.lt ints1 ints2 }

  let eq { ints = ints1; bools = _ } { ints = ints2; bools = _ } =
    { bot with bools = Interval.eq ints1 ints2 }
end

module MakeEnv (V : sig
  type t

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  type value = V.t
  type t

  val make : (string * value) list -> t
  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val get : t -> var -> value
  val mapi : (var -> value -> value) -> t -> t
  val pp : Format.formatter -> t -> unit
end = struct
  module M = Map.Make (String)

  type value = V.t
  type t = value M.t option

  let bot = None
  let is_bot = function None -> true | Some _ -> false

  let leq env1 env2 =
    match (env1, env2) with
    | None, _ -> true
    | _, None -> false
    | Some m1, Some m2 -> M.equal V.leq m1 m2

  let join_gen join env1 env2 =
    match (env1, env2) with
    | None, e | e, None -> e
    | Some m1, Some m2 ->
        Some (M.union (fun _ v1 v2 -> Some (join v1 v2)) m1 m2)

  let join = join_gen V.join
  let widen = join_gen V.widen

  let meet env1 env2 =
    match (env1, env2) with
    | None, _ | _, None -> None
    | Some m1, Some m2 -> (
        let exception FoundBot in
        try
          Some
            (M.merge
               (fun _ ov1 ov2 ->
                 match (ov1, ov2) with
                 | None, _ | _, None -> None
                 | Some v1, Some v2 ->
                     let v12 = V.meet v1 v2 in
                     if V.is_bot v12 then raise FoundBot else Some v12)
               m1 m2)
        with FoundBot -> None)

  let pp fmt =
    let open Format in
    function
    | None -> pp_print_string fmt "⊥"
    | Some m ->
        fprintf fmt "@[<v>{ @[<v>%a@]@ }@]"
          (pp_print_seq
             ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
             (fun fmt (x, v) -> fprintf fmt "@[<v 2>%s -> @[%a@]@]" x V.pp v))
          (M.to_seq m)

  let get env x =
    match env with
    | None -> V.bot
    | Some m -> ( match M.find_opt x m with None -> V.bot | Some v -> v)

  let make l =
    if List.exists (fun (_, v) -> V.is_bot v) l then bot
    else Some (List.fold_left (fun m (x, v) -> M.add x v m) M.empty l)

  let mapi f = function
    | None -> None
    | Some m -> (
        let exception FoundBot in
        try
          Some
            (M.mapi
               (fun x v ->
                 let w = f x v in
                 if V.is_bot w then raise FoundBot else w)
               m)
        with FoundBot -> None)
end

module Env = MakeEnv (V)

module A : sig
  val call_prim : prim -> V.t list -> V.t
  val cond : expr -> bool -> Env.t -> Env.t
end = struct
  open Abstract_domains

  let rec eval_bkw e v env =
    match e with
    | Var x ->
        Env.mapi (fun y vy -> if String.equal x y then V.meet v vy else vy) env
    | Prim (prim, es) -> eval_bkw_prim prim es v env
    | Call (_, _) -> env
    | IfThenElse (e_cond, e_true, e_false) ->
        Env.join
          (eval_bkw e_true v (eval_bkw e_cond (V.singleton_bool true) env))
          (eval_bkw e_false v (eval_bkw e_cond (V.singleton_bool false) env))

  and eval_bkw_prim prim es v env =
    match (prim, es) with
    | Bool b, [] -> if V.leq (V.singleton_bool b) v then env else Env.bot
    | Bool _, _ -> Env.bot
    | Int n, [] -> if V.leq (V.singleton_int n) v then env else Env.bot
    | Int _, _ -> Env.bot
    | Plus, [ e1; e2 ] ->
        let v1 = eval env e1 and v2 = eval env e2 in
        if
          (Interval.is_bot @@ V.get_ints v1)
          || (Interval.is_bot @@ V.get_ints v2)
        then Env.bot
        else
          Env.meet
            (eval_bkw e1 (V.minus v v2) env)
            (eval_bkw e2 (V.minus v v1) env)
    | Plus, _ -> Env.bot
    | Minus, [ e1; e2 ] ->
        let v1 = eval env e1 and v2 = eval env e2 in
        if
          (Interval.is_bot @@ V.get_ints v1)
          || (Interval.is_bot @@ V.get_ints v2)
        then Env.bot
        else
          Env.meet
            (eval_bkw e1 (V.plus v v2) env)
            (eval_bkw e2 (V.minus v1 v) env)
    | Minus, _ -> Env.bot
    | Mult, [ _; _ ] -> env
    | Mult, _ -> Env.bot
    | Le, [ e1; e2 ] ->
        if
          V.leq (V.singleton_bool true) v
          && (not @@ V.leq (V.singleton_bool false) v)
        then
          (* case true *)
          let i1 = V.get_ints @@ eval env e1
          and i2 = V.get_ints @@ eval env e2 in
          if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
          else
            let i1 =
              Interval.meet i1
                (Interval.make IntBar.MInf (Interval.upper_bound i2))
            and i2 =
              Interval.meet i2
                (Interval.make (Interval.lower_bound i1) IntBar.PInf)
            in
            if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
            else
              Env.meet
                (eval_bkw e1 (V.make_ints i1) env)
                (eval_bkw e2 (V.make_ints i2) env)
        else if
          (not @@ V.leq (V.singleton_bool true) v)
          && V.leq (V.singleton_bool false) v
        then
          (* case false *)
          let i1 = V.get_ints @@ eval env e1
          and i2 = V.get_ints @@ eval env e2 in
          if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
          else
            let i1 =
              Interval.meet i1
                (Interval.make
                   (IntBar.succ @@ Interval.lower_bound i2)
                   IntBar.PInf)
            and i2 =
              Interval.meet i2
                (Interval.make IntBar.MInf
                   (IntBar.pred @@ Interval.upper_bound i1))
            in
            if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
            else
              Env.meet
                (eval_bkw e1 (V.make_ints i1) env)
                (eval_bkw e2 (V.make_ints i2) env)
        else env
    | Le, _ -> Env.bot
    | Lt, [ e1; e2 ] ->
        if
          V.leq (V.singleton_bool true) v
          && (not @@ V.leq (V.singleton_bool false) v)
        then
          (* case true *)
          let i1 = V.get_ints @@ eval env e1
          and i2 = V.get_ints @@ eval env e2 in
          let i1 =
            Interval.meet i1
              (Interval.make IntBar.MInf
                 (IntBar.pred @@ Interval.upper_bound i2))
          and i2 =
            Interval.meet i2
              (Interval.make
                 (IntBar.succ @@ Interval.lower_bound i1)
                 IntBar.PInf)
          in
          if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
          else
            Env.meet
              (eval_bkw e1 (V.make_ints i1) env)
              (eval_bkw e2 (V.make_ints i2) env)
        else if
          (not @@ V.leq (V.singleton_bool true) v)
          && V.leq (V.singleton_bool false) v
        then
          (* case false *)
          let i1 = V.get_ints @@ eval env e1
          and i2 = V.get_ints @@ eval env e2 in
          let i1 =
            Interval.meet i1
              (Interval.make (Interval.lower_bound i2) IntBar.PInf)
          and i2 =
            Interval.meet i2
              (Interval.make IntBar.MInf (Interval.upper_bound i1))
          in
          if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
          else
            Env.meet
              (eval_bkw e1 (V.make_ints i1) env)
              (eval_bkw e2 (V.make_ints i2) env)
        else env
    | Lt, _ -> Env.bot
    | Eq, [ e1; e2 ] ->
        if
          V.leq (V.singleton_bool true) v
          && (not @@ V.leq (V.singleton_bool false) v)
        then
          (* case true *)
          let i1 = V.get_ints @@ eval env e1
          and i2 = V.get_ints @@ eval env e2 in
          if Interval.is_bot i1 || Interval.is_bot i2 then Env.bot
          else
            let i = Interval.meet i1 i2 in
            if Interval.is_bot i then Env.bot
            else
              let v' = V.make_ints i in
              Env.meet (eval_bkw e1 v' env) (eval_bkw e2 v' env)
        else if
          (not @@ V.leq (V.singleton_bool true) v)
          && V.leq (V.singleton_bool false) v
        then
          (* case false *)
          Env.join
            (eval_bkw_prim Lt [ e1; e2 ] (V.singleton_bool true) env)
            (eval_bkw_prim Lt [ e2; e1 ] (V.singleton_bool true) env)
        else env
    | Eq, _ -> Env.bot

  and eval env e =
    match e with
    | Var x -> Env.get env x
    | Prim (prim, es) -> eval_prim env prim es
    | Call (_, _) -> V.top
    | IfThenElse (e_cond, e_true, e_false) ->
        V.join
          (eval (eval_bkw e_cond (V.singleton_bool true) env) e_true)
          (eval (eval_bkw e_cond (V.singleton_bool false) env) e_false)

  and eval_prim env prim es = call_prim prim ((List.map (eval env)) es)

  and call_prim prim vs =
    match (prim, vs) with
    | Bool b, [] -> V.singleton_bool b
    | Bool _, _ -> V.bot
    | Int n, [] -> V.singleton_int n
    | Int _, _ -> V.bot
    | Plus, [ v1; v2 ] -> V.plus v1 v2
    | Plus, _ -> V.bot
    | Minus, [ v1; v2 ] -> V.minus v1 v2
    | Minus, _ -> V.bot
    | Mult, [ v1; v2 ] -> V.mult v1 v2
    | Mult, _ -> V.bot
    | Le, [ v1; v2 ] -> V.le v1 v2
    | Le, _ -> V.bot
    | Lt, [ v1; v2 ] -> V.lt v1 v2
    | Lt, _ -> V.bot
    | Eq, [ v1; v2 ] -> V.eq v1 v2
    | Eq, _ -> V.bot

  let cond e b env = eval_bkw e (V.singleton_bool b) env
end

module MFG = Simple.Make (Env) (V)
module Analyzer = FO_MFG (Env) (V) (MFG) (A)

let%expect_test _ =
  Analyzer.run [| C |]
    [
      ( "F",
        [ "X"; "Y" ],
        IfThenElse
          ( Prim (Eq, [ Var "X"; Prim (Int 0, []) ]),
            Var "Y",
            Call
              ( "G",
                [
                  Call
                    ( "F",
                      [ Prim (Minus, [ Var "X"; Prim (Int 1, []) ]); Var "Y" ]
                    );
                ] ) ) );
      ( "G",
        [ "U" ],
        Prim (Minus, [ Prim (Mult, [ Var "U"; Var "U" ]); Var "U" ]) );
    ]
    "F"
    (Env.make [ ("X", V.singleton_int 1); ("Y", V.singleton_int 2) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    F ->
      [{ X -> {ints = [1,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    F ->
      [{ X -> {ints = [1,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:
    F: { X -> {ints = [0,0]};
         Y -> {ints = [2,2]}
       }

    New graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}]

    Iteration #3:
    Initial graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}]
    New calls:
    G: { U -> {ints = [2,2]}
       }

    New graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}];
    G ->
      [{ U -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}];
    G ->
      [{ U -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}];
    G ->
      [{ U -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}]

    Number of iterations: 5
    Number of calls: 7
    Number of iterations per point:
    F: 3
    G: 2
    Node numbering:
    F: 0
    G: 1
    Widening points:
    F

    Full graph:
    F ->
      [{ X -> {ints = [-∞,1]};
         Y -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}];
    G ->
      [{ U -> {ints = [2,2]}
       }
       ====>
       {ints = [2,2]}]
    Final MFG:
    [{ X -> {ints = [-∞,1]};
       Y -> {ints = [2,2]}
     }
     ====>
     {ints = [2,2]}]

    Final result:
    {ints = [2,2]} |}]

let%expect_test _ =
  Analyzer.run [| C |]
    [
      ( "even",
        [ "n" ],
        IfThenElse
          ( Prim (Eq, [ Var "n"; Prim (Int 0, []) ]),
            Prim (Bool true, []),
            Call ("odd", [ Prim (Minus, [ Var "n"; Prim (Int 1, []) ]) ]) ) );
      ( "odd",
        [ "n" ],
        IfThenElse
          ( Prim (Eq, [ Var "n"; Prim (Int 1, []) ]),
            Prim (Bool true, []),
            IfThenElse
              ( Prim (Eq, [ Var "n"; Prim (Int 0, []) ]),
                Prim (Bool false, []),
                Call ("even", [ Prim (Minus, [ Var "n"; Prim (Int 1, []) ]) ])
              ) ) );
    ]
    "even"
    (Env.make [ ("n", V.singleton_int 3) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    even ->
      [{ n -> {ints = [3,3]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    even ->
      [{ n -> {ints = [3,3]}
       }
       ====>
       ⊥]
    New calls:
    odd: { n -> {ints = [2,2]}
         }

    New graph:
    even ->
      [{ n -> {ints = [3,3]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    even ->
      [{ n -> {ints = [3,3]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:
    even: { n -> {ints = [1,1]}
          }

    New graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:
    odd: { n -> {ints = [-∞,2]}
         }

    New graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       ⊥]

    Iteration #4:
    Initial graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       {bools = {true, false}}]

    Iteration #5:
    Initial graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       {bools = {true, false}}]
    New calls:

    New graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true, false}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       {bools = {true, false}}]

    Number of iterations: 6
    Number of calls: 11
    Number of iterations per point:
    even: 4
    odd: 3
    Node numbering:
    even: 0
    odd: 1
    Widening points:
    even

    Full graph:
    even ->
      [{ n -> {ints = [-∞,3]}
       }
       ====>
       {bools = {true, false}}];
    odd ->
      [{ n -> {ints = [-∞,2]}
       }
       ====>
       {bools = {true, false}}]
    Final MFG:
    [{ n -> {ints = [-∞,3]}
     }
     ====>
     {bools = {true, false}}]

    Final result:
    {bools = {true, false}} |}]

let%expect_test _ =
  Analyzer.run [| C |]
    [
      ( "even",
        [ "n" ],
        IfThenElse
          ( Prim (Eq, [ Var "n"; Prim (Int 0, []) ]),
            Prim (Bool true, []),
            Call ("odd", [ Prim (Minus, [ Var "n"; Prim (Int 1, []) ]) ]) ) );
      ( "odd",
        [ "n" ],
        IfThenElse
          ( Prim (Eq, [ Var "n"; Prim (Int 1, []) ]),
            Prim (Bool true, []),
            IfThenElse
              ( Prim (Eq, [ Var "n"; Prim (Int 0, []) ]),
                Prim (Bool false, []),
                Call ("even", [ Prim (Minus, [ Var "n"; Prim (Int 1, []) ]) ])
              ) ) );
    ]
    "odd"
    (Env.make [ ("n", V.singleton_int 5) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    odd ->
      [{ n -> {ints = [5,5]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    odd ->
      [{ n -> {ints = [5,5]}
       }
       ====>
       ⊥]
    New calls:
    even: { n -> {ints = [4,4]}
          }

    New graph:
    even ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [5,5]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    even ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [5,5]}
       }
       ====>
       ⊥]
    New calls:
    odd: { n -> {ints = [3,3]}
         }

    New graph:
    even ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    even ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       ⊥]
    New calls:
    even: { n -> {ints = [-∞,4]}
          }

    New graph:
    even ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       {bools = {true, false}}]

    Iteration #4:
    Initial graph:
    even ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       ⊥];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       {bools = {true, false}}]
    New calls:

    New graph:
    even ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       {bools = {true, false}}];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       {bools = {true, false}}]

    Number of iterations: 5
    Number of calls: 9
    Number of iterations per point:
    even: 3
    odd: 3
    Node numbering:
    even: 1
    odd: 0
    Widening points:
    odd

    Full graph:
    even ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       {bools = {true, false}}];
    odd ->
      [{ n -> {ints = [-∞,5]}
       }
       ====>
       {bools = {true, false}}]
    Final MFG:
    [{ n -> {ints = [-∞,5]}
     }
     ====>
     {bools = {true, false}}]

    Final result:
    {bools = {true, false}} |}]

let%expect_test _ =
  Analyzer.run [| C |]
    [
      ( "loop",
        [ "n" ],
        IfThenElse
          ( Prim (Lt, [ Var "n"; Prim (Int 0, []) ]),
            Call ("loop", [ Prim (Int 7, []) ]),
            Call ("loop", [ Prim (Minus, [ Var "n"; Prim (Int 1, []) ]) ]) ) );
    ]
    "loop"
    (Env.make [ ("n", V.singleton_int 4) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    loop ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    loop ->
      [{ n -> {ints = [4,4]}
       }
       ====>
       ⊥]
    New calls:
    loop: { n -> {ints = [3,3]}
          }

    New graph:
    loop ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    loop ->
      [{ n -> {ints = [-∞,4]}
       }
       ====>
       ⊥]
    New calls:
    loop: { n -> {ints = [7,7]}
          }

    New graph:
    loop ->
      [{ n -> {ints = [-∞,+∞]}
       }
       ====>
       ⊥]

    Number of iterations: 3
    Number of calls: 3
    Number of iterations per point:
    loop: 3
    Node numbering:
    loop: 0
    Widening points:
    loop

    Full graph:
    loop ->
      [{ n -> {ints = [-∞,+∞]}
       }
       ====>
       ⊥]
    Final MFG:
    [{ n -> {ints = [-∞,+∞]}
     }
     ====>
     ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [
      ( "mc91",
        [ "n" ],
        IfThenElse
          ( Prim (Lt, [ Prim (Int 100, []); Var "n" ]),
            Prim (Minus, [ Var "n"; Prim (Int 10, []) ]),
            Call
              ( "mc91",
                [
                  Call ("mc91", [ Prim (Plus, [ Var "n"; Prim (Int 11, []) ]) ]);
                ] ) ) );
    ]
    "mc91"
    (Env.make [ ("n", V.singleton_int 50) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    mc91 ->
      [{ n -> {ints = [50,50]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    mc91 ->
      [{ n -> {ints = [50,50]}
       }
       ====>
       ⊥]
    New calls:
    mc91: { n -> {ints = [61,61]}
          }

    New graph:
    mc91 ->
      [{ n -> {ints = [50,+∞]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    mc91 ->
      [{ n -> {ints = [50,+∞]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    mc91 ->
      [{ n -> {ints = [50,+∞]}
       }
       ====>
       {ints = [91,+∞]}]

    Number of iterations: 3
    Number of calls: 3
    Number of iterations per point:
    mc91: 3
    Node numbering:
    mc91: 0
    Widening points:
    mc91

    Full graph:
    mc91 ->
      [{ n -> {ints = [50,+∞]}
       }
       ====>
       {ints = [91,+∞]}]
    Final MFG:
    [{ n -> {ints = [50,+∞]}
     }
     ====>
     {ints = [91,+∞]}]

    Final result:
    {ints = [91,+∞]} |}]

let%expect_test _ =
  Analyzer.run [| A; A; A; A; A; C |]
    [
      ( "tak",
        [ "x"; "y"; "z" ],
        IfThenElse
          ( Prim (Lt, [ Prim (Minus, [ Var "x"; Prim (Int 1, []) ]); Var "y" ]),
            Var "y",
            Call
              ( "tak",
                [
                  Call
                    ( "tak",
                      [
                        Prim (Minus, [ Var "x"; Prim (Int 1, []) ]);
                        Var "y";
                        Var "z";
                      ] );
                  Call
                    ( "tak",
                      [
                        Prim (Minus, [ Var "y"; Prim (Int 1, []) ]);
                        Var "z";
                        Var "x";
                      ] );
                  Call
                    ( "tak",
                      [
                        Prim (Minus, [ Var "z"; Prim (Int 1, []) ]);
                        Var "x";
                        Var "y";
                      ] );
                ] ) ) );
    ]
    "tak"
    (Env.make
       [
         ("x", V.singleton_int 4);
         ("y", V.singleton_int 3);
         ("z", V.singleton_int 2);
       ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    tak ->
      [{ x -> {ints = [4,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    tak ->
      [{ x -> {ints = [4,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:
    tak: { x -> {ints = [3,3]};
           y -> {ints = [3,3]};
           z -> {ints = [2,2]}
         }

    New graph:
    tak ->
      [{ x -> {ints = [3,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    tak ->
      [{ x -> {ints = [3,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    tak ->
      [{ x -> {ints = [3,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       {ints = [3,3]}]

    Iteration #3:
    Initial graph:
    tak ->
      [{ x -> {ints = [3,4]};
         y -> {ints = [3,3]};
         z -> {ints = [2,2]}
       }
       ====>
       {ints = [3,3]}]
    New calls:
    tak: { x -> {ints = [2,2]};
           y -> {ints = [2,2]};
           z -> {ints = [4,4]}
         }

    New graph:
    tak ->
      [{ x -> {ints = [2,4]};
         y -> {ints = [2,3]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [3,3]}]

    Iteration #4:
    Initial graph:
    tak ->
      [{ x -> {ints = [2,4]};
         y -> {ints = [2,3]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [3,3]}]
    New calls:
    tak: { x -> {ints = [1,3]};
           y -> {ints = [2,4]};
           z -> {ints = [2,4]}
         }

    New graph:
    tak ->
      [{ x -> {ints = [1,4]};
         y -> {ints = [2,4]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [2,3]}]

    Iteration #5:
    Initial graph:
    tak ->
      [{ x -> {ints = [1,4]};
         y -> {ints = [2,4]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [2,3]}]
    New calls:

    New graph:
    tak ->
      [{ x -> {ints = [1,4]};
         y -> {ints = [2,4]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [2,4]}]

    Number of iterations: 6
    Number of calls: 6
    Number of iterations per point:
    tak: 6
    Node numbering:
    tak: 0
    Widening points:
    tak

    Full graph:
    tak ->
      [{ x -> {ints = [1,4]};
         y -> {ints = [2,4]};
         z -> {ints = [2,4]}
       }
       ====>
       {ints = [2,4]}]
    Final MFG:
    [{ x -> {ints = [1,4]};
       y -> {ints = [2,4]};
       z -> {ints = [2,4]}
     }
     ====>
     {ints = [2,4]}]

    Final result:
    {ints = [2,4]} |}]

let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [
      ( "ack",
        [ "m"; "n" ],
        IfThenElse
          ( Prim
              ( Lt,
                [
                  Prim (Minus, [ Var "m"; Prim (Int 1, []) ]); Prim (Int 0, []);
                ] ),
            Prim (Plus, [ Var "n"; Prim (Int 1, []) ]),
            IfThenElse
              ( Call
                  ( "land",
                    [
                      Prim (Lt, [ Prim (Int 0, []); Var "m" ]);
                      Prim
                        ( Lt,
                          [
                            Prim (Minus, [ Var "n"; Prim (Int 1, []) ]);
                            Prim (Int 0, []);
                          ] );
                    ] ),
                Call
                  ( "ack",
                    [
                      Prim (Minus, [ Var "m"; Prim (Int 1, []) ]);
                      Prim (Int 1, []);
                    ] ),
                Call
                  ( "ack",
                    [
                      Prim (Minus, [ Var "m"; Prim (Int 1, []) ]);
                      Call
                        ( "ack",
                          [
                            Var "m"; Prim (Minus, [ Var "n"; Prim (Int 1, []) ]);
                          ] );
                    ] ) ) ) );
      ( "land",
        [ "x"; "y" ],
        IfThenElse (Var "x", Var "y", Prim (Bool false, [])) );
    ]
    "ack"
    (Env.make [ ("m", V.singleton_int 2); ("n", V.singleton_int 2) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥]
    New calls:
    land: { x -> {bools = {true}};
            y -> {bools = {false}}
          }

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       {bools = {false}}]

    Iteration #3:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [2,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       {bools = {false}}]
    New calls:
    ack: { m -> {ints = [2,2]};
           n -> {ints = [1,1]}
         }

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       {bools = {false}}]

    Iteration #4:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {false}}
       }
       ====>
       {bools = {false}}]
    New calls:
    land: { x -> {bools = {true}};
            y -> {bools = {true, false}}
          }

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {false}}]

    Iteration #5:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {false}}]
    New calls:

    New graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Iteration #6:
    Initial graph:
    ack ->
      [{ m -> {ints = [2,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    New calls:
    ack: { m -> {ints = [1,1]};
           n -> {ints = [1,1]}
         }

    New graph:
    ack ->
      [{ m -> {ints = [1,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Iteration #7:
    Initial graph:
    ack ->
      [{ m -> {ints = [1,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    New calls:
    ack: { m -> {ints = [0,1]};
           n -> {ints = [1,1]}
         }

    New graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Iteration #8:
    Initial graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       ⊥];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    New calls:

    New graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       {ints = [-∞,3]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Iteration #9:
    Initial graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,2]}
       }
       ====>
       {ints = [-∞,3]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    New calls:
    ack: { m -> {ints = [0,1]};
           n -> {ints = [-∞,3]}
         }

    New graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,+∞]}
       }
       ====>
       {ints = [-∞,3]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Iteration #10:
    Initial graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,+∞]}
       }
       ====>
       {ints = [-∞,3]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    New calls:

    New graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,+∞]}
       }
       ====>
       {ints = [-∞,+∞]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]

    Number of iterations: 11
    Number of calls: 21
    Number of iterations per point:
    ack: 7
    land: 4
    Node numbering:
    ack: 0
    land: 1
    Widening points:
    ack

    Full graph:
    ack ->
      [{ m -> {ints = [0,2]};
         n -> {ints = [-∞,+∞]}
       }
       ====>
       {ints = [-∞,+∞]}];
    land ->
      [{ x -> {bools = {true}};
         y -> {bools = {true, false}}
       }
       ====>
       {bools = {true, false}}]
    Final MFG:
    [{ m -> {ints = [0,2]};
       n -> {ints = [-∞,+∞]}
     }
     ====>
     {ints = [-∞,+∞]}]

    Final result:
    {ints = [-∞,+∞]} |}]

let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [
      ( "loop",
        [ "x"; "b" ],
        IfThenElse
          ( Var "b",
            Call
              ( "loop",
                [
                  Prim (Minus, [ Var "x"; Prim (Int 1, []) ]);
                  Prim (Le, [ Prim (Int 0, []); Var "x" ]);
                ] ),
            Var "x" ) );
    ]
    "loop"
    (Env.make [ ("x", V.singleton_int 5); ("b", V.singleton_bool true) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    loop ->
      [{ b -> {bools = {true}};
         x -> {ints = [5,5]}
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    loop ->
      [{ b -> {bools = {true}};
         x -> {ints = [5,5]}
       }
       ====>
       ⊥]
    New calls:
    loop: { b -> {bools = {true}};
            x -> {ints = [4,4]}
          }

    New graph:
    loop ->
      [{ b -> {bools = {true}};
         x -> {ints = [-∞,5]}
       }
       ====>
       ⊥]

    Iteration #2:
    Initial graph:
    loop ->
      [{ b -> {bools = {true}};
         x -> {ints = [-∞,5]}
       }
       ====>
       ⊥]
    New calls:
    loop: { b -> {bools = {true, false}};
            x -> {ints = [-∞,4]}
          }

    New graph:
    loop ->
      [{ b -> {bools = {true, false}};
         x -> {ints = [-∞,5]}
       }
       ====>
       ⊥]

    Iteration #3:
    Initial graph:
    loop ->
      [{ b -> {bools = {true, false}};
         x -> {ints = [-∞,5]}
       }
       ====>
       ⊥]
    New calls:

    New graph:
    loop ->
      [{ b -> {bools = {true, false}};
         x -> {ints = [-∞,5]}
       }
       ====>
       {ints = [-∞,5]}]

    Number of iterations: 4
    Number of calls: 4
    Number of iterations per point:
    loop: 4
    Node numbering:
    loop: 0
    Widening points:
    loop

    Full graph:
    loop ->
      [{ b -> {bools = {true, false}};
         x -> {ints = [-∞,5]}
       }
       ====>
       {ints = [-∞,5]}]
    Final MFG:
    [{ b -> {bools = {true, false}};
       x -> {ints = [-∞,5]}
     }
     ====>
     {ints = [-∞,5]}]

    Final result:
    {ints = [-∞,5]} |}]

(* The following example is extracted from "Minimal function graphs
   are not instrumented", A. Mycroft, M. Rosendahl *)
let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [
      ( "main",
        [],
        Call
          ( "h",
            [
              Call ("f", [ Prim (Int 1, []) ]); Call ("g", [ Prim (Int 2, []) ]);
            ] ) );
      ("f", [ "x" ], Call ("f", [ Var "x" ]));
      ("g", [ "y" ], Call ("g", [ Var "y" ]));
      ("h", [ "x"; "y" ], Prim (Plus, [ Var "x"; Var "y" ]));
    ]
    "main" (Env.make []);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    main ->
      [{
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    main ->
      [{
       }
       ====>
       ⊥]
    New calls:
    f: { x -> {ints = [1,1]}
       }

    New graph:
    f ->
      [{ x -> {ints = [1,1]}
       }
       ====>
       ⊥];
    main ->
      [{
       }
       ====>
       ⊥]

    Number of iterations: 2
    Number of calls: 3
    Number of iterations per point:
    f: 1
    main: 1
    Node numbering:
    f: 1
    main: 0
    Widening points:
    f

    Full graph:
    f ->
      [{ x -> {ints = [1,1]}
       }
       ====>
       ⊥];
    main ->
      [{
       }
       ====>
       ⊥]
    Final MFG:
    [{
     }
     ====>
     ⊥]

    Final result:
    ⊥ |}]

(* The following example is extracted from "Minimal function graphs
   are not instrumented", A. Mycroft, M. Rosendahl *)
let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [
      ( "main",
        [],
        Call
          ( "h",
            [
              Call ("g", [ Prim (Int 2, []) ]); Call ("f", [ Prim (Int 1, []) ]);
            ] ) );
      ("f", [ "x" ], Call ("f", [ Var "x" ]));
      ("g", [ "y" ], Call ("g", [ Var "y" ]));
      ("h", [ "x"; "y" ], Prim (Plus, [ Var "x"; Var "y" ]));
    ]
    "main" (Env.make []);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    main ->
      [{
       }
       ====>
       ⊥]

    Iteration #1:
    Initial graph:
    main ->
      [{
       }
       ====>
       ⊥]
    New calls:
    g: { y -> {ints = [2,2]}
       }

    New graph:
    g ->
      [{ y -> {ints = [2,2]}
       }
       ====>
       ⊥];
    main ->
      [{
       }
       ====>
       ⊥]

    Number of iterations: 2
    Number of calls: 3
    Number of iterations per point:
    g: 1
    main: 1
    Node numbering:
    g: 1
    main: 0
    Widening points:
    g

    Full graph:
    g ->
      [{ y -> {ints = [2,2]}
       }
       ====>
       ⊥];
    main ->
      [{
       }
       ====>
       ⊥]
    Final MFG:
    [{
     }
     ====>
     ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| C; A; A; A; C |]
    [ ("f", [ "x" ], Call ("f", [ Var "x" ])) ]
    "f"
    (Env.make [ ("x", V.singleton_int 42) ]);
  [%expect
    {|
    Iteration #0:
    Initial graph:

    New calls:

    New graph:
    f ->
      [{ x -> {ints = [42,42]}
       }
       ====>
       ⊥]

    Number of iterations: 1
    Number of calls: 1
    Number of iterations per point:
    f: 1
    Node numbering:
    f: 0
    Widening points:
    f

    Full graph:
    f ->
      [{ x -> {ints = [42,42]}
       }
       ====>
       ⊥]
    Final MFG:
    [{ x -> {ints = [42,42]}
     }
     ====>
     ⊥]

    Final result:
    ⊥ |}]

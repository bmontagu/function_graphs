(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Function_graphs

(** A simple static analyzer that performs a non-relational analysis for a While
    language, using a non-standard approach, where an abstraction as a function
    graph is directly computed. *)
module While (Expr : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (BExpr : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (S : sig
  type t

  val bot : t
  val is_bot : t -> bool
  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include Function_graphs.Sigs.MFG with type dom = S.t and type codom = S.t

  val dom : t -> dom list
  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
end) (A : sig
  val assign : Test_common.While.var -> Expr.t -> S.t -> S.t
  val cond : BExpr.t -> bool -> S.t -> S.t
end) : sig
  val run :
    Functional.strategy array ->
    (Expr.t, BExpr.t) Test_common.While.prog ->
    S.t ->
    unit
end = struct
  module P = struct
    type t = (Expr.t, BExpr.t) Test_common.While.prog

    let compare = Test_common.While.compare_prog Expr.compare BExpr.compare
    let pp fmt p = Test_common.While.pp_prog Expr.pp BExpr.pp fmt p
  end

  module PMap = struct
    include Map.Make (P)

    let[@warning "-32"] pp pp_v fmt m =
      let open Format in
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             fprintf fmt "@[<v 2>@[%a@] ->@ @[%a@]@]" P.pp k pp_v v))
        (to_seq m)
  end

  module MFG2 = Indexed.ExtendFinite (PMap) (S) (S) (MFG)

  module MFG3 = struct
    include MFG2

    let widen a1 a2 = widen 256 a1 a2
  end

  module MFG4 = Functional.Make (MFG3) (MFG3)
  module M = Solver.Simple (MFG4)

  let lfp strategy (f : (MFG2.t -> MFG2.t) -> MFG2.t -> MFG2.t) (g : MFG2.t) :
      MFG2.t =
    fst @@ M.fix_gen ~debug:false strategy f g

  type 'a m = { value : 'a } [@@unboxed]

  let return value = { value }
  let ( let+ ) { value = g } f = { value = MFG2.join g (f g).value }
  let mjoin m1 m2 = { value = MFG2.join m1.value m2.value }

  let mlfp strategy (f : (MFG2.t -> MFG2.t m) -> MFG2.t -> MFG2.t m)
      (g : MFG2.t) : MFG2.t m =
    return
    @@ lfp strategy (fun f' g' -> (f (fun h' -> return @@ f' h') g').value) g

  let map_join l map =
    List.fold_left (fun acc x -> mjoin (map x) acc) (return MFG2.bot) l

  let rec analyze strategy p s =
    let open Test_common.While in
    let analyze p s = analyze strategy p s in
    let open MFG2 in
    if S.is_bot s then return bot
    else
      match p with
      | Assign (x, e) ->
          return @@ map (fun (_p, s) -> A.assign x e s) (init (p, s))
      | Skip -> return @@ map (fun (_p, s) -> s) (init (p, s))
      | Seq (p1, p2) ->
          let+ i = return @@ init (p, s) in
          let+ g1 = analyze p1 s in
          let s1 = apply g1 (p1, s) in
          let+ g2 = analyze p2 s1 in
          let s2 = apply g2 (p2, s1) in
          return @@ map (fun _ -> s2) i
      | If (c, p1, p2) ->
          let+ _ = return @@ init (p, s) in
          mjoin
            (let s1 = A.cond c true s in
             let+ g1 = analyze p1 s1 in
             let s1' = apply g1 (p1, s) in
             return @@ map (fun _ -> s1') (init (p, s1)))
            (let s2 = A.cond c false s in
             let+ g2 = analyze p2 s2 in
             let s2' = apply g2 (p2, s) in
             return @@ map (fun _ -> s2') (init (p, s2)))
      | While (c, body) ->
          mlfp strategy
            (fun analyze_loop (g0 : MFG2.t) ->
              map_join
                (MFG.dom @@ apply0 g0 p)
                (fun (s0 : S.t) ->
                  let+ _ = return @@ init (p, s0) in
                  mjoin
                    (let s0' = A.cond c false s0 in
                     return @@ map (fun _ -> s0') (init (p, s0')))
                    (let s0' = A.cond c true s0 in
                     let+ g1 = analyze body s0' in
                     let s1 = apply g1 (body, s0) in
                     let+ g2 = analyze_loop @@ init (p, s1) in
                     let s2 = apply g2 (p, s1) in
                     return @@ map (fun _ -> s2) (init (p, s0')))))
            (init (p, s))

  let run strategy p s =
    let g = (analyze strategy p s).value in
    let res = MFG2.apply g (p, s) in
    Format.printf "Final MFG:@.@[%a@]@.@.Final result:@.@[%a@]@." MFG2.pp g S.pp
      res
end

module MFG = Simple.Make (Test_common.IntervalStore) (Test_common.IntervalStore)

module Analyzer =
  While (Test_common.While.Expr) (Test_common.While.BExpr)
    (Test_common.IntervalStore)
    (MFG)
    (Test_common.While.ValueAnalysis)

let%expect_test _ =
  Analyzer.run [| C |] (Assign ("i", Const 0)) Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      i := 0 ->
        [[]
         ====>
         [i !-> [0,0]]]

      Final result:
      [i !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq (Assign ("i", Const 0), Assign ("j", Const 1)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      i := 0 ->
        [[]
         ====>
         [i !-> [0,0]]];
      j := 1 ->
        [[i !-> [0,0]]
         ====>
         [i !-> [0,0];
          j !-> [1,1]]];
      i := 0; j := 1 ->
        [[]
         ====>
         [i !-> [0,0];
          j !-> [1,1]]]

      Final result:
      [i !-> [0,0];
       j !-> [1,1]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq (Assign ("i", Const 0), Assign ("i", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      i := 0 ->
        [[]
         ====>
         [i !-> [0,0]]];
      i := 2 ->
        [[i !-> [0,0]]
         ====>
         [i !-> [2,2]]];
      i := 0; i := 2 ->
        [[]
         ====>
         [i !-> [2,2]]]

      Final result:
      [i !-> [2,2]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (If (Unknown, Assign ("i", Const 0), Assign ("i", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      i := 0 ->
        [[]
         ====>
         [i !-> [0,0]]];
      i := 2 ->
        [[]
         ====>
         [i !-> [2,2]]];
      if ? then i := 0 else i := 2 ->
        [[]
         ====>
         [i !-> [0,2]]]

      Final result:
      [i !-> [0,2]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (If (Unknown, Assign ("i", Const 0), Assign ("j", Const 2)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      i := 0 ->
        [[]
         ====>
         [i !-> [0,0]]];
      j := 2 ->
        [[]
         ====>
         [j !-> [2,2]]];
      if ? then i := 0 else j := 2 ->
        [[]
         ====>
         [i -> [0,0];
          j -> [2,2]]]

      Final result:
      [i -> [0,0];
       j -> [2,2]] |}]

let%expect_test _ =
  Analyzer.run [| C; A; C |]
    (Seq
       ( Assign ("i", Const 0),
         While (Lt (Var "i", Const 100), Assign ("i", Plus (Var "i", Const 1)))
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[]
       ====>
       [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [1,+∞]]];
    i := 0; while i < 100 do i := i + 1 done ->
      [[]
       ====>
       [i !-> [100,100]]];
    while i < 100 do i := i + 1 done ->
      [[i !-> [0,+∞]]
       ====>
       [i !-> [100,100]]]

    Final result:
    [i !-> [100,100]] |}]

let%expect_test _ =
  Analyzer.run [| C; A; C |]
    (Seq
       ( Assign ("i", Const 0),
         Seq
           ( Assign ("j", Const 0),
             While
               ( Lt (Var "i", Const 100),
                 Seq
                   ( Assign ("j", Const 1),
                     Seq
                       ( While
                           ( Lt (Var "j", Const 100),
                             Assign ("j", Plus (Var "j", Const 1)) ),
                         Assign ("i", Plus (Var "i", Const 1)) ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[]
       ====>
       [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [0,+∞];
        j !-> [100,100]]
       ====>
       [i !-> [1,+∞];
        j !-> [100,100]]];
    j := 0 ->
      [[i !-> [0,0]]
       ====>
       [i !-> [0,0];
        j !-> [0,0]]];
    j := 1 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [0,+∞];
        j !-> [1,1]]];
    j := j + 1 ->
      [[i !-> [0,+∞];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,+∞];
        j !-> [2,+∞]]];
    i := 0;
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[]
       ====>
       [i !-> [100,100];
        j !-> [100,100]]];
    j := 0;
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [0,0]]
       ====>
       [i !-> [100,100];
        j !-> [100,100]]];
    j := 1; while j < 100 do j := j + 1 done; i := i + 1 ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [1,+∞];
        j !-> [100,100]]];
    while j < 100 do j := j + 1 done; i := i + 1 ->
      [[i !-> [0,+∞];
        j !-> [1,1]]
       ====>
       [i !-> [1,+∞];
        j !-> [100,100]]];
    while i < 100 do j := 1; while j < 100 do j := j + 1 done; i := i + 1 done ->
      [[i !-> [0,+∞];
        j !-> [0,+∞]]
       ====>
       [i !-> [100,100];
        j !-> [100,100]]];
    while j < 100 do j := j + 1 done ->
      [[i !-> [0,+∞];
        j !-> [1,+∞]]
       ====>
       [i !-> [0,+∞];
        j !-> [100,100]]]

    Final result:
    [i !-> [100,100];
     j !-> [100,100]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq
       ( Assign ("x", Const 0),
         If
           ( Or (Lt (Var "x", Const 0), Lt (Const 0, Var "x")),
             Assign ("y", Const 1),
             Assign ("y", Const 2) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      x := 0 ->
        [[]
         ====>
         [x !-> [0,0]]];
      y := 2 ->
        [[x !-> [0,0]]
         ====>
         [x !-> [0,0];
          y !-> [2,2]]];
      x := 0; if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[]
         ====>
         [x !-> [0,0];
          y !-> [2,2]]];
      if x < 0 || 0 < x then y := 1 else y := 2 ->
        [[x !-> [0,0]]
         ====>
         [x !-> [0,0];
          y !-> [2,2]]]

      Final result:
      [x !-> [0,0];
       y !-> [2,2]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq
       ( Assign ("x", Unknown),
         If
           ( Or (Lt (Var "x", Const 0), Lt (Const 0, Var "x")),
             Assign ("x", Const 0),
             Skip ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
      Final MFG:
      x := ? ->
        [[]
         ====>
         [x !-> [-∞,+∞]]];
      x := 0 ->
        [[x !-> [-∞,+∞]]
         ====>
         [x !-> [0,0]]];
      skip ->
        [[x !-> [0,0]]
         ====>
         [x !-> [0,0]]];
      x := ?; if x < 0 || 0 < x then x := 0 else skip ->
        [[]
         ====>
         [x !-> [0,0]]];
      if x < 0 || 0 < x then x := 0 else skip ->
        [[x !-> [-∞,+∞]]
         ====>
         [x !-> [0,0]]]

      Final result:
      [x !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run [| C; A; C |]
    (Seq
       ( Assign ("n", Const 50),
         Seq
           ( Assign ("i", Const 0),
             While
               ( Lt (Var "i", Var "n"),
                 Seq
                   ( Assign ("i", Plus (Var "i", Const 1)),
                     Assign ("n", Minus (Var "n", Const 1)) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[n !-> [50,50]]
       ====>
       [i !-> [0,0];
        n !-> [50,50]]];
    i := i + 1 ->
      [[i !-> [0,+∞];
        n !-> [-∞,50]]
       ====>
       [i !-> [1,+∞];
        n !-> [-∞,50]]];
    n := 50 ->
      [[]
       ====>
       [n !-> [50,50]]];
    n := n - 1 ->
      [[i !-> [1,+∞];
        n !-> [-∞,50]]
       ====>
       [i !-> [1,+∞];
        n !-> [-∞,49]]];
    i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[n !-> [50,50]]
       ====>
       [i !-> [1,50];
        n !-> [0,49]]];
    i := i + 1; n := n - 1 ->
      [[i !-> [0,+∞];
        n !-> [-∞,50]]
       ====>
       [i !-> [1,+∞];
        n !-> [-∞,49]]];
    n := 50; i := 0; while i < n do i := i + 1; n := n - 1 done ->
      [[]
       ====>
       [i !-> [1,50];
        n !-> [0,49]]];
    while i < n do i := i + 1; n := n - 1 done ->
      [[i !-> [0,+∞];
        n !-> [-∞,50]]
       ====>
       [i !-> [1,50];
        n !-> [0,49]]]

    Final result:
    [i !-> [1,50];
     n !-> [0,49]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq
       ( Assign ("i", Const 0),
         While (Le (Var "i", Const 100), Assign ("i", Minus (Var "i", Const 1)))
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[]
       ====>
       [i !-> [0,0]]];
    i := i - 1 ->
      [[i !-> [-∞,0]]
       ====>
       [i !-> [-∞,-1]]];
    i := 0; while i <= 100 do i := i - 1 done ->
      [[]
       ====>
       ⊥];
    while i <= 100 do i := i - 1 done ->
      [[i !-> [-∞,0]]
       ====>
       ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq
       ( Assign ("i", Const 0),
         While
           ( And (Unknown, Le (Var "i", Const 100)),
             Assign ("i", Minus (Var "i", Const 1)) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[]
       ====>
       [i !-> [0,0]]];
    i := i - 1 ->
      [[i !-> [-∞,0]]
       ====>
       [i !-> [-∞,-1]]];
    i := 0; while ? && i <= 100 do i := i - 1 done ->
      [[]
       ====>
       [i !-> [-∞,0]]];
    while ? && i <= 100 do i := i - 1 done ->
      [[i !-> [-∞,0]]
       ====>
       [i !-> [-∞,0]]]

    Final result:
    [i !-> [-∞,0]] |}]

let%expect_test _ =
  Analyzer.run [| C |]
    (Seq
       ( Assign ("x", Const 0),
         While
           ( Lt (Var "x", Const 10),
             Seq
               ( Assign ("y", Const 0),
                 While
                   (Lt (Var "y", Var "x"), Assign ("y", Plus (Var "y", Const 1)))
               ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    y := 0 ->
      [[x !-> [0,0];
        y -> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    x := 0; while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[]
       ====>
       ⊥];
    y := 0; while y < x do y := y + 1 done ->
      [[x !-> [0,0];
        y -> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    while x < 10 do y := 0; while y < x do y := y + 1 done done ->
      [[x !-> [0,0];
        y -> [0,0]]
       ====>
       ⊥];
    while y < x do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( While
                   (Le (Var "y", Var "x"), Assign ("y", Plus (Var "y", Const 1))),
                 Assign ("x", Var "y") ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    x := y ->
      [[x !-> [0,+∞];
        y !-> [1,+∞]]
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    y := 1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    y := y + 1 ->
      [[x !-> [1,999];
        y !-> [1,+∞]]
       ====>
       [x !-> [1,999];
        y !-> [2,+∞]]];
    x := 0; y := 1 ->
      [[]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    x := 0; y := 1;
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[]
       ====>
       [x !-> [1000,+∞];
        y !-> [1,+∞]]];
    while y <= x do y := y + 1 done; x := y ->
      [[x !-> [0,999];
        y !-> [1,+∞]]
       ====>
       [x !-> [1,+∞];
        y !-> [1,+∞]]];
    while y <= x do y := y + 1 done ->
      [[x !-> [0,999];
        y !-> [1,+∞]]
       ====>
       [x !-> [0,+∞];
        y !-> [1,+∞]]];
    while x < 1000 do while y <= x do y := y + 1 done; x := y done ->
      [[x !-> [0,+∞];
        y !-> [1,+∞]]
       ====>
       [x !-> [1000,+∞];
        y !-> [1,+∞]]]

    Final result:
    [x !-> [1000,+∞];
     y !-> [1,+∞]] |}]

let%expect_test _ =
  Analyzer.run [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( Seq
                   ( Assign ("y", Const 0),
                     While
                       ( Le (Var "y", Const 1000),
                         Assign ("y", Plus (Var "y", Const 1)) ) ),
                 Assign ("x", Var "y") ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    x := y ->
      [[x !-> [0,0];
        y !-> [1001,1001]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001]]];
    y := 0 ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    y := 1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    y := y + 1 ->
      [[x !-> [0,0];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1,1001]]];
    x := 0; y := 1 ->
      [[]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    y := 0; while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001]]];
    x := 0; y := 1;
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001]]];
    y := 0; while y <= 1000 do y := y + 1 done; x := y ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001]]];
    while x < 1000 do y := 0; while y <= 1000 do y := y + 1 done; x := y done ->
      [[x !-> [0,1001];
        y !-> [1,1001]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001]]]

    Final result:
    [x !-> [1001,1001];
     y !-> [1001,1001]] |}]

let%expect_test _ =
  Analyzer.run [| C; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 1)),
         While
           ( Lt (Var "x", Const 1000),
             Seq
               ( Seq
                   ( Assign ("y", Const 0),
                     While
                       ( Le (Var "y", Const 1000),
                         Assign ("y", Plus (Var "y", Const 1)) ) ),
                 Seq
                   ( Seq
                       ( Assign ("z", Const 0),
                         While
                           ( Le (Var "z", Var "y"),
                             Assign ("z", Plus (Var "z", Const 1)) ) ),
                     Assign ("x", Var "y") ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    x := y ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1002,1002]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    y := 0 ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    y := 1 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    y := y + 1 ->
      [[x !-> [0,0];
        y !-> [0,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1,+∞]]];
    z := 0 ->
      [[x !-> [0,0];
        y !-> [1001,1001]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [0,0]]];
    z := z + 1 ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [0,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1,+∞]]];
    x := 0; y := 1 ->
      [[]
       ====>
       [x !-> [0,0];
        y !-> [1,1]]];
    y := 0; while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001]]];
    z := 0; while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    x := 0; y := 1;
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y ->
      [[x !-> [0,0];
        y !-> [1,1]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    z := 0; while z <= y do z := z + 1 done; x := y ->
      [[x !-> [0,0];
        y !-> [1001,1001]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    while y <= 1000 do y := y + 1 done ->
      [[x !-> [0,0];
        y !-> [0,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001]]];
    while z <= y do z := z + 1 done ->
      [[x !-> [0,0];
        y !-> [1001,1001];
        z !-> [0,+∞]]
       ====>
       [x !-> [0,0];
        y !-> [1001,1001];
        z !-> [1002,1002]]];
    while
    x < 1000
    do
    y := 0; while y <= 1000 do y := y + 1 done;
    z := 0; while z <= y do z := z + 1 done; x := y
    done ->
      [[x !-> [0,+∞];
        y !-> [1,+∞];
        z -> [1002,1002]]
       ====>
       [x !-> [1001,1001];
        y !-> [1001,1001];
        z !-> [1002,1002]]]

    Final result:
    [x !-> [1001,1001];
     y !-> [1001,1001];
     z !-> [1002,1002]] |}]

let%expect_test _ =
  Analyzer.run [| A; C; A; C |]
    (Seq
       ( Seq
           ( Assign ("a1", Const 0),
             Seq
               ( Assign ("a2", Const 0),
                 Seq
                   ( Assign ("a3", Const 0),
                     Seq
                       ( Assign ("a4", Const 0),
                         Seq (Assign ("a5", Const 0), Assign ("r", Const 0)) )
                   ) ) ),
         Seq
           ( While
               (Lt (Var "a1", Const 20), Assign ("a1", Plus (Var "a1", Const 1))),
             Seq
               ( While
                   ( Lt (Var "a2", Const 20),
                     Assign ("a2", Plus (Var "a2", Const 1)) ),
                 Seq
                   ( While
                       ( Lt (Var "a3", Const 20),
                         Assign ("a3", Plus (Var "a3", Const 1)) ),
                     Seq
                       ( While
                           ( Lt (Var "a4", Const 20),
                             Assign ("a4", Plus (Var "a4", Const 1)) ),
                         While
                           ( Lt (Var "a5", Const 20),
                             Assign ("a5", Plus (Var "a5", Const 1)) ) ) ) ) )
       ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    a1 := 0 ->
      [[]
       ====>
       [a1 !-> [0,0]]];
    a1 := a1 + 1 ->
      [[a1 !-> [-∞,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [1,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a2 := 0 ->
      [[a1 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0]]];
    a2 := a2 + 1 ->
      [[a1 !-> [20,20];
        a2 !-> [-∞,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [1,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a3 := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0]]];
    a3 := a3 + 1 ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [-∞,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [1,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a4 := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0]]];
    a4 := a4 + 1 ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [-∞,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [1,20];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a5 := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0]]];
    a5 := a5 + 1 ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [-∞,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [1,20];
        r !-> [0,0]]];
    r := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0 ->
      [[]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0 ->
      [[a1 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a3 := 0; a4 := 0; a5 := 0; r := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a4 := 0; a5 := 0; r := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a5 := 0; r := 0 ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0]]
       ====>
       [a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    a1 := 0; a2 := 0; a3 := 0; a4 := 0; a5 := 0; r := 0;
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a1 < 20 do a1 := a1 + 1 done;
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [0,0];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done;
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done;
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done; while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]];
    while a1 < 20 do a1 := a1 + 1 done ->
      [[a1 !-> [-∞,+∞];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [0,0];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a2 < 20 do a2 := a2 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [-∞,+∞];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [0,0];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a3 < 20 do a3 := a3 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [-∞,+∞];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [0,0];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a4 < 20 do a4 := a4 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [-∞,+∞];
        a5 !-> [0,0];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [0,0];
        r !-> [0,0]]];
    while a5 < 20 do a5 := a5 + 1 done ->
      [[a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [-∞,+∞];
        r !-> [0,0]]
       ====>
       [a1 !-> [20,20];
        a2 !-> [20,20];
        a3 !-> [20,20];
        a4 !-> [20,20];
        a5 !-> [20,20];
        r !-> [0,0]]]

    Final result:
    [a1 !-> [20,20];
     a2 !-> [20,20];
     a3 !-> [20,20];
     a4 !-> [20,20];
     a5 !-> [20,20];
     r !-> [0,0]] |}]

let%expect_test _ =
  Analyzer.run [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("FALSE", Const 0), Assign ("TRUE", Const 1)),
         Seq
           ( Seq (Assign ("x", Const 100), Assign ("b", Var "TRUE")),
             While
               ( And (Le (Var "b", Var "TRUE"), Le (Var "TRUE", Var "b")),
                 Seq
                   ( Assign ("x", Minus (Var "x", Const 1)),
                     If
                       ( Lt (Var "x", Const 0),
                         Assign ("b", Var "TRUE"),
                         Assign ("b", Var "FALSE") ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    FALSE := 0 ->
      [[]
       ====>
       [FALSE !-> [0,0]]];
    TRUE := 1 ->
      [[FALSE !-> [0,0]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1]]];
    b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]];
    b := TRUE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        x !-> [100,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [100,100]]];
    x := 100 ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        x !-> [100,100]]];
    x := x - 1 ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [100,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]];
    FALSE := 0; TRUE := 1 ->
      [[]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1]]];
    x := 100; b := TRUE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [100,100]]];
    x := x - 1; if x < 0 then b := TRUE else b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [100,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]];
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]];
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]];
    if x < 0 then b := TRUE else b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [99,99]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if x < 0 then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [99,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [99,99]]]

    Final result:
    [FALSE !-> [0,0];
     TRUE !-> [1,1];
     b !-> [0,0];
     x !-> [99,99]] |}]

let%expect_test _ =
  Analyzer.run [| A; C; A; C |]
    (Seq
       ( Seq (Assign ("FALSE", Const 0), Assign ("TRUE", Const 1)),
         Seq
           ( Seq (Assign ("x", Const 100), Assign ("b", Var "TRUE")),
             While
               ( And (Le (Var "b", Var "TRUE"), Le (Var "TRUE", Var "b")),
                 Seq
                   ( Assign ("x", Minus (Var "x", Const 1)),
                     If
                       ( Le (Const 0, Var "x"),
                         Assign ("b", Var "TRUE"),
                         Assign ("b", Var "FALSE") ) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    FALSE := 0 ->
      [[]
       ====>
       [FALSE !-> [0,0]]];
    TRUE := 1 ->
      [[FALSE !-> [0,0]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1]]];
    b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,-1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [-∞,-1]]];
    b := TRUE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b -> [1,1];
        x !-> [0,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [0,100]]];
    x := 100 ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        x !-> [100,100]]];
    x := x - 1 ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]];
    FALSE := 0; TRUE := 1 ->
      [[]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1]]];
    x := 100; b := TRUE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [100,100]]];
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,99]]];
    FALSE := 0; TRUE := 1;
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [-∞,99]]];
    x := 100; b := TRUE;
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [-∞,99]]];
    if 0 <= x then b := TRUE else b := FALSE ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [1,1];
        x !-> [-∞,99]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,1];
        x !-> [-∞,99]]];
    while
    b <= TRUE && TRUE <= b
    do
    x := x - 1; if 0 <= x then b := TRUE else b := FALSE
    done ->
      [[FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [-∞,1];
        x !-> [-∞,100]]
       ====>
       [FALSE !-> [0,0];
        TRUE !-> [1,1];
        b !-> [0,0];
        x !-> [-∞,99]]]

    Final result:
    [FALSE !-> [0,0];
     TRUE !-> [1,1];
     b !-> [0,0];
     x !-> [-∞,99]] |}]

let%expect_test _ =
  Analyzer.run [| A; C; A; A; C |]
    (Seq
       ( Seq (Assign ("x", Const 0), Assign ("y", Const 0)),
         While
           ( Le (Var "x", Const 99),
             Seq
               ( Assign ("x", Plus (Var "x", Const 1)),
                 If
                   ( Le (Var "x", Const 49),
                     Assign ("y", Plus (Var "y", Const 1)),
                     Assign ("y", Minus (Var "y", Const 1)) ) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[]
       ====>
       [x !-> [0,0]]];
    x := x + 1 ->
      [[x !-> [0,99];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    y := 0 ->
      [[x !-> [0,0]]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    y := y + 1 ->
      [[x !-> [1,49];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [1,49];
        y !-> [-∞,+∞]]];
    y := y - 1 ->
      [[x !-> [50,100];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [50,100];
        y !-> [-∞,+∞]]];
    x := 0; y := 0 ->
      [[]
       ====>
       [x !-> [0,0];
        y !-> [0,0]]];
    x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 ->
      [[x !-> [0,99];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    x := 0; y := 0;
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]];
    if x <= 49 then y := y + 1 else y := y - 1 ->
      [[x !-> [1,100];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [1,100];
        y !-> [-∞,+∞]]];
    while x <= 99 do x := x + 1; if x <= 49 then y := y + 1 else y := y - 1 done ->
      [[x !-> [0,+∞];
        y !-> [-∞,+∞]]
       ====>
       [x !-> [100,+∞];
        y !-> [-∞,+∞]]]

    Final result:
    [x !-> [100,+∞];
     y !-> [-∞,+∞]] |}]

let%expect_test _ =
  Analyzer.run [| A; C |]
    (Seq
       ( Seq
           ( Seq
               ( Seq
                   ( Seq (Assign ("m", Const 0), Assign ("M", Const 512)),
                     Assign ("x", Unknown) ),
                 Seq (Assign ("min", Var "m"), Assign ("max", Var "M")) ),
             If
               ( Le (Var "min", Var "max"),
                 If (Le (Var "x", Var "max"), Assign ("max", Var "x"), Skip),
                 If (Lt (Var "x", Var "min"), Assign ("max", Var "min"), Skip)
               ) ),
         Seq
           ( Assign ("y", Var "max"),
             If
               ( And (Le (Var "m", Var "y"), Le (Var "y", Var "M")),
                 Assign ("result", Const 1),
                 Assign ("result", Const 0) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    M := 512 ->
      [[m !-> [0,0]]
       ====>
       [M !-> [512,512];
        m !-> [0,0]]];
    m := 0 ->
      [[]
       ====>
       [m !-> [0,0]]];
    max := M ->
      [[M !-> [512,512];
        m !-> [0,0];
        min !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    max := x ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,512]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,512]]];
    min := m ->
      [[M !-> [512,512];
        m !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    result := 0 ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞];
        y !-> [-∞,-1]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,0];
        x !-> [-∞,+∞];
        y !-> [-∞,-1]]];
    result := 1 ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞];
        y !-> [0,512]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [1,1];
        x !-> [-∞,+∞];
        y !-> [0,512]]];
    x := ? ->
      [[M !-> [512,512];
        m !-> [0,0]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        x !-> [-∞,+∞]]];
    y := max ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]];
    skip ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [513,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [513,+∞]]];
    m := 0; M := 512 ->
      [[]
       ====>
       [M !-> [512,512];
        m !-> [0,0]]];
    min := m; max := M ->
      [[M !-> [512,512];
        m !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,1];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]];
    m := 0; M := 512; x := ? ->
      [[]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        x !-> [-∞,+∞]]];
    m := 0; M := 512; x := ?; min := m; max := M ->
      [[]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip ->
      [[]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    m := 0; M := 512; x := ?; min := m; max := M;
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip;
    y := max; if m <= y && y <= M then result := 1 else result := 0 ->
      [[]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,1];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]];
    if
    min <= max
    then
    if x <= max then max := x else skip
    else
    if x < min then max := min else skip ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    if x <= max then max := x else skip ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [512,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞]]];
    if m <= y && y <= M then result := 1 else result := 0 ->
      [[M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]
       ====>
       [M !-> [512,512];
        m !-> [0,0];
        max !-> [-∞,512];
        min !-> [0,0];
        result !-> [0,1];
        x !-> [-∞,+∞];
        y !-> [-∞,512]]]

    Final result:
    [M !-> [512,512];
     m !-> [0,0];
     max !-> [-∞,512];
     min !-> [0,0];
     result !-> [0,1];
     x !-> [-∞,+∞];
     y !-> [-∞,512]] |}]

let%expect_test _ =
  Analyzer.run [| A; C |]
    (Seq
       ( Assign ("n", Const 0),
         While
           ( Bool true,
             If
               ( Lt (Var "n", Const 60),
                 Assign ("n", Plus (Var "n", Const 1)),
                 Assign ("n", Const 0) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    n := 0 ->
      [[n -> [60,+∞]]
       ====>
       [n !-> [0,0]]];
    n := n + 1 ->
      [[n !-> [0,+∞]]
       ====>
       [n !-> [1,60]]];
    n := 0; while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[]
       ====>
       ⊥];
    if n < 60 then n := n + 1 else n := 0 ->
      [[n !-> [0,+∞]]
       ====>
       [n !-> [0,60]]];
    while true do if n < 60 then n := n + 1 else n := 0 done ->
      [[n !-> [0,+∞]]
       ====>
       ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| A; C |]
    (Seq
       ( Assign ("i", Const 0),
         While
           ( Lt (Var "i", Const 4),
             Seq
               ( Assign ("j", Const 0),
                 Seq
                   ( While
                       ( Lt (Var "j", Const 4),
                         Seq
                           ( Assign ("i", Plus (Var "i", Const 1)),
                             Assign ("j", Plus (Var "j", Const 1)) ) ),
                     Assign ("i", Plus (Minus (Var "i", Var "j"), Const 1)) ) )
           ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    i := 0 ->
      [[]
       ====>
       [i !-> [0,0]]];
    i := i + 1 ->
      [[i !-> [-∞,+∞];
        j !-> [-∞,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [0,3]]];
    i := i - j + 1 ->
      [[i !-> [-∞,+∞];
        j !-> [4,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]];
    j := 0 ->
      [[i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====>
       [i !-> [-∞,3];
        j !-> [0,0]]];
    j := j + 1 ->
      [[i !-> [-∞,+∞];
        j !-> [-∞,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [1,4]]];
    i := 0;
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[]
       ====>
       [i !-> [4,+∞];
        j !-> [4,+∞]]];
    i := i + 1; j := j + 1 ->
      [[i !-> [-∞,+∞];
        j !-> [-∞,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [1,4]]];
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1 ->
      [[i !-> [-∞,+∞];
        j !-> [0,0]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]];
    while
    i < 4
    do
    j := 0; while j < 4 do i := i + 1; j := j + 1 done; i := i - j + 1
    done ->
      [[i !-> [-∞,+∞];
        j -> [4,+∞]]
       ====>
       [i !-> [4,+∞];
        j !-> [4,+∞]]];
    while j < 4 do i := i + 1; j := j + 1 done ->
      [[i !-> [-∞,+∞];
        j !-> [-∞,+∞]]
       ====>
       [i !-> [-∞,+∞];
        j !-> [4,+∞]]]

    Final result:
    [i !-> [4,+∞];
     j !-> [4,+∞]] |}]

let%expect_test _ =
  Analyzer.run [| A; C |]
    (While (Bool true, Assign ("x", Const 0)))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 0 ->
      [[x -> [0,0]]
       ====>
       [x !-> [0,0]]];
    while true do x := 0 done ->
      [[x -> [0,0]]
       ====>
       ⊥]

    Final result:
    ⊥ |}]

let%expect_test _ =
  Analyzer.run [| A; C |]
    (Seq
       ( Assign ("x", Const 2),
         While
           ( Lt (Const 0, Var "x"),
             If
               ( Le (Var "x", Const 1),
                 Skip,
                 Assign ("x", Minus (Var "x", Const 1)) ) ) ))
    Test_common.IntervalStore.empty;
  [%expect
    {|
    Final MFG:
    x := 2 ->
      [[]
       ====>
       [x !-> [2,2]]];
    x := x - 1 ->
      [[x !-> [2,2]]
       ====>
       [x !-> [1,1]]];
    skip ->
      [[x !-> [1,1]]
       ====>
       [x !-> [1,1]]];
    x := 2; while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[]
       ====>
       ⊥];
    if x <= 1 then skip else x := x - 1 ->
      [[x !-> [1,2]]
       ====>
       [x !-> [1,1]]];
    while 0 < x do if x <= 1 then skip else x := x - 1 done ->
      [[x !-> [1,2]]
       ====>
       ⊥]

    Final result:
    ⊥ |}]

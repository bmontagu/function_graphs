(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

(** Test module for a static analyzer for a first-order purely functional
    language as described in

    Data Flow Analysis of Applicative Programs Using Minimal Function Graphs

    Neil D. Jones and Alan Mycroft

    Proceedings of the 13th ACM SIGACT-SIGPLAN symposium on Principles of
    programming languages - POPL '86

    ACM Press, 1986

    The analyzer is implemented using a fixpoint solver. *)

type fun_id
type prim
type program

val pp_program : Format.formatter -> program -> unit

module FO_MFG : functor
  (Env : sig
     type t
     type value

     val bot : t
     val is_bot : t -> bool
     val pp : Format.formatter -> t -> unit
     val get : t -> 'a -> value
     val make : ('a * value) list -> t
   end)
  (V : sig
     type t = Env.value

     val bot : t
     val is_bot : t -> bool
     val leq : t -> t -> bool
     val join : t -> t -> t
     val singleton_bool : bool -> t
     val pp : Format.formatter -> t -> unit
   end)
  (MFG : sig
     include Function_graphs.Sigs.MFG with type dom = Env.t and type codom = V.t

     val iter : (dom -> codom -> unit) -> t -> unit
   end)
  (_ : sig
     val call_prim : prim -> V.t list -> V.t
     val cond : 'a -> bool -> Env.t -> Env.t
   end)
  -> sig
  val analyze : MFG.strategy array -> program -> fun_id -> Env.t -> V.t
  val run : MFG.strategy array -> program -> fun_id -> Env.t -> unit
end

module V : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Env : sig
  type t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module MFG : Function_graphs.Sigs.MFG with type dom = Env.t and type codom = V.t

module Analyzer : sig
  val analyze : MFG.strategy array -> program -> fun_id -> Env.t -> V.t
  val run : MFG.strategy array -> program -> fun_id -> Env.t -> unit
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

module Unit : Abstract_domains.Sigs.ABSTRACT_DOMAIN with type t = unit

module UnitMap : sig
  type key = unit
  type 'a t

  val empty : 'a t
  val mem : key -> 'a t -> bool
  val find_opt : key -> 'a t -> 'a option
  val singleton : key -> 'a -> 'a t
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val to_seq : 'a t -> (key * 'a) Seq.t
end

module IntervalStore :
  Abstract_domains.Store.S
    with type key = string
     and type value = Abstract_domains.Interval.t

module CFG : sig
  module Node : sig
    type t = int

    val compare : t -> t -> int
    val equal : t -> t -> bool
    val pp : Format.formatter -> t -> unit
  end

  module NodeMap : sig
    include Map.S with type key = Node.t

    val pp :
      (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  end

  module Reg : sig
    type t = string

    val compare : t -> t -> int
    val equal : t -> t -> bool
  end

  module Instr : sig
    type op = Read | Add | Sub | Le | Lt | Eq
    type arg = R of Reg.t | V of int
    type cond = IsZero of Reg.t | IsLt of arg * arg | IsLe of arg * arg
    type t = Nop | Cond of cond | Op of op * Reg.t * arg list
  end

  module Label : sig
    type t = bool option

    val compare : t -> t -> int
    val equal : t -> t -> bool
    val pp : Format.formatter -> t -> unit
  end

  module NodeLabel : sig
    type t = Node.t * Label.t

    val compare : t -> t -> int
    val equal : t -> t -> bool
    val pp : Format.formatter -> t -> unit
  end

  module NodeLabelMap : sig
    include Map.S with type key = NodeLabel.t

    val pp :
      (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  end

  module ValueAnalysis : sig
    type instr = Instr.t
    type label = Label.t
    type value = IntervalStore.t
    type reg = Reg.t

    open Abstract_domains

    val singleton : int -> Interval.t
    val true_ : Interval.t
    val false_ : Interval.t
    val any_bool : Interval.t
    val eval_arg : Instr.arg -> value -> Interval.t
    val interval_le : Interval.t -> Interval.t -> Interval.t
    val interval_lt : Interval.t -> Interval.t -> Interval.t
    val interval_eq : Interval.t -> Interval.t -> Interval.t
    val eval_op : Instr.op -> Instr.arg list -> value -> Interval.t
    val set_arg : value -> Instr.arg -> Interval.t -> value
    val eval_cond : Instr.cond -> label -> value -> value
    val eval : instr -> label -> value -> value
    val eval_eq : reg -> Node.t -> value -> value

    val value_node_map_reduce :
      value -> reg -> int -> int -> (int -> 'a) -> ('a -> 'b -> 'b) -> 'b -> 'b
  end
end

module While : sig
  type var = string

  val compare_var : var -> var -> int
  val equal_var : var -> var -> bool

  type ('expr, 'bexpr) prog =
    | Assign of var * 'expr
    | Skip
    | Seq of ('expr, 'bexpr) prog * ('expr, 'bexpr) prog
    | If of 'bexpr * ('expr, 'bexpr) prog * ('expr, 'bexpr) prog
    | While of 'bexpr * ('expr, 'bexpr) prog

  val compare_prog :
    ('a -> 'a -> int) ->
    ('b -> 'b -> int) ->
    ('a, 'b) prog ->
    ('a, 'b) prog ->
    int

  val pp_prog :
    (Format.formatter -> 'a -> unit) ->
    (Format.formatter -> 'b -> unit) ->
    Format.formatter ->
    ('a, 'b) prog ->
    unit

  module Expr : sig
    type t =
      | Unknown
      | Const of int
      | Var of var
      | Plus of t * t
      | Minus of t * t

    val compare : t -> t -> int
    val pp : Format.formatter -> t -> unit
  end

  module BExpr : sig
    type t =
      | Unknown
      | Bool of bool
      | Le of Expr.t * Expr.t
      | Lt of Expr.t * Expr.t
      | And of t * t
      | Or of t * t

    val compare : t -> t -> int
    val pp : Format.formatter -> t -> unit
    val negate : t -> t
  end

  module ValueAnalysis : sig
    open Abstract_domains

    val eval_expr : IntervalStore.t -> Expr.t -> Interval.t

    val assign :
      IntervalStore.key -> Expr.t -> IntervalStore.t -> IntervalStore.t

    val bkw_eval_expr :
      Expr.t -> Interval.t -> IntervalStore.t -> IntervalStore.t

    val cond_true : BExpr.t -> IntervalStore.t -> IntervalStore.t
    val cond_false : BExpr.t -> IntervalStore.t -> IntervalStore.t
    val cond : BExpr.t -> bool -> IntervalStore.t -> IntervalStore.t
  end
end

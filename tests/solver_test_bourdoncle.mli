(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Test module for examples found in F. Bourdoncle's Ph.D. thesis on functional
    partitioning and minimal function graphs. *)

open Abstract_domains

module MFG :
  Function_graphs.Sigs.MFG
    with type dom = Interval.t
     and type codom = Interval.t

module S : sig
  val fix_gen :
    debug:bool ->
    MFG.strategy array ->
    ((Interval.t -> Interval.t) -> Interval.t -> Interval.t) ->
    Interval.t ->
    Interval.t * MFG.t

  val fix :
    ((Interval.t -> Interval.t) -> Interval.t -> Interval.t) ->
    Interval.t ->
    Interval.t
end

val run :
  MFG.strategy array ->
  ((Interval.t -> Interval.t) -> Interval.t -> Interval.t) ->
  Interval.t ->
  unit

val f_mc91 : (Interval.t -> Interval.t) -> Interval.t -> Interval.t
val f_mc10 : (Interval.t -> Interval.t) -> Interval.t -> Interval.t
val f_loop_bourdoncle : (Interval.t -> Interval.t) -> Interval.t -> Interval.t

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

type strategy = A | B | C

(** Functor that creates an abstract domain that abstracts a minimal function
    graph, from an abstract domain for the input values of the graph, and an
    abstract domain for the output values of the graph.

    The resulting abstract domain for function graphs is rather naive, as it
    keeps only one abstract value for the (over-approximation) of the set of
    inputs and one abstract value for the (over-approximation) of the set of
    outputs. *)
module Make : functor
  (D1 : sig
     type t

     val is_bot : t -> bool
     val leq : t -> t -> bool
     val join : t -> t -> t
     val widen : t -> t -> t
     val meet : t -> t -> t
     val pp : Format.formatter -> t -> unit
   end)
  (D2 : sig
     type t

     val bot : t
     val leq : t -> t -> bool
     val join : t -> t -> t
     val widen : t -> t -> t
     val meet : t -> t -> t
     val pp : Format.formatter -> t -> unit
   end)
  -> sig
  include
    Sigs.MFG
      with type dom = D1.t
       and type codom = D2.t
       and type strategy = strategy

  val meet : t -> t -> t
  val dom : t -> dom list
  val iter : (dom -> codom -> unit) -> t -> unit
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2024 *)

module type SET = sig
  type t

  val empty : t
  val subset : t -> t -> bool
  val union : t -> t -> t
  val inter : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module FiniteSet (S : SET) : sig
  type t = S.t
  type strategy

  val default_strategy : strategy array
  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen_gen : strategy array -> int -> t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end = struct
  type t = S.t
  type strategy = unit

  let default_strategy = [||]
  let bot = S.empty
  let leq = S.subset
  let join = S.union
  let widen_gen _ _ s1 s2 = join s1 s2
  let widen _ s1 s2 = join s1 s2
  let meet = S.inter
  let pp = S.pp
end

module Make
    (XMap : sig
      type key
      type 'a t

      val empty : 'a t
      val is_empty : 'a t -> bool
      val singleton : key -> 'a -> 'a t
      val find_opt : key -> 'a t -> 'a option
      val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
      val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

      val merge :
        (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

      val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
      val for_all : (key -> 'a -> bool) -> 'a t -> bool
      val iter : (key -> 'a -> unit) -> 'a t -> unit

      val pp :
        (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
    end)
    (YSet : SET) =
  Indexed.MakeFinite (XMap) (FiniteSet (YSet))

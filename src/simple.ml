(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

type strategy = A | B | C

(** Functor that creates an abstract domain that abstracts a minimal function
    graph, from an abstract domain for the input values of the graph, and an
    abstract domain for the output values of the graph.

    The resulting abstract domain for function graphs is rather naive, as it
    keeps only one abstract value for the (over-approximation) of the set of
    inputs and one abstract value for the (over-approximation) of the set of
    outputs. *)
module Make (D1 : sig
  type t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) (D2 : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) =
struct
  type dom = D1.t
  type codom = D2.t
  type t = (dom * codom) option
  type nonrec strategy = strategy

  let pp fmt = function
    | None -> Format.pp_print_string fmt "⊥"
    | Some (d1, d2) ->
        Format.fprintf fmt "@[<v 1>[@[%a@]@ ====>@ @[%a@]]@]" D1.pp d1 D2.pp d2

  let bot = None

  let leq o1 o2 =
    match (o1, o2) with
    | None, _ -> true
    | _, None -> false
    | Some (d1, c1), Some (d2, c2) -> D1.leq d1 d2 && D2.leq c1 c2

  let init d1 = if D1.is_bot d1 then bot else Some (d1, D2.bot)

  let apply o d1 =
    match o with
    | None -> D2.bot
    | Some (d1', d2) -> if D1.(is_bot @@ meet d1 d1') then D2.bot else d2

  let map f = function None -> None | Some (d1, _) -> Some (d1, f d1)

  let join o1 o2 =
    match (o1, o2) with
    | None, o | o, None -> o
    | Some (d11, d12), Some (d21, d22) -> Some (D1.join d11 d21, D2.join d12 d22)

  let get_strategy strategies i =
    let n = Array.length strategies in
    if 1 <= i && i <= n then strategies.(i - 1) else strategies.(n - 1)

  let default_strategy = [| A; B; C |]

  let widen_ strategy o1 o2 =
    match (o1, o2) with
    | None, o | o, None -> o
    | Some (d11, d12), Some (d21, d22) -> (
        match strategy with
        | A -> Some (D1.join d11 d21, D2.join d12 d22)
        | B -> Some (D1.widen d11 d21, D2.join d12 d22)
        | C ->
            if D1.leq d21 d11 then
              (* there is no new input *)
              Some (d11, D2.widen d12 d22)
            else
              (* there some new input *)
              Some (D1.widen d11 d21, D2.join d12 d22))

  let widen_gen strategies n l1 l2 =
    let strategy = get_strategy strategies n in
    widen_ strategy l1 l2

  let widen = widen_gen default_strategy
  let iter f = function None -> () | Some (d1, d2) -> f d1 d2
  let is_bot = function None -> true | Some _ -> false

  let meet o1 o2 =
    match (o1, o2) with
    | None, _ | _, None -> None
    | Some (d1, c1), Some (d2, c2) ->
        let d12 = D1.meet d1 d2 in
        if D1.is_bot d12 then bot else Some (d12, D2.meet c1 c2)

  let dom = function None -> [] | Some (d1, _) -> [ d1 ]
end

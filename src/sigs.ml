(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2024 *)

module type MFG = sig
  type dom
  type codom
  type t
  type strategy

  val bot : t
  val is_bot : t -> bool
  val init : dom -> t
  val leq : t -> t -> bool
  val map : (dom -> codom) -> t -> t
  val apply : t -> dom -> codom
  val join : t -> t -> t
  val meet : t -> t -> t
  val widen_gen : strategy array -> int -> t -> t -> t
  val default_strategy : strategy array
  val widen : int -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Sigs

(** Functor that creates an abstract function graph for [T => X] given a type
    with finite elements [T] and an abstract domain [X]. *)
module MakeFinite (M : sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t
  type strategy

  val default_strategy : strategy array
  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen_gen : strategy array -> int -> t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  include MFG with type t = X.t M.t and type dom = M.key and type codom = X.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
  val dom : t -> dom list
end

(** Functor that creates an abstract function graph for [(T * X) => Y)] given a
    type with finite elements [T] and an abstract function graph for [X => Y].
*)
module ExtendFinite (M : sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t
end) (Y : sig
  type t

  val bot : t
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
end) : sig
  include
    MFG
      with type t = MFG.t M.t
       and type dom = M.key * MFG.dom
       and type codom = MFG.codom
       and type strategy = MFG.strategy

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
  val apply0 : t -> M.key -> MFG.t
  val dom0 : t -> M.key list
end

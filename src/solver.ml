(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Sigs

module Naive (T : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (TMap : sig
  type key = T.t
  type 'a t

  val empty : 'a t
  val singleton : key -> 'a -> 'a t
  val mem : key -> 'a t -> bool
  val find_opt : key -> 'a t -> 'a option
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val to_seq : 'a t -> (key * 'a) Seq.t
end) (X : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (Y : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val iter : (dom -> codom -> unit) -> t -> unit
end) : sig
  val fix_gen :
    debug:bool ->
    MFG.strategy array ->
    ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) ->
    T.t ->
    X.t ->
    Y.t * MFG.t TMap.t

  val fix : ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t
end = struct
  let pp_inputs fmt t mfg =
    MFG.iter
      (fun x _y -> Format.fprintf fmt "@[@[%a@]:@ @[%a@]@]@ " T.pp t X.pp x)
      mfg

  let pp_calls fmt tmap = TMap.iter (fun t mfg -> pp_inputs fmt t mfg) tmap
  let join widen m1 m2 = TMap.union (fun t g1 g2 -> Some (widen t g1 g2)) m1 m2

  let leq m1 m2 =
    TMap.for_all
      (fun t g1 ->
        match TMap.find_opt t m2 with None -> false | Some g2 -> MFG.leq g1 g2)
      m1

  (** [solve ~debug strategies f m_init] computes an over-approximation of the
      minimal function graph for calling [f] on the inputs described by the
      graph [m_init] *)
  let solve ~debug strategies f m_init =
    (* Number of calls to the function f *)
    let nb_calls = ref 0 in

    (* Numbering of the nodes of type [T.t] that have been visited. It
       is used to approximate widening points *)
    let prio = ref TMap.empty in

    (* retrieves this numbering *)
    let get_prio t = TMap.find_opt t !prio in

    (* Initializes the numbering of a node *)
    let init_prio : T.t -> unit =
      let next_prio =
        let p = ref 0 in
        fun () ->
          let n = !p in
          incr p;
          n
      in
      fun t ->
        prio :=
          TMap.update t
            (function None -> Some (next_prio ()) | Some _ as o -> o)
            !prio
    in

    (* Records whether nodes are subject to widening *)
    let widen = ref TMap.empty in

    (* Asks whether a node is a widening node *)
    let is_widen t =
      match TMap.find_opt t !widen with None -> false | Some () -> true
    in

    (* Mark a node as a widening node *)
    let set_widen t = widen := TMap.update t (fun _ -> Some ()) !widen in

    (* [update_widen_status new_t caller_t] updates the widening
       status of [new_t], knowing that the caller node was [caller_t] *)
    let update_widen_status new_t caller_t =
      match (get_prio new_t, get_prio caller_t) with
      | Some new_p, Some caller_p -> if new_p <= caller_p then set_widen new_t
      | None, Some _ -> ()
      | _, None -> assert false
    in

    (* [func] returns the graph [m] "updated" by the functional [f],
       and the graph that describes the "new points" that have been
       asked by doing so *)
    let func m =
      (* reference that records the newly discovered calls *)
      let new_calls = ref TMap.empty in

      (* [record_calls new_t new_arg new_t_mfg] records a call on
         [new_t] and [new_arg], or does nothing if it is already
         present in [new_t_mfg] *)
      let record_calls new_t new_arg new_t_mfg =
        let call_mfg = MFG.init new_arg in
        if not @@ MFG.leq call_mfg new_t_mfg then
          new_calls :=
            TMap.update new_t
              (function
                | None -> Some call_mfg
                | Some mfg -> Some (MFG.join mfg call_mfg))
              !new_calls
      in

      (* [f_rec caller_t new_t new_arg] records the call to [new_t]
         and [new_arg] (knowing that it was called by [caller_t]), and
         returns the result provided by the current graph [m] for
         [new_t] and [new_arg] *)
      let f_rec caller_t new_t new_arg =
        update_widen_status new_t caller_t;
        let new_t_mfg =
          match TMap.find_opt new_t m with Some g -> g | None -> MFG.bot
        in
        record_calls new_t new_arg new_t_mfg;
        MFG.apply new_t_mfg new_arg
      in

      (* [m] updated with a call to [f] *)
      let updated_m =
        TMap.mapi
          (fun t mfg ->
            init_prio t;
            MFG.map
              (fun arg ->
                incr nb_calls;
                f (f_rec t) t arg)
              mfg)
          m
      in

      (updated_m, !new_calls)
    in

    (* [lfp m_init func m] computes an over-approximation of the least
       fixed point of the function [func] that is above [m_init]. The
       function [func] produces two results: the "updated" results for
       the already known questions, and the "new questions". The call
       to [lfp] returns three results: the over-approximation of the
       least fixed point, the total number of iterations, and the
       number of iterations per input of type [T.t]. *)
    let lfp m_init func =
      (* total number of iterations *)
      let nb_iter = ref 0 in
      (* number of iterations per input of type [T.t] *)
      let iterations = ref TMap.empty in
      (* retrieves the current iteration number for a given [t] *)
      let get_iterations t =
        match TMap.find_opt t !iterations with None -> 0 | Some n -> n
      in
      (* increases the iteration number for a given [t] *)
      let incr_iterations t =
        iterations :=
          TMap.update t
            (function None -> Some 1 | Some n -> Some (n + 1))
            !iterations
      in
      (* the iteration loop *)
      let rec loop m =
        let updated_m, new_points = func m in
        let no_new_called_t =
          TMap.for_all (fun t _ -> TMap.mem t m) new_points
        in
        let new_m =
          (* make sure we stay above the initial iteration point, following
             Nicolas Halbwachs and Julien Henry. 2012.
             “When the Decreasing Sequence Fails.”
             In: Static Analysis. Springer Berlin Heidelberg, 198–213.
          *)
          join (fun _ mfg1 mfg2 -> MFG.join mfg1 mfg2) m_init
          @@ join
               (fun t mfg1 mfg2 ->
                 MFG.widen_gen strategies (get_iterations t) mfg1 mfg2)
               updated_m new_points
        in
        if leq new_m m then m
        else
          let new_m =
            join
              (fun t mfg1 mfg2 ->
                if no_new_called_t && is_widen t then
                  MFG.widen_gen strategies (get_iterations t) mfg1 mfg2
                else mfg2)
              m new_m
          in
          TMap.iter
            (fun t new_mfg ->
              match TMap.find_opt t m with
              | None -> incr_iterations t
              | Some mfg ->
                  if no_new_called_t && (not @@ MFG.leq new_mfg mfg) then
                    incr_iterations t)
            new_m;
          if debug then
            Format.printf
              "Iteration #%i:@.Initial graph:@.@[<v>%a@]@.New \
               calls:@.@[<v>%a@]@.New graph:@.@[<v>%a@]@.@."
              !nb_iter (TMap.pp MFG.pp) m pp_calls new_points (TMap.pp MFG.pp)
              new_m;
          incr nb_iter;
          loop new_m
      in
      let res = loop TMap.empty in
      (res, !nb_iter, !iterations)
    in
    (* initialise the priorities *)
    TMap.iter (fun t _ -> init_prio t) m_init;
    (* compute the fixed point *)
    let m, nb_iter, iterations = lfp m_init func in
    (m, nb_iter, !nb_calls, iterations, !prio, !widen)

  (* print the number of iterations per point *)
  let pp_iterations fmt m =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (k, i) ->
           fprintf fmt "@[%a@]: %i" T.pp k i))
      (TMap.to_seq m)

  (* print the widening points *)
  let pp_widen fmt m =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq
         ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
         (fun fmt (k, ()) -> T.pp fmt k))
      (TMap.to_seq m)

  (* Fixpoint solver that is parameterised by the strategy. It returns
     the final result and an abstraction of the minimal function
     graph *)
  let fix_gen ~debug strategies (f : (T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t)
      (t_start : T.t) (start : X.t) : Y.t * MFG.t TMap.t =
    let m, nb_iter, nb_calls, iterations, priorities, widen =
      solve ~debug strategies f (TMap.singleton t_start @@ MFG.init start)
    in
    let result_mfg = Option.get @@ TMap.find_opt t_start m in
    let result = MFG.apply result_mfg start in
    if debug then
      Format.printf
        "Number of iterations: %i@.Number of calls: %i@.Number of iterations \
         per point:@.@[<v>%a@]@.Node numbering:@.@[<v>%a@]@.Widening \
         points:@.@[<v>%a@]@.@.Full graph:@.@[<v>%a@]@.Final \
         MFG:@.@[<v>%a@]@.@.Final result:@.@[<v>%a@]@."
        nb_iter nb_calls pp_iterations iterations pp_iterations priorities
        pp_widen widen (TMap.pp MFG.pp) m MFG.pp result_mfg Y.pp result;
    (result, m)

  (* Fixpoint solver *)
  let fix (f : (T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) (t_start : T.t)
      (start : X.t) : Y.t =
    fst @@ fix_gen ~debug:false MFG.default_strategy f t_start start
end

module Simple (G : MFG) = struct
  type fun_t = G.dom -> G.codom

  let alpha (next : fun_t -> fun_t) (strategies : G.strategy array) (n : int)
      (g : G.t) : G.t =
    let calls : G.t ref = ref G.bot in
    let current : fun_t =
     fun (x : G.dom) : G.codom ->
      calls := G.join (G.init x) !calls;
      G.apply g x
    in
    let new_g = G.map (next current) g in
    G.widen_gen strategies n new_g !calls

  let lfp ~debug (strategies : G.strategy array) (init : G.t)
      (f : G.strategy array -> int -> G.t -> G.t) : G.t * int =
    let rec iter n g =
      let new_g = G.join init (f strategies n g) in
      if G.leq new_g g then (g, n)
      else (
        if debug then
          Format.printf
            "Iteration #%i:@.Initial graph:@.@[<v>%a@]@.New \
             graph:@.@[<v>%a@]@.@."
            n G.pp g G.pp new_g;
        iter (n + 1) (G.widen_gen strategies n g new_g))
    in
    iter 0 G.bot

  let fix_gen ~debug (strategies : G.strategy array) (f : fun_t -> fun_t)
      (x : G.dom) : G.codom * G.t =
    let g_init = G.init x in
    let g, nb_iter = lfp ~debug strategies g_init (alpha f) in
    let res = G.apply g x in
    if debug then
      Format.printf "Number of iterations: %i@.@.Final MFG:@.@[<v>%a@]@."
        nb_iter G.pp g;
    (res, g)

  let fix (f : fun_t -> fun_t) (x : G.dom) : G.codom =
    let g_init = G.init x in
    let g = fst @@ lfp ~debug:false G.default_strategy g_init (alpha f) in
    G.apply g x
end

module Simple2 (T : sig
  type t
end) (M : sig
  type key = T.t
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
end) (X : sig
  type t
end) (Y : sig
  type t

  val bot : t
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
end) =
struct
  module G = Indexed.ExtendFinite (M) (X) (Y) (MFG)
  module Solver = Simple (G)

  type fun_t = T.t -> X.t -> Y.t
  type fun2_t = T.t * X.t -> Y.t

  let transform (f : fun_t -> fun_t) : fun2_t -> fun2_t =
   fun h (t, x) -> f (fun t0 x0 -> h (t0, x0)) t x

  let fix_gen ~debug (strategies : MFG.strategy array) (f : fun_t -> fun_t)
      (t : T.t) (x : X.t) : Y.t * G.t =
    let res, g = Solver.fix_gen ~debug strategies (transform f) (t, x) in
    let gt = G.apply0 g t in
    if debug then Format.printf "Full graph:@.@[<v>%a@]@." MFG.pp gt;
    (res, g)

  let fix (f : fun_t -> fun_t) (t : T.t) (x : X.t) : Y.t =
    Solver.fix (transform f) (t, x)
end

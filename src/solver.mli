(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Sigs

module Naive (T : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (TMap : sig
  type key = T.t
  type 'a t

  val empty : 'a t
  val singleton : key -> 'a -> 'a t
  val mem : key -> 'a t -> bool
  val find_opt : key -> 'a t -> 'a option
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val to_seq : 'a t -> (key * 'a) Seq.t
end) (X : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (Y : sig
  type t

  val pp : Format.formatter -> t -> unit
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val iter : (dom -> codom -> unit) -> t -> unit
end) : sig
  val fix_gen :
    debug:bool ->
    MFG.strategy array ->
    ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) ->
    T.t ->
    X.t ->
    Y.t * MFG.t TMap.t
  (** Fixpoint solver that is parameterised by the strategy. It returns the
      final result and an abstraction of the minimal function graph *)

  val fix : ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t
  (** Fixpoint solver *)
end

module Simple (G : MFG) : sig
  val fix_gen :
    debug:bool ->
    G.strategy array ->
    ((G.dom -> G.codom) -> G.dom -> G.codom) ->
    G.dom ->
    G.codom * G.t

  val fix : ((G.dom -> G.codom) -> G.dom -> G.codom) -> G.dom -> G.codom
end

module Simple2 (T : sig
  type t
end) (TMap : sig
  type key = T.t
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
end) (X : sig
  type t
end) (Y : sig
  type t

  val bot : t
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
end) : sig
  val fix_gen :
    debug:bool ->
    MFG.strategy array ->
    ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) ->
    T.t ->
    X.t ->
    Y.t * MFG.t TMap.t

  val fix : ((T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t) -> T.t -> X.t -> Y.t
end

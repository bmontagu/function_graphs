(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

open Sigs

(** Functor that creates an abstract function graph for [T => X] given a type
    with finite elements [T] and an abstract domain [X]. *)
module MakeFinite (M : sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t
  type strategy

  val default_strategy : strategy array
  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen_gen : strategy array -> int -> t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  include MFG with type t = X.t M.t and type dom = M.key and type codom = X.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
  val dom : t -> dom list
end = struct
  type t = X.t M.t
  type dom = M.key
  type codom = X.t
  type strategy = X.strategy

  let default_strategy = X.default_strategy
  let bot = M.empty
  let is_bot = M.is_empty

  let leq m1 m2 =
    M.for_all
      (fun k x1 ->
        match M.find_opt k m2 with None -> false | Some x2 -> X.leq x1 x2)
      m1

  let join_gen join (m1 : t) (m2 : t) =
    M.union (fun _ x1 x2 -> Some (join x1 x2)) m1 m2

  let join (m1 : t) (m2 : t) = join_gen X.join m1 m2

  let dom_le (m1 : t) (m2 : t) =
    M.for_all
      (fun t _ -> match M.find_opt t m2 with None -> false | Some _ -> true)
      m1

  let widen_gen strategies n (m1 : t) (m2 : t) =
    if dom_le m2 m1 then join_gen (X.widen_gen strategies n) m1 m2
    else join m1 m2

  let widen n m1 m2 =
    if dom_le m2 m1 then join_gen (X.widen n) m1 m2 else join m1 m2

  let init k = M.singleton k X.bot
  let map f m = M.mapi (fun k _x -> f k) m
  let apply m k = match M.find_opt k m with None -> X.bot | Some x -> x
  let iter = M.iter
  let pp fmt m = M.pp X.pp fmt m
  let dom m = M.fold (fun k _ acc -> k :: acc) m []

  let meet m1 m2 =
    M.merge
      (fun _ o1 o2 ->
        match (o1, o2) with
        | None, _ | _, None -> None
        | Some x1, Some x2 -> Some (X.meet x1 x2))
      m1 m2
end

(** Functor that creates an abstract function graph for [(T * X) => Y)] given a
    type with finite elements [T] and an abstract function graph for [X => Y].
*)
module ExtendFinite (M : sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t
end) (Y : sig
  type t

  val bot : t
end) (MFG : sig
  include MFG with type dom = X.t and type codom = Y.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
end) : sig
  include
    MFG
      with type t = MFG.t M.t
       and type dom = M.key * MFG.dom
       and type codom = MFG.codom
       and type strategy = MFG.strategy

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
  val apply0 : t -> M.key -> MFG.t
  val dom0 : t -> M.key list
end = struct
  type t = MFG.t M.t
  type dom = M.key * MFG.dom
  type codom = MFG.codom
  type strategy = MFG.strategy

  let default_strategy = MFG.default_strategy
  let bot : t = M.empty

  let leq (g1 : t) (g2 : t) =
    M.for_all
      (fun t mfg1 ->
        match M.find_opt t g2 with
        | None -> false
        | Some mfg2 -> MFG.leq mfg1 mfg2)
      g1

  let join_gen join (g1 : t) (g2 : t) =
    M.union (fun _ mfg1 mfg2 -> Some (join mfg1 mfg2)) g1 g2

  let join (g1 : t) (g2 : t) = join_gen MFG.join g1 g2

  let dom_le (g1 : t) (g2 : t) =
    M.for_all
      (fun t _ -> match M.find_opt t g2 with None -> false | Some _ -> true)
      g1

  let widen_gen strategies n (g1 : t) (g2 : t) =
    if dom_le g2 g1 then join_gen (MFG.widen_gen strategies n) g1 g2
    else join g1 g2

  let widen n g1 g2 = widen_gen default_strategy n g1 g2
  let init ((t, x) : M.key * X.t) : t = M.singleton t (MFG.init x)

  let map (f : M.key * X.t -> Y.t) (g : t) : t =
    M.mapi (fun t mfg -> MFG.map (fun x -> f (t, x)) mfg) g

  let apply (g : t) ((t, x) : M.key * X.t) : Y.t =
    match M.find_opt t g with None -> Y.bot | Some mfg -> MFG.apply mfg x

  let apply0 (g : t) (t : M.key) : MFG.t =
    match M.find_opt t g with None -> MFG.bot | Some mfg -> mfg

  let pp fmt (g : t) = M.pp MFG.pp fmt g

  let iter (f : M.key * X.t -> Y.t -> unit) (g : t) : unit =
    M.iter (fun t mfg -> MFG.iter (fun x -> f (t, x)) mfg) g

  let is_bot = M.is_empty

  let meet (g1 : t) (g2 : t) : t =
    M.merge
      (fun _t o1 o2 ->
        match (o1, o2) with
        | None, _ | _, None -> None
        | Some x1, Some x2 ->
            let x12 = MFG.meet x1 x2 in
            if MFG.is_bot x12 then None else Some x12)
      g1 g2

  let dom0 m = M.fold (fun k _ acc -> k :: acc) m []
end

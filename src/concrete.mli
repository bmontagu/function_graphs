(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2024 *)

(** Functor that creates a concrete function graph, i.e., a (finite) partial map
    whose values are (finite) sets *)
module Make (XMap : sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val mapi : (key -> 'a -> 'a) -> 'a t -> 'a t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (YSet : sig
  type t

  val empty : t
  val subset : t -> t -> bool
  val union : t -> t -> t
  val inter : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  include Sigs.MFG with type dom = XMap.key and type codom = YSet.t

  val meet : t -> t -> t
  val iter : (dom -> codom -> unit) -> t -> unit
  val dom : t -> dom list
end

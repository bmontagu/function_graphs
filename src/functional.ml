(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023-2024 *)

(** Tests whether a list is empty *)
let list_is_empty = function [] -> true | _ -> false

type strategy = A | B | C

(** Functor that creates an abstract domain that abstracts a minimal function
    graph, from an abstract domain for the input values of the graph, and an
    abstract domain for the output values of the graph.

    The resulting abstract domain for function graphs is "functional" in the
    sense of F. Bourdoncle [1]. It is implemented as an association list, that
    maps elements of the domain to elements of the codomain. The meaning is that
    of a function that maps an element [x] to the meet of all the elements of
    the codomain that are associated to the minimal elements of the domain that
    covers [x], i.e., that intersects with [x].

    [1] Bourdoncle, F. Abstract Interpretation by Dynamic Partitioning J. Funct.
    Program., 1992, 2, 407-423 *)
module Make (D1 : sig
  type t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) (D2 : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) =
struct
  type dom = D1.t
  type codom = D2.t
  type t = (dom * codom) list
  type nonrec strategy = strategy

  let bot = []
  let is_bot = list_is_empty

  let pp fmt l =
    let open Format in
    if is_bot l then pp_print_string fmt "⊥"
    else
      fprintf fmt "@[<v 1>[%a]@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (d1, d2) ->
             fprintf fmt "@[@[%a@]@ ====>@ @[%a@]@]" D1.pp d1 D2.pp d2))
        l

  (** [diag l d1 d1' c] is the intersection of [c] with the intersection of all
      the [b] such that [(a,b)] belongs to [l] and [d1 <= a <= d1'] *)
  let diag l d1 d1' c =
    List.fold_left
      (fun acc (d'', c'') ->
        if D1.leq d1 d'' && D1.leq d'' d1' then D2.meet acc c'' else acc)
      c l

  (** [safe_value l d] computes the safe value of [d] in [l] *)
  let safe_value l d =
    List.fold_left
      (fun acc (d', c') ->
        let d1 = D1.meet d' d in
        if (not @@ D1.leq d' d) && (not @@ D1.is_bot d1) then
          let c'' = diag l d1 d' c' in
          D2.join c'' acc
        else acc)
      D2.bot l

  (** [is_safe l' d c] tells whether [c] is above the safe value of [d] in [l']
  *)
  let is_safe l' d c =
    List.for_all
      (fun (d', c') ->
        D1.leq d' d
        ||
        let d1 = D1.meet d' d in
        D1.is_bot d1
        ||
        let c'' = diag l' d1 d' c' in
        D2.leq c'' c)
      l'

  let is_present l d0 =
    List.exists (fun (d, _c) -> D1.leq d d0 && D1.leq d0 d) l

  (** [leq l1 l2] tests the inclusion of [l1] in [l2] *)
  let leq l1 l2 =
    List.for_all
      (fun (d1, c1) ->
        List.exists
          (fun (d2, c2) -> D1.leq d1 d2 && D1.leq d2 d1 && D2.leq c1 c2)
          l2)
      l1
    && List.for_all (fun (d2, c2) -> is_present l1 d2 || is_safe l1 d2 c2) l2

  (** the [init] function, that creates an abstract value with a key and an
      empty codomain *)
  let init d1 = if D1.is_bot d1 then [] else [ (d1, D2.bot) ]

  let opt_join join o d = match o with None -> d | Some d' -> join d' d

  (** [joins1 l] computes the join of the [a] such that there exists a [b] such
      that [(a,b)] belongs to [l] *)
  let joins1 l =
    List.fold_left (fun oacc (d, _c) -> Some (opt_join D1.join oacc d)) None l

  (** [apply l d1] is the abstract application of [l] to [d1] *)
  let apply l d1 =
    List.fold_left
      (fun acc (d, c) ->
        let m = D1.meet d d1 in
        if D1.is_bot m then acc else D2.join acc @@ diag l m d c)
      D2.bot l

  let map f l = List.map (fun (d1, _) -> (d1, f d1)) l

  let rec find_elt a = function
    | [] -> raise Not_found
    | (a1, b1) :: l -> if D1.leq a a1 && D1.leq a1 a then b1 else find_elt a l

  (** [select_present_new ~wrt:l1 l2] sorts the entries of [l2] with respect to
      those of [l1]. More precisely, it returns the entries of [l2] that already
      have a corresponding entry in [l1], and the entries of [l2] that are new
  *)
  let select_present_new ~wrt:l1 l2 =
    List.partition (fun (d2, _c2) -> is_present l1 d2) l2

  let rec insert ((a, b) as p) = function
    | [] -> [ (a, b) ]
    | ((a', b') as p') :: l ->
        if D1.leq a a' && D1.leq a' a then (a', D2.meet b b') :: l
        else p' :: insert p l

  let insert_all lnew l = List.fold_left (fun acc p -> insert p acc) l lnew

  (** normalize the entries of [l]:
      - two entries [(a1,b1)] and [(a2,b2)] with [a1] equivalent to [a2] are
        replaced with a single entry [(a1, meet b1 b2)]
      - if two entries [(a1,b1)] and [(a2,b2)] are present and either not
        comparable or equivalent, then the entry [(meet a1 a2, meet b1 b2)] is
        also added, provided [(meet a1 a2)] is not empty.

      Remark: this produces a semantically equivalent abstract value, but it is
      in general not comparable for the inclusion test.

      Warning: this could break the ultimate stationarity of widening. *)
  let rec normalize l =
    let l' =
      List.fold_left
        (fun acc (a1, b1) ->
          List.fold_left
            (fun acc (a2, b2) ->
              let a12 = D1.meet a1 a2 in
              if D1.is_bot a12 || is_present l a12 then acc
              else
                let b12 =
                  if D1.leq a1 a2 then
                    if D1.leq a2 a1 then
                      (* a1 and a2 are equivalent *)
                      D2.meet b1 b2
                    else (* a2 is strictly smaller than a1 *) b1
                  else if D1.leq a2 a1 then
                    (* a2 is strictly smaller than a2 *)
                    b2
                  else (* a1 and a2 are not comparable *) D2.meet b1 b2
                in
                insert (a12, b12) acc)
            acc l)
        [] l
    in
    match l' with [] -> l | _ -> normalize (insert_all l' l)

  (** normalization is disabled by default *)
  let normalize =
    if false (* set to [true] to enable normalization *) then normalize
    else Fun.id

  (** sort by smaller entries first *)
  let rec sort l =
    let rec select_min = function
      | [] -> assert false
      | [ p ] -> (p, [])
      | ((a1, _) as p1) :: l ->
          let ((a2, _) as p2), rem = select_min l in
          if D1.leq a1 a2 then (p1, p2 :: rem) else (p2, p1 :: rem)
    in
    match l with
    | [] | [ _ ] -> l
    | _ ->
        let p, rem = select_min l in
        p :: sort rem

  let join_gen ~safe join l1 l2 =
    let l1 = normalize l1 and l2 = normalize l2 in
    let l1_and_l2, l1_not_l2 = select_present_new ~wrt:l2 l1
    and l2_and_l1, l2_not_l1 = select_present_new ~wrt:l1 l2 in
    let l1_not_l2' =
      List.map (fun (a1, b1) -> (a1, join b1 (safe_value l2 a1))) l1_not_l2
    and l2_not_l1' =
      List.map
        (fun (a2, b2) -> (a2, if safe then join (safe_value l1 a2) b2 else b2))
        l2_not_l1
    and l12' =
      List.map
        (fun (a1, b1) ->
          let b2 = find_elt a1 l2_and_l1 in
          (a1, join b1 b2))
        l1_and_l2
    in
    sort @@ normalize @@ l1_not_l2' @ l2_not_l1' @ l12'

  (** [join l1 l2] is the abstract union of [l1] and [l2] *)
  let join l1 l2 = join_gen ~safe:true D2.join l1 l2

  let rec find_above d = function
    | [] -> None
    | (d', _) :: l -> if D1.leq d d' then Some d' else find_above d l

  let new_point strategy d c orig =
    match strategy with
    | A -> (d, c)
    | B ->
        let new_d =
          match find_above d orig with
          | None -> opt_join D1.join (joins1 orig) d
          | Some d' -> d'
        in
        (new_d, c)
    | C ->
        let new_d =
          match find_above d orig with
          | None -> opt_join D1.widen (joins1 orig) d
          | Some d' -> d'
        in
        (new_d, c)

  let rec add_point d c = function
    | [] -> [ (d, c) ]
    | ((d', c') as p) :: l ->
        if D1.leq d' d && D1.leq d d' then (d', D2.join c c') :: l
        else p :: add_point d c l

  let add_new_points strategy orig new_points acc =
    List.fold_left
      (fun acc (d, c) ->
        let d', c' = new_point strategy d c orig in
        add_point d' c' acc)
      acc new_points

  let join2 ~no_new_points strategy =
    if no_new_points then match strategy with A | B -> D2.join | C -> D2.widen
    else D2.join

  let widen_ strategy l1 l2 =
    let present_points, new_points = select_present_new ~wrt:l1 l2 in
    let new_points =
      List.map (fun (a, b) -> (a, D2.join b (safe_value l2 a)))
      @@ add_new_points strategy l1 new_points []
    in
    let l2' = present_points @ new_points in
    let safe =
      (* all strategies must end with [C] *)
      match strategy with
      | A | B -> false
      | C -> true
    in
    join_gen ~safe
      (join2 ~no_new_points:(list_is_empty new_points) strategy)
      l1 l2'

  let get_strategy strategies i =
    let n = Array.length strategies in
    if 1 <= i && i <= n then strategies.(i - 1) else strategies.(n - 1)

  let widen_gen strategies n l1 l2 =
    let strategy = get_strategy strategies n in
    widen_ strategy l1 l2

  let default_strategy =
    ignore B;
    [| A; C; A; A; A; C |]

  let widen = widen_gen default_strategy
  let iter f l = List.iter (fun (d1, d2) -> f d1 d2) l
  let dom l = List.map fst l

  let meet l1 l2 =
    let l12 =
      List.fold_left
        (fun acc (d1, c1) ->
          List.fold_left
            (fun acc (d2, c2) ->
              let d12 = D1.meet d1 d2 in
              if D1.is_bot d12 then acc
              else
                let c12 = D2.meet c1 c2 in
                insert (d12, c12) acc)
            acc l2)
        [] l1
    in
    sort @@ normalize l12
end

module Test = struct
  open Abstract_domains
  module MFG = Make (Interval) (Interval)

  let print_mfg mfg = MFG.pp Format.std_formatter mfg
  let print_interval i = Interval.pp Format.std_formatter i
  let interval i j = Interval.make (IntBar.Int i) (IntBar.Int j)
  let int13 = interval 1 3
  let int05 = interval 0 5
  let int16 = interval 1 6
  let int47 = interval 4 7
  let int89 = interval 8 9

  let%expect_test _ =
    print_mfg MFG.bot;
    [%expect {| ⊥ |}]

  let%expect_test _ =
    print_mfg @@ MFG.init int13;
    [%expect {|
      [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(map (fun _ -> int47) @@ init int13));
    [%expect {|
      [[1,3] ====> [4,7]] |}]

  (* Tests about join *)

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int13) (init int13)));
    [%expect {|
      [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int13) (init int05)));
    [%expect {|
      [[1,3] ====> ⊥;
       [0,5] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int47) (init int05)));
    [%expect {|
      [[0,5] ====> ⊥;
       [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int47) @@ join (init int05) (init int89)));
    [%expect
      {|
      [[0,5] ====> ⊥;
       [8,9] ====> ⊥;
       [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (init int47)
         @@ join (init int05)
         @@ join (init int89) (init int13)));
    [%expect
      {|
      [[1,3] ====> ⊥;
       [0,5] ====> ⊥;
       [8,9] ====> ⊥;
       [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (map (fun _ -> int89) @@ init int47)
         @@ join (map (fun _ -> int47) @@ init int05)
         @@ join
              (map (fun _ -> int05) @@ init int89)
              (map (fun _ -> int89) @@ init int13)));
    [%expect
      {|
      [[1,3] ====> [4,9];
       [0,5] ====> [4,9];
       [8,9] ====> [0,5];
       [4,7] ====> [4,9]]
    |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (map (fun _ -> int13) @@ init int13)
         @@ map (fun _ -> int05)
         @@ init int05));
    [%expect {|
      [[1,3] ====> [0,5];
       [0,5] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (map (fun _ -> int05) @@ init int05)
         @@ map (fun _ -> int13)
         @@ init int13));
    [%expect {|
      [[1,3] ====> [0,5];
       [0,5] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(join [ (int13, int13); (int05, int05) ] [ (int13, int89) ]));
    [%expect {|
      [[1,3] ====> [1,9];
       [0,5] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(join [ (int13, int13); (int05, int05) ] [ (int05, int89) ]));
    [%expect {|
      [[1,3] ====> [1,9];
       [0,5] ====> [0,9]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(join [ (int13, int13); (int05, int05) ] [ (int16, int89) ]));
    [%expect
      {|
      [[1,3] ====> [1,9];
       [0,5] ====> [0,9];
       [1,6] ====> [0,9]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join
           [ (int13, int13); (int05, int05) ]
           [ (int13, int47); (int05, int89) ]));
    [%expect {|
      [[1,3] ====> [1,7];
       [0,5] ====> [0,9]] |}]

  (* Tests about widening *)

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 10 (init int13) (init int13)));
    [%expect {|
      [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 10 (init int13) (init int05)));
    [%expect {|
      [[1,3] ====> ⊥;
       [-∞,+∞] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 10 (init int47) (init int05)));
    [%expect {|
      [[4,7] ====> ⊥;
       [-∞,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 10 (init int13) (init int89)));
    [%expect {|
      [[1,3] ====> ⊥;
       [1,+∞] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 10 (init int47) @@ join (init int05) (init int89)));
    [%expect
      {|
      [[4,7] ====> ⊥;
       [4,+∞] ====> ⊥;
       [-∞,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 10 (init int47)
         @@ join (init int05)
         @@ join (init int89) (init int13)));
    [%expect
      {|
      [[4,7] ====> ⊥;
       [4,+∞] ====> ⊥;
       [-∞,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 10 (join (init int47) (init int05))
         @@ join (init int89) (init int13)));
    [%expect
      {|
        [[0,5] ====> ⊥;
         [4,7] ====> ⊥;
         [0,+∞] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 10 (map (fun _ -> int05) @@ init int47)
         @@ join (map (fun _ -> int13) @@ init int05)
         @@ join
              (map (fun _ -> int05) @@ init int89)
              (map (fun _ -> int89) @@ init int13)));
    [%expect
      {|
        [[4,7] ====> [0,9];
         [4,+∞] ====> [0,5];
         [-∞,7] ====> [1,9]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 10
           (join (map (fun _ -> int13) @@ init int05)
           @@ join
                (map (fun _ -> int05) @@ init int89)
                (map (fun _ -> int89) @@ init int13))
           (map (fun _ -> int05) @@ init int47)));
    [%expect
      {|
        [[1,3] ====> [0,9];
         [0,5] ====> [0,5];
         [8,9] ====> [0,5];
         [0,9] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(widen 10 [ (int13, int13); (int05, int05) ] [ (int13, int89) ]));
    [%expect {|
      [[1,3] ====> [1,+∞];
       [0,5] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(widen 10 [ (int13, int13); (int05, int05) ] [ (int05, int89) ]));
    [%expect {|
      [[1,3] ====> [1,+∞];
       [0,5] ====> [0,+∞]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(widen 10 [ (int13, int13); (int05, int05) ] [ (int16, int89) ]));
    [%expect
      {|
      [[1,3] ====> [1,9];
       [0,5] ====> [0,9];
       [0,+∞] ====> [8,9]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 10
           [ (int13, int89); (int05, int05) ]
           [ (int13, int13); (int05, int16) ]));
    [%expect {|
      [[1,3] ====> [-∞,9];
       [0,5] ====> [0,+∞]] |}]

  (* tests from F. Bourdoncle's Ph.D thesis *)
  let bourdoncle_loop =
    [
      (interval 0 0, interval 100 100);
      ( Interval.make (IntBar.Int 0) IntBar.PInf,
        Interval.make (IntBar.Int 100) IntBar.PInf );
      (interval 1 1, interval 100 100);
      (interval 1 100, interval 100 100);
    ]

  let%expect_test _ =
    print_mfg bourdoncle_loop;
    [%expect
      {|
      [[0,0] ====> [100,100];
       [0,+∞] ====> [100,+∞];
       [1,1] ====> [100,100];
       [1,100] ====> [100,100]] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_loop
    @@ Interval.make IntBar.MInf (IntBar.Int (-1));
    [%expect {| ⊥ |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_loop @@ interval 0 0;
    [%expect {| [100,100] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_loop @@ interval 1 100;
    [%expect {| [100,100] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_loop @@ interval 0 100;
    [%expect {| [100,+∞] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_loop
    @@ Interval.make (IntBar.Int 101) IntBar.PInf;
    [%expect {| [100,+∞] |}]

  let bourdoncle_mc91 =
    [
      (interval 0 50, interval 91 91);
      ( Interval.make (IntBar.Int 0) IntBar.PInf,
        Interval.make (IntBar.Int 91) IntBar.PInf );
      (interval 11 111, interval 91 101);
      (interval 11 61, interval 91 91);
      (interval 22 72, interval 91 91);
      (interval 22 111, interval 91 101);
      (interval 91 101, interval 91 91);
    ]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91
    @@ Interval.make IntBar.MInf (IntBar.Int (-1));
    [%expect {| ⊥ |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 0 50;
    [%expect {| [91,91] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 51 72;
    [%expect {| [91,91] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 0 51;
    [%expect {| [91,+∞] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 0 72;
    [%expect {| [91,+∞] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 73 90;
    [%expect {| [91,101] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 91 101;
    [%expect {| [91,91] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91 @@ interval 102 111;
    [%expect {| [91,101] |}]

  let%expect_test _ =
    print_interval @@ MFG.apply bourdoncle_mc91
    @@ Interval.make (IntBar.Int 112) IntBar.PInf;
    [%expect {| [91,+∞] |}]

  let%expect_test _ =
    print_mfg @@ [ (interval 0 50, Interval.bot) ];
    [%expect {|
      [[0,50] ====> ⊥] |}]

  let%expect_test _ =
    print_mfg
    @@ MFG.widen 1
         [ (interval 0 50, Interval.bot) ]
         [ (interval 11 61, Interval.bot) ];
    [%expect {|
      [[11,61] ====> ⊥;
       [0,50] ====> ⊥] |}]

  let%expect_test _ =
    print_mfg
    @@ MFG.widen 2
         (MFG.widen 2
            [
              (interval 0 50, Interval.bot);
              (Interval.make (IntBar.Int 0) IntBar.PInf, Interval.bot);
            ]
            [ (interval 11 111, Interval.bot) ])
         [
           (interval 0 50, Interval.bot);
           ( Interval.make (IntBar.Int 0) IntBar.PInf,
             Interval.make (IntBar.Int 91) IntBar.PInf );
         ];
    [%expect {|
        [[0,50] ====> ⊥;
         [0,+∞] ====> [91,+∞]] |}]

  let print_test msg b =
    Format.fprintf Format.std_formatter "%s: %s@." msg (if b then "✔" else "̱✘")

  let print_interval msg i =
    Format.fprintf Format.std_formatter "%s = %a@." msg Interval.pp i

  let%expect_test _ =
    (* counter-example for the lub property of join *)
    let a1 = interval 0 4 and a1' = interval 0 3 and a2 = interval 1 5 in
    let a0 = Interval.meet a1' a2
    and b1 = interval 6 8
    and b1' = interval 7 8 in
    let b2 = b1' in
    print_test "a1' <= a1" (Interval.leq a1' a1);
    print_test "a1 </= a1'" (not @@ Interval.leq a1 a1');
    print_test "a1' </= a2" (not @@ Interval.leq a1' a2);
    print_test "a2 </= a1'" (not @@ Interval.leq a2 a1');
    print_test "a1 </= a2" (not @@ Interval.leq a1 a2);
    print_test "a2 </= a1" (not @@ Interval.leq a2 a1);
    print_test "meet a2 a1 </= a1'" (not @@ Interval.(leq (meet a2 a1) a1'));
    print_test "meet a1' a </= bot" (not @@ Interval.(leq (meet a1' a0) bot));
    print_test "meet a2 a </= bot" (not @@ Interval.(leq (meet a2 a0) bot));
    print_test "a0 <= a1'" (Interval.leq a0 a1');
    print_test "a1' </= a" (not @@ Interval.leq a1' a0);
    print_test "a0 <= a2" (Interval.leq a0 a2);
    print_test "a2 </= a" (not @@ Interval.leq a2 a0);
    print_test "b1' <= b1" (Interval.leq b1' b1);
    print_test "b2 <= b1'" (Interval.leq b2 b1');
    print_test "b1 </= b1'" (not @@ Interval.leq b1 b1');
    let l1 = [ (a1, b1); (a1', b1') ] and l2 = [ (a2, b2) ] in
    let l12 = MFG.join l1 l2 in
    let l = (a0, Interval.join b1' b2) :: l12 in
    Format.fprintf Format.std_formatter "join l1 l2 = @[%a@]@." MFG.pp l12;
    print_interval "safe_value l1 a2" (MFG.safe_value l1 a2);
    print_interval "safe_value l2 a1" (MFG.safe_value l2 a1);
    print_interval "safe_value l2 a1'" (MFG.safe_value l2 a1');
    print_interval "safe_value l1 a0" (MFG.safe_value l1 a0);
    print_interval "safe_value l2 a0" (MFG.safe_value l2 a0);
    print_interval "safe_value l12 a0" (MFG.safe_value l12 a0);
    print_test "l1 <= join l1 l2" (MFG.leq l1 l12);
    print_test "l2 <= join l1 l2" (MFG.leq l2 l12);
    print_test "l1 <= l" (MFG.leq l1 l);
    print_test "l2 <= l" (MFG.leq l2 l);
    print_test "join l1 l2 </= l" (not @@ MFG.leq l12 l);
    print_test "l </= join l1 l2" (not @@ MFG.leq l l12);
    [%expect
      {|
      a1' <= a1: ✔
      a1 </= a1': ✔
      a1' </= a2: ✔
      a2 </= a1': ✔
      a1 </= a2: ✔
      a2 </= a1: ✔
      meet a2 a1 </= a1': ✔
      meet a1' a </= bot: ✔
      meet a2 a </= bot: ✔
      a0 <= a1': ✔
      a1' </= a: ✔
      a0 <= a2: ✔
      a2 </= a: ✔
      b1' <= b1: ✔
      b2 <= b1': ✔
      b1 </= b1': ✔
      join l1 l2 = [[1,5] ====> [6,8];
                    [0,3] ====> [7,8];
                    [0,4] ====> [6,8]]
      safe_value l1 a2 = [6,8]
      safe_value l2 a1 = [7,8]
      safe_value l2 a1' = [7,8]
      safe_value l1 a0 = [7,8]
      safe_value l2 a0 = [7,8]
      safe_value l12 a0 = [6,8]
      l1 <= join l1 l2: ✔
      l2 <= join l1 l2: ✔
      l1 <= l: ✔
      l2 <= l: ✔
      join l1 l2 </= l: ✔
      l </= join l1 l2: ✔ |}]
end

(** @author Benoît Montagu <benoit.montagu@inria.fr> *)

(** Copyright © Inria 2023 *)

type strategy = A | B | C

(** Tests whether a list is empty *)
let list_is_empty = function [] -> true | _ -> false

(** Functor that creates an abstract domain that abstracts a minimal function
    graph, from an abstract domain for the input values of the graph, and an
    abstract domain for the output values of the graph.

    The resulting abstract domain for function graphs is "elementary" in the
    sense of F. Bourdoncle [1]. It is implemented as an association list, that
    maps elements of the domain to elements of the codomain. The meaning is that
    of a function that maps an element [x] to the join of all the elements of
    the codomain that are associated to an element of the domain that covers
    [x], i.e., that intersects with [x].

    The widening strategy is simple: when entries that are not covered must be
    added, they are first widenened with some already present entries.

    [1] Bourdoncle, F. Abstract Interpretation by Dynamic Partitioning J. Funct.
    Program., 1992, 2, 407-423 *)
module Make (D1 : sig
  type t

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) (D2 : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end) =
struct
  type dom = D1.t
  type codom = D2.t
  type t = (dom * codom) list
  type nonrec strategy = strategy

  let pp fmt =
    let open Format in
    function
    | [] -> pp_print_string fmt "⊥"
    | l ->
        fprintf fmt "@[[%a@]]"
          (pp_print_list
             ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
             (fun fmt (d1, d2) ->
               Format.fprintf fmt "@[@[%a@]@ ====>@ @[%a@]@]" D1.pp d1 D2.pp d2))
          l

  let bot = []
  let is_bot = function [] -> true | _ -> false

  let leq l1 l2 =
    List.for_all
      (fun (d1, c1) ->
        List.exists
          (fun (d2, c2) -> D1.leq d1 d2 && D1.leq d2 d1 && D2.leq c1 c2)
          l2)
      l1

  let init d1 = if D1.is_bot d1 then bot else [ (d1, D2.bot) ]

  let apply l d1 =
    List.fold_left
      (fun acc (d, c) ->
        if D1.(is_bot @@ meet d1 d) then acc else D2.join acc c)
      D2.bot l

  let map f l = List.map (fun (d1, _) -> (d1, f d1)) l

  let rec find d1 = function
    | [] -> None
    | ((d, c) as p) :: l -> (
        if D1.leq d1 d && D1.leq d d1 then Some (c, l)
        else
          match find d1 l with
          | None -> None
          | Some (c', l') -> Some (c', p :: l'))

  let add join d c l =
    if D1.is_bot d then l
    else
      match find d l with
      | None -> (d, c) :: l
      | Some (c', l') -> (d, join c c') :: l'

  let join_gen join2 l1 l2 =
    List.fold_left (fun acc (d2, c2) -> add join2 d2 c2 acc) l1 l2

  let join l1 l2 = join_gen D2.join l1 l2
  let get_dom l = List.fold_left (fun acc (d, _c) -> D1.join d acc) D1.bot l

  let above d l =
    List.filter_map (fun (d', _) -> if D1.leq d d' then Some d' else None) l

  let minima =
    let insert l d =
      if List.exists (fun d' -> D1.leq d' d) l then l
      else d :: List.filter (fun d' -> not @@ D1.leq d d') l
    in
    fun l -> List.fold_left insert [] l

  let widen_ l1 l2 =
    if list_is_empty l1 then l2
    else if list_is_empty l2 then l1
    else
      let dom1 = get_dom l1 in
      List.fold_left
        (fun acc (d2, c2) ->
          match find d2 acc with
          | None ->
              let d' =
                let over_d2 = minima (above d2 l1) in
                if list_is_empty over_d2 then dom1
                else List.fold_left D1.join D1.bot over_d2
              in
              add D2.widen (D1.widen d' d2) c2 acc
          | Some (c', l') -> add D2.widen d2 (D2.widen c' c2) l')
        l1 l2

  let widen_ = function A -> join | B -> join_gen D2.widen | C -> widen_

  let get_strategy strategies i =
    let n = Array.length strategies in
    if 1 <= i && i <= n then strategies.(i - 1) else strategies.(n - 1)

  let widen_gen strategies n l1 l2 =
    let strategy = get_strategy strategies n in
    widen_ strategy l1 l2

  let default_strategy = [| A; B; C |]
  let widen = widen_gen default_strategy
  let iter f l = List.iter (fun (d1, d2) -> f d1 d2) l
  let dom l = List.map fst l

  let meet l1 l2 =
    List.fold_left
      (fun acc (d1, c1) ->
        List.fold_left
          (fun acc (d2, c2) ->
            let d12 = D1.meet d1 d2 in
            if D1.is_bot d12 then acc
            else
              let c12 = D2.meet c1 c2 in
              add D2.join d12 c12 acc)
          acc l2)
      [] l1
end

module Test = struct
  open Abstract_domains
  module MFG = Make (Interval) (Interval)

  let print_mfg mfg = MFG.pp Format.std_formatter mfg
  let int13 = Interval.make (IntBar.Int 1) (IntBar.Int 3)
  let int05 = Interval.make (IntBar.Int 0) (IntBar.Int 5)
  let int47 = Interval.make (IntBar.Int 4) (IntBar.Int 7)
  let int89 = Interval.make (IntBar.Int 8) (IntBar.Int 9)

  let%expect_test _ =
    print_mfg MFG.bot;
    [%expect {| ⊥ |}]

  let%expect_test _ =
    print_mfg @@ MFG.init int13;
    [%expect {| [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(map (fun _ -> int47) @@ init int13));
    [%expect {| [[1,3] ====> [4,7]] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int13) (init int13)));
    [%expect {| [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int13) (init int05)));
    [%expect {| [[0,5] ====> ⊥; [1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int47) (init int05)));
    [%expect {| [[0,5] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(join (init int47) @@ join (init int05) (init int89)));
    [%expect {| [[0,5] ====> ⊥; [8,9] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (init int47)
         @@ join (init int05)
         @@ join (init int89) (init int13)));
    [%expect {| [[0,5] ====> ⊥; [1,3] ====> ⊥; [8,9] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         join (map (fun _ -> int89) @@ init int47)
         @@ join (map (fun _ -> int47) @@ init int05)
         @@ join
              (map (fun _ -> int05) @@ init int89)
              (map (fun _ -> int89) @@ init int13)));
    [%expect
      {| [[0,5] ====> [4,7]; [1,3] ====> [8,9]; [8,9] ====> [0,5]; [4,7] ====> [8,9]] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 5 (init int13) (init int13)));
    [%expect {| [[1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 5 (init int13) (init int05)));
    [%expect {| [[-∞,+∞] ====> ⊥; [1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 5 (init int47) (init int05)));
    [%expect {| [[-∞,7] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 5 (init int13) (init int89)));
    [%expect {| [[1,+∞] ====> ⊥; [1,3] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg @@ MFG.(widen 5 (init int47) @@ join (init int05) (init int89)));
    [%expect {| [[-∞,7] ====> ⊥; [4,+∞] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5 (init int47)
         @@ join (init int05)
         @@ join (init int89) (init int13)));
    [%expect {| [[-∞,7] ====> ⊥; [4,+∞] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5 (join (init int47) (init int05))
         @@ join (init int89) (init int13)));
    [%expect {| [[0,+∞] ====> ⊥; [0,5] ====> ⊥; [4,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5 (map (fun _ -> int05) @@ init int47)
         @@ join (map (fun _ -> int13) @@ init int05)
         @@ join
              (map (fun _ -> int05) @@ init int89)
              (map (fun _ -> int89) @@ init int13)));
    [%expect
      {|
      [[-∞,7] ====> [1,+∞]; [4,+∞] ====> [0,5]; [4,7] ====> [0,5]] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5
           (join
              (init (Interval.make (IntBar.Int (-1)) (IntBar.Int 7)))
              (init int05))
         @@ map (fun _ -> int13)
         @@ init int13));
    [%expect {| [[0,5] ====> [1,3]; [-1,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5
           (join (init (Interval.make (IntBar.Int (-1)) (IntBar.Int 7)))
           @@ join (init int05)
                (init (Interval.make (IntBar.Int 1) (IntBar.Int 6))))
         @@ map (fun _ -> int13)
         @@ init int13));
    [%expect
      {| [[0,6] ====> [1,3]; [0,5] ====> ⊥; [1,6] ====> ⊥; [-1,7] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         widen 5
           (join (init (Interval.make (IntBar.Int 1) (IntBar.Int 2)))
           @@ join (init int05)
                (init (Interval.make (IntBar.Int 2) (IntBar.Int 6))))
         @@ map (fun _ -> int13)
         @@ init int13));
    [%expect {| [[0,5] ====> [1,3]; [2,6] ====> ⊥; [1,2] ====> ⊥] |}]

  let%expect_test _ =
    (print_mfg
    @@ MFG.(
         meet (join (init int05) (init int89)) @@ join (init int47) (init int13))
    );
    [%expect {| [[4,5] ====> ⊥; [1,3] ====> ⊥] |}]
end

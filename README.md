An OCaml library that defines the interval of abstract function
graphs, and abstract domain that represents partial functions. The
library also defines fixpoint solvers that are based on abstract
function graphs, and that compute abstractions of the minimal function
graph of a functional.

The library comes with several static analysis examples on simple
programming languages such as IMP (aka while programs), REC (Winskel's
set of first-order recursive functions), and variants of CFGs (control
flow graphs).

Please have a look at the [online documentation](https://bmontagu.gitlabpages.inria.fr/function_graphs/function_graphs/index.html).
